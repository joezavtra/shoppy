import multiprocessing

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

bind = '127.0.0.1:8000'
# bind = 'unix:/tmp/shoppy_gunicorn.sock'
logfile = '/var/log/shoppy_gunicorn.log'
pidfile = '/tmp/gunicorn.pid'
timeout = 600
workers = multiprocessing.cpu_count() * 2 + 1

