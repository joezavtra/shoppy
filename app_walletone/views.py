# coding=utf-8
from django.conf import settings
from django.db import transaction
from django.http.response import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from Core.decorators import render_to
from Core.models import ClientPayment
from app_walletone import utils
# from app_walletone.models import WalletOnePayment


@transaction.commit_on_success
@csrf_exempt
def callback(r):
    params = dict()
    for p in utils.PARAMS:
        if p in r.POST:
            params[p] = r.POST[p]

    if not params['WMI_MERCHANT_ID'] == settings.WALLETONE_ID:
        return HttpResponse(
            u'WMI_RESULT=RETRY&WMI_DESCRIPTION=Неправильный идентификатор '
            u'кошелька получателя')

    sign = r.POST.get('WMI_SIGNATURE')
    if not sign:
        return HttpResponse(
            u'WMI_RESULT=RETRY&WMI_DESCRIPTION=Не указана цифровая подпись')

    if not utils.get_signature(params=params, secret_key=settings.WALLETONE_SECRET) == sign:
        return HttpResponse(
            u'WMI_RESULT=RETRY&WMI_DESCRIPTION=Цифровая подпись не валидна')

    payment_no = params.get('WMI_PAYMENT_NO', None)
    if not payment_no:
        return HttpResponse(
            u'WMI_RESULT=RETRY&WMI_DESCRIPTION=Не передан идентификатор заказа')

    try:
        payment = ClientPayment.objects.get(pk=payment_no)
        if params['WMI_ORDER_STATE'] == 'Accepted':
            payment.status = ClientPayment.STATUS_OK
            payment.shop.paid_till = payment.shop.paid_till + timezone.timedelta(days=payment.days)
            payment.shop.save()
        else:
            payment.status = ClientPayment.STATUS_NO
        payment.save()
        # w1_payment = WalletOnePayment(
        #     payment=payment,
        #     status=WalletOnePayment.STATUS_OK if params['WMI_ORDER_STATE'] == 'Accepted' else WalletOnePayment.STATUS_NO
        # )
        # w1_payment.save()
        # w1_payment.payment.status = WalletOnePayment.STATUS_OK if params['WMI_ORDER_STATE'] == 'Accepted' else WalletOnePayment.STATUS_NO
        # w1_payment.payment.save()
        return HttpResponse('WMI_RESULT=OK')
    except:
        return HttpResponse(
            u'WMI_RESULT=RETRY&WMI_DESCRIPTION=Ошибка на сервере')


# @render_to('app_walletone/test_order_form.html')
# def test(r):
#     orders = []
#     for payment in WalletOnePayment.objects.all():
#         order = dict(
#             WMI_MERCHANT_ID=settings.WALLETONE_ID,
#             WMI_PAYMENT_AMOUNT=payment.get_amount(),
#             WMI_CURRENCY_ID='643',
#             WMI_PAYMENT_NO=payment.get_payment_no(),
#             WMI_DESCRIPTION=payment.desc,
#             WMI_SUCCESS_URL='http://localhost:8001/walletone/callback/',
#             WMI_FAIL_URL='http://localhost:8001/walletone/callback/'
#         )
#         sign = utils.get_signature(order, settings.WALLETONE_SECRET)
#         order['WMI_SIGNATURE'] = sign
#         orders.append(order)
#     return dict(orders=orders)