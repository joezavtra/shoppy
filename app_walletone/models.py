# coding=utf-8
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.
from Core.models import Client, Shop, ClientPayment
from app_walletone import utils


# class WalletOnePayment(models.Model):
#     STATUS_OK = 1
#     STATUS_NO = 2
#     STATUS_CHOICES = (
#         (STATUS_OK, u'Оплачен'),
#         (STATUS_NO, u'Не оплачен'),
#     )
#     payment = models.ForeignKey(ClientPayment, related_name='walletone_payments')
#     status = models.PositiveIntegerField(
#         choices=STATUS_CHOICES, default=STATUS_NO)
