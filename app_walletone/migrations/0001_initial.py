# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'WalletOneOrder'
        db.create_table(u'app_walletone_walletoneorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('to_user_id', self.gf('django.db.models.fields.CharField')(max_length=12)),
            ('order_id', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('currency', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('amount', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'app_walletone', ['WalletOneOrder'])


    def backwards(self, orm):
        # Deleting model 'WalletOneOrder'
        db.delete_table(u'app_walletone_walletoneorder')


    models = {
        u'app_walletone.walletoneorder': {
            'Meta': {'object_name': 'WalletOneOrder'},
            'amount': ('django.db.models.fields.FloatField', [], {}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'to_user_id': ('django.db.models.fields.CharField', [], {'max_length': '12'})
        }
    }

    complete_apps = ['app_walletone']