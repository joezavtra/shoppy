# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'WalletOnePayment.to_user_id'
        db.add_column(u'app_walletone_walletonepayment', 'to_user_id',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=12),
                      keep_default=False)

        # Adding field 'WalletOnePayment.order_id'
        db.add_column(u'app_walletone_walletonepayment', 'order_id',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=200),
                      keep_default=False)

        # Adding field 'WalletOnePayment.desc'
        db.add_column(u'app_walletone_walletonepayment', 'desc',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'WalletOnePayment.currency'
        db.add_column(u'app_walletone_walletonepayment', 'currency',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=10),
                      keep_default=False)

        # Adding field 'WalletOnePayment.amount'
        db.add_column(u'app_walletone_walletonepayment', 'amount',
                      self.gf('django.db.models.fields.FloatField')(default=None),
                      keep_default=False)

        # Adding field 'WalletOnePayment.state'
        db.add_column(u'app_walletone_walletonepayment', 'state',
                      self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'WalletOnePayment.to_user_id'
        db.delete_column(u'app_walletone_walletonepayment', 'to_user_id')

        # Deleting field 'WalletOnePayment.order_id'
        db.delete_column(u'app_walletone_walletonepayment', 'order_id')

        # Deleting field 'WalletOnePayment.desc'
        db.delete_column(u'app_walletone_walletonepayment', 'desc')

        # Deleting field 'WalletOnePayment.currency'
        db.delete_column(u'app_walletone_walletonepayment', 'currency')

        # Deleting field 'WalletOnePayment.amount'
        db.delete_column(u'app_walletone_walletonepayment', 'amount')

        # Deleting field 'WalletOnePayment.state'
        db.delete_column(u'app_walletone_walletonepayment', 'state')


    models = {
        u'Core.client': {
            'Meta': {'object_name': 'Client'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'clients'", 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.clientpayment': {
            'Meta': {'object_name': 'ClientPayment'},
            'amount': ('django.db.models.fields.FloatField', [], {}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payments'", 'to': u"orm['Core.Client']"}),
            'desc': ('django.db.models.fields.TextField', [], {'default': "''"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payments'", 'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2'})
        },
        u'Core.datasource': {
            'Meta': {'object_name': 'DataSource'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Client']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 4, 0, 0)'}),
            'mode': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u'Default'", 'max_length': '100'}),
            'next_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 4, 0, 0)'}),
            'processing': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 4, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'root_cat_oid': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'yml_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'Core.shop': {
            'Meta': {'object_name': 'Shop'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 4, 0, 0)'}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'shops'", 'to': u"orm['Core.Client']"}),
            'contacts': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'datasources': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'shops'", 'symmetrical': 'False', 'to': u"orm['Core.DataSource']"}),
            'delivery': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'hello_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'notice': ('django.db.models.fields.TextField', [], {'default': 'u\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"> <head>  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  <title>Demystifying Email Design</title>  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>  <style>    .list {background:#eee}    .list th {font-size: 150%; padding: 15px; }    .list td {text-align: right; padding: 10px 15px; background:#fff;}    .list td.name {text-align: left}  </style></head><body><table style="width:800px"><tr><td><h1>\\u041f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u044c</h1><table class="list" cellspacing=1><tr><td>\\u0418\\u043c\\u044f: </td><td><a href="{{ order.cart.customer.get_profile_url }}">{{ order.name }}</a></td></tr><tr><td>\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d: </td><td> {{ order.phone }}</td></tr><tr><td>Email: </td><td>{{ order.email }}</td></tr><tr><td>\\u0410\\u0434\\u0440\\u0435\\u0441: </td><td>{{ order.address }}</td></tr><tr><td>\\u041f\\u0412\\u0417: </td><td>[{{ order.deliverypoint.oid }}] {{ order.deliverypoint.name }}</td></tr><tr><td>\\u041a\\u043e\\u043c\\u043c\\u0435\\u043d\\u0442\\u0430\\u0440\\u0438\\u0439 \\u043a \\u0437\\u0430\\u043a\\u0430\\u0437\\u0443:</td><td>{{ order.comment }}</td></tr></table></td></tr><tr><td><h1>\\u0418\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u043e \\u0437\\u0430\\u043a\\u0430\\u0437\\u0435</h1><table class="list" cellspacing=1><tr><th>\\u2116</th><th>\\u0430\\u0440\\u0442\\u0438\\u043a\\u0443\\u043b</th><th>\\u043d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435</th><th>\\u0446\\u0435\\u043d\\u0430</th><th>\\u043a\\u043e\\u043b-\\u0432\\u043e</th><th>\\u0438\\u0442\\u043e\\u0433\\u043e</th></tr>{% for item in order.cart.items.all %}<tr><td>{{forloop.counter}}</td><td>{{ item.product.oid }}</td><td class=\\\'name\\\'>{{ item.product.name|safe }}</td><td>{{ item.product.price }}&nbsp;\\u0440\\u0443\\u0431.</td><td>{{ item.count }}&nbsp;\\u0448\\u0442.</td><td><strong>{{ item.price }}</strong>&nbsp;\\u0440\\u0443\\u0431.</td></tr>{% endfor %}<tr>  <td colspan=5 style="text-align: right; font-size: 150%"><strong>\\u0418\\u0442\\u043e\\u0433\\u043e:</strong></td>  <td style="text-align: right; font-size: 150%"><strong>{{ order.cart.total }}</strong>&nbsp;\\u0440\\u0443\\u0431.</td></tr>{% if discount %}<tr>  <td colspan=5 style="text-align: right; font-size: 120%">    {{ order.cart.discount.name }}:    {% if order.cart.coupon %}    <br>\\u043f\\u043e \\u043a\\u0443\\u043f\\u043e\\u043d\\u0443 &laquo;{{ order.cart.coupon.key }}&raquo;    {%endif%}  </td>  <td style="text-align: right; font-size: 120%">{{ discount.value }}</td></tr><tr>  <td colspan=5 style="text-align: right; font-size: 150%"><strong>\\u0421 \\u0443\\u0447\\u0435\\u0442\\u043e\\u043c \\u0441\\u043a\\u0438\\u0434\\u043a\\u0438:</strong></td>  <td style="text-align: right; font-size: 150%"><strong>{{ discount.price }}</strong>&nbsp;\\u0440\\u0443\\u0431.</td></tr>{%endif%}</table></td></tr><tr><td align="right"><img src="https://shoppyboom.ru/images/design/main_logo.gif"></td></tr></table></body></html>\'', 'null': 'True', 'blank': 'True'}),
            'paid_till': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'user_from': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_reply': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_sign': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user_subject': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'shops'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'app_walletone.walletonepayment': {
            'Meta': {'object_name': 'WalletOnePayment'},
            'amount': ('django.db.models.fields.FloatField', [], {}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'payment': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'walletone_payments'", 'to': u"orm['Core.ClientPayment']"}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'to_user_id': ('django.db.models.fields.CharField', [], {'max_length': '12'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['app_walletone']