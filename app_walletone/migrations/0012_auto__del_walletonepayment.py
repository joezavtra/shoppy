# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'WalletOnePayment'
        db.delete_table(u'app_walletone_walletonepayment')


    def backwards(self, orm):
        # Adding model 'WalletOnePayment'
        db.create_table(u'app_walletone_walletonepayment', (
            ('currency', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('amount', self.gf('django.db.models.fields.FloatField')()),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('order_id', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('payment', self.gf('django.db.models.fields.related.ForeignKey')(related_name='walletone_payments', to=orm['Core.ClientPayment'])),
            ('to_user_id', self.gf('django.db.models.fields.CharField')(max_length=12)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('desc', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'app_walletone', ['WalletOnePayment'])


    models = {
        
    }

    complete_apps = ['app_walletone']