from django.conf.urls import patterns, url
from app_walletone.views import callback

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


urlpatterns = patterns('',
    url(r'^callback/$', callback, name='walletone_callback'),
    # url(r'^test/$', test, name='walletone_test'),
)