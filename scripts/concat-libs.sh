#!/bin/bash

# Requires:
#   npm install -g uglify-js clean-css
set -e

JS_MIN_PATH=static/dist/app-lib
CSS_MIN_PATH=static/dist/app-lib

JS_LIBS=$(cat << EOF
static/js/libs/jquery-1.10.1.js
static/js/libs/jquery.mask.js
static/js/libs/cloudzoom.js
static/bootstrap/js/bootstrap.min.js
static/js/libs/underscore.js
static/js/libs/backbone-min.js
static/js/libs/backbone.marionette.min.js
static/js/libs/backbone.syphon.js
static/js/libs/jquery.bxslider.js
static/js/libs/jquery.mCustomScrollbar.js
static/js/libs/typeahead.min.js
EOF)

CSS_LIBS=$(cat << EOF
static/css/icons-family.css
static/css/jquery.bxslider.css
static/css/slider.css
static/bootstrap/css/bootstrap.css
static/css/jquery.scroll.css
static/css/cloudzoom.css
EOF)

uglifyjs $JS_LIBS -o $JS_MIN_PATH
JS_MD5=$(md5 -q $JS_MIN_PATH)
JS_MD5=${JS_MD5:0:4}
JS_OUT=$JS_MIN_PATH-$JS_MD5.min.js
mv $JS_MIN_PATH $JS_OUT

cleancss $CSS_LIBS -o $CSS_MIN_PATH
CSS_MD5=$(md5 -q $CSS_MIN_PATH)
CSS_MD5=${CSS_MD5:0:4}
CSS_OUT=$CSS_MIN_PATH-$CSS_MD5.min.css
mv $CSS_MIN_PATH $CSS_OUT

echo "Minified files:"
echo $JS_OUT
echo $CSS_OUT
