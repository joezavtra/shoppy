#!/bin/sh
set -e

virtualenv env
cat scripts/init_db.sql | sudo mysql
cp settings.py.example settings.py

./env/bin/pip install -r requirements-pip.txt

./env/bin/python manage.py syncdb --noinput
./env/bin/python manage.py migrate Core

./env/bin/python manage.py createsuperuser

if grep --quiet shoppy.dev /etc/hosts; then
  echo "Host exists in /etc/hosts"
else
  echo "Adding host to /etc/hosts"
  echo "127.0.0.1 shoppy.dev" | sudo tee -a /etc/hosts
fi

echo "$(tput setaf 3)Copy-paste site config to 'http'\
  section of nginx.conf and reboot nginx$(tput sgr 0)"
echo "\tPath: /usr/local/etc/nginx/nginx.conf"
echo "\tReboot CMD: sudo nginx -s reload"
echo "\tSite Config: -->"
cat scripts/shoppy.dev.nginx.conf
