#!/bin/bash
set -e

# Install epel repo
mkdir -p ~/tmp
cd ~/tmp
wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
sudo rpm -ivh epel-release-6-8.noarch.rpm

sudo yum -y install \
  python-devel python-setuptools\
  libxslt libxslt-devel\
  gcc\
  gcc-c++\
  mysql mysql-server mysql-devel\
  zlib\
  zlib-devel\
  openssl\
  libxml2\
  memcached\
  libevent libevent-devel\
  ImageMagick-devel\
  nginx\
  sphinx

sudo easy_install pip
sudo pip install virtualenv
sudo pip install supervisor


# Install libmemcached
wget https://launchpad.net/libmemcached/1.0/1.0.18/+download/libmemcached-1.0.18.tar.gz
tar -xvf libmemcached-1.0.18.tar.gz
cd libmemcached-1.0.18
./configure
make
sudo make install
