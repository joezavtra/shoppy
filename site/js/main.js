$(function () {
//    $('.form input[type=submit]').click(function () {
//        $(this).parents('#seven').addClass('green');
//        $(this).parents('.form').addClass('hide');
//        $(this).parents('#seven').find('.mes').show(0);
//        return false;
//    });

    /* Сравнение тарифов */
    $('.fancy').fancybox({
        margin: 10,
        padding: 10,
        wrapCSS:'send',
        scrolling:'yes',
        afterShow: function () {
            ga && ga('send', 'event', 'button', 'click', 'sravnenie');
        }
    });

    /* Стать партнером */
    $('.fancy2').fancybox({
        margin:0,
        padding:0,
        boxShadow:0,
        wrapCSS:'part',
        scrolling:'visible',
        afterShow: function () {
            ga && ga('send', 'event', 'button', 'click', 'partner');
        }
    });

  /* Политика конфиденциальности */
  $('.fancy3').fancybox({
    margin: 20,
    padding: 10,
    scrolling:'yes'
//    afterShow: function () {
//      ga && ga('send', 'event', 'button', 'click', 'sravnenie');
//    }
  });

    $(".scrolly").click(function () {
      var $target = $(this);
      var elementClick = $target.attr("href");
      var destination = $(elementClick).offset().top;
      var tariff = $target.data('tariff');

      /* Google Analitycs - Кнопочка заказать при выборе тарифов */
      if(typeof tariff !== 'undefined'){
        ga && ga('send', 'event', 'button', 'click', tariff);
      }

      /* .want - ссылка "Хочу также" */
      if($target.hasClass('active_state') && !$target.hasClass('want')){
        return false;
      }

      if ($.browser.safari) {
        $('body').animate({ scrollTop:destination - 92 }, 1100);
      } else {
        $('html').animate({ scrollTop:destination - 92 }, 1100);
      }

      if(!$target.hasClass('want')){
        $('.active_state').removeClass('active_state');
        $target.addClass('active_state');
      }

      return false;
    });

    $('.order').click(function(){
        $('#tariff').val($(this).attr('rel'));
    });

    /* Форма: 3 клика */
//    $("#login-form").validationEngine('attach', {
//        promptPosition:"bottomLeft",
//        scroll:false,
//        onValidationComplete:function (form, status) {
//            /* Google Analitycs */
//            ga && ga('send','event','button','click','forma_short');
//            if (status) {
//                $.get('/cabinet/signup/', $('#login-form').serializeArray(), function (data) {
//                    if (data.status) {
//                        ga && ga('send','event','button','click','finish');
//                        $.fancybox('#success', {
//                            margin:0,
//                            padding:0,
//                            boxShadow:0,
//                            wrapCSS:'part',
//                            scrolling:'visible'
//                        });
//                        $('#login-form input[name=email],#login-form input[name=phone],').val('');
//                    } else {
//                        $.fancybox('#error', {
//                            margin:0,
//                            padding:0,
//                            boxShadow:0,
//                            wrapCSS:'part',
//                            scrolling:'visible'
//                        });
//                    }
//                });
//            }
//        }
//    });

    /* Форма: Стать партнером */
    $("#partners-form").validationEngine('attach', {
        promptPosition:"bottomRight",
        scroll:false,
        onValidationComplete:function (form, status) {
            if (status) {
                $.get('/cabinet/signup/', $('#partners-form').serializeArray(), function (data) {
                    if (data.status) {
                        ga && ga('send','event','button','click','finish');
                        $.fancybox('#success', {
                            margin:0,
                            padding:0,
                            boxShadow:0,
                            wrapCSS:'part',
                            scrolling:'visible'
                        });
                        $('#click-form input[name=email],#click-form input[name=phone],').val('');
                    } else {
                        $.fancybox('#error', {
                            margin:0,
                            padding:0,
                            boxShadow:0,
                            wrapCSS:'part',
                            scrolling:'visible'
                        });
                    }
                });
            }
        }
    });

    /* Форма: Оставить заявку */
    $("#main-form").validationEngine('attach', {
        promptPosition:"centerRight",
        scroll:false,
        onValidationComplete:function (form, status) {
            /* Google Analitycs */
            ga && ga('send', 'event', 'button', 'click', 'forma_full');
            if (status) {
                $.get('/cabinet/signup/', $("#main-form").serializeArray(), function (data) {
                    if (data.status) {
                        $('#seven').addClass('green');
                        $('#seven .form').addClass('hide');
                        $('#seven .mes').show(0);
                    }
                });
            }
        }
    });

  /* Инициализация слайдера в разделе "Клиенты" */
  var ClientSlider = $('#clients-slider').bxSlider({
    mode: 'fade',
    randomStart: true,
    infiniteLoop: true,
    pager: false,
    controls: false,
    adaptiveHeight: true
  });

  /* Ссылка "Другой кейс" */
  $('#other-case').on('click', function (e) {
    e.preventDefault();

    ClientSlider.goToNextSlide();
  });

    $('#presentation').on('click', function () {
        ga && ga('send', 'event', 'button', 'click', 'presentation');
    });

    $('input[name="referer"]').val(document.referrer);
});