﻿// JavaScript Document

function ValidateEmail(email)
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  {
    return true
  }
    return false
}

$(document).ready(function(){

	//Реакция на нажатие кнопки
	$("input#button").click(function(){
        var el = $("input#text_field");
        el.removeClass('wrong');

        var email = el.val();
        if( ValidateEmail(email) ){
            ga && ga('send','event','button','click','signup');
            $.get("/cabinet/signup/",{email:email})
                    .success(function(data){
                        $("div#form").empty();
                        $("div#form").html("<p><span class='bold_m'>Ура!</span> Скоро ты все узнаешь!</p>");
                    });

        }else{
            el.addClass('wrong');
            ga && ga('send','event','button','click','signup_fail',0);
        }
	})
})