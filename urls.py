# -*- coding: utf-8 -*-
import analytics
import analytics.client
import app_boom.urls
import Cabinet
import Core

from django.contrib.auth.decorators import login_required
from django.conf.urls import patterns, include, url
from django.conf import settings
from tastypie.api import Api
from Cabinet.wizards import ShopNewWizard, FORMS
from robokassa import urls as robokassa_urls
from app_checkout import urls as checkout_urls
from app_walletone import urls as walletone_urls
from Core.api.resources import *
from django.contrib import admin

import app_boom.urls
from django.conf import settings

Cabinet.analytics = analytics.client.Client(
    secret=settings.SEGMENT_IO_CABINET_KEY,
    stats=analytics.Statistics()
)

Core.analytics = analytics.client.Client(
    secret=settings.SEGMENT_IO_APP_KEY,
    stats=analytics.Statistics()
)

admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(BannerResource())
v1_api.register(CallbackOrderResource())
v1_api.register(CartItemResource())
v1_api.register(CartResource())
v1_api.register(CategoryResource())
v1_api.register(CheckoutResource())
v1_api.register(CityResource())
v1_api.register(ClientResource())
v1_api.register(CommentResource())
v1_api.register(CustomerResource())
v1_api.register(DeliveryPointResource())
v1_api.register(LandingResource())
v1_api.register(NewsResource())
v1_api.register(OfferResource())
v1_api.register(OrderHistoryResource())
v1_api.register(OrderResource())
v1_api.register(PageResource())
v1_api.register(ProductResource())
v1_api.register(SearchProductFullResource())
v1_api.register(SearchProductResource())
v1_api.register(ShopResource())
v1_api.register(FastOrderResource())
v1_api.register(SlotProductsResource())
v1_api.register(UserResource())

urlpatterns = patterns('',
    url(r'^sys/doc/', include('django.contrib.admindocs.urls')),
    url(r'^sys/', include(admin.site.urls)),
    url(r'^boom/', include(app_boom.urls)),

    (r'^api/', include(v1_api.urls)),
)

urlpatterns += patterns('Core.views',
    url(r'^r/$', 'redirect'),
    url(r'^share/$', 'share'),
)
urlpatterns += patterns('App.views',
    url(r'^specification/$', 'app_specification'),
    url(r'^$', 'shop'),
    url(r'^vk/$', 'shopVK'),
    url(r'^ok/$', 'shop_odnoklassniki'),
    url(r'^fb/tab/(?P<redirect>.*)$', 'shopFB', kwargs=dict(tab=True)),
    url(r'^fb/(?P<redirect>.*)$', 'shopFB', kwargs=dict(tab=False)),
)
urlpatterns += patterns('Cabinet.views',
    url(r'^cabinet/$', 'main', name='main'),
    url(r'^cabinet/\d+/$', 'main', name='main'),
    url(r'^cabinet/api/', include('Cabinet.urls')),
    url(r'^cabinet/signup/$', 'signup', name='signup'),
    url(r'^cabinet/verify/(?P<token>[-0-9a-z]+)/$', 'account_verify', name='account_verify'),
    url(r'^cabinet/login/$', 'login_view', name='login'),
    url(r'^cabinet/recovery/$', 'password_recovery', name='password_recovery'),
    url(r'^cabinet/recovery/(?P<token>.+)/$', 'password_recovery_step2', name='password_recovery_step2'),
    url(r'^cabinet/logout/$', 'logout_view', name='logout'),
    url(r'^cabinet/change_password/$', 'change_password', name='change_password'),
    url(r'^cabinet/client/$', 'client_profile_list', name='client_profile_list'),
    url(r'^cabinet/client/user/vk-auth/$', 'client_user_vk_auth', name='client_user_vk_auth'),
    url(r'^cabinet/customer/(?P<customer_pk>\d+)/$', 'customer_view', name='customer_view'),
    url(r'^cabinet/client/(?P<client_pk>\d+)/$', 'client_profile', name='client_profile'),
    url(r'^cabinet/client/(?P<client_pk>\d+)/shop/$', 'client_shop_list', name='client_shop_list'),
    url(r'^cabinet/client/(?P<client_pk>\d+)/user/list/$', 'client_user_list', name='client_user_list'),
    url(r'^cabinet/client/(?P<client_pk>\d+)/user/add/$', 'client_user_edit_or_create', name='client_user_create'),
    url(r'^cabinet/client/(?P<client_pk>\d+)/user/(?P<user_pk>\d+)/$', 'client_user_edit_or_create', name='client_user_edit'),
    url(r'^cabinet/client/(?P<client_pk>\d+)/user/(?P<user_pk>\d+)/delete/$', 'client_user_delete', name='client_user_delete'),
    url(r'^cabinet/shop/new/$', login_required(ShopNewWizard.as_view(FORMS)), name='new_shop'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/$', 'shop', name='shop'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/category/calculate/$', 'shop_calculate_cat_product_count', name='shop_calculate_cat_product_count'),
    # url(r'^cabinet/(?P<shop_id>\d+)/shop/truncate/$', 'shop_truncate_data', name='shop_truncate_data'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/payments/$', 'shop_payment_list', name='shop_payment_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/payments/(?P<payment_pk>\d+)/$', 'delete_payment', name='delete_payment'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/settings/$', 'shop_base', name='shop_base'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/email/$', 'shop_email', name='shop_email'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/tariff/$', 'shop_change_tariff', name='shop_change_tariff'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/integration/$', 'shop_integration_list', name='shop_integration_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/integration/(?P<platform_code>\w+)/$', 'shop_integration_service', name='shop_integration_service'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/integration/(?P<platform_code>\w+)/delete/$', 'integration_service_delete', name='integration_service_delete'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/social/$', 'shop_social', name='shop_profile'),
    url(r'^cabinet/(?P<shop_id>\d+)/shop/advanced/$', 'shop_advanced', name='shop_extra'),
    url(r'^cabinet/(?P<shop_id>\d+)/datasource/$', 'shop_datasource_list', name='shop_datasource_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/datasource/create/$', 'shop_datasource_edit_or_create', name='shop_datasource_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/datasource/(?P<datasource_pk>\d+)/$', 'shop_datasource_edit_or_create', name='shop_datasource_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/datasource/(?P<datasource_pk>\d+)/truncate/$', 'shop_datasource_truncate_data', name='shop_datasource_truncate'),
    url(r'^cabinet/(?P<shop_id>\d+)/datasource/(?P<datasource_pk>\d+)/update/$', 'shop_datasource_force_update', name='shop_datasource_force_update'),
    url(r'^cabinet/(?P<shop_id>\d+)/categories/$', 'categories', name='shop_categories'),
    url(r'^cabinet/(?P<shop_id>\d+)/categories/create/$', 'shop_category_create', name='shop_category_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/categories/(?P<category_id>\d+)/edit/$', 'shop_category_edit', name='shop_category_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/categories/(?P<category_pk>\d+)/delete/$', 'category_delete', name='category_delete'),
    url(r'^cabinet/(?P<shop_id>\d+)/categories/(?P<category_id>\d+)/toggle-state/$', 'catetgory_status_toggle', name='shop_catetgory_status_toggle'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods-groups/$', 'product_group_list', name='product_group_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods-groups/(?P<group_pk>\d+)/$', 'product_group_edit_or_create', name='product_group_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods-groups/create/$', 'product_group_edit_or_create', name='product_group_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods/$', 'goods', name='shop_goods'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods/create/$', 'goods_edit_or_create', name='shop_good_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods/(?P<productid>\d+)/$', 'goods_edit_or_create', name='shop_good_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods/(?P<product_pk>\d+)/offers/$', 'shop_offer_list', name='shop_offer_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/goods/(?P<product_pk>\d+)/view/$', 'good_view', name='good_view'),
    url(r'^cabinet/(?P<shop_id>\d+)/comment/$', 'shop_comment_list', name='shop_comments'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/$', 'pages', name='shop_pages'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/(?P<pageid>\d+)/$', 'pages_edit', name='shop_pages_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/news/$', 'news', name='shop_news'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/news/(?P<newsid>\d+)/$', 'news_edit', name='shop_news_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/$', 'landing_list', name='landing_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/create/$', 'landing_create', name='landing_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/$', 'landing_view', name='landing_view'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/main/$', 'landing_section_main', name='lsection_main'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/counter/$', 'landing_section_counter', name='lsection_counter'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/products/$', 'landing_section_products', name='lsection_products'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/compare/$', 'landing_section_compare', name='lsection_compare'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/comments/$', 'landing_section_comments', name='lsection_comments'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/simple/create/$', 'landing_section_simple', name='lsection_simple_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/simple/(?P<section_pk>\d+)/$', 'landing_section_simple', name='lsection_simple_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/pages/landing/(?P<landing_pk>\d+)/simple/(?P<section_pk>\d+)/delete/$', 'landing_section_simple_delete', name='lsection_simple_delete'),
    url(r'^cabinet/(?P<shop_id>\d+)/orders/callback/$', 'callback_orders', name='callback_orders'),
    url(r'^cabinet/(?P<shop_id>\d+)/orders/$', 'orders', name='shop_orders'),
    url(r'^cabinet/(?P<shop_id>\d+)/orders/(?P<order_pk>\d+)/view/$', 'order_view', name='order_view'),
    url(r'^cabinet/(?P<shop_id>\d+)/orders/(?P<orderid>\d+)/preview/$', 'order_notice_preview', name='order_notice_preview'),
    url(r'^cabinet/(?P<shop_id>\d+)/look/$', 'look', name='shop_look'),
    url(r'^cabinet/(?P<shop_id>\d+)/category_move/$', 'category_move', name='shop_category_move'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/discount/$', 'shop_discount_list', name='shop_discount_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/discount/create/$', 'shop_discount_edit_or_create', name='shop_discount_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/discount/(?P<discount_id>\d+)/edit/$', 'shop_discount_edit_or_create', name='shop_discount_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/banner/$', 'shop_banner_list', name='shop_banner_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/banner/create/$', 'shop_banner_edit_or_create', name='shop_banner_create'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/banner/(?P<banner_id>\d+)/edit/$', 'shop_banner_edit_or_create', name='shop_banner_edit'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/banner/(?P<banner_pk>\d+)/delete/$', 'shop_banner_delete', name='shop_banner_delete'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/notification/$', 'shop_notification_list', name='shop_notification_list'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/notification/create/$', 'shop_notification_add', name='shop_notification_add'),
    url(r'^cabinet/(?P<shop_id>\d+)/stock/subscription/$', 'subscription_list', name='subscription_list'),
)

# Робокасса
urlpatterns += patterns('',
    url(r'^robokassa/', include(robokassa_urls)),
)

# Checkout
urlpatterns += patterns('',
    url(r'^checkout/', include(checkout_urls)),
)

# Wallet One
urlpatterns += patterns('',
    url(r'^walletone/', include(walletone_urls)),
)

# PayOnline
# urlpatterns += patterns('',
#     url(r'^payonline/', include('payonline.urls')),
# )


if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
    urlpatterns += patterns('Cabinet.views',
        url(r'^test/', 'test_view'),
    )
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )
