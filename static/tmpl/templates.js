SBApp.Templates = {};
JST = {};

/* Айтем категории: ссылка */
JST['categories:item'] = _.template('<a href="#category/<%= id %>" data-alias="category" title="<%= name %>" class="categories__link js-show-products"><%= name %>&nbsp;<small class="products-count"><%= products_count %></small><% if ( !_.isEmpty(children) ) { %><span class="categories__arrow"><i class="sb-icon-arrow-down"></i><i class="sb-icon-arrow-up"></i></span><% } %></a>');

/* Товары */
/* Список */
SBApp.Templates.ProductListView = {
  toolbar: '<div class="toolbar clearfix">\
      <div class="toolbar__item pull-left">\
        <span class="toolbar__item-title">Сортировать:</span>\
        <% if (!_.isEmpty(SBApp.productsOrderTypes)) { %>\
          <select class="toolbar__item-select" id="toolbar__item-select">\
            <% _.each(SBApp.productsOrderTypes, function (orderType) { %>\
              <option value="<%= orderType.type %>" <% if (orderType.type === SBApp.productsOrderBy) { %> selected="selected" <% } %>><%= orderType.title %></option>\
            <% }) %>\
          </select>\
        <% } %>\
      </div>\
      <div class="toolbar__item pull-right toolbar__item-disptype">\
        <span class="js-toolbar__link-type-switcher toolbar__link <% if (SBApp.productListType === "grid") { %> toolbar__link_active <% } %>" data-list-type="grid"><i class="sb-icon-th toolbar__item-icon"></i>сеткой</span>\
        <span class="js-toolbar__link-type-switcher toolbar__link <% if (SBApp.productListType === "detail") { %> toolbar__link_active <% } %>" data-list-type="detail"><i class="sb-icon-th-list toolbar__item-icon"></i>списком</span>\
      </div>\
  </div>',

  type: 'grid'
};

/* Товар в списке */
/* TODO: Избавиться от SBproductHead */
SBApp.Templates.ProductView = {
  className: function(){ return this[ SBApp.productListType ].className; },
  grid: {
    className: 'span3 SBproductGrid',
    body: '<div class="product-item">\
             <h5 class="SBproductHead"><%= name %></h5>\
             <div style="background-image: url(\'<%= cover %>\')" class="SBproductCover img-rounded"></div>\
             <div class="product-item__price" style="text-align: center;"><%= price_str %></div>\
             <div class="product-item__toolbar g-clearfix" style="text-align: center;">\
               <% if(!this.model.hasVariants()) { %>\
                 <a class="SBcartAdd btn" style="float: none;"><i class="sb-icon-cart"></i> Купить</a>\
               <% } else { %>\
                 <a class="btn product__details"><i class="sb-icon-search"></i> Детали</a>\
               <% } %>\
             </div>\
             <div class="product-item__desc"><% if (!_.isEmpty(jQuery.trim(desc))) { print(desc) } else { %><i>Без описания.</i><% } %></div>\
             <% if (SBApp.fast_order) { %>\
               <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
             <% } %>\
          </div>'
  },
  detail: {
    className: 'span9 SBproductDetail',
    body: '<div class="product-item">\
             <div class="row">\
               <div class="span3"><img src="<%= cover %>"></div>\
               <div class="span5">\
                 <h4 class="SBproductHead"><%= name %></h4>\
                 <div class="product-item__toolbar g-clearfix">\
                   <% if(!this.model.hasVariants()) { %>\
                     <a class="SBcartAdd btn"><i class="sb-icon-cart"></i> Купить</a>\
                   <% } else { %>\
                     <a class="btn pull-right product__details"><i class="sb-icon-search"></i> Детали</a>\
                   <% } %>\
                   <div class="product-item__price"><%= price_str %></div>\
                 </div>\
                 <div class="product-item__desc" style="display: none;"><% if (!_.isEmpty(jQuery.trim(desc))) { print(desc) %><% } else { %><i>Без описания.</i><% } %></div>\
               </div>\
             </div>\
           </div>'
  }
};

/* Карточка товара: Основной шаблон */
JST["product:profile"] = _.template('<div class="row">\
  <div class="span9"><ul id="product__breadcrumb" class="breadcrumb"></ul></div></div>\
  <div class="row">\
  <!-- Левая колонка -->\
  <div class="span3">\
    <div class="tabbable tabs-below">\
      <% if( productView.model.has("images") ) { %>\
        <!-- JST["product:profile:images"] -->\
        <%= renderImages({ images: productView.model.get("images")} ) %>\
      <% } %>\
      <% if ( productView.model.hasVariants() ) { %>\
        <!-- JST["product:profile:variants"] -->\
        <%= renderVariants({ variants: productView.model.get("variants") })%>\
      <% } %>\
      <!-- JST["product:profiles:socials"] -->\
      <%= renderSocials()  %>\
      <%= buttonAddToCart({ product: product, disabled: product.variants.length > 0}) %>\
    </div>\
  </div>\
  <!-- Правая колонка  -->\
  <div class="span6">\
    <!-- JST["product:profile:info"] -->\
    <%= productInfo({ product: product, price_str: price_str }) %>\
    <ul class="nav nav-tabs product-tabs" id="product-tabs">\
      <% if( !_.isEmpty(JSON.parse(product.misc)) ) { %>\
        <li class="active"><a href="#product-desc" data-toggle="tab">Характеристики</a></li>\
      <% } %>\
      <% if( !SBApp.Platform.isDomain() ) { %>\
      <li>\
        <a href="#product-comments" data-toggle="tab">Обсуждение <% if( commentsCount !== 0 ) { %>(<% print( commentsCount ) %>)<% } %></a>\
      </li>\
      <% } %>\
    </ul>\
    <div class="tab-content">\
      <% if( !_.isEmpty(JSON.parse(product.misc)) ) { %>\
        <div class="tab-pane active" id="product-desc">\
          <!-- JST["product:profile:characteristics"] -->\
          <%= renderChars({misc: product.misc}) %>\
        </div>\
      <% } %>\
    <div class="tab-pane" id="product-comments">\
      <!-- Форма добавления комментария -->\
      <!-- Выводится только в приложении ВК и FB -->\
      <% if( !SBApp.Platform.isDomain() ) { %>\
        <!-- JST["comments:form:add"] -->\
        <%= formAddComment({ userPhoto: userPhoto }) %>\
        <!-- Вывод комментариев -->\
        <% if(!productView.model.hasComments()) { %>\
          <i>Пока никто ничего не написал.</i>\
        <% } else { %>\
          <!-- JST["comments"] -->\
          <%= renderComments({ comments: productView.model.get("comments"), renderComment: renderComment, reply: false }) %>\
        <% } %>\
      <% } %>\
    </div>\
  </div>\
</div>');

/* Карточка товара: Параметры товара */
JST["product:profile:variants"] = _.template('<% _.each(variants, function(variant, key) { %>\
  <div class="variant-list" data-name="<%= variant.name  %>">\
    <h5 class="variant-list__title"><%= variant.title %>:</h5>\
    <ul>\
      <% _.each(variant.items, function(item, key) { %>\
        <li><a data-oid="<%= item.oid %>" class="variant-list-item"><%= item.value %></a></li>\
      <% });%>\
    </ul>\
  </div>\
<% }); %>');

/* Кнопка: Добавить в корзину */
JST['button:add-to-cart'] = _.template('<button class="btn btn-block SBcartAdd" id="SBcartAdd" <% if (disabled) { %> disabled="disabled" <% } %> data-oid="<%= product.oid %>" data-pid="<%= product.id %>" type="button"><i class="sb-icon-cart"></i> Купить</button>');

/* Карточка товара: Блок Загловок, цена, описание */
JST['product:profile:info'] = _.template('<h3 class="product-profile__title"><%= product.name %></h3>\
<h3 class="product-profile__price"><span class="price"><%= price_str %></span></h3>\
<% if( product.model_oid ) { %>\
  <p class="articulus">Артикул товара: <span class="articulus__value"><%= product.model_oid %></span></p>\
<% } %>\
<p class="product-profile__desc"><%= product.desc %></p>');

<!-- Карточка товара: Блок Характеристики -->
JST["product:profile:characteristics"] = _.template('<table class="table">\
  <tbody>\
    <% _.each(_.pairs(JSON.parse(misc)), function(row) { %>\
      <tr><th><%= row[0] %></th><td><%= row[1] %></td>\
    <% }); %>\
  </tbody>\
</table>');

/* Картинка с CloudZoom */
/* Карточка товара: Таб с картинками */
JST["product:profile:images"] = _.template('<div class="product-profile__images">\
  <!-- Большая картинка -->\
  <div class="cloud-zoom__preview">\
    <a href="<%= images[0] %>" class="cloud-zoom" id="zoomer" rel="">\
      <img src="<%= images[0] %>" />\
    </a>\
  </div>\
  <div class="cloud-zoom__nav">\
    <% if(images.length > 1) {  %>\
      <% _.each(images, function(imageSrc, key) { %>\
        <a href="<%= imageSrc %>" class="<% if (key === 0) { %> active <%}%> cloud-zoom-gallery product-profile__images-item cloud-zoom__item g-inline-block" title="" rel="useZoom: \'zoomer\', smallImage: \'<%= imageSrc %>\'"><img src="<%= imageSrc %>" class="" /></a>\
      <% }); %>\
    <% } %>\
  </div>\
</div>');

/* Карточка товара: Соц. кнопки
TODO: Разбить на более мелкие компоненты, например кнопка ВКонтакте отдельно, Фейсбук отдельно  */
JST["product:profile:socials"] = _.template('<% if (SBApp.Platform.isVK()) { %>\
<div class="social-btns" id="social-btns">\
  <div class="social-btn" id="social-btns__vk-like"></div>\
  <div class="social-btn" id="social-btns__vk-share"></div>\
</div>\
<% } %>\
<% if ( SBApp.Platform.isOK() ) { %>\
<div class="social-btns" id="social-btns">\
  <div id="ok_shareWidget"></div>\
</div>\
<% } %>\
<% if (SBApp.Platform.isFacebook()) { %>\
  <div class="social-btns" id="social-btns">\
    <div class="social-btn social-btns__fb-like" id="social-btns__fb-like">\
      <fb:like href="<%= SBApp.router.getShareUrl() %>" layout="button_count" action="like" show_faces="false" share="true"></fb:like>\
    </div>\
  </div>\
 <% } %>');

/* Комментарии: форма добавления */
JST["comments:form:add"] = _.template('<div class="add-comment" id="add-comment">\
<div class="row-fluid add-comment__message">\
  <div class="span2 add-comment__userpic"><img src="<%= userPhoto %>" /></div>\
  <div class="span10 add-comment__text">\
    <textarea class="span12 add-comment__textarea" id="add-comment__textarea" placeholder="Ваш комментарий..."></textarea><br />\
  </div>\
</div>\
<div class="row-fluid">\
  <button class="btn btn-small disabled" id="add-comment__btn">Отправить</button>\
</div>\
</div>');

/* Комментарии: основной блок */
JST["comments"] = _.template('<ul class="comments <% if (reply) { %>comments_reply <% } %>" id="comments" >\
  <% _.each(comments, function (comment, key) { %>\
    <%= renderComment({ comment: comment }) %>\
  <% }) %>\
</ul>');

/* Комментарии: один комментарий */
JST["comments:item:user:auth"] = _.template('<li class="comments__item">\
  <% if( comment.customer && comment.customer.profile ) { %>\
  <div class="row">\
      <div class="span comments__item-photo">\
        <% if (SBApp.Platform.isVK()) { %>\
          <% if (comment.customer.profile.photo_50) { %>\
            <img src="<%= comment.customer.profile.photo_50 %>" /><br />\
          <% } %>\
        <% } %>\
        <% if (SBApp.Platform.isFacebook()) { %>\
          <img src="//graph.facebook.com/<%= comment.customer.profile.id %>/picture" /><br />\
        <% } %>\
        <% if (SBApp.Platform.isOK()) { %>\
          <img src="<%= comment.customer.profile.pic50x50%>" /><br />\
        <% } %>\
      </div>\
      <div class="span4">\
        <p class="comments__item-info">\
          <% if (SBApp.Platform.isVK()) { %>\
            <a class="comments__item-user" href="//vk.com/id<%= comment.customer.profile.id %>" target="_blank"><%= comment.customer.profile.first_name %>&nbsp;<%= comment.customer.profile.last_name %></a><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small></p>\
          <% } %>\
          <% if (SBApp.Platform.isFacebook()) { %>\
            <a class="comments__item-user" href="//facebook.com/<%= comment.customer.profile.id %>" target="_blank"><%= comment.customer.profile.name %></a><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small></p>\
          <% } %>\
          <% if (SBApp.Platform.isOK()) { %>\
            <a class="comments__item-user" href="//www.odnoklassniki.ru/profile/<%= comment.customer.profile.uid %>" target="_blank"><%= comment.customer.profile.name %></a><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small></p>\
          <% } %>\
        <p class="comments__item-text"><%= comment.text %></p>\
      </div>\
  </div>\
  <% } %>\
  <% if ( comment.reply_list.length > 0 && comment.customer && comment.customer.profile ) { %>\
    <%= JST["comments"]({ comments: comment.reply_list, renderComment:  JST["comments:item:user:admin"], reply: true }) %>\
  <% } %>\
</li>');

JST["comments:item:user:admin"] = _.template('<li class="comments__item">\
  <div class="row">\
      <div class="span comments__item-photo">\
        <img src="" /><br />\
      </div>\
      <div class="span4">\
        <p class="comments__item-info">\
          <span class="comments__item-user">Администрация</span><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small>\
        </p>\
        <p class="comments__item-text"><%= comment.text %></p>\
      </div>\
  </div>\
</li>');


/* Форма заказа, по сервису CheckOut */
JST["form:order:checkout"] = _.template('<form id="SBorderFormCheckOut" <% if (hidden) { %>style="display: none;" <%}%> ><input type="hidden" name="ticket" value="<%= ticket %>" />\
  <input type="hidden" name="callbackURL" value="<%= callbackUrl %>" />\
  <% _.each(cartItems, function (cartItem, key) { %>\
    <input type="hidden" name="names[<%= key %>]" value="<%= cartItem["product"]["name"] %>" />\
    <input type="hidden" name="codes[<%= key %>]" value="<%= cartItem["product"]["model_oid"] %>" />\
    <input type="hidden" name="quantities[<%= key %>]" value="<%= cartItem["count"] %>" />\
    <input type="hidden" name="costs[<%= key %>]" value="<%= cartItem["product"]["price"] %>" />\
    <input type="hidden" name="paycosts[<%= key %>]" value="<%= cartItem["product"]["price"] %>" />\
    <input type="hidden" name="weights[<%= key %>]" value="1" />\
  <% })%>\
  <% if (discount !== 0) { %>\
    <input type="hidden" name="names[<%= cartItems.length %>]" value="<%= discount["name"] %>" />\
    <input type="hidden" name="codes[<%= cartItems.length %>]" value="<%= discount["value"]%>" />\
    <input type="hidden" name="quantities[<%= cartItems.length %>]" value="1" />\
    <input type="hidden" name="costs[<%= cartItems.length %>]" value="<%= discount["price"] %>" />\
    <input type="hidden" name="paycosts[<%= cartItems.length %>]" value="<%= discount["price"] %>" />\
    <input type="hidden" name="weights[<%= cartItems.length %>]" value="1" />\
  <% } %>\
  <input type="submit" style="display: none">\
  <% if(!hidden) { %>\
  <input type="button" class="btn" onclick="order_checkout(\'SBorderFormCheckOut\')" value="Оформить доставку" />\
  <% } %>\
</form>');

JST["breadcrumbs:item:link"] = _.template('<a href="#category/<%= category.id %>"><%= category.name %></a><span class="divider"><%= separator %></span>');

JST["breadcrumbs:item:active"] = _.template('<%= name %>');

/* Корзина */
JST['cart:order:form'] = _.template('<form class="form-horizontal order-form" id="SBorderForm" style="display: none;">\
  <h2>Оформление заказа</h2>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderName">Получатель:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderName" placeholder="Имен Фамилёв" data-required="true" data-type="text" data-valid="<%= valid_username %>" value="<%= username %>">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderPhone">Телефон:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderPhone" placeholder="8 495 111-11-11" data-required="true" data-type="phone" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderEmail">Email:</label>\
    <div class="controls">\
      <input type="email" class="span5 сontrol-input" id="SBorderEmail" placeholder="name@surname.ru" data-required="true" data-type="email" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderAddress">Адрес доставки:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderAddress" placeholder="Улица Доставочная, дом 1" data-required="true" data-type="text" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label" for="SBorderComment">Комментарий:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderComment" placeholder="" data-required="false">\
    </div>\
  </div>\
  <div class="control-group">\
    <div class="controls-row">\
      <div class="controls">\
        <span class="required-control">*</span>&nbsp;поле, обязательное для заполнения\
      </div>\
    </div>\
  </div>\
  <div class="controls-row">\
    <button class="btn pull-right SBcartOrder" id="SBcartOrder" type="button" disabled="disabled"><i class="sb-icon-ok icon-white"></i> Заказ оформлен</button>\
  </div>\
</form>');

/* Форма заказа. Используется в модальном окне. */
JST['order:new'] = _.template('<div class="control-group">\
    <label class="control-label control-required" for="new-order-name">Получатель:</label>\
    <div class="controls">\
      <input type="text" class="сontrol-input" name="name" id="new-order-name" placeholder="Имен Фамилёв" data-required="true" data-type="text" data-valid="<%= isUsernameValid() %>" value="<%= username() %>">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="new-order-phone">Телефон:</label>\
    <div class="controls">\
      <input type="text" class="сontrol-input js-new-order-phone" name="phone" id="new-order-phone" placeholder="8 495 111-11-11" data-required="true" data-type="phone" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="new-order-email">Email:</label>\
    <div class="controls">\
      <input type="email" class="сontrol-input" name="email" id="new-order-email" placeholder="name@surname.ru" data-required="true" data-type="email" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="new-order-address">Адрес доставки:</label>\
    <div class="controls">\
      <input type="text" class="сontrol-input" name="address" id="new-order-address" placeholder="Улица Доставочная, дом 1" data-required="true" data-type="text" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label" for="new-order-comment">Комментарий:</label>\
    <div class="controls">\
      <input type="text" class="сontrol-input" name="comment" id="new-order-comment" placeholder="" data-required="false">\
    </div>\
  </div>\
  <div class="control-group">\
    <div class="controls-row">\
      <div class="controls">\
        <span class="required-control">*</span>&nbsp;поле, обязательное для заполнения\
      </div>\
    </div>\
  </div>\
  <div class="control-group">\
    <div class="controls-row">\
      <div class="controls"><button class="btn new-order__btn js-new-order__btn" id="new-order__btn" type="button" disabled="disabled"><i class="sb-icon-ok icon-white"></i> Отправить</button></div>\
    </div>\
    <input type="hidden" name="app_type" value="<%= appType() %>"\
  </div>');

JST['cart'] = _.template('<h1 class="page-title">Корзина</h1>\
<% if (SBApp.cart.isEmpty()) { %>\
  <p style="font-size: 14px;">Пока в вашей корзине нет товаров.<p>\
<% } else { %>\
  <table class="table table-hover" id="cart-items">\
    <col style="width:50px">\
    <col>\
    <colgroup span="4" style="text-align:center">\
      <col style="width:50px">\
      <col style="width:100px">\
      <col style="width:50px">\
    </colgroup>\
    <thead>\
      <tr>\
        <th class="img"></th>\
        <th class="name">Название</th>\
        <th class="price">Цена</th>\
        <th class="count"><nobr>Кол-во</nobr></th>\
        <th class="price">Итого</th>\
        <th class="remove"></th>\
      </tr>\
    </thead>\
  </table>\
  <div class="controls-row">\
    <button class="btn pull-right SBcartPrepare" id="SBcartPrepare" type="button" <% if (SBApp.cart.isEmpty()) { %> disabled <% } %>><i class="sb-icon-thumbs-up icon-white"></i> Оформить заказ</button>\
  </div>\
<% } %>');

JST['cart:foot'] = _.template('<tr class="lead">\
  <td colspan="4" style="text-align: right"><strong>Итого:</strong></td>\
  <td colspan=2><%= SBApp.getPriceWithCurrency(total) %></td>\
</tr>\
<% if (discount) {%>\
  <tr>\
    <td colspan="4" style="text-align:right"><%= discount.name %></td>\
    <td colspan="2"><%= discount.value %></td>\
  </tr>\
  <tr class="lead">\
    <td colspan="4" style="text-align: right"><strong>Со скидкой:</strong></td>\
    <td colspan="2"><%= SBApp.getPriceWithCurrency(discount.price) %></td>\
  </tr>\
<% } %>\
<% if( SBApp.hasPromocode() ) { %>\
  <tr class="promocode">\
    <td><label for="promocode-field">Промокод:</label></td>\
    <td colspan="4"><input type="text" id="promocode-field" class="span2 promocode-field"/> <a class="btn disabled" id="promocode-apply">Применить</a></td>\
  </tr>\
<% } %>');

/* Товар в корзине */
JST['cart:item'] = _.template('<td><img src="<%= product.cover %>"></td>\
<td><a href="#" class="SBproductHead js-product-profile"><%= product.name %></a><% if (offer) { %><br /><span class="muted">Размер: <%= offer.size %></span><% } %></td>\
<td><%= SBApp.getPriceWithCurrency(product.price) %></td>\
<td><a class="sb-cart__item-dec js-decrement" href="#">-</a>&nbsp;<%= count %>&nbsp;<a class="sb-cart__item-inc js-increment" href="#">+</a></td>\
<td><%= SBApp.getPriceWithCurrency(price_with_discount.price) %></td>\
<td><button class="btn btn-mini sb-cart__item-trash js-trash" type="button"><i class="sb-icon-trash"></i></button></td>'
);


/* Баннер */
JST['banner'] = _.template('<div class="sb-banner__container" ></div>');

JST['banner:controls'] = _.template('<div id="sb-banner__prev" class="sb-banner__prev sb-banner__control"><i class="sb-icon-chevron-left sb-banner__icon"/></div>\
<div id="sb-banner__next" class="sb-banner__next sb-banner__control"><i class="sb-icon-chevron-right sb-banner__icon"/></div>');

/* Текстовый баннер */
JST['banner:item:text'] = _.template('<div><%= text %></div>');

/* Графический баннер */
JST['banner:item:graphical'] = _.template('<div class="sb-banner-graph__img" title="<%= product.name %>" style="background-image: url(\'<%= image_url %>\');">&nbsp;</div>');

/* Продуктовый баннер */
JST['banner:item:carousel'] = _.template('<div class="span3"><img src="<%= cover %>" class="sb-banner__img" /></div>\
<div class="span9 text-left">\
  <h4 class="sb-banner__item-title"><%= name %></h4>\
  <p class="sb-banner__item-price"><%= SBApp.getPriceWithCurrency(price) %></p>\
  <div class="sb-banner__item-desc"><% if (!_.isEmpty(jQuery.trim(desc))) { print(desc) }else { %><i>Без описания.</i><% } %></div>\
</div>');

/* ПВЗ */
SBApp.Templates.DeliveryCityListView = {
  container: '.SBdelivery',
  tagName: 'select',
  className: 'SBdeliveryCity offset2'
};

SBApp.Templates.DeliveryCityView = {
  body: '<option value="<%= id %>"><%= name %></option>'
};

SBApp.Templates.DeliveryPointListView = {
  container: '.SBdelivery',
  tagName: 'div',
  className: 'SBdeliveryMap'
};

SBApp.Templates.DeliveryPointView = {
  body: '<p class="point__desc"><%= point_name %><br>\
        <span class="point__work-time"><%= json_data.work_time %></span></p>\
        <p><strong>Адрес:</strong><br /><%= json_data.address %></p>\
        <p><strong>Телефон:</strong><br /><%= json_data.telephone %></p>\
        <a class="btn pull-right SBdeliveryChoice" data-id="<%= id %>"><i class="sb-icon-thumbs-up icon-white"></i> выбрать</a>'
};

/* Новости */
JST['news'] = _.template('<h1 class="page-title">Новости</h1><div class="sb-news-list"></div>');

JST['news:item'] = _.template('<h2><%= name %></h2>\
<small class="muted"><%= dateAsString() %></small>\
<p class="sb-news-list__item-text"><%= content %></p><hr />');

/* Пагинатор */
JST['pagination:title'] = _.template('<span class="pagination__title"><%= totalCount %>&nbsp;<%= SBApp.getWordWithRightEnding(totalCount) %>:</span>');

JST['pagination:item'] = _.template('<a href="#" data-offset="<%= offset %>" id="page-<%= page %>"><%= page %></a>');

JST['pagination:next'] = _.template('<li class="<%= className %>"><a href="#" id="next-page">&raquo;</a></li>');
JST['pagination:prev'] = _.template('<li class="<%= className %>"><a href="#" id="previous-page">&laquo;</a></li>');
JST['pagination:dots'] = _.template('<li class="three-dots"><a>...</a></li>');

JST['pagination:infinite:title'] = 'Еще товары';
JST['pagination:infinite'] = _.template('<button class="btn js-show-more btn-default"><%= JST["pagination:infinite:title"] %></button>');

SBApp.Templates.OrderView = {
  el: '.SBcontent',

  orderSuccess: '<h1></h1>\
    <p>Ваш заказ <% if (orderId) { %> <b>№ <%= orderId %></b><% } %> успешно оплачен.</p>'
};

/* Поиск: поля ввода */
JST['search:form'] = _.template('<input type="text" name="" class="search__input" id="search__input" placeholder="Поиск..." />');
/*body: '<i class="sb-icon-search search__icon"></i><input type="text" name="" class="search__input" id="search__input" placeholder="Искать..." /><a class="search__btn disabled" id="search__reset"><i class="sb-icon-reset search__icon search__icon-reset"></i></a>' */

JST['search:results:title'] = _.template('<h3 class="search-results__title">\
<% if(query.getResultsCount() > 0) { %>\
  Вы искали "<%= query.getQueryString() %>", нашлось <%= query.getResultsCount() %> <%= SBApp.getWordWithRightEnding(query.getResultsCount(), {nom: "результат", gen: "результата", plu: "результатов"}) %>\
<% } else { %>\
  По запросу "<%= query.getQueryString() %>" ничего не нашлось.\
<% } %></h3>');

/* Заказ отправлен */
JST['orders:item:send'] = _.template('<h1>Спасибо!</h1>\
  <p>Ваш заказ <b>№ <%= id %></b> успешно оформлен.</p>\
  <% if (!_.isEmpty(email)) { %>\
   <p>На адрес <b><%= email %></b> было отправлено письмо с информацией о заказе.</p>\
  <% } %>\
  <% if( status === 1 && !_.isEmpty(robokassa_url) ) { %>\
  <div>\
    <a href="<%= robokassa_url %>" target="_blank" class="btn">Оплатить</a>&nbsp;<img src="/static/images/robokassa.jpg" />\
  </div>\
<% } %>');

JST['orders:item:failed'] = _.template('<h1>Неудачная оплата</h1>\
  <p>Не удалось провести оплату по заказу <% if (id) { %> <b>№ <%= id %></b><% } %>. Попробуйте позже</p>');

/* Информация о заказе чекаут */
/*
 comment - Комментарий пользователя
 deliveryId - идентификатор службы доставки в платформе
 address - Адрес доставки
 deliveryPlace - Населённый пункт покупателя
 deliveryOrderCost - Общая стоимость заказа (с доставкой)
 orderId - идентификатор заказа в платформе
 clientPhone - Телефон покупателя
 deliveryCost - Стоимость доставки
 deliveryStreetId - Улица покупателя
 deliveryMinTerm - Минимальное количество дней доставки
 clientFIO - ФИО покупателя
 deliveryPlaceId идентификатор постамата или ПВЗ
 deliveryMaxTerm Максимальное количество дней доставки
 deliveryPostindex Почтовый индекс адреса доставки покупателя
 deliveryWeight Общий вес товаров
 clientEmail e-mail покупателя
 deliveryType Тип выбранной доставки, постамат, ПВЗ или курьер (postamat, pvz, express)
 * */
JST["orders:item:checkout:success"] = _.template('<h4>Информация о заказе</h4>\
<!-- Клиент -->\
<% if(order.clientFIO) {%> <p><b>ФИО:</b> <%- order.clientFIO %></p> <% } %>\
<% if(order.clientEmail) {%><p><b>E-mail покупателя:</b></p><% } %>\
<% if(order.clientPhone) {%> <p><b>Телефон покупателя:</b> <%- order.clientPhone %></p> <% } %>\
<!-- Доставка -->\
<% if(order.address) {%> <p><b>Адрес доставки:</b> <%- order.address %></p> <% } %>\
<% if( order.deliveryType) { %><p><b>Тип доставки:</b> <%= SBApp.delivery(order.deliveryType) %></p><% } %>\
<% if (order.deliveryOrderCost) { %><p><b>Цена заказа (с доставкой):</b> <%= SBApp.getPriceWithCurrency(order.deliveryOrderCost) %></p><% } %>\
<% if( SBApp.services.robokassa && (status === 1 || status === 3) && !_.isEmpty(robokassa_url) ) { %>\
  <div>\
    <a href="<%= robokassa_url %>" target="_blank" class="btn">Оплатить</a>&nbsp;<img src="/static/images/robokassa.jpg" />\
  </div>\
<% } %>');

JST['search:results:item'] = _.template('<div class="row">\
  <div class="span1 search-results__item-cover" style="background-image: url(\'<%= cover %>\');"></div>\
  <div class="span6 search-results__item-info">\
    <h5 class="search-results__item-title"><%= name %></h5>\
    <div class="search-results__item-desc"><% if (!_.isEmpty(jQuery.trim(desc))) { print(desc) }\
      else { %><i>Без описания.</i><% } %></div>\
        </div>\
    <div class="span1 search-results__item-price"><%= SBApp.getPriceWithCurrency(price) %></div>\
  </div>\
</div>');

JST['typeahead:item'] = _.template('<div class="tt-suggestion__inner"><%= name %></div>');

JST['orders:list:item'] = _.template('<h5 class="order-item__title">&#8470; <%= id %>&nbsp;&nbsp;<time><%= SBApp.getDateAsString(cdate, "short", false) %></time>&nbsp;<span class="order-item__status"><%= SBApp.orderTextStatus(status)%></span><span class="order-item__collapse"><i class="icon-chevron-down icon-white"></i></span></h5>\
  <div class="order-item__content">\
    <table class="table">\
      <col style="width: 50px">\
      <col>\
      <colgroup span="4" style="text-align:center">\
        <col style="width: 50px">\
        <col style="width: 100px">\
        <col style="width: 50px">\
      </colgroup>\
      <tfoot>\
        <tr>\
          <td colspan="4" style="text-align: right">Сумма заказа:</td><td colspan="2"><%= SBApp.getPriceWithCurrency( cart.total_sum ) %></td>\
        </tr>\
      </tfoot>\
      <thead>\
        <tr>\
          <th class="img"></th>\
          <th class="name">Название</th>\
          <th class="price">Цена</th>\
          <th class="count"><nobr>Кол-во</nobr></th>\
          <th class="price">Итого</th>\
        </tr>\
       </thead>\
      <% _.each(cart.items, function (item, key) { %>\
      <tr>\
        <td><img src="<%= item.product.cover%>" style="width: 35px;"></td>\
        <td><%= item.product.name %></td>\
        <td><%= SBApp.getPriceWithCurrency(item.product.price) %></td>\
        <td><%= item.count %></td>\
        <td><%= SBApp.getPriceWithCurrency(item.product.price * item.count) %></td>\
      </tr>\
      <% }) %>\
    </table>\
  </div>\
 ');

JST['modal'] = _.template('<div class="modal-header">\
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>\
  <% if (title) { %>\
    <h4 class="modal-header__title text-center"><%= title %></h4>\
  <% } %>\
</div><div class="modal-body"></div><div class="modal-footer"></div>');

JST['order:callback'] = _.template('<div class="recall" id="recall"><div class="span6">\
  <div class="product-preview">\
    <div class="product-preview__cover img-rounded">\
      <img src="<%= cover %>" />\
    </div>\
    <div class="text-center"><span class="product-preview__price"><%= SBApp.getPriceWithCurrency(price) %></span></div>\
    <h5 class="product-preview__title text-center"><%= name %></h5>\
  </div>\
</div>\
<div class="span6">\
  <form class="recall-form">\
    <div class="control-group <% if(SBApp.userModel.get(\'name\')){ %>success<%}%>">\
      <label class="control-label" for="recall-name">Ваше имя:</label>\
      <div class="controls">\
        <input type="text" class="сontrol-input" id="recall-name" placeholder="Имен Фамилёв" data-required="true" data-type="text" data-valid="<% if(SBApp.userModel.get(\'name\')){ %>true<%}else{%>false<%}%>" value="<%= SBApp.userModel.get(\'name\') %>">\
        <span class="help-inline"></span>\
      </div>\
    </div>\
    <div class="control-group">\
      <label class="control-label" for="recall-phone">Телефон:</label>\
      <div class="controls">\
        <input type="text" class="сontrol-input" id="recall-phone" placeholder="8 495 111-11-11" data-required="true" data-type="phone" data-valid="false">\
        <span class="help-inline"></span>\
      </div>\
    </div>\
    <div class="control-group">\
      <button class="btn js-send-recall btn-send-recall" type="button" disabled="disabled">Я жду звонка</button>\
    </div>\
  </form>\
</div>\
</div>');

JST['subscribe:alert'] = _.template('<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>\
<form class="form-inline sb-subscribe__form">\
  <b class="alert-heading sb-subscribe__heading">Узнавать первым о новинках и скидках</b>\
  <input type="email" class="sb-subscribe__email" id="sb-subscribe__email" placeholder="Ваш email" />\
  <button type="button" class="btn js-subscribe">Подписаться</button>\
</form>');

JST['subscribe:message:error'] = _.template('<b class="text-error"><%= error %></b>');

JST['subscribe:message:success'] = _.template('<b class="alert-heading sb-subscribe__heading">Спасибо!</b>');

SBApp.Templates.Validator = {
  controlRequired: 'Поле обязательно для заполнения',
  controlEmailNotValid: 'Введенное значение не соответсвует формату электронной почты',
  controlPhoneNotValid: 'Введенное значение не соотвествует формату телефонного номера'
};


JST['staticpage'] = _.template('<h1 class="page-title"><%= title %></h1><div class="page-content"><%= content %></div>');

/* Блок рекомендация в корзине */
JST['recommend'] = _.template('<h4 class="sb-cart-recommend__title">Также рекомендуем</h4><div class="sb-cart-recommend__container clearfix"></div>');

JST['recommend:item'] = _.template('<div class="sb-recommend__item-cover" style="background-image: url(\'<%= cover %>\');"></div>\
<div class="sb-cart-recommend_item-title"><%= name %></div>\
<div class="text-center sb-cart-recommend__item-price"><%= SBApp.getPriceWithCurrency(price) %></div>\
<div class="text-center"><a class="btn btn-mini js-add" href="#">В корзину</a></button></div>');

JST['cart:preview'] = _.template('<div class="sb-cart-preview__container"></div><hr style="margin: 5px 0;" /><div class="sb-cart-preview__summary"><%= orderData() %></div><div class="text-center" style="margin-top: 10px;"><button class="btn btn-default js-close" style="margin-right: 8px;">Продолжить покупку</button>&nbsp;<button class="btn btn-success js-to-cart"><i class="sb-icon-cart"></i>&nbsp;Перейти в корзину</button></div>');

/* В модальном окне содержимое корзины */
JST['cart:preview:item'] = _.template('<div class="sb-cart-preview__item-img" style="background-image: url(\'<%= product.cover %>\')"></div>\
  <div class="sb-cart-preview__item-title"><%= product.name %></div>\
  <div class="sb-cart-preview__item-content"><% if (offer) { %><span class="muted">Размер: <%= offer.size %></span><br /><% } %>\
  <span class="muted">Кол-во: <%= count %></span><br />\
  <span><%= SBApp.getPriceWithCurrency(price_with_discount.price) %></span></div>');

JST['page:not:found'] = _.template('<h2></h2>');
