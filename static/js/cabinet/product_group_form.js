/**
 * Created by zhuravlev on 30.06.14.
 */


$(function(){
    var ProductModel = Backbone.Model.extend({
        url: "/cabinet/api/v1/product/?shop_id=" + shop_pk,
        idAttribute: '_id',
        label: function(){
            return this.get("name");
        }
    });

    var ProductCollection = Backbone.Collection.extend({
        url: "/cabinet/api/v1/product/?shop_id=" + shop_pk,
        model: ProductModel,
        parse: function(data){
            return data.objects;
        }
    });


    var ProductCollectionResultItemView = Backbone.View.extend({
        template: _.template('<div class="media">\
          <a class="pull-left" href="<%= href %>#">\
            <img class="media-object" src="<%= image_src %>">\
          </a>\
          <div class="media-body">\
            <h4 class="media-heading">\
            <%= media_head %> <%= price_str %>\
            <button class="del btn btn-warning btn-mini">Удалить</button>\
            </h4>\
            <%= media_body %>\
          </div>\
        </div>'),

        events: {
            'click .del': 'onClick'
        },
        onClick: function(){
            this.model.destroy();
            this.remove();
        },
        render: function(){
            var price_str = this.model.get('price') + ' ' + currency_str;
            this.$el.html(this.template({
                href: '',
                price_str: price_str,
                image_src: this.model.get('cover'),
                media_head: this.model.get('name'),
                media_body: this.model.get('desc')
            }));
            return this;
        }
    });

    var ProductCollectionResultView = Backbone.View.extend({
        el: "#res",
        initialize: function() {
            this.render();
            this.listenTo( this.collection, 'add', this.addItem );
        },
        render: function(){
            if (this.collection.models.length) {
                this.$el.html('');
                _.forEach(this.collection.models, this.addItem, this);
            }
        },
        addItem: function(model){
            this.$el.append(new ProductCollectionResultItemView({model: model}).render().$el);
        }
    });

    var SubmitView = Backbone.View.extend({
        el: "form#js-products",
        events: {
            'click input[type=submit]': 'onSubmit'
        },
        onSubmit: function(event){
            event.preventDefault();
            var inp = $(this.$el.find('input.js-products')[0]);
            var val = _.map(this.collection.models, function(p_model){
                return p_model.get('id');
            }).join(',');
            inp.val(val);
            this.$el.submit();
            return this;
        }
    });

    var resultCollection = new ProductCollection();
    var ids = $('input.js-products').val();

    if (ids.length !== 0) {
        var url =  "/cabinet/api/v1/product/?shop_id=" + shop_pk + '&id__in=' + ids;
        resultCollection.fetch({url: url});
    }

    new AutocompleteView({
        srcCollection: new ProductCollection(),
        resultCollection: resultCollection,
        listenInput: "input#autocomplete",
        queryParameter: "query"
    }).render();

    new ProductCollectionResultView({collection: resultCollection}).render();
    new SubmitView({collection: resultCollection}).render();

});