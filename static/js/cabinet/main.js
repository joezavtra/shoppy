/**
 * Created with PyCharm.
 * User: sketcher
 * Date: 14.08.13
 * Time: 18:29
 * To change this template use File | Settings | File Templates.
 */

$.ajaxSetup({
  cache: false
});

var CabinetApp = new Marionette.Application();

$(function () {
    var uiColor = '#eeeeee';
    var wysiwygConfig = {
      allowedContent: true,
      language: 'ru',
      uiColor: uiColor,
      /* Какие кнопки показывать на "Тулбаре" */
      toolbar: [
        { name: 'document', groups: [ 'mode' ], items: [ 'Source' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike'] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize' ] }
      ]
    };

    // Отображаем список ссылок на разделы редактирования магазина если ссылка
    // на какой то раздел магазина активна
    $('#menu-left .shop-section-link .active').parent().show();

    /* Тулбар висвиг Лэндинг Заголовка */
    $('.wysiwyg-title').ckeditor({
      allowedContent: true,
      uiColor: uiColor,
      toolbarGroups: [
        { name: 'styles', groups: [ 'fonts' ] },
        { name: 'paragraph',   groups: [ 'basicstyles' ] },
        { name: 'colors'}
      ]
    });

    $('.wysiwyg').ckeditor(wysiwygConfig);
    $(".switcher").bootstrapSwitch();
});
