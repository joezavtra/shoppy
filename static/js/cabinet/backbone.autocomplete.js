/**
 * Created by zhuravlev on 01.07.14.
 */

var AutoCompleteItemView = Backbone.View.extend({
    tagName: "li",
    template: _.template('<%= label %>'),

    events: {
        "click": "select"
    },

    render: function(){
        this.$el.html(this.template({
            "label": this.model.label()
        }));
        return this;
    },

    select: function () {
        this.options.parent.select(this.model);
        return false;
    }
});


var AutocompleteView = Backbone.View.extend({
    /*
    * Коллекция, по которой надо искать. Должна иметь метод search(string),
    * где string - строка, по которой ищут. Модель коллекции должна иметь метод
    * .label() для генерации текста для найденного элемента
    * */
    srcCollection: null,
    /*
    * Коллекция, в которую добавляются выбранные пользователем элементы
    * */
    resultCollection: null,
    /*
    * Элемент (input), за которым следит представление(туда пользователь вводит
    * поисковую строку)
    * */
    listenInput: null,
    /*
    * Время ожидания перед отправкой запроса
    * */
    wait: 300,
    /*
    * Имя GET параметра по которому необходимо производить поиск
    * */
    queryParameter: "query",
    /*
    * Минимальное кол-во символов, после который начнется поиск
    * */
    minKeywordLength: 2,
    /*
    * Текущее состояние
    * */
    currentText: "",
    /*
    * Представление для итема
    * */
    itemView: AutoCompleteItemView,

    el: "ul.autocomplete",

    initialize: function (options) {
        _.extend(this, options);
        this.listenInput = $(this.listenInput);
        // this.listenInput.blur(_.bind(this.hide, this));
        this.filter = _.debounce(this.filter, this.wait);
    },

    render: function () {
        this.listenInput.attr("autocomplete", "off");

        this.$el.width(this.listenInput.outerWidth());

        this.listenInput
            .keyup(_.bind(this.keyup, this))
            .keydown(_.bind(this.keydown, this))
            .after(this.$el);

        return this;
    },

    keydown: function (event) {
        if (event.keyCode == 38) return this.move(-1);
        if (event.keyCode == 40) return this.move(+1);
        if (event.keyCode == 13) return this.onEnter();
        if (event.keyCode == 27) return this.hide();
    },

    keyup: function () {
        var keyword = this.listenInput.val();
        if (this.isChanged(keyword)) {
            this.currentText = keyword;
            if (this.isValid(keyword)) {
                this.filter(keyword);
            } else {
                this.hide()
            }
        }
    },

    filter: function (keyword) {
        if (this.srcCollection.url) {

            var parameters = {};
            parameters[this.queryParameter] = keyword;

            this.srcCollection.fetch({
                success: function () {
                    this.loadResult(this.srcCollection.models, keyword);
                }.bind(this),
                data: parameters
            });

        } else {
            this.loadResult(
                this.srcCollection.filter(function (model) {
                    return model.label().toLowerCase().indexOf(keyword) !== -1
            }), keyword);
        }
    },

    isValid: function (keyword) {
        return keyword.length > this.minKeywordLength
    },

    isChanged: function (keyword) {
        return this.currentText != keyword;
    },

    move: function (position) {
        var current = this.$el.children(".active");
        var siblings = this.$el.children();
        var index = current.index() + position;
        if (siblings.eq(index).length == 0) {
            index = 0;
        }
        current.removeClass("active");
        siblings.eq(index).addClass("active");
        return false;
    },

    onEnter: function () {
        this.$el.children(".active").click();
        return false;
    },

    loadResult: function (model, keyword) {
        this.currentText = keyword;
        this.show().reset();
        if (model.length) {
            _.forEach(model, this.addItem, this);
            this.show();
        } else {
            this.hide();
        }
    },

    addItem: function (model) {
        this.$el.append(new this.itemView({
            model: model,
            parent: this
        }).render().$el);
    },

    select: function (model) {
        var label = model.label();
//        this.listenInput.val(label);
//        this.currentText = label;
        this.onSelect(model);
        return false;
    },

    reset: function () {
        this.$el.empty();
        return this;
    },

    hide: function () {
        this.$el.hide();
        return this;
    },

    show: function () {
        this.$el.show();
        return this;
    },

    onSelect: function (model) {
        this.resultCollection.add(model);
        this.listenInput.val('');
        this.hide();
    }

});
