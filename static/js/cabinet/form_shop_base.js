/**
 * Created by zhuravlev on 08.04.14.
 */
$(function(){
    function onChangeMode(){
        if($('[id$=mode]').val() == '2'){
            $('[id$=yml_url]').parent().parent().hide();
        }else{
            $('[id$=yml_url]').parent().parent().show();
        }
    }

    function onChangeDs(){
        if($('[id$=assortment]').val() == ''){
            $('[id$=mode]').parent().parent().show();
            $('[id$=yml_url]').parent().parent().show();
            onChangeMode();
        }else{
            onChangeMode();
            $('[id$=mode]').parent().parent().hide();
            $('[id$=yml_url]').parent().parent().hide();
        }
    }

    onChangeDs();
    onChangeMode();
    $('[id$=mode]').change(function(){
        onChangeMode();
    });
    $('[id$=assortment]').change(function(){
        onChangeDs();
    });
});