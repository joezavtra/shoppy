/* Содержит основные, базовые вьюхи кабинета */
CabinetApp.module('Common.Views', function (Views, Cabinet, Backbone, Marionette, $, _) {
  /* Модального окна "Отключить источник" */
  Views.ModalView = Marionette.ItemView.extend({
    className: 'modal hide fade'
  });

  /* Модальное окно: "Отключить источник"*/
  Views.DataSourceModal = Views.ModalView.extend({
    template: _.template('<div class="modal-header">\
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
      <h3>Вы уверены, что хотите отключить &laquo;<%= name %>&raquo;?</h3>\
    </div>\
    <div class="modal-body">\
      <p>Все товары и категории источника будут убраны из магазина.</p>\
    </div>\
    <div class="modal-footer">\
      <button class="btn" data-dismiss="modal" aria-hidden="true">Не отключать</button>\
      <a href="./?del_datasource=<%= id %>" class="btn btn-danger">Да, хочу отключить</a>\
    </div>')
  });

  /* Модальное окно: Опросить источник */
  Views.PullDataSourceModal = Views.ModalView.extend({

    ui: {
      pullBtn: '.js-ds-pull'
    },

    events: {
      'click @ui.pullBtn' : 'pullDatasource'
    },

    template: _.template('<div class="modal-header">\
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>\
      <h3>Опросить источник &laquo;<%= name %>&raquo;?</h3>\
    </div>\
    <div class="modal-body">\
      <p>Опрос может занять продолжительное время, в зависимости от количества товаров и категорий.</p>\
    </div>\
    <div class="modal-footer">\
      <button class="btn" data-dismiss="modal" aria-hidden="true">Нет, спасибо</button>\
      <button type="button" class="btn btn-danger js-ds-pull" data-url="<%= url %>">Да</button>\
    </div>'),

    pullDatasource: function (event) {
      var $self = $(event.currentTarget),
          url = $self.data('url'),
          that = this;

      event.preventDefault();

      /* Если заблокирована */
      if ( this.ui.pullBtn.prop('disabled') ) {
        return;
      }

      this.ui.pullBtn.prop('disabled', true).text('Опрашивается...');

      $.ajax({
        url: url
      });

      _.delay( function () {
        that.$el.modal('hide');

        location.reload(true);
      }, 3000);
    }
  });

  /* Модальное окно: Очистить источник */
  Views.ClearDataSourceModal = Views.ModalView.extend({
    template: _.template('<div class="modal-header">\
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>\
      <h3>Очистить данные источника &laquo;<%= name %>&raquo;?</h3>\
    </div>\
    <div class="modal-body">\
      <p>Удаление всех данных, связанных с источником: товары, категории, заказы</p>\
    </div>\
    <div class="modal-footer">\
      <button class="btn" data-dismiss="modal" aria-hidden="true">Нет, спасибо</button>\
      <a href="<%= url %>" class="btn btn-danger js-ds-clear">Да</a>\
    </div>')
  });

  /* Модальное окно: Изменить статус заказа*/
  Views.OrderStatusModal = Views.ModalView.extend({
    events: {
      'click .js-send-order-comment' : 'sendComment'
    },

    ui: {
      commentInput: '.comment'
    },

    template: _.template('<div class="modal-header">\
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>\
        <h3>Заказ #<%= id %> - &laquo;<%= status_text %>&raquo;</h3>\
      </div>\
      <div class="modal-body">\
        <div class="controls">\
          <textarea class="span12 comment" placeholder="Введите комментарий"></textarea>\
        </div>\
      </div>\
      <div class="modal-footer">\
        <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>\
        <button class="btn btn-primary js-send-order-comment" data-id="<%= id %>" data-status-val="<%= status_val %>" >Сохранить</button>\
      </div>\
  </div>'),

    /* Оставить комментарий к заказу */
    sendComment: function (event) {
      event.preventDefault();

      var order_id = this.model.get('id'),
          status_val = this.model.get('status_val'),
          comment = $.trim( this.ui.commentInput.val() ),
          data = {
            status: status_val,
            approved_comment: comment
          };

      $.ajax({
        url: '/cabinet/api/v1/order/' + order_id,
        type: 'PATCH',
        data: JSON.stringify(data),
        contentType: 'application/json',
        dataType: 'json',
        complete: function (data) {
          location.reload();
        }
      });
    }
  });
});