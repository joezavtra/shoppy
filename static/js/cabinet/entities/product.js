/* Сущности продукта */
var ProductModel = Backbone.Model.extend({
  url: "/cabinet/api/v1/product/?shop_id=" + shop_pk,
  idAttribute: '_id',
  label: function() {
    return this.get("name");
  }
});

var ProductCollection = Backbone.Collection.extend({
  url: "/cabinet/api/v1/product/?shop_id=" + shop_pk,
  model: ProductModel,
  parse: function(data) {
    return data.objects;
  }
});