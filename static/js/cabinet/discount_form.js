/**
 * Created by zhuravlev on 30.06.14.
 */


$(function(){
    var ProductModel = Backbone.Model.extend({
        url: function(){
          if (discount_pk){
            return "/cabinet/api/v1/product/?discount_pk=" + discount_pk + "&shop_id=" + shop_pk;
          }else{
            return "/cabinet/api/v1/product/?shop_id=" + shop_pk;
          }
        },
        label: function(){
            return this.get("name");
        }
    });

    var CategoryModel = Backbone.Model.extend({
        url: function(){
          if (discount_pk){
            return "/cabinet/api/v1/category/?discount_pk=" + discount_pk + "&shop_id=" + shop_pk;
          }else{
            return "/cabinet/api/v1/category/?shop_id=" + shop_pk;
          }
        },
        label: function(){
            var label = this.get("name");
            if (this.get("parent_name")) {
              label += " (" + this.get("parent_name") + ")";
            }
            return label;
        }
    });

    var ProductCollection = Backbone.Collection.extend({
        url: function(){
          if (discount_pk){
            return (
                "/cabinet/api/v1/product/?discount_pk=" +
                discount_pk +
                "&shop_id=" +
                shop_pk
            );
          }else{
            return "/cabinet/api/v1/product/?shop_id=" + shop_pk;
          }
        },
        model: ProductModel,
        parse: function(data){
            return data.objects;
        }
    });

    var CategoryCollection = Backbone.Collection.extend({
        url: function(){
          if (discount_pk){
            return (
                "/cabinet/api/v1/category/?discount_pk=" +
                discount_pk +
                "&shop_id=" +
                shop_pk
            );
          }else{
            return "/cabinet/api/v1/category/?shop_id=" + shop_pk;
          }
        },
        model: CategoryModel,
        parse: function(data){
            return data.objects;
        }
    });

    var ProductCollectionResultItemView = Backbone.View.extend({
        tagName: "div",
        template: _.template($("#product-description-tmpl").html()),

        events: {
            'click .del': 'onClick'
        },
        onClick: function(){
            this.collection.remove(this.model);
            this.remove();
        },
        render: function(){
            if(discount_pk) {
                var price_str = '(' + this.model.get('price') + ' ' + currency_str + ' => ' + this.model.get('price_with_discount') + ' ' + currency_str + ')';
            }else{
                var price_str = '(' + this.model.get('price') + ' '+currency_str+')';
            }
            this.$el.html(this.template({
                href: '',
                price_str: price_str,
                image_src: this.model.get('cover'),
                media_head: this.model.get('name'),
                media_body: this.model.get('desc')
            }));
            return this;
        }
    });

    var ProductCollectionResultView = Backbone.View.extend({
        el: "[data-autocomplete-results=products]",
        initialize: function() {
            this.productCol = this.options.productCol;
            this.render();
            this.listenTo(this.productCol, 'all', this.render);
        },
        render: function(){
            this.$el.html('');
            if (this.productCol.models.length) {
                _.forEach(this.productCol.models, this.addItem, this);
            }
        },
        addItem: function(model){
            var viewHash = {
                model: model,
                collection: this.productCol
            };

            this.$el.append(
                new ProductCollectionResultItemView(viewHash)
                    .render()
                    .$el
            );
        }
    });

    var CategoryCollectionResultItemView = Backbone.View.extend({
        tagName: "tr",
        template: _.template($("#category-description-tmpl").html()),

        events: {
            'click .del': 'onClick'
        },
        onClick: function(){
            this.collection.remove(this.model);
            this.remove();
        },
        render: function(){
            this.$el.html(
                this.template(this.model.attributes)
            );
            return this;
        }
    });

    var CategoryCollectionResultView = Backbone.View.extend({
        el: "[data-autocomplete-results=categories]",
        initialize: function() {
            this.categoryCol = this.options.categoryCol;
            this.render();
            this.listenTo(this.categoryCol, 'all', this.render);
        },
        render: function(){
            this.$el.html('');
            if (this.categoryCol.models.length) {
                _.forEach(this.categoryCol.models, this.addItem, this);
            }
        },
        addItem: function(model){
            var viewHash = {
                model: model,
                collection: this.categoryCol
            };

            this.$el.append(
                new CategoryCollectionResultItemView(viewHash)
                .render()
                .$el
            );
        }
    });

    var SubmitView = Backbone.View.extend({
        el: "form#js-products",
        events: {
            'click input[type=submit]': 'onSubmit',
            'change input[name=application]': 'onChangeAppl'
        },

        initialize: function() {
            this.productCol = this.options.productCol;
            this.categoryCol = this.options.categoryCol;
        },

        render: function() {
          this.onChangeAppl();
        },

        onChangeAppl: function() {
          var $checked = this.$('[name=application]:checked'),
              $productsId = this.$('[name=products_id]')
              $categoriesId = this.$('[name=categories_id]');

          $productsId.val('');
          $categoriesId.val('');

          this.$('[data-show=cart]').hide();
          this.$('[data-show=products]').hide();
          this.$('[data-show=categories]').hide();

          this.$('[data-show=' + $checked.val() + ']').show();
          // clear autocomplete inputs
          this.$('[data-autocomplete]').val();

          // clear autocomplete results
          this.productCol.reset();
          this.categoryCol.reset();
        },

        onSubmit: function(event){
            event.preventDefault();
            var $inpProducts = this.$('[name=products_id]'),
                $inpCategories = this.$('[name=categories_id]'),
                prodVal,
                catVal;

            prodVal = _.map(this.productCol.models, function(p_model){
                return p_model.get('id');
            }).join(',');

            catVal = _.map(this.categoryCol.models, function(c_model){
                return c_model.get('id');
            }).join(',');

            $inpProducts.val(prodVal);
            $inpCategories.val(catVal);

            this.$el.submit();
            return this;
        }
    });

    var productResultCol = new ProductCollection();
    var categoryResultCol = new CategoryCollection();

    var product_ids = $('input[name=products_id]').val();
    if (product_ids.length !== 0) {
        if (discount_pk) {
            var url =  (
                "/cabinet/api/v1/product/?format=json" +
                "&discount_pk=" + discount_pk +
                "&shop_id=" + shop_pk +
                '&id__in=' + product_ids
            );
        } else {
            var url =  (
              "/cabinet/api/v1/product/?format=json" +
              "shop_id=" + shop_pk +
              '&id__in=' + product_ids
            );
        }
        productResultCol.fetch({url: url});
    }

    var category_ids = $('input[name=categories_id]').val();
    if (category_ids.length !== 0) {
        if (discount_pk) {
            var url =  (
                "/cabinet/api/v1/category/?format=json" +
                "&discount_pk=" + discount_pk +
                "&shop_id=" + shop_pk +
                '&id__in=' + category_ids
            );
        } else {
            var url =  (
              "/cabinet/api/v1/category/?format=json" +
              "shop_id=" + shop_pk +
              '&id__in=' + category_ids
            );
        }
        categoryResultCol.fetch({url: url});
    }

    new AutocompleteView({
        el: $('[data-autocomplete-tips=products]'),
        srcCollection: new ProductCollection(),
        resultCollection: productResultCol ,
        listenInput: "[data-autocomplete=products]",
        queryParameter: "query"
    }).render();

    new AutocompleteView({
        el: $('[data-autocomplete-tips=categories]'),
        srcCollection: new CategoryCollection(),
        resultCollection: categoryResultCol,
        listenInput: "[data-autocomplete=categories]",
        queryParameter: "query"
    }).render();

    new ProductCollectionResultView({
        productCol:productResultCol
    }).render();

    new CategoryCollectionResultView({
        categoryCol: categoryResultCol
    }).render();

    new SubmitView({
        productCol: productResultCol,
        categoryCol: categoryResultCol
    }).render();

    new Backbone.View.extend({
      el: ''
    });

    function showCouponsIfNeeded() {
        var $coupons = $('[data-show=coupon_discount]'),
            $switch = $('input[name=coupon_discount]');
        if ($switch.is(':checked')) { return $coupons.show(); }
        $coupons.hide();
    }

    function showAdditionalIfNeeded() {
      var $switch = $('input[name=show_additional]')
          $additional = $('[data-show=additional-options-enabled]');

      if ($switch.is(':checked')) { return $additional.show(); }
      $additional.hide();
    }

    $('input[name=coupon_discount]')
      .on('switchChange.bootstrapSwitch', showCouponsIfNeeded);

    $('input[name=show_additional]')
      .on('switchChange.bootstrapSwitch', showAdditionalIfNeeded);

    showCouponsIfNeeded();
    showAdditionalIfNeeded();

});
