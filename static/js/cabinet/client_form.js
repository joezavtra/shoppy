$(function(){
    var legal_fields = ['l_org_name', 'l_inn', 'l_kpp', 'l_okved', 'l_okpo',
        'l_ogrn', 'l_bank', 'l_bik', 'l_corr_account', 'l_current_account',
        'l_legal_addr', 'l_fact_addr', 'l_tel', 'l_email', 'l_fio_gendir',
        'l_permission', 'l_doverennost_number', 'l_doverennost_date'
    ];
    var legal_fields_str = _.map(legal_fields, function(item){
        return '#id_' + item;
    });
    function onTypeChange(){
        var c_type = $('#id_client_type');
        var selected = c_type.find(':selected');
        if (selected.val() == '1' || selected.val() == '' || selected.val() == undefined){
            _.each(legal_fields_str, function(item){
                $(item).parent().hide();
            });
        }else{
            _.each(legal_fields_str, function(item){
                $(item).parent().show();
                onPermissionChange();
            });
        }
    }


    function onPermissionChange(){
        var permission = $('#id_l_permission');
        var selected = permission.find(':selected');
        var doverennost_fields = ['#id_l_doverennost_number', '#id_l_doverennost_date'];
        if(selected.val() == '1'){
            _.each(doverennost_fields, function(item){
                $(item).parent().show();
            });
        }else{
            _.each(doverennost_fields, function(item){
                $(item).parent().hide();
            });
        }
    }




    $('#id_client_type').change(onTypeChange);
    $('#id_l_permission').change(onPermissionChange);

    onPermissionChange();
    onTypeChange();
});