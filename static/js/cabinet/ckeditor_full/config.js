/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
  /* Путь до css файла шрифта */
  config.contentsCss = '/static/css/cabinet/ckeditor_fonts.css';

  //the next line add the new font to the combobox in CKEditor
  config.font_names = 'Gotham Pro Bold/gotham_pro_bold;' + config.font_names;
  config.font_names = 'Gotham Pro Medium/gotham_pro_medium;' + config.font_names;
  config.font_names = 'Solomon Sans/solomon_sans;' + config.font_names;
};