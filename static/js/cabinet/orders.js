/**
 * Created by zhuravlev on 02.06.14.
 */
$( function () {
  /* TODO: Нигде не используется, возможно выпилить?! */
  $('.sb-order-delete').click(function(index, element){
    element = $(this);
    var order_id = element.attr('data-id');
    var delete_url = '/cabinet/api/v1/order/' + order_id;
    $.ajax({
      url: delete_url,
      type: 'DELETE',
      success: function(data){
        console.log(data);
        $('#order-' + order_id).remove();
      }
    });
  });

  /* Изменить статус заказа */
  $('.js-change-order-status').on('click', function () {
    var $el = $(this),
        orderModel = new Backbone.Model({
          id: $el.data('order-id'),
          status_val: $el.data('status-val'),
          status_text: $el.data('status-text')
        }),
        orderModalView;

    orderModalView = new CabinetApp.Common.Views.OrderStatusModal({ model: orderModel });

    orderModalView.on('show', function () {
      orderModalView.$el.modal('show');
    });

    CabinetApp.orderModalReg.show( orderModalView );
  });
});