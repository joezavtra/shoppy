$(function () {
  var REG = /landing\/(\d+)/,
      landingId = window.location.pathname.match(REG)[1],
      submitToServer;

  submitToServer = _.throttle(function (data) {
    $.ajax({
      url: '/cabinet/api/v1/landing/' + landingId + '/section-move/',
      dataType: 'json',
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: JSON.stringify(data),
      type: 'PATCH'
    });
  }, 500);

  $('.sections').sortable({
    'stop': function () {
      var $this = $(this),
          sections;

      sections = _.map($this.children(), function (section) {
        var $section = $(section);
        return {
          'id': $section.data('section-pk'),
          'type': $section.data('section-type')
        };
      });

      submitToServer(sections);
    }
  });
});
