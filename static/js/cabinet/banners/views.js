var BannerItemView = Marionette.ItemView.extend({
  tagName: 'tr',

  ui: {
    'productInput' : '.js-product-auto-complete',
    'fileImg'      : '.js-banner-item__img',
    'removeBtn'    : '.js-remove-item'
  },

  events: {
    'typeahead:selected @ui.productInput' : 'onProductSelect',
    'click              @ui.removeBtn'    : 'removeItem'
  },

  template: '#banner-item-template',

  removeItem: function (e) {
    e.preventDefault();

    var that = this;

    this.$el.hide( 500, function () {
      that.destroy();
    });
  },

  onDestroy: function () {
    this.ui.productInput.typeahead('destroy');
  },

  onRender: function () {
    this.ui.fileImg.styler();
    this.ui.productInput.typeahead(typeahead_settings);
  },

  onProductSelect: function (e, product) {
    this.$el.find('.product-id').val(product.id);
  }
});