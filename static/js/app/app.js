var SBApp = new Marionette.Application();

SBApp.productListType = 'grid';
SBApp.productsOnPage = 9;

/* Хранит конфигурации магазина */
SBApp.config = {
  /* Формат номера телефона в магазине */
  PHONE_MASK: '8 999 999-99-99'
};

/* Кастомное чтение полей формы */
/* Из поля телефон выпиливаем маску из - и пробелов */
Backbone.Syphon.InputReaders.register('text', function ($el) {
  var inputValue = $el.val();

  return ($el.data('type') === 'phone') ? inputValue.replace(/[\-\s]/g, '') : inputValue;
});

SBApp.isLayout = function (layout) {
  return SBApp.layout && SBApp.layout === layout;
};

SBApp.hasPromocode = function () {
  return SBApp.promocode_enabled || false;
}

/* state = app url, возвращает текущий статус, либо меняет значение, если есть вход. параметр  */
SBApp.state = function (state) {
  SBApp.currentState = ( typeof state === 'undefined' ) ? SBApp.currentState : state;
  return SBApp.currentState;
};

/* Получить символ валюту по коду */
SBApp.getCurrencyChar = function (currency_code) {
  var symbol = '';

  switch (currency_code) {
    case 'RUB':
      symbol = '<span class="ruble">a</span>';
      break;

    case 'USD':
      symbol = '$';
      break;

    case 'EUR':
      symbol = '€';
      break;

    default:
      symbol = false;
      break;
  }

  return symbol;
};

/* Получить валюту текущего магазина */
SBApp.getCurrency = function () {
  var currency_obj = SBApp.shop.currency,
      currency_code = currency_obj['code'],
      currency;

  switch (currency_code) {
    case 'USD':
      currency = '$';
      break;

    case 'EUR':
      currency = '€';
      break;

    default:
      currency = currency_obj['text'];
      break;
  }

  return currency;
};

SBApp.getCurrencyCode = function () {
  var currencyObj = SBApp.shop.currency;

  return currencyObj['code'];
}

SBApp.getPriceWithCurrency = function (price) {
  var currency_obj = SBApp.shop.currency,
      priceAndCurrency = SBApp.getNumberWithSpaces(price),
      currency_summ = '';

    if( currency_obj['code'] === 'USD' ) {
      currency_summ = [ SBApp.getCurrency(), priceAndCurrency];
    }
    else {
      currency_summ = [ priceAndCurrency, '&nbsp;', SBApp.getCurrency() ];
    }

  return currency_summ.join('');
};

/* Основные параметры продукта */
SBApp.getProductProps = function (product) {
  return {
    id: product.get('id'),
    name: product.get('name'),
    price: product.get('price')
  }
};

/* Основные параметры заказа */
SBApp.getOrderProps = function (model) {
  var cart = model.get('cart'),
      orderId = model.get('id'),
      orderProps,
      product = {},
      products = [];

  _.each(cart['items'], function (productItem) {
    product = {
      id: productItem['product_id'],
      name: productItem['product']['name'],
      price: productItem['price_with_discount']['price'],
      quantity: productItem['count']
    };

    products.push(product);
  });

  orderProps = {
    orderId: orderId,
    total: cart['total_sum'],
    currency: SBApp.getCurrencyCode(),
    products: products
  }

  return orderProps;
};

SBApp.$appWrapper = $('#app-wrapper');

SBApp.twoColumnsLayout = function () {
  var $wrapper = SBApp.$appWrapper.find('#SBwrapper');

  $wrapper.empty();
  SBApp.$appWrapper.addClass('two-column-layout');

  $wrapper.empty().append('<div class="row">\
    <div class="span3 SBnavigate">\
      <div id="categories" class="categories"></div>\
    </div>\
    <div class="span9">\
      <div class="SBcontent" id="content"></div>\
      <div class="sb-social" id="sb-social"></div>\
    </div>\
  </div>');
};

SBApp.oneColumnLayout = function () {
  var $wrapper = SBApp.$appWrapper.find('#SBwrapper');
  var $navbar = SBApp.$appWrapper.find('#sb-header');
  var windowWidth = $(window).width();

  SBApp.$appWrapper.addClass('one-column');

  /* Шаблон блока категорий */
  $navbar.find('.navbar-inner').append('<div class="SBnavigate">\
    <div id="categories" class="categories" />\
  </div>');

  /* Шаблон блока продуктов */
  $wrapper.append('<div class="SBcontent" id="content" />');

  /* Место для соц. виджетов */
  $('#app-wrapper').append('<div class="sb-social" id="sb-social" />');

  SBApp.addRegions({
    socialReg: '#sb-social'
  });
};

//=======================
//   Подготовка
//=======================

/* Кастомный события приложения */

/* @param childModel{Backbone Model} Модель дочернего элемента
 * @param modalSettings{Object} - Параметр настроек модального окна
 * modal_type - в modalSettings тип модального окна
 * Через модель передаются настройки модального окна
 * Через коллекцию параметры дочерних эл-ов окна
 */
  SBApp.on('modal:show', function (childModel, modalSettings) {
    var childCollection = new Backbone.Collection(childModel);
    var modalModel = new Backbone.Model(modalSettings);
    var modalView = new SBApp.ModalView({ model: modalModel, collection: childCollection });

    modalView.on('childview:modal:close', function () {
      modalView.$el.modal('hide');
    });

    SBApp.modalRegion.show( modalView );
});

SBApp.on('modal:show:success', function (options) {
  var title = options.title || '',
      text = options.text || '',
      $modal = $('#modal');

  if (title) {
    $modal.find('.modal-header__title').html(title);
  }

  if (text) {
    $modal.find('.modal-body').html('<div>' + text + '</div>');
  }
});

/* Подсветить активную категорию и показать подкатегории, если есть */
SBApp.on('active_category_item', function (categoryId) {
  var $categories = $('#categories');
  var $activeCategory = $categories.find('.sb-categories__item-' + categoryId);

  $categories.find('li ul.nav').hide();
  $categories.find('.sb-categories__item.active').removeClass('active');
  $activeCategory.addClass('active');

  if(SBApp.layout !== 'one-column') {
    $activeCategory.parents('ul.nav').show();
    $activeCategory.children('ul.nav').show();
  }
  else{
    $activeCategory.parents('.categories_level_1').addClass('active');
  }

  var activePage = SBApp['has_landing'] ? 'catalog' : '';
  SBApp.navView.state.set('activePage', activePage);
});

/* Событие происходит, когда пользователь переходит с одной страницы на другую */
SBApp.on('page_click_increment', function () {
  var customer = SBApp.userModel,
      prop = 'page_click',
      page_click = customer.get(prop);

  customer.set(prop, page_click + 1);
});

/* Удаляет лендинг view из региона */
SBApp.on('landing:clear', function () {
  SBApp.wrapperRegion.reset();
});

/* Метод перерисовывает приложение если находились на странице лендинга и перешли в магазин */
SBApp.on('app:rebuild', function () {
  if (SBApp['has_landing'] && SBApp.landingShowed) {
    SBApp.landingShowed = false;
    SBApp.trigger('landing:clear');
    SBApp.prepare();
    SBApp.afterLandingInit();
  }
});

/* Событие скроллит в нужное место. Учитываются разое приложение
 * Принимает hash
 *
 * offset - положения эл-та, до кот. нужно скроллить
 * duration - продолжительность анимации
 *
  * */
SBApp.on('scroll:to', function (options) {
  var offsetTop = options.offset || 0,
      duration = options.duration || 2000,
      vkOffsetTop = 70;

  /* ВКонтакте */
  if (SBApp.Platform.isVK() ) {
    window.VK.callMethod('scrollWindow', offsetTop + vkOffsetTop, duration);
  }

  /* Facebook */
  else if (SBApp.Platform.isFacebook() ) {
    window.FB.Canvas.scrollTo(0, offsetTop);
  }

  /* Для домена */
  else {
    $('html, body').animate({
      scrollTop: offsetTop
    }, duration);
  }
});

/* Возвращает основную информацию о магазине
 * @return {Object} */
SBApp.analyticsProps = function () {
  return {
    shopId: SBApp.shop.id,
    shopAppType: SBApp.shop.app_type,
    shopName: SBApp.shop.name,
    shopDomain: SBApp.shop.domain,
    locationHash: window.location.hash
  }
}

/* Analytics SegmentIO */

/* Событие трекает действия пользователя */
/* @param eventType{String} - Тип события, например 'Product Added'
 * @param props{Object} - св-ва, кот. необх. отправить
 * */
SBApp.on('segment:io:track', function (eventType, props) {
  var props = props || {};

  window.analytics.track(eventType, props, SBApp.analyticsProps());
});


/* TODO: Подумать над инициализацией приложения
   Реализовать View приложения, которая хранит в себе ссылки на разные лог. блоки
 */
SBApp.prepare = function() {
  // Инициализац. регионов
  SBApp.modalRegion = new Marionette.Region({ el: '#sb-modal-region' });
  SBApp.topBannerReg = new Marionette.Region({ el: '#sb-top-banner-reg' });
  SBApp.bannerS1Region = new Marionette.Region({ el: '#sb-banner-slot-1' });

  if (SBApp.layout === 'two-columns') {
    SBApp.twoColumnsLayout();
  }
  else {
    SBApp.oneColumnLayout();
  }

  SBApp.trigger('banner:load', 'slot1');

  if (!SBApp.modalRegion){
    SBApp.modalRegion = new Marionette.Region({ el: '#sb-modal-region' });
  }
  if (!SBApp.topBannerReg) {
    SBApp.topBannerReg = new Marionette.Region({ el: '#sb-top-banner-reg' });
  }

  SBApp.addRegions({
    contentRegion: '.SBcontent'
  });
};

/* Стартовая инициализация приложения */
SBApp.init = function () {
  var tzo = -(new Date().getTimezoneOffset()/60);
  document.cookie = 'SBTZO='+tzo+'; path=/'

  SBApp.cart = new SBApp.Cart({ id: SBApp.userModel.get('cart_id') });
  SBApp.cartView = new SBApp.CartView();
  SBApp.navView = new SBApp.NavView();

  /* Изменить cart_id пользователя, если изменится id корзины */
  SBApp.userModel.listenTo(SBApp.cart, 'change:id', SBApp.userModel.changeCartId);
  SBApp.hook.afterInit();
};

/* TODO: Сделать через марионетовское приложение
   Временное решение. Нужно было быстро добавить все св-ва системы, чтобы работало приложение
 * Обходит все настройки магазина и добавляет их к глобальному объекту
 *
 *
 * */
SBApp.addOptions = function (options) {
  for(var option in options) {
    if( options.hasOwnProperty(option) ) {
      SBApp[option] = options[option];
    }
  }
};

/* Выполняется, если перешли со страницы landing на приложение */
SBApp.afterLandingInit = function () {
  SBApp.cartView = new SBApp.CartView();

  if ( !SBApp.Platform.isFacebook() ) {
    /* Чтобы блок категорий не перерисовался в состоянии, кот. был до страницы лэндинга */
    SBApp.activeCategory = undefined;
    /* Показать блок категорий */
    SBApp.CategoryApp.Controller.showCategories();
  }

  /* В 1 колоночном показать продукты */
  if (!SBApp.isLayout('two-columns')) {
    SBApp.router.changeURL('category/'.concat(SBApp.activeCategory), true);
  }

  /* Виджет сообщество */
  if ( SBApp.Platform.isVK() && SBApp.hasGroupId() ) {
    SBApp.execute('vk:widget:group', 'sb-social', {}, SBApp.getGroupId() );
  }
}

//$(function(){

 // $(window).on('mousewheel touchstart', function(e){
 //   e.preventDefault();
 // });
//});

SBApp.addRegions({
  wrapperRegion: '#SBwrapper'
});

SBApp.addOptions(options);

/* Верхний баннер с подпиской или с кнопкой "Пригласить друга" */
SBApp.addInitializer( function () {
  if( SBApp.shop.show_subscribe_form ) {
    SBApp.TopBanner.Controller.showSubscribeView();
  }
});
