SBApp.module('FBManager', function (FBManager, SBApp, Backbone, Marionette, $, _){
  /* Комманда меняет размеры iframe */
  SBApp.commands.setHandler('fb:canvas:set:size', function (width, height) {
    var FB = window.FB;

    FB.Canvas.setSize({ width: width, height: height });
  });

  /* Parses and renders XFBML markup in a document on the fly */
  /*
  * @param element {Dom element},
  * @param func {Func} вызовется по окончанию отрисовки виджета
  * */
  SBApp.commands.setHandler('fb:parse', function (element, func) {
    var FB = window.FB,
        func = func || function () {};

    FB.XFBML.parse( element, func);
  });
});