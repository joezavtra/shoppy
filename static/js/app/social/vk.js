var Social = {};


Social.VK = (function (VK, SBApp, $, _) {
  /* Через API контакта запрашивает права приложения. Ответ приходит в битовой маске
   * Фун-ция возвращает либо битовую маску настроек либо объект ошибки из ВК
  /* @param {Integer} user_id */
  var fetchingAppPermissions = function (userId) {
    var defered = $.Deferred();

    VK.api('account.getAppPermissions', { user_id: userId }, function (resp) {
      defered.resolve(resp);
    });

    return defered.promise();
  }

  /* @param {Number} - битовая маска нужной настройки
   * @param {Number} - битовая маска настроек приложения */
  var hasPermission = function (bitMask, permsBitMask) {
    return bitMask & permsBitMask;
  };

  /* @param {Number} - битовая маска нужных настроек */
  var showSettingsBox = function (bitMask) {
    VK.callMethod('showSettingsBox', bitMask);
  };

  var API = {
    removeCallback: VK.removeCallback,
    addCallback: VK.addCallback,
    callMethod: VK.callMethod,
    fetchingAppPermissions: fetchingAppPermissions,
    hasPermission: hasPermission,
    showSettingsBox: showSettingsBox
  };

  return API;
})(VK, SBApp, jQuery, _);

SBApp.hasGroupId = function () {
  var shop = SBApp.shop,
      param = 'vk_group';

  return (shop && shop[param] && shop[param] !== 'null') ? true : false;
}

/* Получить id группы в ВК */
SBApp.getGroupId = function () {
  return SBApp.shop.vk_group;
}

/* Комманда инициализ. создание виджета "Сообщество"
* element_id {String} - обязательный, id элемента, который будет являться контейнером для блока сообщества.
  options {Object} - необязательный
  {
     width: задает ширину блока в пикселах (целое число > 120). При значении "auto" подстраивается под ширину блока.
     height: задает высоту блока в пикселах (целое число от 200 до 1200).
     mode: 0 - отображать участников сообщества. 1 - отображать только название сообщества. 2 - отображать стену сообщества.
     wide: 0 - стандартный режим. 1 - при отображении стены этот режим меняет отображение списка постов, добавляя "мне нравится" и фотографию сообщества.
     color1 — цвет фона виджета в формате RRGGBB.
     color2 — цвет текста в формате RRGGBB.
     color3 — цвет кнопок в формате RRGGBB.
   }
 group_id - id группы ВК.
*/

SBApp.commands.setHandler("vk:widget:group", function (element_id, options, group_id) {
  if (!window.VK) {
    return;
  }

  var API = window.VK,
      width = options.width || 'auto',
      height = options.height || '121',
      wide = options.wide || 0,
      mode = options.mode || 0,
      color1 = options.color1 || 'FFFFFF',
      color2 = options.color2 || '2B587A',
      color3 = options.color3 || '5B7FA6';

  API.Widgets.Group(element_id, { width: width, height: height, wide: wide, mode: mode, color1: color1, color2: color2, color3: color3 }, group_id);
});

/* Открывает окно для приглашения друзей пользователя в приложение. */
SBApp.commands.setHandler('vk:show:invite:box', function () {
  window.VK.callMethod('showInviteBox');
});