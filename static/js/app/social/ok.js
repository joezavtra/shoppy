var Social = {};

Social.OK = (function (FAPI, $) {
  var API = FAPI;

      /* возвращает объект, его необходимо вызвать перед инициализацией для получения параметров инициализации */
      var requestParams = API.Util.getRequestParameters();

  /* Получить параметры запроса */
  var _getParam = function (param) {
    return requestParams[param];
  };

  /* Получить данные о тек. юзере */
  /*
  * {String} fields: "name,gender,age,pic50x50" (Полный перечень параметров: http://apiok.ru/wiki/pages/viewpage.action?pageId=46137373#API%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F%28%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9%29-users.getCurrentUser)
  * {Function} callback
  */
  var _currentUser = function (fields, callback) {
    API.Client.call({"method":"users.getCurrentUser", "fields": fields}, callback);
  };

  /* Создает кнопку "Класс"
   * {String} id     - dom элемента
   * {String} url    - урла шаринга
   * {String} params - параметры вида кнопки пример: "{width:145,height:30,st:'oval',sz:20,ck:1}"
   * */
  var _shareButton = function (id, url, params) {
    OK.CONNECT.insertShareWidget(id, url, params);
  };

  /* {String} fields как в _currentUser
  *  {String} uids - id пользователей, разделенный запятыми */
  var _getUsers = function (fields, uids, callback) {
    API.Client.call({"method": "users.getInfo", "fields": fields, "uids": uids}, callback);
  };

  return {
    API: API,
    getParam: _getParam,
    currentUser: _currentUser,
    getUsers: _getUsers,
    shareButton: _shareButton
  };

})(FAPI, jQuery);

/* Инициализация приложения в Одноклассниках, 4 параметра на вход:
  * {String}   serverUrl
  * {String}   apiConnectionName
  * {Function} success
  * {Function} error
  * */
Social.OK.API.init(Social.OK.getParam('api_server'), Social.OK.getParam('apiconnection'),
  function () {
    /* Получить данные о текущем посетителе */
    Social.OK.currentUser('name,gender,age,pic50x50', function (method, resp, data) {

      if(resp) {
        SBApp.userModel.set({
          name:  resp['name'],
          photo: resp['pic50x50']
        });
      }
      else {
        console.error('Ошибка, это все она: ');
        console.log(data);
      }
    });
  },
  function () {}
);

