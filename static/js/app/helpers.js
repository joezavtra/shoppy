/* 3000 -> "3&nbsp;000"
  @param {Number}
*/
SBApp.spacer = '&nbsp;';
SBApp.getNumberWithSpaces = function (number, spacer) {
  var s = spacer || SBApp.spacer;

  if( !number ) {
    return 0;
  }
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, s);
};


SBApp.orderTextStatus = function (status) {
  var text = '';

  switch(status) {
    case 1:
      text = 'сформирован';
      break;
    case 2:
      text = 'оплачен';
      break;
    case 3:
      text = 'не оплачен';
      break;
  }
  return text;
}

/*
  Возвращает дату строкой напр.:"12 марта 2013"
  @param {String} date
  @param {String} monthsType (short) - сокращенная или полное имя месяца
  @param {Boolean} time (short) - показывать время
*/
SBApp.getDateAsString = function (date, monthsType, time) {
  var dateObj = new Date(date);
  var months = (monthsType === 'short') ? 'янв.,фев.,мар.,апр.,мая,июн.,июл.,авг.,сент.,окт.,ноя.,дек.'.split(',') : 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря'.split(',');
  var day = dateObj.getUTCDate();
  var monthIndex = dateObj.getUTCMonth();
  var year = dateObj.getUTCFullYear();
  var hours = dateObj.getUTCHours();
  var minutes = dateObj.getUTCMinutes();
  var today = new Date();
  var timeString = (time === true) ? ' в ' + hours + ':' + ((minutes < 10) ? '0' + minutes : minutes) : '';
  year = year.toString().substr(2);

  return day + ' ' + months[monthIndex] + ' ' + year + timeString;
};

/* Число больше 9, возвращает строку с 0 впереди, иначе  */
_.mixin({
  zeroFirst: function (digit) {
    var digit = digit < 10 ? '0' + digit : digit;
    return digit.toString();
  },

  capitalize : function(string) {
    return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
  }
});

SBApp.getDate = function (dateStr) {
  var dateObj = new Date(dateStr),
      month = _.zeroFirst( dateObj.getUTCMonth() + 1 ),
      day= _.zeroFirst( dateObj.getUTCDate() ),
      hours = _.zeroFirst( dateObj.getUTCHours() ),
      minutes = _.zeroFirst( dateObj.getUTCMinutes()),
      seconds = _.zeroFirst( dateObj.getUTCSeconds() );

  return dateObj.getUTCFullYear() + '/'  + month + '/' + day + ' ' + hours + ':' + minutes + ':' + seconds;
}

/* postamat, pvz, express */
SBApp.delivery = function (type) {
  var t;

  switch (type) {
    case 'pvz':
      t = 'ПВЗ';
      break;
    case 'postamat':
      t = 'Постамат';
      break;
    case 'express':
      t = 'Курьер';
      break;
    default :
      t = 'Не указан';
      break;
  }
  return t;
}

/**
 * Возвращает слово с правильным окончанием в зависимости от числа
 *
 * @param {Number} number  Число
 * @param {Object} cases   Варианты слова {nom: 'товар', gen: 'товара', plu: 'товаров'} (nom — Nominativ, именительный падеж; gen — Genetiv, родительный падеж; plu — Plural, множественное число.)
 * @return {String}
 */
SBApp.getWordWithRightEnding = function (number, cases) {
    var word = '';
    number = Math.abs(number);
    cases = cases || {nom: 'товар', gen: 'товара', plu: 'товаров'};

    if (number.toString().indexOf('.') > -1) {
      word = cases.gen;
    }
    else {
      word = (
        number % 10 == 1 && number % 100 != 11
          ? cases.nom
          : number % 10 >= 2 && number % 10 <= 4 && (number % 100 < 10 || number % 100 >= 20)
            ? cases.gen
            : cases.plu
      );
    }

    return word;
};

SBApp.Popover = {
  timeoutId: 0,

  successMessage: function () {
    var amount = SBApp.cart.totalAmount();

    return 'Товаров: <b>' + SBApp.cart.getItemsCount() + '&nbsp;шт.</b><br />Сумма: <b>' + SBApp.getPriceWithCurrency(amount) + '</b>';
  },

  getBaseSettings: function () {
    var self = this;

    return {
      placement: 'bottom',
      content: self.successMessage(),
      html: true,
      trigger: 'manual',
      container: 'body'
    };
  },

  $container: $('#cart-badge'),

  showOnCartAdd: function(){
    var self = this;
    var settings = {
      title: 'Товар успешно добавлен',
      placement: 'bottom',
      content: self.successMessage(),
      html: true,
      trigger: 'manual',
      container: 'body'
    };

    if(this.isVisible()){
      this.destroy();
    }

    this.show(settings);

    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(function () {
      self.destroy();
    }, 2000);
  },

  isVisible: function () {
    return this.$container.next('.popover:visible');
  },

  destroy: function () {
    this.$container.popover('destroy');
  },

  show: function (settings) {
    var s = settings || this.getBaseSettings();

    this.$container.popover(s);
    this.$container.popover('show');
  },

  hide: function () {
    this.$container.popover('hide');
  }
};

SBApp.Platform = {
  isVK: function () {
    return SBApp["shop"]["platform"] === 'vk';
  },

  isFacebook: function () {
    return SBApp["shop"]["platform"] === 'fb';
  },

  isOK: function () {
    return SBApp["shop"]["platform"] === 'ok';
  },

  isDomain: function () {
    return !this.isVK() && !this.isFacebook() && !this.isOK();
  },

  current: function () {
    return SBApp["shop"]["platform"];
  },

  getType: function () {
    return SBApp["shop"]["app_type"];
  },

  userParam: function () {
    var platform = this.current(),
        param = '';

    if( !_.isEmpty(platform) ) {
      param = platform.concat('_id');
    }

    return param;
  }
};

SBApp.SocialWorker = {
  cacheProfiles: []
};