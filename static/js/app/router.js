SBApp.Router = Backbone.Router.extend({
  routes: function () {
    /*
      VK routes осуществляется через pageSocial
      FB routes нет
      Сайт через routes
    */
    var routes = {};

    if (!SBApp.Platform.isVK()) {
      routes = {
        ''                        : 'startPage',
        'home'                    : 'home',
        'news'                    : 'news',
        'cart'                    : 'cart',
        'page/:alias'             : 'page',
        'category/:id'            : 'category',
        'product/:id'             : 'product',
        'search/:query/results'   : 'searchResults',
        'order/:orderId/success'  : 'orderSuccess',
        'order/:orderId/delivery' : 'orderDelivery',
        'orders'                  : 'orderList',
        'orders/:id'              : 'orderList',
        'landing'                 : 'landing',
        '*page'                   : 'pageNotFound'
      };
    }

    return routes;
  },

  pageNotFound: function () {
    SBApp.contentRegion.show( new SBApp.NotFoundView() );
  },

  startPage: function () {
    var page = ( SBApp['has_landing'] ) ? 'landing' : 'home';

    SBApp.router.changeURL(page, true);
  },

  category: function (categoryId) {
    SBApp.trigger('app:rebuild');
    SBApp.state('category');
    var $content = $('#content');

    SBApp.activeCategory = categoryId;

    if ($content.find('#products').length === 0) {
      $content.empty();
    }

    if (SBApp.productsBlock) {
      SBApp.productsBlock.remove();
    }

    SBApp.productsBlock = new SBApp.ProductListView();

    SBApp.trigger('active_category_item', categoryId);
    SBApp.trigger('page_click_increment');

    window.analytics.page(
      'category',
      categoryId,
      SBApp.analyticsProps()
    );
  },

  landing: function () {
    if (!SBApp['has_landing']) {
      this.changeURL('home', true);
      return;
    }

    SBApp.state('landing');
    SBApp.landingShowed = true;
    SBApp.LP.Show.Controller.showLandingItems();


    window.analytics.page('landing', SBApp.analyticsProps());
  },

  product: function (productId) {
    SBApp.trigger('app:rebuild');

    SBApp.activeProduct = productId;

    var fetchingProduct = SBApp.request('product:entity', productId);

    $.when( fetchingProduct).done( function (product) {
      if( product === undefined ) {
        SBApp.contentRegion.show( new SBApp.NotFoundView({ text: 'Товар не найден' }) );
      }

      else {
        SBApp.activeCategory = product.getCategoryId();
        SBApp.trigger('active_category_item', SBApp.activeCategory);

        if (SBApp.productsBlock) {
          SBApp.productsBlock.remove();
        }

        SBApp.productsBlock = new SBApp.ProductFullView({ model: product });

        window.analytics.page(
          'product',
          productId + ': ' + product.get('name'),
          SBApp.analyticsProps()
        )
      }
    });

    SBApp.state('product');
    SBApp.trigger('page_click_increment');
  },

  home: function () {
    SBApp.state('home');

    /* Если есть лендинг, появляется  */
    if (SBApp['has_landing'] && !SBApp.activeCategory) {
      SBApp.activeCategory = SBApp.request('category:id:active');
    }

    if (SBApp.activeCategory) {
      this.changeURL('category/'.concat(SBApp.activeCategory), true);
    } else {
      window.analytics.page('home', SBApp.analyticsProps());
    }
  },

  news: function () {
    SBApp.trigger('app:rebuild');

    var alias = 'news',
        fetchingNews = SBApp.request('news:entities');

    $.when( fetchingNews).done( function (newsList) {
      SBApp.contentRegion.show( new SBApp.NewsView({ collection: newsList }) );
    });

    SBApp.navView.state.set('activePage', alias);

    /* Google Analitycs цель */
    ga && ga('send', {
      'hitType': 'pageview',
      'page': alias
    });

    SBApp.trigger('page_click_increment');
    SBApp.state(alias);
    window.analytics.page(alias, SBApp.analyticsProps());
  },

  cart: function () {
    SBApp.trigger('app:rebuild');

    var $content = $('#content'),
        alias = 'cart';

    $content.empty();
    SBApp.navView.state.set('activePage', alias);

    /* Первая инициализация:  */
    if (!SBApp.cart.has('total_sum') ) {
      SBApp.cartView.listenToOnce(SBApp.cart, 'sync', SBApp.cartView.render);
    } else{
      SBApp.cartView.render();
    }

    /* Google Analitycs цель */
    ga && ga('send', {
      'hitType': 'pageview',
      'page': alias
    });

    SBApp.state(alias);
    SBApp.trigger('page_click_increment');
    window.analytics.page('cart', SBApp.analyticsProps());
  },

  /* Страниц., добавляемые через кабинет */
  page: function (alias) {
    var fetchingPage = SBApp.request('page:entity', alias);

    SBApp.trigger('app:rebuild');

    /* Запросить содержимое страницы с сервера */
    $.when( fetchingPage ).done( function (page) {

      /* Показать 404 */
      if (page === undefined) {
        SBApp.contentRegion.show( new SBApp.NotFoundView() );
      }

      /* Показать содержим. страницы */
      else {
        SBApp.contentRegion.show( new SBApp.StaticPageView({ model: page }) );
        SBApp.navView.state.set('activePage', alias);

        /* Google Analitycs цель */
        ga && ga('send', {
          'hitType': 'pageview',
          'page': alias
        });

        SBApp.state(alias);

        $('.page a').each(function (key, a) {
          $(a).attr('target', '_blank');
        });
      }
    });

    SBApp.trigger('page_click_increment');
  },

  /* Отрабатывает в ВКонтакте, когда меняется хеш в браузере */
  /* Бага в ВК: Когда остается последняя запись в истории, при нажатии на кнопку "Вперед" урл меняется правильно, но
   onLocationChanged не отрабатывает */
  onLocationChanged: function (page) {
    if ( _.isEmpty(page) ) {
      page = ( SBApp['has_landing'] ) ? 'landing' : 'home';

      if (page === 'landing') {
        SBApp.state('landing');
        SBApp.landingShowed = true;
      }
    }

    /* Google Analitics Переход на раздел корзины */
    if(page === 'cart'){
      ga && ga('send','event','button','click','cart');
    }

    SBApp.router.pageSocial(page);
  },

  /* Результаты поиска */
  searchResults: function (query) {
    SBApp.trigger('app:rebuild');

    var searchQuery = new SBApp.SearchQuery();
    var searchResults = new SBApp.SearchView({ collection: searchQuery });

    searchQuery.start(query);
    SBApp.state('search');
  },

  getCheckoutUrl: function (orderId) {
    var locationObj = document.location,
        site = locationObj["protocol"] + '//' + locationObj["host"],
        url = [site, '/checkout/callback/' + orderId, '/'].join('');

    return url;
  },

  /* /share/?sid=<SHOP_ID>&pid=<PRODUCT_ID>&type=(vk | fb | fb_tab | ok) */
  getShareUrl: function () {
    var domain = [document.location.protocol, '//', document.location.host].join(''),
        shareUrl = [domain + '/share/?sid=' + SBApp.shop.id, 'pid=' + SBApp.activeProduct, 'type=' + SBApp.Platform.getType()].join('&');

    return shareUrl;
  },

  /* Отрисовывает разделы по событию VK - onLocationChanged в зависимости от url
    onLocationChanged происходит при первой инициализации приложения или по нажатию
    кнопок браузера "Назад" и "Вперед". В setLocation параметром true это событие нигде не вызываю.
    Прямые урлы Facebook работают через этот метод
   */
  /* TODO: Оптимизировать методу */
  pageSocial: function (url) {
    var matches = [],
        product = 0,
        pattern;

    /* urls: /catalog, /news, /cart */
    if ( /^(home|catalog|news|cart|landing)$/.test(url) ) {
      this[url]();
    }

    /* url: /category/category_id */
    else if ( /category\/(\d+)/.test(url) ) {
      pattern = /category\/(\d+)/;
      matches = url.match(pattern);
      this.category(matches[1]);
    }

    /* urls: /page/:pageName */
    else if( /page\/([a-zA-Z0-9_]+)/.test(url) ) {
      pattern = /page\/([a-zA-Z0-9_]+)/;
      matches = url.match(pattern);
      this.page(matches[1]);
    }

    /* url продукта: /product/product_id */
    else if (/product\/(\d+)/.test(url)) {
      pattern = /product\/(\d+)/;
      matches = url.match(pattern);
      product = matches[1];

      this.product(product);
    }

    /* urls: /order/success, /order/send, /order/failed */
    else if (/order\/(\d+)\/(success|send|failed|delivery)/.test(url)) {
      pattern = /order\/(\d+)\/(success|send|failed|delivery)/;
      matches = url.match(pattern);
      var orderId = matches[1];
      var status = matches[2];

      switch (status) {
        case 'success':
          this.orderSuccess(orderId);
        break;
        case 'send':
          SBApp.OrderApp.Controller.showOrderSend(orderId);
        break;
        case 'failed':
          SBApp.OrderApp.Controller.showOrderFailed(orderId);
          break;
        case 'delivery':
          this.orderDelivery(orderId);
        break;
      }
    }

    else if (/orders\/(\d+)/.test(url)) {
      pattern = /orders\/(\d+)/;
      matches = url.match(pattern);
      var orderId = matches[1];
      this.orderList(orderId);
    }

    else if (/orders/.test(url)) {
      this.orderList();
    }

    /* url: /search/result */
    else if ( /search\/([\W\w\s]*)\/result/.test(url) ) {
      pattern = /search\/([\W\w\s]*)\/result/;
      matches = url.match(pattern);
      this.searchResults( decodeURIComponent(matches[1]) );
    }

    else {
      this.pageNotFound();
    }
  },

  changeURL: function (url, trigger) {
    var t = trigger || false,
        url = url || 'home';

    ga && ga('send', {
      'hitType': 'pageview',
      'page': url
    });

    /* Vkontakte */
    if (SBApp.Platform.isVK() && typeof VK !== "undefined") {
      VK.callMethod('setLocation', url, t);
    }
    else{
      /* Фейсбук или Одноклассники. Пришли на страницу по ссылке. */
      if( (SBApp.Platform.isFacebook() || SBApp.Platform.isOK()) && !_.isEmpty(SBApp.redirect)){
        var redirect = SBApp.redirect;
        /* Не выполнять обработчики routes, потому как выполняется pageSocial */
        t = false;

        this.pageSocial(redirect);
        SBApp.redirect = "";
      }

      this.navigate(url, { trigger: t });
    }
  },

  /* TODO: Перенести в OrderApp */
  orderDelivery: function (orderId) {
    var order = new SBApp.Order({ id: orderId }),
        orderView = new SBApp.OldOrderView({ model: order });

    order.fetch({
      success: function (orderModel, resp, options) {
        var ticket, cart, orderForm, callbackUrl;

        /* checkout_order_id есть, показываем форму */
        if(SBApp.services.checkout && orderModel.has('checkouts') && orderModel.get('checkouts').length === 0 ){
          callbackUrl = SBApp.router.getCheckoutUrl(orderId);
          ticket = orderModel.get('checkout_ticket');
          cart = orderModel.get('cart');

          if(cart['discount']) {
            cart['discount']['price'] -= cart['total_sum'];
          }

          orderForm = JST['form:order:checkout']({
            orderId: orderModel.get('id'),
            cartItems: cart.items,
            discount: cart['discount'] || 0,
            ticket: ticket,
            callbackUrl: callbackUrl,
            hidden: false
          });

          orderView.$el.html( orderForm );
          orderView.$el.prepend('<h1>Доставка</h1>');
        }
        /* Попали случайно */
        else if(!SBApp.services.checkout) {
          SBApp.router.pageNotFound();
        }
        /* Доставка оформлена */
        else if (orderModel.get('checkouts').length > 0){
          orderView.$el.html('<h2>На этот заказ уже оформлена доставка</h2>');
          var orderInfo = JST['orders:item:checkout:success']({ order: orderModel.get('checkouts')[0], robokassa_url: orderModel.get('robokassa_url'), status: orderModel.get('status') });
          orderView.$el.append( orderInfo );
        }
      }
    });
  },

  /* TODO: Перенести в OrderApp */
  orderSuccess: function (orderId) {
    SBApp.trigger('app:rebuild');

    var order = new SBApp.Order({ id: orderId }),
        orderView = new SBApp.OldOrderView({ model: order });

    order.fetch({
      success: function (orderModel, resp, options) {
        /* checkout_order_id есть, показываем инфорамцию заказа */
        if( orderModel.has('checkouts') && orderModel.get('checkouts').length > 0 ){
          orderView.$el.html('<h2>Заказ успешно оформлен</h2>');
          var orderInfo = JST['orders:item:checkout:success']( {order: orderModel.get('checkouts')[0], robokassa_url: orderModel.get('robokassa_url'), status: orderModel.get('status') });
          orderView.$el.append( orderInfo );
        }
        else{
          orderView.renderSuccess(orderId);
        }
      }
    });
  },

  /* TODO: Перенести в OrderApp */
  orderList: function (orderId) {
    SBApp.trigger('app:rebuild');

    var orderList = new SBApp.OrderList(),
        $content = $('#content'),
        orderListView;

    $content.empty();
    orderListView = new SBApp.OrderListView({ collection: orderList, orderId: orderId });
    $content.append( orderListView.$el );
  }
});
