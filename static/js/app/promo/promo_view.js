SBApp.BannerItemView = Marionette.ItemView.extend({
  className: 'row-fluid sb-banner__item',

  template: _.template('<div></div>'),

  events: {
    'click' : 'showProductProfile'
  },

  showProductProfile: function (e) {
    var bannerType = this.options.parentModel.get('type_code'),
        slot = this.options.parentModel.get('slot'),
        eventName = 'Viewed Product From ' + _(bannerType).capitalize() + ' Banner, ' + _(slot).capitalize(),
        product;

    e.preventDefault();

    /* Для текст. и граф. баннера показывать продукт, если есть объект продукта */
    if (bannerType !== 'carousel' && !this.model.has('product') ) {
      return;
    }

    product = (bannerType !== 'carousel') ? new Backbone.Model( this.model.get('product') ) : this.model;

    SBApp.activeProduct = product.get('id');

    $('#nav-menu').find('.active').removeClass('active');
    /* SegmentIO: Viewed Product */
    SBApp.trigger('segment:io:track', eventName, SBApp.getProductProps( product ) );
    SBApp.router.changeURL('product/'.concat(SBApp.activeProduct), true);
  },

  onRender: function () {
    if ( this.model.get('type_code') !== 'carousel' && this.model.has('product') ) {
      this.$el.css('cursor', 'pointer');
    }
  }
});

//======================
//    Представления View
//======================

/* Текстовое содержимое баннера */
SBApp.BannerItemText = SBApp.BannerItemView.extend({
  template: JST['banner:item:text']
});

/* Графич. содержимое баннера */
SBApp.BannerItemGraphical = SBApp.BannerItemView.extend({
  template: JST['banner:item:graphical']
});

/* Карусельное содержимое баннера */
SBApp.BannerItemCarousel = SBApp.BannerItemView.extend({
   template: JST['banner:item:carousel']
});

/* Основное представление баннера */
SBApp.BannerView = Marionette.CompositeView.extend({
  ui: {
    controlPrev: '#sb-banner__prev',
    controlNext: '#sb-banner__next'
  },

  events: {
    'click @ui.controlPrev' : 'prevItem',
    'click @ui.controlNext' : 'nextItem'
  },

  className: function () {
    var bannerType = this.model.get('type_code'),
        base_css;

    if (bannerType === 'text') {
      base_css = 'sb-banner alert alert-warning sb-banner-text';
    }

    else if(bannerType === 'graphical') {
      base_css = 'sb-banner text-center sb-banner-graph';
    }

    else {
      base_css = 'sb-banner sb-banner-carousel';
    }


    return base_css;
  },

  getChildView: function () {
    var bannerType = this.model.get('type_code'),
        childView;

    switch ( bannerType ) {
      case 'text':
        childView = SBApp.BannerItemText;
        break;

      case 'carousel':
        childView = SBApp.BannerItemCarousel;
        break;

      case 'graphical':
        childView = SBApp.BannerItemGraphical;
        break;

      default:
        childView = SBApp.BannerItemView;
        break;
    }

    return childView;
  },

  childViewOptions : function () {
    return {
      parentModel: this.model
    };
  },

  childViewContainer: '.sb-banner__container',

  template: JST['banner'],

  timeoutId: 0,

  controlsTemp: JST['banner:controls'],

  initialize: function () {
    this.sliderConfig = { controls: false, pager: true, mode: 'fade', auto: true, pause: 3000, speed: 1000 , captions: this.model.get('type_code') === 'graphical'};

    /* для е5 TODO: выпилить и сделать по феншую */
    if(SBApp.shop.id === 4) {
      this.sliderConfig.auto = false;
    }
  },

  nextItem: function () {
    this.$slider.stopAuto();
    this.$slider.goToNextSlide();

    if ( this.doAutoPlay() ) {
      this.startAutoPlay();
    }
  },

  prevItem: function () {
    this.$slider.stopAuto();
    this.$slider.goToPrevSlide();

    if ( this.doAutoPlay() ) {
      this.startAutoPlay();
    }
  },

  doAutoPlay: function () {
    return this.sliderConfig.auto;
  },

  doControls: function () {
    return this.collection.length > 1;
  },

  onRender: function () {
    /* Если слайдов > 1 и тип баннера графич. или карусельный */
    if ( this.model.get('type_code') !== 'text' && this.collection.length > 1) {

      if( this.doControls() ) {
        this.$el.append( this.controlsTemp() );
      }

      this.$slider = this.$el.find('.sb-banner__container').bxSlider( this.sliderConfig );
    }
  },

  startAutoPlay: function () {
    if ( this.timeoutId ) {
      clearTimeout( this.timeoutId );
    }

    this.timeoutId = setTimeout(this.$slider.startAuto(), 3000);
  }
});