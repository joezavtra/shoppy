SBApp.module('Banner', function (Banner, SBApp, Backbone, Marionette, $, _) {
  SBApp.on('banner:load', function (slot) {
    var fetchingBanner = SBApp.request('banner:entity', slot);

    //  1. Получить данные баннера
    $.when( fetchingBanner ).done( function (bannerModel) {
      if ( bannerModel && !bannerModel.isEmpty() ) {
        SBApp.trigger('banner:show', slot, bannerModel);
      }
      else {
        /* TODO: Ошибка на сервере, баннер не приходит */
      }
    });
  });

  // Показать баннер
  SBApp.on('banner:show', function (slot, banner) {
    var bannerItems = new Backbone.Collection( banner.get('data')),
        bannerView;

    // Не показываем баннер, если он текстовый сообщение пустое
    if ( banner.is('text') &&  _.isEmpty( $.trim( banner.get('data').text ) )) {
      return;
    }

    bannerView = new SBApp.BannerView({ model: banner, collection: bannerItems });

    if( slot === 'slot1') {
      SBApp.bannerS1Region.show( bannerView );
    }

    if( slot === 'slot2') {
      SBApp.bannerS2Region = new Marionette.Region({ el: '#sb-banner-slot-2' });
      SBApp.bannerS2Region.show( bannerView );
    }

    if( slot === 'slot3') {
      SBApp.bannerS3Region = new Marionette.Region({ el: '#sb-banner-slot-3' });

      SBApp.bannerS3Region.show( bannerView );
    }
  });
});