/* Представления для модуля категорий */
SBApp.CategoryListView = Backbone.View.extend({
  container: '#categories',

  tagName: 'ul',

  className: function () {
    var baseClasses = 'nav sb-categories';

    if ( SBApp.isLayout('two-columns') ) {
      baseClasses += ' nav-list nav-stacked';
    }

    return baseClasses;
  },

  initialize: function (options) {
    if (options && options.children) {
      this.collection = new SBApp.Entities.CategoryCollection(options.children);
    }

//    if (!initial) {
//      this.collection.fetch();
//      this.render();
//
//      this.listenTo(this.collection, 'sync', this.render);
//    }
  },

  /* Поиск активной категории  */
  findChild: function (cat_model, cats_collection) {
    var child_id,
        children;

    /* Ищем id первой дочерней категории */
    if (cat_model.has('children')) {
      children = cat_model.get('children');
      child_id = children[0];

      /* Ищем модель найденной категории */
      if (child_id) {
        cat_model = cats_collection.findWhere({id: child_id});
        cat_model = this.findChild(cat_model, cats_collection);
      }
    }

    return cat_model;
  },

  getActiveCat: function (cats_сollection) {
    var cat_model,
      active_cat;

    /* Нашли первую корневую категорию */
    cat_model = cats_сollection.findWhere({ parent: null, level: 1 });

    /* Поиск первой дочерней категории */
    active_cat = (cat_model) ? this.findChild(cat_model, cats_сollection) : cat_model;

    return active_cat;
  },

  render: function (quiet) {
    this.renderCategories(quiet);

    if (!quiet) {
      var el = $(this.container);
      el.append(this.$el);
    }

    /* Для двухколоночного макета */
    if( SBApp.isLayout('two-columns') && (SBApp.state() === 'category' || SBApp.state() === 'product') ) {
      SBApp.trigger('active_category_item', SBApp.activeCategory);
    }

    return this;
  },

  renderCategories: function (quite) {
    this.collection.each( function (catModel) {
      // Отрабатывает если вложенная, для верхнего уровня фильтр по паренту
      if( quite === true ||  catModel.get('parent') == null ) {
        this.renderCategory(catModel);
      }
    }, this);
  },

  renderCategory: function (item) {
    var categoryView = new SBApp.CategoryView({ model: item });
    this.$el.append(categoryView.render().el);
  },

  hideAll: function () {
    this.$el.find('.nav-dropdown').removeClass('nav-dropdown').css('width', 'auto');
    this.$el.find('.with-dropdown').removeClass('with-dropdown');
    this.$el.find('li ul.nav').hide();
  }
});

SBApp.CategoryView = Backbone.View.extend({
  tagName: 'li',

  className: function () {
    var baseClass = 'sb-categories__item',
        levelClass = 'categories_level_' + this.model.get('level'),
        withIdClass = 'sb-categories__item-' + this.model.get('id');

    return [baseClass, levelClass, withIdClass].join(' ');

  },

  template: JST['categories:item'],

  events: function () {
    var events = {};

    /* В VK: переход на прямые урлы */
    if ( SBApp.Platform.isVK() ) {
      events = { 'click a[data-alias]' : 'changeRoute' };
    }

    /* Одноколоночный макет: скрыть, показать дропдаун */
    if( SBApp.isLayout('one-column') ) {
      /* mouseenter и mouseleave работают только на корневой категории */
      events['mouseenter'] = 'showDropdown';
      events['mouseleave'] = 'hideDropdown';
      events['click .js-show-products'] = 'showProductsFromDropdown';
    }

    return events;
  },

  initialize: function () {
    SBApp.on('active_category_item', function (id) {
      id = parseInt(id);
      if (this.model.id === id ||
          this.model.containsChildId(id)) {
        this.renderSubcategories();
      }

    }.bind(this));
  },

  hasActiveCategory: function () {
    return !!SBApp.activeCategory;
  },

  renderSubcategories: function () {
    if (!this.model.hasSubcategories()) {
      return;
    }

    if (this.subRendered) {
      return;
    }

    this.subRendered = true;
    this.$el.addClass('active');
    this.$el.append( this.subcategories() );
  },

  /* Показать продукты из категории в дропдауне */
  showProductsFromDropdown: function (e) {
    e.stopPropagation();
    this.hideDropdown(e);
  },

  /* Только для категорий первого уровня */
  enableDropdown: function () {
    return this.isRootCategory() && this.model.hasSubcategories();
  },

  /* Скрывают или показывают дропдаун из подкатегорий. Нужны, для одноколоночной страницы! */
  hideDropdown: function (event) {
    var $parent;

    if ( this.doHide( event.type ) ) {
      $parent = $('#categories').find('.is_mouseenter');
      $parent.removeClass('is_mouseenter');
      $parent.find('.nav').hide();

      return;
    }

    if ( this.isRootCategory() ) {
      this.$el.removeClass('is_mouseenter');
      this.$el.find('> .nav').hide();
    }
  },

  /* Прятать dropdown или нет */
  doHide: function (eventType) {
    return !this.isRootCategory() && eventType === 'click';
  },

  /* Корневая категория */
  isRootCategory: function () {
    return this.model.get('level') === 1;
  },

  /* Только для категорий первого уровня */
  showDropdown: function (event) {
    if ( !this.enableDropdown() ) {
      return;
    }

    var $dropdown = this.$el.find('> .nav'),
        $target = $(event.currentTarget),
        offsetX;

    this.$el.addClass('is_mouseenter');
    /* jQuery не может получить координаты элемента с "display: none" */
    $dropdown.show();
    this.$el.find('.nav').show();
    offsetX = $dropdown.offset().left;

    /* Ширина дропдауна < ширины li элемента */
    if ( $dropdown.width() < $target.width() ) {
      $dropdown.css('width', $target.outerWidth());
    }

    /* Ширина айфрейма < (координата x эл-та + ширина дропдауна) */
    if( $(window).width() < (offsetX + $dropdown.width()) ){
      $dropdown.css({right: 0, margin: 0, left: 'auto'});
    }
  },

  /* Добавление view подкатегорий в $el html текущей категории */
  subcategories: function () {
    var children = this.model.getChildren(),
        view = new SBApp.CategoryListView({ children: children }),
        subcatView;

    subcatView = view.render(true).$el;

    return subcatView;
  },

  render: function () {
    var templ = $( this.template(this.model.toJSON())),
        categoryId = this.model.get('id'),
        activeCategoryId = parseInt(SBApp.activeCategory);

    if (!this.model.hasSubcategories() &&
        !this.hasActiveCategory() &&
        _.isEmpty(SBApp.navView.state.get('activePage'))) {
      SBApp.activeCategory = categoryId;


      if (SBApp.state() === 'home') {
        SBApp.router.changeURL('category/'.concat(categoryId), true);
        SBApp.state('category');
      }


      this.$el.addClass('active');
    }

    this.$el.append(templ);

    if (categoryId === activeCategoryId ||
        this.model.containsChildId(activeCategoryId)) {
      this.renderSubcategories();
    }

    if (!this.hasActiveCategory() &&
        _.isEmpty(SBApp.navView.state.get('activePage'))) {
      this.renderSubcategories();
    }

    return this;
  }
});

_.extend(SBApp.CategoryView.prototype, SBApp.Mixins.VK);
