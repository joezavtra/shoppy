SBApp.module('CategoryApp.Controller', function (Controller, ContactManager, Backbone, Marionette, $, _) {
  Controller.showCategories = function () {
    var fetchingCategories = SBApp.request('category:entities');

    /* Запрос категорий */
    $.when( fetchingCategories ).done( function (categories) {
      SBApp.categoryBlock = new SBApp.CategoryListView({ collection: categories });

      SBApp.categoryBlock.render(false);
    });
  };
});