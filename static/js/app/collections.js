/* Переопределить метод parse для коллекции */
Backbone.Collection.prototype.parse = function (response) {
  return response.objects || response;
};

Backbone.Collection.prototype.isEmpty = function () {
  return this.length === 0;
}

/*
Порядок сортировки продуктов:
  "-param" - сортировка по убыванию
  "param"  - сортировка по возрастанию
*/
SBApp.productsOrderTypes = [
  {title: 'От А до Я',           type: 'name'},
  {title: 'По возрастанию цены', type: 'price'},
  {title: 'По убыванию цены',    type: '-price'},
  {title: 'По новизне',          type: '-mtime'}
];

SBApp.productsOrderBy = 'name';

/* TODO: Перенести в сущность продукта */
SBApp.ProductList = Backbone.Collection.extend({
  model: SBApp.Product,
  category: SBApp.activeCategory,
  offset: 0,
  limit: 9,
  total_count: 0,
  order_by: SBApp.productsOrderBy,

  url: function () {
    return '/api/v1/product/?' + this.params();
  },

  getParam: function (param) {
    if(this[param]) return this[param];
  },

  parse: function (response) {
    this.limit = response.meta.limit;
    this.offset = response.meta.offset;
    this.total_count = response.meta.total_count;

    return response.objects || response;
  },

  params: function () {
    this.category = SBApp.activeCategory;
    this.order_by = SBApp.productsOrderBy;

    var p = {limit: this.limit, offset: this.offset, category: this.category , order_by: this.order_by};
    return jQuery.param(p);
  }
});

SBApp.SearchQuery = Backbone.Collection.extend({
  model: SBApp.Product,
  queryString: '',
  offset: 0,
  limit: 9,
  total_count: 0,

  url: function () {
    return '/api/v1/search/product-full/?' + this.params();
  },

  params: function () {
    var params = {limit: this.limit, offset: this.offset, query: this.queryString};

    return jQuery.param(params);
  },

  parse: function (response) {
    this.limit = response.meta.limit;
    this.offset = response.meta.offset;
    this.total_count = response.meta.total_count;

    return response.objects || response;
  },

  start: function (query) {
    this.offset = 0;
    this.queryString = query;
    this.fetch({reset: true});
  },

  getResultsCount: function () {
    return this.total_count;
  },

  getLimit: function () {
    return this.limit;
  },

  getQueryString: function () {
    return this.queryString;
  }
});

/* #TODO: Переделать вывод комментариев в карточке продукта */
SBApp.Comments = Backbone.Collection.extend({
  model: SBApp.Comment,

  url: function () {
    return '/api/v1/comments/?' + this.params();
  },

  parse: function (response) {
    return response.objects || response;
  },

  params: function () {
    this.productId = SBApp.activeProduct;
    this.user = SBApp.userModel.get('id');

    var p = {customer_id: this.user, product: this.productId};

    return jQuery.param(p);
  }
});

SBApp.DeliveryPointList = Backbone.Collection.extend({
  model: SBApp.DeliveryPoint,

  url: function () {
    return '/api/v1/deliverypoint/?' + this.params();
  },

  params: function () {
    var p = {
      city: SBApp.deliveryCity
    };

    return jQuery.param(p);
  }
});

SBApp.DeliveryCityList = Backbone.Collection.extend({
  model: SBApp.DeliveryCity,

  url: '/api/v1/city/'
});

SBApp.OrderList = Backbone.Collection.extend({
  model: SBApp.Order,

  url: function () {
    return '/api/v1/order/' + this.params();
  },

  params: function () {
    var p = {
      format: 'json',
      customer: SBApp.userModel.get('id')
    };
    return jQuery.param(p);
  }
});

/* Блок избранных товаров в корзине */
SBApp.CartRecommendList = Backbone.Collection.extend({
  model: SBApp.Product,

  url: function () {
    return '/api/v1/slot-products/?' + this.params();
  },

  params: function () {
    var p = jQuery.param({
      slot: 'cart',
      limit: 5
    });

    return p;
  },

  isEmpty: function () {
    return !this.length;
  }
});
