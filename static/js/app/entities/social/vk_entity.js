SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  var VK = window.VK;

  var API = {
    /*
      group_id {String} - идентификатор или короткое имя сообщества.
      user_id {Number}  - идентификатор пользователя.
      user_ids {String} - идентификаторы пользователей, не более 500.
      extended {Number} - 1 — вернуть ответ в расширенной форме. По умолчанию — 0.
      флаг, может принимать значения 1 или 0

      Если участник группы, возвращает 1, не участник - 0, ошибка undefined.
  */
    isUserGroupMember: function (group_id, user_id, user_ids, extended) {
      var defered = $.Deferred(),
          user_ids = user_ids || '',
          extended = extended || 0;

      VK.api('groups.isMember', { group_id: group_id, user_id: user_id, user_ids: user_ids, extended: extended }, function (data) {
        if (data.response) {
          defered.resolve(data.response);
        }
        else if(data.error) {
          defered.resolve(undefined);
        }
      });

      return defered.promise();
    },

    /* Через API контакта запрашивает данные о пользователе или пользователях */
    /* @param {Integer} || {String} user_ids - идентификатор число или строка из id через запятую */
    /* @param {Srting}  fields */
    getUserData: function (user_ids, fields) {
      var defered = $.Deferred();

      VK.api('users.get', {user_ids: user_ids, fields: fields}, function (r) {
        if (r.response) {
          defered.resolve(r.response);
        }
        else {
          defered.resolve(undefined);
        }
      });

      return defered.promise();
    }
  };

  /* Возвращает информацию о том, является ли пользователь участником сообщества. */
  SBApp.reqres.setHandler('vk:groups:is:member', function (group_id, user_id, user_ids, extended) {
    return API.isUserGroupMember(group_id, user_id, user_ids, extended);
  });

  /* Возвращает информацию об одном или нескольких пользователях */
  SBApp.reqres.setHandler('vk:users:get', function (user_ids, fields) {
    return API.getUserData(user_ids, fields);
  })
});