SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  var API = {
    getCurrentUser: function () {
      var defer = $.Deferred(),
          FB = window.FB;

      FB.api('/me', function(response) {
        defer.resolve(response);
      });

      return defer.promise();
    }
  }

  /* Получить информацию о тек. посетителе приложения */
  SBApp.reqres.setHandler('fb:me', function () {
    return API.getCurrentUser();
  });
});