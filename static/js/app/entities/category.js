//======================
//    Категории
//======================
/* На данный момент нигде не исользуются */
SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  /* Модель категории */
  Entities.Category = Backbone.Model.extend({
    urlRoot: '/api/v1/category/',

    defaults: {
      name: 'Категория'
    },

    hasSubcategories: function () {
      return this.has('children') && this.get('children').length > 0;
    },

    getChildren: function () {
      var children = [],
        child;

      /* Получить модели подкатегорий */
      _.each( this.get('children'), function (childId) {
        child = this.collection.get(childId);
        children.push(child);
      }, this);

      // cache children
      this.getChildren = function () {
        return children.slice();
      }

      return children.slice();
    },

    containsChildId: function (id) {
      var children = this.getChildren(),
        child;

      while (children.length > 0) {
        child = children.pop();
        if (child.id === id) {
          return true;
        }
        children = _.union(children, child.getChildren());
      }

      return false;
    }
  });

  /* Коллекция категорий */
  Entities.CategoryCollection = Backbone.Collection.extend({
    model: Entities.Category,

    url: '/api/v1/category/',

    comparator: 'level',

    /* Возвращает массив моделей категорий по уровню. Если level не указан, возвр. корневые категории */
    getByLevel: function (level) {
      var level = level || 1;

      return this.where({ level: level });
    },

    getCategories: function (parent, level) {
      var parent = parent || null,
        level = level || 1;

      return this.where({ parent: parent, level: level});
    },

    /* Активная категория, первая категория в выборке */
    getActiveCategory: function () {
      var catsCollection = categories,
          catId = undefined;

      if ( !catsCollection.isEmpty() ) {
        catId = catsCollection.at(0).get('id');
      }

      return catId;
    }
  });

  var categories = new Entities.CategoryCollection();

  var API = {
    getCategoryEntities: function () {
      var defer = $.Deferred();

      /* Запросить, если категории пустые */
      if (categories.isEmpty ()) {
        categories.fetch({
          success: function (data) {
            defer.resolve(data);
          },
          error: function () {
            defer.resolve(undefined);
          }
        });
      }
      else {
        defer.resolve( categories );
      }

      return defer.promise();
    },

    getActiveCategoryId: function () {
      return categories.getActiveCategory();
    }
  }

  SBApp.reqres.setHandler('category:entities', function () {
    return API.getCategoryEntities();
  });

  /* Получить активную категорию */
  SBApp.reqres.setHandler('category:id:active', function () {
    return API.getActiveCategoryId();
  });
});