/*
статусы заказа:
 1 - новый
 2 - в обработке
 3 - выполнен
 4 - отменен
 5 - ждет оплаты
 6 - не оплачен
 7 - оплачен
*/
SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  Entities.Order = Backbone.Model.extend({
    urlRoot: '/api/v1/order/',

    defaults: {
      name: 'Заказ'
    },

    parse: function (resp) {
      var checkoutOrder;

      /* Стоимость заказа в чекауте + стоимость доставки */
      if(resp.checkouts && resp.checkouts[0]) {
        checkoutOrder = resp.checkouts[0];

        if(checkoutOrder.deliveryCost) {
          checkoutOrder.deliveryOrderCost += checkoutOrder.deliveryCost;
        }
      }

      return resp;
    }
  });

  var API = {
    getOrderEntity: function (id) {
      var order = new Entities.Order({ id: id }),
          defer = $.Deferred();

      order.fetch({
        success: function (data) {
          defer.resolve(data);
        },
        error: function () {
          defer.resolve(undefined);
        }
      });

      return defer.promise();
    }
  }

  SBApp.reqres.setHandler('order:entity', function (id) {
    return API.getOrderEntity(id);
  });
});