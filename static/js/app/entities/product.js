/* Сущность продукта */
SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  Entities.Product = Backbone.Model.extend({
    urlRoot: '/api/v1/product/',

    defaults: {
      name: 'Товар',
      variants: []
    },

    /*
     true  - у товара есть доп. параметры, кот. могут влиять на его цену
     false - параметров нет
     */
    hasVariants: function () {
      return this.get('variants').length > 0;
    },

    getCommentsCount: function () {
      var commentsCount = 0,
          param = SBApp.Platform.current()  + '_id';

      _.each(this.get('comments'), function (comment) {
        if( comment.customer && !_.isNull(comment.customer[param]) ) {
          commentsCount += 1;
        }
      });
      return commentsCount;
    },

    hasComments: function () {
      return this.getCommentsCount() > 0;
    },

    getComments: function () {
      return this.get('comments');
    },

    /* Возвращает id комментаторов для данноый соц. сети
     * @param  {String} platform
     * @return {Array}  socialIds
     * */
    getSpeakersId: function (platform) {
      var comments = this.get('comments'),
          socialIds = [],
          field = platform.concat('_id');

      _.each(comments, function (comment) {
        if(comment.customer && comment.customer[field]){
          socialIds.push(comment.customer[field]);
        }
      });

      return _.uniq(socialIds);
    },

    getCategoryId: function () {
      /* get('category') сейчас возвращает url из которого нужно достать id категории*/
      var categoryId = 0;
      var productCats = this.get('category');
      var pattern  = /category\/(\d+)/;
      var categoryUrl;

      if(_.isArray(productCats)){
        categoryUrl = productCats[0];
        categoryId = categoryUrl.match(pattern)[1];
      }

      return parseInt(categoryId);
    },

    variantsASC: function (variants) {
      var variants = variants || [];

      _.each(variants, function (variant) {
        variant['items'] = _.sortBy(variant['items'], function (item){
          return item['value'];
        });
      });

      return variants;
    },

    hasImages: function () {
      return !_.isEmpty(this.get('images'));
    },

    hasMoreThanOneImg: function () {
      return this.get('images').length > 1;
    },

    hasOnlyOneImg: function (images) {
      return this.get('images').length === 1;
    },

    parse: function (resp) {
      resp.variants = this.variantsASC(resp.variants);

      return resp;
    }
  });

  var API = {
    getProductEntity: function (id) {
      var product = new Entities.Product({id: id}),
          defer = $.Deferred();

      product.fetch({
        success: function (data) {
          defer.resolve(data);
        },
        error: function () {
          defer.resolve(undefined);
        }
      });

      return defer.promise();
    }
  }

  SBApp.reqres.setHandler('product:entity', function (productId) {
    return API.getProductEntity(productId);
  });
});