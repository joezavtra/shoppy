SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  Entities.LandingItem = Backbone.Model.extend({
    urlRoot: '/api/v1/landing/'
  });

  Entities.LandingItemList = Backbone.Collection.extend({
    model: Entities.LandingItem,

    comparator: 'order',

    url: '/api/v1/landing/',

    parse: function (resp) {
      var resp = resp.objects[0]['sections'] || resp,
          counter = _.findWhere(resp, {type: "counter", show_in_footer: true}),
          maxOrderItem,
          footerCounter;


      /* Если счетчик нужно продублировать внизу страницы */
      if ( counter ) {
        /* Клонирую объект счетчика*/
        footerCounter = _.clone( counter );

        /* Нахожу объект с max order */
        maxOrderItem = _.max(resp, function (item) {return item.order; });

        /* Меняю order второго счетчика */
        footerCounter['order'] = maxOrderItem['order'] + 1;

        /* Добавляю объект счетчика в конец массива ответа */
        resp.push(footerCounter);
      }

      return resp;
    }
  });

  var landingEntities = new Entities.LandingItemList();

  var API = {
    getLandingEntities: function () {
      var defer = $.Deferred();

      if ( landingEntities.isEmpty() ) {
        landingEntities.fetch({
          success: function (land) {
            defer.resolve(land);
          },
          error: function () {
            defer.resolve(undefined);
          }
        });
      }
      else {
        defer.resolve(landingEntities);
      }

      return defer.promise();
    }
  }

  SBApp.reqres.setHandler('landing:entities', function () {
    return API.getLandingEntities();
  });
});