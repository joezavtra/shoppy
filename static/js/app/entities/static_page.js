/* Сущность статич. страницы. Все разделы, кот. пользователи самостоятельно могут создать в кабинете
*  Основные данные о страницах, кроме содержимого, приходят в хэш SBApp.shop.pages. Если посетитель обращается на страницу впервые,
*  запрашивается содержимое статич. страницы с сервера, если заход уже был, берем содержимое из памяти.
*/

SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  Entities.PageItem = Backbone.Model.extend({
    urlRoot: '/api/v1/page/'
  });

  Entities.PageList = Backbone.Collection.extend({
    model: Entities.PageItem
  });

  var pages = new Entities.PageList( SBApp.shop.pages );

  var API = {
    getPageEntity: function (alias) {
      var defer = $.Deferred(),
          page;

      page = pages.findWhere({ page_alias: alias });

      /* Страницы не существует */
      if (page === undefined) {
        defer.resolve(undefined);
      }

      /* Если содержимое статич. страницы не пустое, возвращ. найденную модель */
      else if( page.has('content') ) {
        defer.resolve(page);
      }
      /* Иначе запрашиваем содержимое страницы с сервера */
      else {
        page.fetch({
          success: function (data) {
            defer.resolve(data);
          },
          error: function () {
            defer.resolve(undefined);
          }
        });
      }

      return defer.promise();
    }
  }

  SBApp.reqres.setHandler('page:entity', function (alias) {
    return API.getPageEntity(alias);
  });
});