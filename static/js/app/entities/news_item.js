/* Сущность новости */
SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  Entities.NewsItem = Backbone.Model.extend({
    urlRoot: '/api/v1/news/',

    defaults: {
      name: 'Новость'
    }
  });

  Entities.News = Backbone.Collection.extend({
    model: Entities.NewsItem,

    url: '/api/v1/news/'
  });

  var news = new Entities.News();

  var API = {
    getNewsEntities: function () {
      var defer = $.Deferred();

      if ( !news.isEmpty() ) {
        defer.resolve(news);
      }
      else {
        news.fetch({
          success: function (data) {
            defer.resolve(data);
          },
          error: function () {
            defer.resolve(undefined);
          }
        });
      }

      return defer.promise();
    }
  }

  SBApp.reqres.setHandler('news:entities', function () {
    return API.getNewsEntities();
  });
});