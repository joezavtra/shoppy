/* Модуль Баннера для работы с сущностями
/*
 * slot: 1 - под шапкой на каждой странице приложения
 *       2 - в корзине под товарами
 *       3 - над списком товаров
 * */
SBApp.module('Entities', function (Entities, SBApp, Backbone, Marionette, $, _) {
  Entities.Banner = Backbone.Model.extend({
    defaults: {
      slot : '',
      total_count: 0
    },

    initialize: function (options) {
      if (options.slot) {
        this.set('slot', options.slot);
      }
    },

    is: function (type) {
      return type === this.get('type_code');
    },

    url: function () {
      return '/api/v1/banner/?format=json&slot=' + this.get('slot');
    },

    parse: function (resp) {
      if(resp.meta.total_count) {
        this.set('total_count', resp.meta.total_count);
      }

      return resp.objects[0] || resp;
    },

    isEmpty: function () {
      var isEmpty = false;

      if (this.has('objects') && this.get('objects').length === 0) {
        isEmpty = true;
      }
      else {
        isEmpty = this.has('data') && this.get('data').length === 0;
      }

      return isEmpty;
    }
  });

  // Закэшированные баннеры
  var cacheBanners = new Backbone.Collection();

  var API = {
    getFromCache: function (slot) {
      return cacheBanners.findWhere({ slot: slot });
    },

    bannerIsInCache: function (slot) {
      return this.getFromCache(slot);
    },

    /* Запросить данные баннера по слоту */
    getBannerEntity: function (slot) {
      var banner;
      var defer = $.Deferred();

      // Если модель баннера лежит в кэше, забераем ее
      if (this.bannerIsInCache(slot)) {
        banner = this.getFromCache(slot);

        defer.resolve(banner);
      }
      // Иначе запрашиваем с сервера
      else {
        banner = new Entities.Banner({ slot: slot });
        cacheBanners.add(banner);

        banner.fetch({
          success: function (data) {
            defer.resolve(data);
          },
          error: function () {
            defer.resolve(undefined);
          }
        });
      }

      return defer.promise();
    }
  };

  SBApp.reqres.setHandler('banner:entity', function (slot) {
    return API.getBannerEntity(slot);
  });
});