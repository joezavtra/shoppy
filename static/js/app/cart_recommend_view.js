/* Блок избранных товаров для корзины */
SBApp.CartRecommendItemView = Marionette.ItemView.extend({
  className: 'sb-cart-recommend__item pull-left',

  events: {
    'click' : 'productProfile',
    'click .js-add' : 'addToCart'
  },

  template: JST['recommend:item'],

  addToCart: function (event) {
    event.preventDefault();
    event.stopPropagation();

    var cartItem = new SBApp.CartItem();

    /* Google Analitycs Добавление из каталога */
    ga && ga('send','event','button','click','add_cart_recommend');

    cartItem.save(
      { product_id: this.model.get('id'), original_id: this.model.get('oid'), cart_id: SBApp.cart.get('id') },
      { complete: function () {
          SBApp.cart.fetch({
            cache: false,
            do_render: true,
            success: function (cartModel, resp, options) {
              SBApp.Popover.showOnCartAdd();
            }
          });
        }
      }
    );
  }
});

SBApp.CartRecommendView = Marionette.CompositeView.extend({
  template: JST['recommend'],

  id: 'sb-cart-recommend',

  className: 'sb-cart-recommend clearfix',

  childView: SBApp.CartRecommendItemView,

  childViewContainer: '.sb-cart-recommend__container'
});

_.extend(SBApp.CartRecommendItemView.prototype, SBApp.Mixins.Product);