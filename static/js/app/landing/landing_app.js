SBApp.addRegions({
  landingReg: '.lp'
});

/* LP - Landing Page :-) */
SBApp.module('LP', function (LP, SBApp, Backbone, Marionette, $, _) {
  LP.Router = Marionette.AppRouter.extend({
    appRoutes: {
      'landing' : 'showLanding'
    }
  });

  var API = {
    showLanding: function () {
      LP.Show.Controller.showLandingItems();
    }
  }
});