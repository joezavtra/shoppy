SBApp.module('LP', function (LP, SBApp, Backbone, Marionette, $, _) {
  /* Обратный отсчет до акции, собития и форма */
  LP.PromoView = Marionette.ItemView.extend({
    className: 'lp-promo lp-block row-fluid',

    template: _.template('<h1 class="lp-block-title"><%= title %></h1><div class="lp-promo__container"><%= countDownBlock() %><%= button() %></div>'),

    templateHelpers: function () {
      return {
        countDownBlock: function () {
          return '<div class="lp-countdown text-center">\
            <div class="lp-countdown-sector g-inline-block">\
              <div class="lp-countdown-days">00</div>\
              <div class="lp-countdown-title">дни</div>\
            </div>\
            <div class="lp-countdown-sector g-inline-block">\
              <div class="lp-countdown-hrs">00</div>\
              <div class="lp-countdown-title">часы</div>\
            </div>\
            <div class="lp-countdown-sector g-inline-block">\
              <div class="lp-countdown-mins">00</div>\
              <div class="lp-countdown-title">минуты</div>\
            </div>\
            <div class="lp-countdown-sector g-inline-block">\
              <div class="lp-countdown-secs">00</div>\
              <div class="lp-countdown-title">секунды</div>\
            </div>\
          </div>';
        },

        button: function () {
          return '<div class="lp-form">\
            <form class="" action="">\
              <div class="lp-form-item">\
                <button type="submit" class="btn btn-block btn-large js-slide-to">Участвовать</button>\
              </div>\
            </form>\
          </div>';
        }
      }
    },

    ui: {
      'btn' : '.js-slide-to'
    },

    events: {
      'update.countdown': 'updateCountdown',
      'click @ui.btn' : 'slideToBlock'
    },

    slideToBlock: function (e) {
      e.preventDefault();

      this.trigger('scroll:to', '.lp-products');
    },

    updateCountdown: function (e) {
      var offsetObj = e.offset;

      this.$el.find('.lp-countdown-days').html( _.zeroFirst( offsetObj['totalDays'] ));
      this.$el.find('.lp-countdown-hrs').html( _.zeroFirst( offsetObj['hours'] ));
      this.$el.find('.lp-countdown-mins').html( _.zeroFirst( offsetObj['minutes'] ));
      this.$el.find('.lp-countdown-secs').html(_.zeroFirst( offsetObj['seconds'] ));
    },

    onRender: function () {
      var dateStr = SBApp.getDate( this.model.get('date') );

      this.$el.countdown( dateStr );

      this.el.style.cssText += this.model.get('custom_style');
    }
  });

  LP.GoodsItemView = Marionette.ItemView.extend({
    className: 'lp-products-item span3',

    events: {
      'click .js-new-order' : 'order',
      'click' : 'productProfile'
    },

    template: _.template('<h4 class="lp-products-item-title"><%= name %></h4>\
    <div class="lp-products-item-img" style="background-image: url(\'<%= cover %>\');"></div><div class="lp-products-item-toolbar"><span class="lp-products-item__price"><%= SBApp.getPriceWithCurrency(price) %></span><%= button() %></div></div>'),

    templateHelpers: function () {
      return {
        button: function () {
          return '<button class="btn js-new-order lp-product-item__btn">Купить</button>'
        }
      }
    },

    order: function (e) {
      e.preventDefault();
      e.stopPropagation();

      SBApp.trigger('modal:show', this.model,  { title: 'Оформить заказ', modal_type: 'new-order'});
    }
  });

  _.extend(LP.GoodsItemView.prototype, SBApp.Mixins.Product);

  LP.GoodsListView = Marionette.CompositeView.extend({
    className: 'lp-products lp-block row-fluid',

    childViewContainer: '.lp-products__container',

    childView: LP.GoodsItemView,

    initialize: function () {
      this.collection = new Backbone.Collection( this.model.get('products') );
    },

    ui: {
      productsContainer: '.lp-products__container'
    },

    template: _.template('<h1 class="lp-block-title"><%= title %></h1>\
    <div class="lp-products__container clearfix"></div>'),

    onShow: function() {
        this.el.style.cssText += this.model.get('custom_style');

        /* Отцентровать продукты */
        if (this.collection.length < 4) {
          var offset = '?',
              count = this.collection.length,
              ml = parseInt(this.ui.productsContainer.find('.lp-products-item').eq(1).css('margin-left'), 10) || 0,
              pw = this.ui.productsContainer.find('.lp-products-item').eq(0).outerWidth(),
              cw = this.ui.productsContainer.outerWidth(),
              w = pw * count + ml * ( count - 1),
              M = (cw - w) / 2;

          this.ui.productsContainer.find('.lp-products-item').eq(0).css('margin-left', M);

          this.ui.productsContainer.addClass('text-center');

        }
    }
  });

  LP.BaseView = Marionette.ItemView.extend({
    template: _.template('<div></div>')
  });

  /* Основной блок */
  LP.MainView = Marionette.ItemView.extend({
    className: 'lp-main lp-block row-fluid',

    template: _.template('<div class="span12">\
      <h1 class="lp-block-title"><%= title %></h1>\
      <div class="lp-main__text"><%= desc %></div>\
    </div>'),

    onRender: function(){
        this.el.style.cssText += this.model.get('custom_style');
    }
  });

  /* Фривольный раздел */
  LP.SimpleView = Marionette.ItemView.extend({
    className: 'lp-simple lp-block row-fluid',

    template: _.template('<div class="span12">\
      <h1 class="lp-block-title"><%= title %></h1>\
      <div class="lp-simple__text"><%= text %></div>\
    </div>'),

    onRender: function(){
        this.el.style.cssText += this.model.get('custom_style');
    }
  });

  /* Айтем отзыва */
  LP.ReviewsItem = Marionette.ItemView.extend({
    className: 'lp-reviews-item row-fluid',

    template: _.template('<div class="span12">\
      <h4 class="lp-reviews-item-title"><%= author %></h4>\
      <div class="lp-reviews-item-msg"><%= text %></div>\
    </div>')
  });

  /* Список отзывов */
  LP.ReviewsList = Marionette.CompositeView.extend({
    className: 'lp-reviews lp-block row-fluid',

    childView: LP.ReviewsItem,

    childViewContainer: '.lp-reviews-container',

    initialize: function () {
      this.collection = new Backbone.Collection( this.model.get('comments') );
    },

    template: _.template('<h1 class="lp-block-title"><%= title %></h1>\
      <div class="lp-reviews-container"></div>'),

    onRender: function(){
        this.el.style.cssText += this.model.get('custom_style');
    }
  });

  /* Достоинства и недостатки */
  LP.CompareView = Marionette.ItemView.extend({
    className: 'lp-compare lp-block row-fluid',

    template: _.template('<div class="lp-compare__benefits">\
      <div class="row-fluid">\
        <div class="span6">\
          <div>\
            <h1><%= left.title %></h1>\
            <div><%= left.text %></div>\
          </div>\
        </div>\
        <div class="span6">\
          <div>\
            <h1><%= right.title %></h1>\
            <div><%= right.text %></div>\
          </div>\
        </div>\
      </div>\
    </div>'),

    onRender: function(){
        this.el.style.cssText += this.model.get('custom_style');
    }
  });

  /* Родительское вью, в него отрисовываются все блоки лендинга */
  LP.ParentView = Marionette.CollectionView.extend({
    className: 'lp',

    getChildView: function (item) {
      var blockType = item.get('type'),
          view = LP.BaseView;

      /* Основной блок */
      if ( blockType === 'main' ) {
        view = LP.MainView;
      }

      /* Отзывы клиентов */
      else if (blockType === 'comments') {
        view = LP.ReviewsList;
      }
      /* Преимущества */
      else if (blockType === 'compare') {
        view = LP.CompareView;
      }
      /* Счетчик */
      else if (blockType === 'counter') {
        view = LP.PromoView;
      }

      /* Товары */
      else if (blockType === 'products') {
       view = LP.GoodsListView;
      }

      /* Свободный раздел */
      else if (blockType === 'simple') {
        view = LP.SimpleView;
      }

      return view;
    },

    initialize: function () {
      var that = this;

      this.on('childview:scroll:to', function (childView, el_selector) {
        var offsetTop = that.$el.find(el_selector).offset().top,
            vkTopOffset = 70,
            animationTime = 1000;

        if (SBApp.Platform.isVK()) {
          VK.callMethod('scrollWindow', offsetTop + vkTopOffset, animationTime);
        }

        else {
          $('html, body').animate({
            scrollTop: offsetTop
          }, animationTime);
        }
      });
    },

    onRender: function () {
      this.$el.append('<div id="lp-social" />');
    }
  });
});