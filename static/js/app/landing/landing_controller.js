SBApp.module('LP.Show', function (Show, SBApp, Backbone, Marionette, $, _) {
  Show.Controller = {
    showLandingItems: function () {
      var fetchingLanding = SBApp.request('landing:entities');

      $.when( fetchingLanding ).done( function (landingItems) {
        var landingView = new SBApp.LP.ParentView({ collection: landingItems });

        SBApp.wrapperRegion.show( landingView );

        /* Виджет сообщество */
        if ( SBApp.Platform.isVK() && SBApp.hasGroupId() ) {
          SBApp.execute('vk:widget:group', 'lp-social', {}, SBApp.getGroupId() );
        }
      });
    }
  };
});