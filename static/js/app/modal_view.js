/* Модальное окно "Обратный звонок */
SBApp.CallbackOrderView = Marionette.ItemView.extend({
  className: 'row-fluid',

  template: JST['order:callback'],

  ui: {
    'phoneInput' : '#recall-phone'
  },

  events: {
    'click .js-send-recall' : 'sendCallback',
    'change input'          : 'onInputBlur',
    'keyup  input'          : 'onInputBlur'
  },

  onRender: function () {
    this.ui.phoneInput.mask( SBApp.config['PHONE_MASK'] );
  },

  onBeforeDestroy: function () {
    this.ui.phoneInput.unmask( SBApp.config['PHONE_MASK'] );
  },

  sendCallback: function (e) {
    var callbackOrder = new SBApp.CallbackOrder();
    var that = this;
    var json = {
      name: $.trim( this.$el.find('#recall-name').val() ),
      phone: this.$el.find('#recall-phone').val().replace(/[\-\s]/g, ''),
      pid: this.model.get('id'),
      app_type: SBApp.Platform.getType()
    };

    callbackOrder.save(json, {
      complete: function (model) {
        /* TODO: Сделать через отдельную вью, с успешным сообщением */
        that.$el.find('#recall').fadeOut('slow', function () {
          SBApp.trigger('modal:show:success', {text: 'Спасибо! Оператор скоро свяжeтся с вами.'});

          _.delay(function () {
            that.trigger('modal:close');
          }, 3000);
        });
      }
    });

    /* Google analitics event - Клик по "Жду звонка" */
    ga && ga('send','event','button','click','callback_save');

    SBApp.trigger('segment:io:track', 'Saved Call Me Back', SBApp.getProductProps( this.model ));
  },

  onInputBlur: function (event) {
    var $input = $(event.target),
      val = $.trim($input.val()),
      required = $input.data('required'),
      controlType = $input.data('type');

    $input.parents('.control-group').removeClass('success');

    /* Обязательное поле пустое */
    if (_.isEmpty(val) && required) {
      // this.showControlError($input, SBApp.Templates.Validator.controlRequired);
      $input.data('valid', false);
    }

    /* Phone поле */
    else if (controlType === 'phone' && required && !/^[\s()+-]*([0-9][\s()+-]*){6,20}$/.test(val)) {
      this.showControlError($input, 'Неверный формат телефона');
      $input.data('valid', false);
    }

    /* Если дошли сюда, значит поле валидное */
    else {
      if (required) {
        this.showControlSuccess($input);
      }

      this.hideControlError($input);
      $input.data('valid', true);
    }

    /* Проверяем, все ли поля валидны */
    this.$el.find('.js-send-recall').prop('disabled', (this.getNotValidFields() > 0) ? 'disabled' : '');
  },

  showControlError: function ($control, errorMessage) {
    $control.parents('.control-group').addClass('error');
    $control.siblings('.help-inline').html(errorMessage).css('display', 'inline-block');
  },

  showControlSuccess: function ($control) {
    $control.parents('.control-group').addClass('success');
  },

  hideControlError: function ($control) {
    $control.parents('.control-group').removeClass('error');
    $control.siblings('.help-inline').hide();
  },

  /* Возвращает кол-во не валидных полей */
  getNotValidFields: function () {
    var nVF = 0;

    this.$el.find('form').find('input').each(function (key, control) {
      var $control = $(control);
      var required = $control.data('required');
      var controlIsValid = $control.data('valid');

      /* Поля, обязательные для заполнения */
      if(required && !controlIsValid){
        nVF += 1;
      }
    });

    return nVF;
  }
});

/* Модальное окно "Добавить приложение в избранное для ВК" */
SBApp.FavSocApp = Marionette.ItemView.extend({
  className: 'row-fluid',
  events: {
    'click .js-close' : 'closeModal',
    'click .js-add'   : 'addApp'
  },

  /* Битовая маска "Добавить приложение в избранное */
  BIT_MASK: 256,

  /* TODO: Вынести в templates с хорошим названием */
  template: _.template('<div class="sb-fav-app__text">Чтобы не потерять ссылку на магазин, добавьте его в левое меню!</div>\
   <div class="text-center sb-fav-app__controls"><button class="btn btn-default js-close">Нет, спасибо</button>&nbsp;<button class="btn btn-success js-add">Добавить в меню</button></div>'),

  initialize: function () {
    var that = this;

    /* Обработчик изменения настроек приложения ВК */
    Social.VK.addCallback('onSettingsChanged', function (bitMaskAppSettings) {
      var is_settings_changed = that.BIT_MASK & bitMaskAppSettings;

      if (is_settings_changed) {
        SBApp.trigger('modal:show:success', { text: 'После обновления страницы, ссылка на магазин появится в левом меню.', title: 'Спасибо!' });

        _.delay(function () {
          that.trigger('modal:close');
        }, 3000);
      }
    });
  },

  addApp: function () {
    var bit_mask = 256;

    Social.VK.showSettingsBox(bit_mask);

    /* Клик по "добавить в избранное - google analitics event */
    ga && ga('send','event','button','click','vk_favourites');

    SBApp.trigger('segment:io:track', 'Added App To Favourites');
  },

  closeModal: function () {
    this.trigger('modal:close');
  },

  onDestroy: function () {
    /* Удалить ненужный VK */
    Social.VK.removeCallback('onSettingsChanged');
  }
});

SBApp.OrderForm = Marionette.ItemView.extend({
  tagName: 'form',

  id: 'new-order-form',

  className: 'form-horizontal new-order-form',

  template: JST['order:new'],

  ui: {
    'phoneInput' : '.js-new-order-phone',
    'orderBtn'   : '.js-new-order__btn'
  },

  events: {
    'change input'        : 'onInputBlur',
    'keyup  input'        : 'onInputBlur',
    'click  @ui.orderBtn' : 'newOrder'
  },

  templateHelpers: function () {
    return {
      username: function () {
        return $.trim( SBApp.userModel.get('name') );
      },

      isUsernameValid: function () {
        return ( this.username() ) ? true : false;
      },

      appType: function () {
        return SBApp.Platform.getType();
      }
    }
  },

  onRender: function () {
    this.ui.phoneInput.mask( SBApp.config['PHONE_MASK'] );
  },

  onBeforeDestroy: function () {
    this.ui.phoneInput.unmask( SBApp.config['PHONE_MASK'] );
  },

  newOrder: function (e) {
    e.stopPropagation();
    e.preventDefault();

    var formJSON = Backbone.Syphon.serialize( this );
    var fastOrder = new SBApp.FastOrder( formJSON );
    var that = this;
    var oid = this.model.get('oid');
    var pid = this.model.get('id');

    fastOrder.set('original_id', oid);
    fastOrder.set('product_id', pid);

    fastOrder.save({}, {
      statusCode: {
        201: function () {
          that.$el.fadeOut('slow', function () {
            SBApp.trigger('modal:show:success', {text: 'Спасибо! Оператор скоро свяжeтся с вами.', title: 'Заказ оформлен'});

            _.delay(function () {
              that.trigger('modal:close');
            }, 3000);
          });
        }
      }
    });
  },

  onInputBlur: function (event) {
    var $input = $(event.target),
        val = $.trim($input.val()),
        required = $input.data('required'),
        controlType = $input.data('type');

    $input.parents('.control-group').removeClass('success');

    /* Обязательное поле пустое */
    if (_.isEmpty(val) && required) {
      $input.data('valid', false);
    }

    /* Phone поле */
    else if (controlType === 'phone' && required && !/^[\s()+-]*([0-9][\s()+-]*){6,20}$/.test(val)) {
      this.showControlError($input, 'Неверный формат телефона');
      $input.data('valid', false);
    }

    /* Email поле */
    else if (controlType === 'email' && required && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val)) {
      this.showControlError($input, SBApp.Templates.Validator.controlEmailNotValid);
      $input.data('valid', false);
    }

    /* Если дошли сюда, значит поле валидное */
    else {
      if (required) {
        this.showControlSuccess($input);
      }

      this.hideControlError($input);
      $input.data('valid', true);
    }

    /* Проверяем, все ли поля валидны */
    this.ui.orderBtn.prop('disabled', (this.getNotValidFields() > 0) ? 'disabled' : '');
  },

  showControlError: function ($control, errorMessage) {
    $control.parents('.control-group').addClass('error');
    $control.siblings('.help-inline').html(errorMessage).css('display', 'inline-block');
  },

  showControlSuccess: function ($control) {
    $control.parents('.control-group').addClass('success');
  },

  hideControlError: function ($control) {
    $control.parents('.control-group').removeClass('error');
    $control.siblings('.help-inline').hide();
  },

  /* Возвращает кол-во не валидных полей */
  getNotValidFields: function () {
    var nVF = 0;

    this.$el.find('form').find('input').each(function (key, control) {
      var $control = $(control);
      var required = $control.data('required');
      var controlIsValid = $control.data('valid');

      /* Поля, обязательные для заполнения */
      if(required && !controlIsValid){
        nVF += 1;
      }
    });

    return nVF;
  }
});

SBApp.CartPreviewItem = Marionette.ItemView.extend({
  className: 'sb-cart-preview__item g-inline-block',

  template: JST['cart:preview:item']
});

SBApp.CartPreview = Marionette.CompositeView.extend({
  className: 'sb-cart-preview',

  template: JST['cart:preview'],

  childViewContainer: '.sb-cart-preview__container',

  childView: SBApp.CartPreviewItem,

  events: {
    'click .js-to-cart' : 'toCart',
    'click .js-close'   : 'closeModal'
  },

  closeModal: function () {
    this.trigger('modal:close');

    /* Google analitics event - Клик по "Продолжить покупку" */
    ga && ga('send','event','button','click', 'continue_shopping');
    SBApp.trigger('segment:io:track', 'Continue Shopping');
  },

  toCart: function () {
    this.trigger('modal:close');
    SBApp.router.changeURL('cart', true);

    SBApp.trigger('scroll:to', { offset: $('#content').offset().top, duration: 500});

    /* Google analitics event - Клик по "Перейти в корзину" */
    ga && ga('send','event','button','click','go_to_cart');
    SBApp.trigger('segment:io:track', 'Go To Cart From Modal');
  },

  templateHelpers: function () {
    return {
      orderData: function () {
        var count = SBApp.cart.getItemsCount();

        return '<h5 style="margin-top: 0;" class="text-center">В вашей корзине ' + SBApp.cart.getItemsCount() + '&nbsp;' + SBApp.getWordWithRightEnding( count ) +  '&nbsp;на сумму&nbsp;' + SBApp.getPriceWithCurrency( SBApp.cart.totalAmount() ) + '</h5>';
      }
    }
  },

  initialize: function () {
    this.collection = new Backbone.Collection( this.model.get('items') );
  },

  onRender: function () {
    /* Выравнивать по центру товары, если их меньше 3 */
    if (this.model.get('items').length < 3) {
      this.$el.find( this.childViewContainer ).addClass('text-center');
    }
  }
});

SBApp.ModalView = Marionette.CompositeView.extend({
  id: 'modal',

  events: {
    'hidden': 'onHidden'
  },

  className: 'modal hide fade sb-modal',

  childViewContainer: '.modal-body',

  template: JST['modal'],

  /* Определяется тип содержимого модального окна */
  getChildView: function () {
    var childView,
        modalType = this.model.get('modal_type');

    switch (modalType) {
      /* Форма обратного звонка */
      case 'callback-order':
        childView = SBApp.CallbackOrderView;
        break;
      /* Форма "Добавить в избранное" (Появл. только в ВК) */
      case 'fav-app':
        childView = SBApp.FavSocApp;
        break;
      /* Форма нового заказа, работает на лендинге */
      case 'new-order':
        childView = SBApp.OrderForm;
        break;
      case 'cart-preview':
        childView = SBApp.CartPreview;
        break;
    }

    return childView;
  },

  /* Bootstrap событие модального окна. Срабатывает, когда модальное окно полностью скроется */
  onHidden: function () {
    this.destroy();
  },

  onRender: function () {
    this.$el.modal('show');
  },

  onShow: function () {
    var that = this, modalHeight, bodyHeight, position, marginTop;

    /* Положение модального окна при скролле в ВК */
    if ( SBApp.Platform.isVK() ) {
      VK.addCallback('onScroll', function (currentPosition, windowHeight) {
        that.$el.css('top', currentPosition);
      });

      VK.callMethod('scrollSubscribe', true);
    }

    /* Положение модального окна при скролле в Фейсбук табе */
    else if ( SBApp.Platform.isFacebook() ) {
      modalHeight = this.$el.outerHeight();
      bodyHeight = $('body').outerHeight();

      this.onFacebookScroll = setInterval( function () {
        window.FB.Canvas.getPageInfo( function (page) {
          if (page.clientHeight > modalHeight) {
            marginTop = (page.clientHeight - modalHeight) / 2;
            position = page.scrollTop - marginTop;

            /* Не скроллить, если модальное окно выйдет за пределы айфрема */
            if (position < 0 || (position + modalHeight) > bodyHeight) {
              return;
            }

            that.$el.css('top', position);
          }
        });
      }, 500);
    }
  },

  onDestroy: function () {
    if (SBApp.Platform.isVK() ) {
      VK.removeCallback('onScroll');
    }
    else if ( SBApp.Platform.isFacebook() ) {
      clearInterval( this.onFacebookScroll )
    }
  }
});