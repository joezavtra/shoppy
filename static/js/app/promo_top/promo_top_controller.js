SBApp.module('TopBanner.Controller', function (Controller, SBApp, Backbone, Marionette, $, _) {
  /* Показать баннер подписки */
  Controller.showSubscribeView = function () {
    SBApp.topBannerReg.show( new SBApp.TopBanner.SubscribeView({ model: SBApp.userModel }) );
  };

  /* Показать "Пригласить друга" преимущественно в ВК */
  Controller.showFriendsInviting = function () {
    /* TODO: Возможность передавать текст для вывода перед кнопкой */
    var invitingView = new SBApp.TopBanner.InviteFriendView();

    invitingView.on('show', function () {
      invitingView.$el.hide().slideDown(500);
    });

    SBApp.topBannerReg.show( invitingView );

    SBApp.trigger('segment:io:track', 'User Invited Friends To App');
  };
});