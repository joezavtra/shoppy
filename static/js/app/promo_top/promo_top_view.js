SBApp.module('TopBanner', function (TopBanner, SBApp, Backbone, Marionette, $, _) {
  /* Вьюха подписки на новости магазина. В  основе оформления bootstrap alert */
  TopBanner.SubscribeView = Marionette.CompositeView.extend({
    className: 'sb-subscribe alert alert-warning fade in text-center',

    template: JST['subscribe:alert'],

    error_template: JST['subscribe:message:error'],

    success_template: JST['subscribe:message:success'],

    ui: {
      'emailInput' : '#sb-subscribe__email'
    },

    events: {
      'click .js-subscribe': 'subscribe',
      'closed' : 'onClosed'
    },

    hideDelay: 2000,
    fadeOutDelay: 800,
    slideUpDelay: 600,

    initialize: function () {
      this.listenTo( this.model, 'invalid', this.showError );
    },

    showError: function (model, error) {
      var errorTempl = this.error_template({ error: error });

      this.ui.emailInput.popover('destroy').popover({content: errorTempl, placement: 'bottom', html: true}).popover('show');
    },

    onClosed: function () {
      this.closeSubscribe();
    },

    closeSubscribe: function () {
      this.destroy();
    },

    onSubscribe: function () {
      var that = this;

      this.$el.find('.sb-subscribe__form').fadeOut(this.fadeOutDelay, function () {
        that.$el.addClass('alert-success').append( that.success_template() );

        _.delay( function () {
          that.$el.slideUp({
            delay: that.slideUpDelay,
            complete: function () {
              that.closeSubscribe();
            }
          });
        }, that.hideDelay);
      });
    },

    subscribe: function (e) {
      e.preventDefault();
      e.stopPropagation();

      var that = this;

      /* Валидация поля email */
      var email = $.trim( this.ui.emailInput.val() );

      this.model.save({ email: email }, {
        validate: true,
        patch: true,
        statusCode: {
          202: function () {
            that.onSubscribe();
          }
        }
      });

      /* Клик по "Подписаться" - google analitics event */
      ga && ga('send','event','button','click','subscribe');

      SBApp.trigger('segment:io:track', 'User Subscribed', { email: email });
    }
  });

  TopBanner.InviteFriendView = Marionette.ItemView.extend({
    className: 'sb-invitation text-center alert alert-info',

    template: _.template('<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <span class="sb-invitation__text alert-heading"></span><button class="btn btn-default js-invite-friends">Пригласить друзей</button>'),

    events: {
      'click .js-invite-friends' : 'invite'
    },

    invite: function (e) {
      var that = this;

      e.preventDefault();

      SBApp.execute('vk:show:invite:box');

      _.delay( function () {
        that.$el.slideUp({
          delay: 2000,
          complete: function () {
            that.destroy();
          }
        });
      }, 2000)
    },

    initialize: function (options) {
      options || (options = {});

      this.text = options.text || 'Приглашайте друзей в приложение';
    },

    onRender: function () {
      this.$('.sb-invitation__text').text( this.text );
    }
  });
});

