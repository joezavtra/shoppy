//======================
//    Категории
//======================
SBApp.Category = Backbone.Model.extend({
  urlRoot: '/api/v1/category/',

  defaults: {
    name: 'Категория'
  },

  hasSubcategories: function () {
    return this.has('children') && this.get('children').length > 0;
  },

  getChildren: function () {
    var children = [],
        child;

    /* Получить модели подкатегорий */
    _.each( this.get('children'), function (childId) {
      child = this.collection.get(childId);
      children.push(child);
    }, this);

    // cache children
    this.getChildren = function () {
      return children.slice();
    }

    return children.slice();
  },

  containsChildId: function (id) {
    var children = this.getChildren(),
        child;

    while (children.length > 0) {
      child = children.pop();
      if (child.id === id) {
        return true;
      }
      children = _.union(children, child.getChildren());
    }

    return false;
  }
});

//======================
//    Товары
//======================
/* TODO: Удалить, когда ProductList вынесу в сущности продукта */
SBApp.Product = Backbone.Model.extend({
  url: function () {
    return '/api/v1/product/' + SBApp.activeProduct;
  },

  defaults: {
    name: 'Товар',
    variants: []
  },

  /*
    true  - у товара есть доп. параметры, кот. могут влиять на его цену
    false - параметров нет
   */
  hasVariants: function () {
    return this.get('variants').length > 0;
  },

  getCommentsCount: function () {
    var commentsCount = 0,
        param = SBApp.Platform.current()  + '_id';

    _.each(this.get('comments'), function (comment) {
      if( comment.customer && !_.isNull(comment.customer[param]) ) {
        commentsCount += 1;
      }
    });
    return commentsCount;
  },

  hasComments: function () {
    return this.getCommentsCount() > 0;
  },

  getComments: function () {
    return this.get('comments');
  },

  /* Возвращает id комментаторов для данноый соц. сети
  * @param  {String} platform
  * @return {Array}  socialIds
  * */
  getSpeakersId: function (platform) {
    var comments = this.get('comments'),
        socialIds = [],
        field = platform.concat('_id');

    _.each(comments, function (comment) {
      if(comment.customer && comment.customer[field]){
        socialIds.push(comment.customer[field]);
      }
    });

    return _.uniq(socialIds);
  },

  getCategoryId: function () {
    /* get('category') сейчас возвращает url из которого нужно достать id категории*/
    var categoryId = 0;
    var productCats = this.get('category');
    var pattern  = /category\/(\d+)/;
    var categoryUrl;

    if(_.isArray(productCats)){
      categoryUrl = productCats[0];
      categoryId = categoryUrl.match(pattern)[1];
    }

    return parseInt(categoryId);
  },

  variantsASC: function (variants) {
    var variants = variants || [];

    _.each(variants, function (variant) {
      variant['items'] = _.sortBy(variant['items'], function (item){
        return item['value'];
      });
    });

    return variants;
  },

  hasImages: function () {
    return !_.isEmpty(this.get('images'));
  },

  hasMoreThanOneImg: function () {
    return this.get('images').length > 1;
  },

  hasOnlyOneImg: function (images) {
    return this.get('images').length === 1;
  },

  parse: function (resp) {
    resp.variants = this.variantsASC(resp.variants);

    return resp;
  }
});

//======================
//    Корзина
//======================
/* Статус заказа: 1 - сформирован, 2 - оплачен, 3 - не оплачен */
SBApp.Order = Backbone.Model.extend({
  urlRoot: '/api/v1/order/',

  defaults: {
    name: 'Заказ'
  },

  parse: function (resp) {
    var checkoutOrder;

    /* Стоимость заказа в чекауте + стоимость доставки */
    if(resp.checkouts && resp.checkouts[0]) {
      checkoutOrder = resp.checkouts[0];

      if(checkoutOrder.deliveryCost) {
        checkoutOrder.deliveryOrderCost += checkoutOrder.deliveryCost;
      }
    }

    return resp;
  }
});

SBApp.CartItem = Backbone.Model.extend({
  urlRoot: '/api/v1/cartitem/',

  defaults: {
    name: 'Продукт'
  }
});

SBApp.Cart  = Backbone.Model.extend({
  urlRoot: '/api/v1/cart/',

  defaults: {
    name: 'Корзина'
  },

  isEmpty: function () {
    var items = [];

    if (this.has('items') ) {
      items = this.get('items');
    }

    return !items.length > 0;
  },

  /* Возвращает итоговую сумму. Если есть скидка -> итоговую сумму со скидкой */
  totalAmount: function () {
    var amount = this.get('total_sum'),
        discount = this.get('discount');

    /* Если есть скидка */
    if ( discount && amount !== 0 ) {
      amount = discount.price;
    }
    return amount;
  },

  getItemsCount: function () {
    var items = this.get('items'),
        itemsCount = 0;

    _.each(items, function (item) {
      itemsCount += item.count;
    });

    return itemsCount;
  },

  /* Получить новую корзину. Необходима после создания заказа, старая корзина удаляется */
  getNew: function () {
    this.fetch({
      url: '/api/v1/cart/?format=json',
      reset: true,
      cache: true
    });
  },

  parse: function (resp) {
    var json = resp;

    if( !_.isUndefined(resp['objects']) && _.isArray(resp['objects']) ) {
      json = resp['objects'][0];
    }

    return json;
  },

  /* TODO: добавить возможность добавлять callback, если передается
   Перезапросить корзину */
  update: function (options) {
    var do_render = options.do_render;

    this.fetch({
      cache: false,
      reset: true,
      do_render: do_render
    });
  }
});

//======================
//    ПВЗ
//======================
SBApp.DeliveryCity = Backbone.Model.extend({
  defaults: {
    name: 'Город доставки'
  }
});

SBApp.DeliveryPoint = Backbone.Model.extend({
  defaults: {
    name: 'Пункт выдачи заказов'
  }
});

SBApp.SearchResultsItem = Backbone.Model.extend({
  defaults: {}
});


/* TODO: подумать над этим! */
SBApp.AppModel = Backbone.Model.extend({
  defaults: {
  }
});

SBApp.Comment = Backbone.Model.extend({
  url: '/api/v1/comments/'
});

SBApp.User = Backbone.Model.extend({
  urlRoot: '/api/v1/customer/',

  defaults: {
    email: '',
    page_click: 0
  },

  parse: function (resp) {
    return resp.objects[0] || resp;
  },

  changeCartId: function (model, cart_id, options) {
    this.set('cart_id', cart_id);
  },

  validate: function (attrs, options) {
    if( _.isEmpty(attrs.email) ) {
      return 'Укажите email';
    }

    else if ( attrs.email && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(attrs.email) ) {
      return 'Неверный формат';
    }
  }
});

SBApp.CallbackOrder = Backbone.Model.extend({
  url: '/api/v1/callback-order/'
});

SBApp.FastOrder = Backbone.Model.extend({
  url: '/api/v1/fast-order/'
})
