SBApp.module("OrderApp.Controller", function(Controller, ContactManager, Backbone, Marionette, $, _) {
  /* Заказ Отправлен */
  Controller.showOrderSend = function(id) {
    var fetchingOrder = SBApp.request('order:entity', id);

    $.when( fetchingOrder).done( function (order) {
      var view;

      if (order === undefined) {
        view = new SBApp.NotFoundView({ text: 'Заказ не найден' });
      }

      else {
        view = new SBApp.OrderView({ model: order, status: 'order-send' });
      }

      SBApp.contentRegion.show( view );
    });
  };

  Controller.showOrderFailed = function (id) {
    var order = new SBApp.Entities.Order({id: id});

    SBApp.contentRegion.show( new SBApp.OrderView({ model: order, status: 'order-failed' }) );
  };
});