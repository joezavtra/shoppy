/* Отвечает за все, что связано с заказами */
SBApp.module('OrderApp', function (OrderApp, SBApp, Backbone, Marionette, $, _) {
  OrderApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      'order/:id/send'   : 'orderSend',
      'order/:id/failed' : 'orderFailed'
    }
  });

  var API = {
    orderSend: function (id) {
      OrderApp.Controller.showOrderSend(id);
    },

    /* Если оплата заказа не прошла */
    orderFailed: function (id) {
      OrderApp.Controller.showOrderFailed(id);
    }
  }

  SBApp.addInitializer(function () {
    new OrderApp.Router({ controller: API });
  });
});