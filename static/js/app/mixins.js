SBApp.Mixins = {};

SBApp.Mixins.Product = {

  /* Переход на карточку товара */
  productProfile: function (e) {
    e.preventDefault();
    SBApp.activeProduct = this.model.get('id');

    $('#nav-menu').find('.active').removeClass('active');

    SBApp.router.changeURL('product/'.concat(SBApp.activeProduct), true);
  },

  profileFromCart: function (e) {
    e.preventDefault();
    SBApp.activeProduct = this.model.get('product')['id'];

    $('#nav-menu').find('.active').removeClass('active');

    SBApp.router.changeURL('product/'.concat(SBApp.activeProduct), true);
  }
};

SBApp.Mixins.Banner = {
  productProfile: function (e) {
    var product;

    e.preventDefault();

    if( !this.model.has('product') ) {
      return;
    }

    product = this.model.get('product');
    SBApp.activeProduct = product['id'];

    $('#nav-menu').find('.active').removeClass('active');
    SBApp.router.changeURL('product/'.concat(SBApp.activeProduct), true);
  }
};

SBApp.Mixins.VK = {
  changeRoute: function (e) {
    e.preventDefault();
    e.stopPropagation();

    var $target = $(e.currentTarget);
    var url = $target.prop('href').split('#')[1];

    VK.callMethod('setLocation', url, true);
    VK.callMethod('setTitle', $target.data('title'));
  }
}