SBApp.productListOrderBy = 'name';

/* Вид списка продуктов */
SBApp.ProductListView = Backbone.View.extend({
  className: 'SBproductList',

  type: SBApp.Templates.ProductListView.type,

  events: {
    'click .js-toolbar__link-type-switcher' : 'changeListType',
    'change #toolbar__item-select'          : 'changeSort'
  },

  activeClassName: 'toolbar__link_active',

  activeSelector: '.toolbar__link_active',

  fetchingProducts: function () {
    var defer = $.Deferred();

    SBApp.productList.fetch({
      reset: true,
      success: function (collection, resp, options) {
        defer.resolve(collection);
      }
    });

    return defer.promise();
  },

  changeSort: function (event) {
    var orderBy = $(event.currentTarget).val(),
        that = this;

    SBApp.productsOrderBy = orderBy;
    SBApp.productList.offset = 0;

    $.when( this.fetchingProducts() ).done(function () {
      that.state.set('sort_type', orderBy);
    });
  },

  isTypeActive: function (display_type) {
    return display_type === SBApp.productListType;
  },

  changeListType: function (event) {
    var $button = $(event.currentTarget),
        display_type = $button.data('listType'),
        activeSelector = this.activeSelector,
        activeClass = this.activeClassName;

    if (this.isTypeActive(display_type)) {
      return;
    }

    SBApp.productListType = $button.data('listType');
    $button.siblings(activeSelector).removeClass(activeClass);
    $button.addClass(activeClass);

    this.renderProducts();
  },

  initialize: function () {
    var toolbarTemplate = _.template(SBApp.Templates.ProductListView.toolbar);

    this.state = new Backbone.Model({
      defaults: {
        sort_type: SBApp.productListType
      }
    });

    this.$parent = $('.SBcontent');

    SBApp.productList = new SBApp.ProductList();

    this.listenToOnce(SBApp.productList, 'sync', this.render);
    this.listenTo(SBApp.productList, 'add', this.addProductItem);
    this.listenTo(this.state, 'change:sort_type', this.resort);

    SBApp.productList.fetch({reset: true});


    this.$el.empty();

    /* Тулбар */
    this.$el.append(toolbarTemplate);

    /* Блок для списка товаров */
    this.$el.append('<div class="row SBproductListContainer" id="products" />');

    this.$parent.append(this.$el);
    this.$productsContainer = this.$el.find('.row');

   /*TODO: Так делать плохо! */
   if($('#sb-banner-slot-3').length === 0) {
     this.$parent.prepend('<div id="sb-banner-slot-3" class="sb-banner-slot-3" />');
     SBApp.trigger('banner:load', 'slot3');
   }
  },

  isPaginationNeeded: function () {
    return SBApp.productList.getParam('total_count') !== SBApp.productList.length;
  },

  resort: function (model, value) {
    this.listenToOnce(SBApp.productList, 'sync', this.renderProducts);
  },

  renderProducts: function () {
    this.$productsContainer.empty();

    /* Вывод каждого продукта */
    SBApp.productList.each(this.addProductItem, this);

    if( this.isPaginationNeeded() ) {
      this.$pagination.stopListening();
      this.$pagination.remove();

      this.$pagination = new SBApp.InfinitePaginationView({ collection: SBApp.productList, container: this.$el.find('.row') });
      this.$el.append( this.$pagination.render().el );
    }
  },

  render: function () {
    this.$productsContainer.empty();

    /* Вывод каждого продукта */
    SBApp.productList.each(this.addProductItem, this);

    /* Отрисовка страниц */
    if( this.isPaginationNeeded() ) {
      if( this.$pagination ) {
        this.$pagination.stopListening();
        this.$pagination.remove();
      }

      this.$pagination = new SBApp.InfinitePaginationView({collection: SBApp.productList, container: this.$el.find('.row') });
      this.$el.append( this.$pagination.render().el );
    }


    return this;
  },

  addProductItem: function (item) {
    var productView = new SBApp.ProductView({ model: item });

    this.$productsContainer.append( productView.render().el );
  }
});

SBApp.ProductView = Marionette.ItemView.extend({
  events: {
    'click .SBcartAdd'        : 'addProductToCart',
    /* SBApp.Mixins.Product.productProfile */
    'click .product__details' : 'productProfile',
    'click .product-item'     : 'productProfile',
    'click .js-show-modal'    : 'showModal'
  },

  className: function(){ return this[ SBApp.productListType ].className; },

  grid: {
    className: SBApp.Templates.ProductView.grid.className,
    template: _.template(SBApp.Templates.ProductView.grid.body)
  },

  detail: {
    className: SBApp.Templates.ProductView.detail.className,
    template: _.template(SBApp.Templates.ProductView.detail.body)
  },

  showModal: function (e) {
    e.preventDefault();
    e.stopPropagation();

    SBApp.trigger('modal:show', this.model,  { title: 'Перезвоните мне', modal_type: 'callback-order'});

    /* Открытие окна перезвоните */
    ga && ga('send','event','button','click','callback_open');

    SBApp.trigger('segment:io:track', 'Opened Call Me Back');
  },

  addProductToCart: function (event) {
    event.preventDefault();
    event.stopPropagation();

    var cartItem = new SBApp.CartItem();

    /* Google Analitycs Добавление из каталога*/
    ga && ga('send','event','button','click','add_cart_catalog');

    SBApp.trigger('segment:io:track', 'Added Product', SBApp.getProductProps(this.model));

    cartItem.save(
      { product_id: this.model.get('id'), original_id: this.model.get('oid'), cart_id: SBApp.cart.get('id') },
      { complete: function () {
        SBApp.cartView.onProductAdd();
      }}
    );
  },

  render: function () {
    var info = this[ SBApp.productListType ],
        desc = $.trim( this.model.get('desc') ),
        price_str = SBApp.getPriceWithCurrency( this.model.get('price') ),
        tmp;

    this.className = info.className;
    this.template = info.template;

    /* Передавать в шаблон только нужные данные */
    tmp = $( this.template({
      name: this.model.get('name'),
      cover: this.model.get('cover'),
      desc: desc,
      model_oid: this.model.get('model_oid'),
      price_str: price_str
    }));

    this.$el.append(tmp);

    return this;
  }
});

/* Хлебные крошки, используются в карточке продукта */
SBApp.BreadcrumbView = Backbone.View.extend({
  el: '#product__breadcrumb',
  className: 'breadcrumb',

  findCats: function (categoryId) {
//    var category = SBApp.categoryBlock.collection.findWhere({ id: categoryId });
    var category = this.categories.findWhere({ id: categoryId });

    if ( !_.isUndefined(category) ) {
      this.breadcrumbList.unshift(category);

      if( !_.isNull( category.get('parent') ) ) {
        categoryId = category.get('parent');
        this.findCats(categoryId);
      }
    }
  },

  initialize: function () {
    var categoryId = this.model.getCategoryId();

    /* Список всех категорий */
    this.categories = this.options.categories;
    this.breadcrumbList = new Backbone.Collection;

    this.findCats(categoryId);
  },

  addBreadcrumbItem: function (model, key) {
    var len = this.breadcrumbList.length,
        separator = (key !== len - 1 || len === 1) ? '/' : '',
        view;

    view = new SBApp.BreadcrumbItemViewLink({ model: model, separator: separator });
    this.$el.append( view.render() );
  },

  render: function () {
    var view;

    this.breadcrumbList.each(this.addBreadcrumbItem, this);

    if(this.breadcrumbList.length === 1){
      view = new SBApp.BreadcrumbItemViewActive({ name: this.model.get('name') });
      this.$el.append( view.render() );
    }

    return this;
  }
});

SBApp.BreadcrumbItemViewLink = Backbone.View.extend({
  tagName:  'li',
  template: JST['breadcrumbs:item:link'],
  events: function () {
    var events = {};

    if( SBApp.Platform.isVK()) {
      events['click a'] = 'changeRoute';
    }

    return events;
  },
  render: function () {
    this.$el.append(this.template( {category: this.model.toJSON(), separator: this.options.separator }));

    return this.$el;
  }
});

SBApp.BreadcrumbItemViewActive = Backbone.View.extend({
  tagName:  'li',
  className: 'active',
  template: JST['breadcrumbs:item:active'],
  render: function () {
    this.$el.append(this.template( this.product ));

    return this.$el;
  },

  initialize: function (json) {
    this.product = json;
  }
});


/* Вид карточки продукта */
SBApp.ProductFullView = Marionette.ItemView.extend({
  container: '.SBcontent',
  className: 'SBproductFull',

  events: {
    'click .variant-list-item'      : 'changeVariant',
    'click #SBcartAdd'              : 'addProductToCart',
    'click #add-comment__btn'       : 'addComment',
    'keyup #add-comment__textarea'  : 'onTextareaKeyUp'
  },

  changeButtonState: function () {
    var $addButton = this.$el.find('#SBcartAdd'),
        isButtonDisabled = $addButton.prop('disabled');

    if(isButtonDisabled){
      $addButton.prop('disabled', false);
    }
  },

  changeVariant: function (event) {
    var $target = $(event.target),
        $parent = $target.parent('li'),
        oid = $target.data('oid');

    if($parent.hasClass('active')){
      return false;
    }

    this.$el.find('#SBcartAdd').data('oid', oid);
    $parent.siblings('.active').removeClass('active').end().addClass('active');
    this.changeButtonState();
  },

  addProductToCart: function (event) {
    var $addButton = this.$el.find('#SBcartAdd'),
        productProps = { id: $addButton.data('pid'), name: this.model.get('name'), price: this.model.get('price') },
        cartItem;

    /* Google Analytics - Добавление с карточки товара */
    ga && ga('send','event','button','click','add_cart_item');

    /* Не используется getProductProps, если выбран товар с параметром, id будет равен pid. Пока оставил так. */
    SBApp.trigger('segment:io:track', 'Added Product', productProps);

    cartItem = new SBApp.CartItem();
    cartItem.save({
      product_id: $addButton.data('pid'), original_id: $addButton.data('oid'), cart_id: SBApp.cart.get('id')
    },{
      complete: SBApp.cartView.onProductAdd
    });
  },

  disableAddCommentButton: function () {
    this.$el.find('#add-comment__btn').addClass('disabled');
  },

  enableAddCommentButton: function () {
    this.$el.find('#add-comment__btn').removeClass('disabled');
  },

  onTextareaKeyUp: function (e) {
    var $addButton = this.$el.find('#add-comment__btn');
    var textValue = $.trim($(e.currentTarget).val());

    if(!_.isEmpty(textValue)){
      this.enableAddCommentButton();
    }
    else{
      this.disableAddCommentButton();
    }
  },

  preload: function (images) {
    var imgObj;

    _.each(images, function (image) {
      imgObj = new Image();
      imgObj.src = image;
    }, this);
  },

  /* model передается при переходе из Поиска в карточку товара */
  initialize: function () {
    var speakersId = this.model.getSpeakersId( SBApp.Platform.current() ),
        self = this;

    this.product = this.model;
    this.tab = (this.product.get('tab')) ? this.product.get('tab') : 'first';
    this.preload( this.model.get('images') );

    if (SBApp.Platform.isVK()) {
      this.prepareCommentsData(speakersId.join(','));
    }

    else if(SBApp.Platform.isFacebook()){
      this.getFBUsers(speakersId.join(','), 'name');
    }

    else if(SBApp.Platform.isOK()) {
      Social.OK.getUsers("name,gender,age,pic50x50", speakersId.join(','), function (method, resp, data) {
        self.addProfiles(resp);
        self.render();
      });
    }

    else{
      this.render();
    }
  },

  /* @param {String} socialIds - '2324,23423,11,2333' */
  prepareCommentsData: function (socialIds) {
    /* Получить данные по комментаторам */
    if (!_.isEmpty(socialIds)) {
      this.getVkUsers(socialIds, "photo_50");

      return;
    }
    this.render();
  },

  /* @param {Object} profiles - объект, кот. возвращает соц. сеть
  *  Дописывает в модель продукта профиль соц. сети
  *  TODO: кривая реализация добавления профиля, переделать
  * */
  addProfiles: function (profiles) {
    var comments = this.product.getComments(),
        profile,
        field = SBApp.Platform.current() + '_id',
        param = (SBApp.Platform.isOK()) ? 'uid' : 'id';

    _.each(comments, function (comment) {
      profile = _.find(profiles, function(profile) {
          if(comment.customer && comment.customer[field]) {
            return comment.customer[field] == profile[param];
          }
      });

      if(_.isObject(comment.customer)){
        comment.customer["profile"] = profile;
      }
    });
  },

  /* 1. Получить id комментаторов Facebook'a -> сформировать ids=""
     2. Получить профили комментаторов на Facebook
     3. Вывести комментарии
   */
  getFBUsers: function (uids, fields) {
    var param = (fields) ? '&fields='.concat(fields) : '';
    var self = this;

    FB.api('/?ids=' + uids + param, function(response) {
      self.addProfiles(response);
      self.render();
    });
  },

  getVkUsers: function (ids, fields) {
    var self = this,
        fetchingUsers = SBApp.request('vk:users:get', ids, fields);

    $.when( fetchingUsers ).done(function (users) {
      if (users) {
        self.addProfiles(users);
        self.render();
      }
    });

    return;
  },

  sendComment: function (commentSettings) {
    $.ajax({
      type: 'POST',
      url: '/api/v1/comments/',
      data: commentSettings,
      contentType: 'application/json',
      dataType: 'json',
      cache: false,
      complete: function (data) {
        var newProduct = new SBApp.Product();
        newProduct.set('tab', 'last');
        newProduct.fetch({
          reset: true,
          success: function () {
            SBApp.productsBlock = new SBApp.ProductFullView({model: newProduct});
          }
        });
      }
    });

    this.enableAddCommentButton();
  },

  addComment: function (e) {
    var $form = this.$el.find('#add-comment'),
        $textarea = $form.find('textarea'),
        comment = $.trim( $textarea.val() ),
        productId = this.model.get('id'),
        customerName = SBApp.userModel.get('name'),
        dataJSON;

    if(_.isEmpty(comment)){
      return;
    }
    dataJSON = JSON.stringify({ text: comment, product: productId, customer_name: customerName });

    this.sendComment(dataJSON);
    this.disableAddCommentButton();
  },

  breadcrumbs: function (categories) {
    var view = new SBApp.BreadcrumbView({ model: this.model, categories: categories});
    return view.render();
  },

  render: function () {
    var that = this;
    var product = this.product.toJSON();
    var price_str = SBApp.getPriceWithCurrency( this.model.get('price') );
    var Template = JST;
    var productTemplate = Template['product:profile']({
          productInfo: Template['product:profile:info'],
         renderImages: Template['product:profile:images'],
         renderVariants: Template['product:profile:variants'],
        renderSocials: Template['product:profile:socials'],
          renderChars: Template["product:profile:characteristics"],
       renderComments: Template['comments'],
        renderComment: Template['comments:item:user:auth'],
       formAddComment: Template['comments:form:add'],
      buttonAddToCart: Template['button:add-to-cart'],
              product: product,
        commentsCount: this.model.getCommentsCount(),
          productView: this,
            userPhoto: SBApp.userModel.get('photo'),
            price_str: price_str
    });
    this.$el.append(productTemplate);

    var el = $(this.container).empty();
    el.append(this.$el);

    /* Хлебные крошки */
    var fetchingCategories = SBApp.request('category:entities');

    $.when( fetchingCategories).done(function (categories) {
      that.breadcrumbs( categories );
    });

    /* Виджет ВКонтакте */
    if(SBApp.Platform.isVK()){
      this.initVKWidgets();
    }

    /* Виджет Фейсбук */
    if(SBApp.Platform.isFacebook()) {
      /* Шары лайки */
      SBApp.execute('fb:parse', this.$el.find('#social-btns__fb-like')[0]);
    }

    /* Виджет Одноклассники */
    if(SBApp.Platform.isOK()) {
      Social.OK.shareButton("ok_shareWidget", SBApp.router.getShareUrl(),"{width:145,height:30,st:'oval',sz:20,ck:1}");
    }

//    $('#product-tabs a:' + this.tab).tab('show');
    /* TODO:  Нужно было быстро показывать для Пудры вкладку "Обсуждения" */
    if (SBApp.shop.id === 17) {
      $('.product-discuss').tab('show');
    }
    else{
      $('#product-tabs a:' + this.tab).tab('show');
    }
    $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom({'zoomWidth':480, 'zoomHeight':330, 'adjustX':10});

    /* TODO: это должно быть в контроллере, выпилить отсюда как можно скорее */
    SBApp.trigger('scroll:to', {offset: 0, duration: 500});
  },

  initVKWidgets: function () {
    var btn,
        shareUrl = SBApp.router.getShareUrl();

    /* SBApp.activeProduct - page_id передаем, чтобы сбрасывать кэш */
    VK.Widgets.Like('social-btns__vk-like', {
      type: 'mini',
      pageTitle: this.model.get('name'),
      pageDescription: this.model.get('desc'),
      pageImage: this.model.get('cover'),
      pageUrl: shareUrl
    }, SBApp.activeProduct);

    btn = VK.Share.button({
      url: encodeURIComponent(shareUrl)
    }, {
      text: 'Поделиться',
      type: 'round'
    });
    this.$el.find('#social-btns__vk-share').html(btn);
  }
});

SBApp.CartView = Backbone.View.extend({
  template: JST['cart'],

  container: '.SBcontent',

  className: 'sb-cart',

  events: function () {
    var events = {
      'click #SBcartPrepare' : 'orderForm',
      'click #SBcartOrder'   : 'placeOrder',
      'change input'         : 'onInputBlur',
      'keyup  input'         : 'onInputBlur',
      'change select'        : 'onSelectChange'
    };

    /* Для сервиса Чекаут на кнопке "Оформить заказ" другой обработчик */
    if ( SBApp.services.checkout ) {
      events['click #SBcartPrepare'] =  'checkoutOrderForm';
    }

    return events;
  },

  initialize: function () {
    this.listenTo(SBApp.cart, 'sync', this.render);
  },

  orderFormTempl: JST['cart:order:form'],

  onSelectChange: function (e) {
    var $select = this.$( e.currentTarget );
    var $submitBtn = this.$el.find('#SBcartOrder');

    $select.parents('.control-group').removeClass('success');

    /* Select обязателен для заполнения и не выбран */
    if ($select.data('required') && $select.val() === 'None') {
      this.showControlError($select, 'Вы не выбрали город');
      $select.data('valid', false);
    }
    else {
      this.hideControlError($select);
      $select.data('valid', true);
      this.showControlSuccess($select);
    }

    $submitBtn.prop('disabled', (this.getNotValidFields() > 0) ? 'disabled' : '');
  },

  /* Обработчик по добавлению в корзину */
  onProductAdd: function () {
    SBApp.cart.fetch({
      cache: false,
      do_render: false,
      success: function (cart, resp, options) {
        /* 1. Отрисовать модальное окно и его содержимое */
        SBApp.trigger('modal:show', cart, { title: 'Товар добавлен', modal_type: 'cart-preview' });
//        SBApp.Popover.showOnCartAdd();
      }
    });
  },

  /* Сервис Checkout: форма оформления заказа */
  checkoutOrderForm: function () {
    var self = this,
        order = new SBApp.Order(),
        cart_id = SBApp.cart.get('id');

    order.save({
      cart: cart_id,
      app_type: SBApp.Platform.getType(),
      coupon: '',
      phone: '',
      email: '',
      comment: ''
    }, {
      cache: false,
      success: function (orderModel, resp, options) {
        var orderId = orderModel.get('id'),
            cart = orderModel.get('cart'),
            orderForm;

        /* Если есть скидка добаляем в форму чекаут как продукт с отриц. суммой, discount.price - cart.total_sum */
        if(cart['discount']) {
          cart['discount']['price'] -= cart['total_sum'];
        }

        orderForm = JST['form:order:checkout']({
          orderId     : orderId,
          cartItems   : cart['items'],
          discount    : cart['discount'] || 0,
          ticket      : orderModel.get('checkout_ticket'),
          callbackUrl : SBApp.router.getCheckoutUrl(orderId),
          hidden      : true
        });

        self.$el.append(orderForm);

        window.order_checkout('SBorderFormCheckOut');
        SBApp.cart.getNew();
        SBApp.router.changeURL(['order/', order.get('id'), '/delivery'].join(''), true);
      }
    });
    ga && ga('send','event','button','click','order_start');
  },

  /* Форма оформления заказа */
  orderForm: function (event) {
    var self = this,
        userName = SBApp.userModel.get('name'),
        validUserName = !_.isEmpty(userName),
        formTemplate = this.orderFormTempl({ username: userName, valid_username: validUserName });

    this.$el.find('#cart-items').after( formTemplate );
    $('#SBcartPrepare').parent('div').slideUp('fast');
    $('#SBorderForm').slideDown('fast');
    $('#SBorderPhone').mask( SBApp.config['PHONE_MASK'] );

    SBApp.deliveryCity = undefined;
    SBApp.deliveryCityBlock = new SBApp.DeliveryCityListView();
    SBApp.deliveryPointBlock = new SBApp.DeliveryPointListView();

    $(".SBdeliveryChoose, .SBdeliveryChange").on('click', function () {
      self.$el.find('.SBdeliveryChoose').hide();
      self.$el.find('.SBdelivery').slideDown('fast');
    });

    /* Google Analitics "Оформить заказ"*/
    ga && ga('send','event','button','click','order_start');
  },

  showControlError: function ($control, errorMessage) {
    $control.parents('.control-group').addClass('error');
    $control.siblings('.help-inline').html(errorMessage).css('display', 'block');
  },

  showControlSuccess: function ($control) {
    $control.parents('.control-group').addClass('success');
  },

  hideControlError: function ($control) {
    $control.parents('.control-group').removeClass('error');
    $control.siblings('.help-inline').hide();
  },

  /* Возвращает кол-во не валидных полей */
  getNotValidFields: function () {
    var nVF = 0;
    var $controls = this.$('#SBorderForm input, #SBorderForm select');

    $controls.each(function (key, control) {
      var $control = $(control);
      var required = $control.data('required');
      var controlIsValid = $control.data('valid');
      /* Поля, обязательные для заполнения */
      if(required && !controlIsValid){
        nVF += 1;
      }
    });

    return nVF;
  },

  onInputBlur: function (event) {
    var $input = $(event.target),
        val = $.trim($input.val()),
        required = $input.data('required'),
        controlType = $input.data('type');

    $input.parents('.control-group').removeClass('success');
    /* Обязательное поле пустое */
    if (_.isEmpty(val) && required) {
      // this.showControlError($input, SBApp.Templates.Validator.controlRequired);
      $input.data('valid', false);
    }

    /* Email поле */
    else if (controlType === 'email' && required && !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val)) {
      this.showControlError($input, SBApp.Templates.Validator.controlEmailNotValid);
      $input.data('valid', false);
    }

    /* Phone поле */
    else if (controlType === 'phone' && required && !/^[\s()+-]*([0-9][\s()+-]*){6,20}$/.test(val)) {
      this.showControlError($input, SBApp.Templates.Validator.controlPhoneNotValid);
      $input.data('valid', false);
    }

    /* Если дошли сюда, значит поле валидное */
    else {
      if (required) {
        this.showControlSuccess($input);
      }

      this.hideControlError($input);
      $input.data('valid', true);
    }

    /* Проверяем, все ли поля валидны */
    this.$el.find('#SBcartOrder').prop('disabled', (this.getNotValidFields() > 0) ? 'disabled' : '');
  },

  placeOrder: function(event) {
    var order = new SBApp.Order(),
        $city = this.$el.find('#sb-order-city'),
        address = this.$el.find('#SBorderAddress').val(),
        phone = this.$el.find('#SBorderPhone').val().replace(/[\-\s]/g, ''); /* Избавляемся от символов маски */

    /* Если есть поле город и не пустое, конкатинируем с адресом и сохр. в address
     * есть в магазина сушигелери и сушисити */
    if ( $city && $.trim($city.val()) ) {
      address = $city.val() + ', ' + address;
    }

    order.save({
      'cart'  : SBApp.cart.get('id'),
      'app_type': SBApp.Platform.getType(),
      'name': this.$el.find('#SBorderName').val(),
      'phone': phone,
      'email': this.$el.find('#SBorderEmail').val(),
      'address': address,
      'comment': this.$el.find('#SBorderComment').val(),
      'coupon': '',
      'deliveryPoint': this.$el.find('#SBdeliveryPoint').val()
    },
    {
      success: function (model, resp, options) {
        var orderId = model.get('id'),
            cart = model.get('cart'),
            orderProps = SBApp.getOrderProps( model );

        SBApp.cart.getNew();
        SBApp.router.changeURL('order/'+ orderId +'/send', true);

        SBApp.trigger('segment:io:track', 'Completed Order', orderProps);
      }
    });

    $(event.currentTarget).prop('disabled', 'disabled');

    /* Клик по кнопке "Заказ оформлен" Google Analitycs */
    ga && ga('send','event','button','click','order_finish');
  },

  initialize: function () {
    this.listenTo(SBApp.cart, 'sync', this.fire);

    SBApp.cart.update({do_render: false});
  },

  fire: function(model, resp, options) {
    var do_render = options.do_render;

    if(do_render){
      this.render();
    }
  },

  render: function () {
    this.$el.empty();
    var tmp = this.template({ cart: SBApp.cart.toJSON(), orderForm: this.orderFormTempl });
    var itemView;

    /* Тело корзины */
    this.$el.append(tmp);

    var $container = $(this.container);
    var $cartList = this.$el.find('#cart-items');
    var footView;

    /* Очищаем основной контейнер */
    $container.empty();

    /* json айтемов корзины */
    _.each(SBApp.cart.get('items'), function (cartItem) {
      itemView = this.renderItem(cartItem);
      itemView.appendTo($cartList);
    }, this);

    /* Cart Summary */
    footView = new SBApp.CartFootView({ model: SBApp.cart });
    this.$el.find('thead').after( footView.render().el );
    $container.append(this.$el);

    // Блок баннера
    this.$el.append('<div id="sb-banner-slot-2" class="sb-banner-slot-2"></div>');
    SBApp.trigger('banner:load', 'slot2');


    /* Cart Recommendations */
    var recommendList = new SBApp.CartRecommendList();

    /* Запрашиваем рекоммендации для блока корзины */
   recommendList.fetch({
     success: function (list, resp, options) {
       var recommendView;

       if( !list.isEmpty() ) {
         recommendView = new SBApp.CartRecommendView({ collection: list });

         $container.append( recommendView.render().el );
       }
    }
   });

    this.delegateEvents();
  },

  /* Принимает json айтема корзины */
  renderItem: function (cartItem) {
    var itemModel = new SBApp.CartItem(cartItem),
        itemView = new SBApp.CartItemView({ model: itemModel });

    return itemView.render();
  }
});

SBApp.CartFootView = Backbone.View.extend({
  tagName: 'tfoot',

  className: 'sb-cart__summary',

  template: JST['cart:foot'],

  events: {
    'click #promocode-apply' : 'applyPromocode',
    'keyup #promocode-field' : 'checkPromocode',
    'blur  #promocode-field' : 'checkPromocode'
  },

  initialize: function () {
    this.listenTo(SBApp.cart, 'sync', this.render);
  },

  render: function () {
    var discount = this.model.get('discount'),
        total = this.model.get('total_sum'),
        templ = $(this.template({discount: discount, total: total}));

    this.$el.empty().append(templ);

    return this;
  },

  /* Применить промокод */
  applyPromocode: function (e) {
    var $button = $(e.currentTarget);

    if($button.hasClass('disabled')){
      return;
    }

    var promocode = $.trim(this.$el.find('#promocode-field').val());

    SBApp.cart.save({
      coupon: promocode,
      app_type: SBApp.Platform.getType()
    }, { do_render: true });
  },

  checkPromocode: function (e) {
    var promocode = $.trim($(e.currentTarget).val()),
        method = (_.isEmpty(promocode) ? 'disablePromocodeButton' : 'enablePromocodeButton');

    /* Хипстер хак #1 в действии */
    this[method]();
  },

  enablePromocodeButton: function () {
    this.$el.find('#promocode-apply').removeClass('disabled');
  },

  disablePromocodeButton: function () {
    this.$el.find('#promocode-apply').addClass('disabled');
  }
});

SBApp.CartItemView = Backbone.View.extend({
  tagName: 'tr',

  className: 'sb-cart__item',

  events: {
    'click .js-decrement'  : 'decrement',
    'click .js-increment'  : 'increment',
    'click .js-trash'      : 'trash',
    /* SBApp.Mixins.Product.profileFromCart */
    'click .js-product-profile' : 'profileFromCart'
  },

  template: JST['cart:item'],

  getTemplateSettings: function () {
    return {
      id: this.model.get('id'),
      cover: this.model.get('product')['cover'],
      product: this.model.get('product'),
      offer: this.model.get('offer'),
      count: this.model.get('count'),
      price_with_discount:  this.model.get('price_with_discount')
    };
  },

  render: function () {
    var templ = $(this.template( this.getTemplateSettings() ));

    this.$el.empty().append(templ);

    return this.$el;
  },

  trash: function () {
    var that = this,
        product = this.model.get('product'),
        productProps = { id: product['id'], name: product['name'], quantity: this.model.get('count'), price: this.model.get('price_with_discount')['price'] };

    SBApp.trigger('segment:io:track', 'Removed Product', productProps);

    this.model.destroy({
      success: function () {
        SBApp.cart.fetch({
          do_render: true,
          success: function () {
            that.stopListening();
            that.remove();
          }
        });
      }
    });
  },

  decrement: function (event) {
    var $target = $(event.currentTarget),
        count = this.model.get('count'),
        that = this;

    event.preventDefault();
    $target.removeClass('js-decrement');

    /* Последний айтем корзины удаляем из списка */
    if(count === 1) {
      this.trash();
      return;
    }

    this.model.save({count: count - 1}, {
      success: function () {
        SBApp.cart.fetch({
          do_render: false,
          success: function () {
            that.render();
          }
        });
      }
    });
  },

  increment: function (event) {
    var $target = $(event.currentTarget),
        count = this.model.get('count'),
        that = this;

    event.preventDefault();
    $target.removeClass('js-increment');

    this.model.save({count: count + 1}, {
      success: function () {
        SBApp.cart.fetch({
          do_render: false,
          success: function () {
            that.render();
          }
        });
      }
    });
  }
});

SBApp.DeliveryCityListView = Backbone.View.extend({
  container: '.SBdelivery',
  tagName: 'select',
  className: 'SBdeliveryCity offset2',

  events: {
    "change": "chooseCity"
  },

  initialize: function () {
    var el = $(this.container).empty();
    el.append(this.$el);

    SBApp.deliveryCityList = new SBApp.DeliveryCityList();
    SBApp.deliveryCityList.fetch({reset:true});
    this.listenTo(SBApp.deliveryCityList, 'reset', this.render);
  },

  render: function () {
    this.$el.empty();

    SBApp.deliveryCityList.each(function (item) {
      var city = this.renderItem(item);
      this.$el.append(city);
    }, this);

    this.delegateEvents(this.events);
  },

  renderItem: function (item) {
    var deliveryCityView = new SBApp.DeliveryCityView({ model: item });
    var deliveryCity = deliveryCityView.render();
    return deliveryCity;
  },

  chooseCity: function (event) {
    var $el = $(event.currentTarget);
    SBApp.deliveryCity = $el.val();

    SBApp.deliveryPointBlock.refresh();
  }

});

SBApp.DeliveryCityView = Backbone.View.extend({
  template: _.template(SBApp.Templates.DeliveryCityView.body),

  render: function () {
    var item = this.model;
    var json = item.toJSON();

    var city = this.template(json);
    if ( !SBApp.deliveryCity ) {
      SBApp.deliveryCity = item.get('id');
      SBApp.deliveryPointBlock.refresh();
    }

    return city;
  }
});

/* Вьюха с картой */
SBApp.DeliveryPointListView = Backbone.View.extend({
  container: SBApp.Templates.DeliveryPointListView.container,
  tagName: SBApp.Templates.DeliveryPointListView.tagName,
  className: SBApp.Templates.DeliveryPointListView.className,
  id: SBApp.Templates.DeliveryPointListView.className,
  lat: 0,
  lng: 0,

  events: {
    "click .SBdeliveryChoice": "choosePoint"
  },

  initialize: function () {
    SBApp.deliveryPointList = new SBApp.DeliveryPointList();
    this.listenTo(SBApp.deliveryPointList, 'reset', this.fillMarkers);
    this.render();
  },

  render: function () {
    var el = $(this.container);

    /* Не рисуем карту, если нет контейенера */
    if(!el[0]){
      return;
    }

    this.$el.empty();
    el.append(this.$el);
    this.createMap();
  },

  createMap: function () {
    this.map = new ymaps.Map(this.id, {
      center: [ymaps.geolocation.latitude, ymaps.geolocation.longitude],
      zoom: 10
    });
    this.map.controls.add('zoomControl');
  },

  refresh: function(){
    SBApp.deliveryPointList.fetch({reset:true});
  },

  fillMarkers: function(){
    var points = [];
    if(this.clusterer){
        this.clusterer.removeAll();
    }
    this.clusterer = new ymaps.Clusterer({
      clusterDisableClickZoom: true,
      preset: 'twirl#darkorangeDotIcon'
    });

    this.lng = 0;
    this.lat = 0;
    SBApp.deliveryPointList.each(function (item) {
      points.push(this.renderItem(item));
      this.lat += item.get('lat');
      this.lng += item.get('lng');
      // console.log(this.lat+" "+this.lng);
      // console.log(item.get('lat')+" "+item.get('lng'));
    }, this);
    this.lat /= SBApp.deliveryPointList.length;
    this.lng /= SBApp.deliveryPointList.length;
    this.map.setCenter([this.lat,this.lng]);
    this.map.panTo([
        [this.lat,this.lng]
    ], {
        duration: 300
    });

    this.clusterer.add(points);
    if ( this.map != undefined ){
      this.map.geoObjects.add(this.clusterer);
    }
  },

  renderItem: function (item) {
    var deliveryPointView = new SBApp.DeliveryPointView({ model: item });
    var deliveryPoint = deliveryPointView.render();

    return deliveryPoint;
  },

  choosePoint: function (event) {
    event.preventDefault();

    var $el = $(event.currentTarget);
    var id = $el.data('id');
    var point = SBApp.deliveryPointList.get(id);
    var address = point.get('name') + " " + point.get('json_data')['address'];

    $('.SBdelivery').slideUp('fast');
    $('.SBdeliveryChange').show();
    $('.SBdeliveryAddress').html( address );
    $('.SBorderAddress').val( address).data('valid','true').change();
    $('.SBdeliveryPoint').val( point.get('id') );
  }

});

SBApp.DeliveryPointView = Backbone.View.extend({
  tagName: SBApp.Templates.DeliveryPointView.tagName,
  className: SBApp.Templates.DeliveryPointView.className,
  template: _.template(SBApp.Templates.DeliveryPointView.body),

  render: function () {
    var item = this.model;
    var json = item.toJSON();
    var pointInfo = item.get('json_data');
    var point = new ymaps.Placemark([item.get('lat'), item.get('lng')], {
      // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
      balloonContentHeader: item.get('name'),
      balloonContentBody: this.template(json),
      hintContent: pointInfo['address']
    }, { preset: 'twirl#darkorangeDotIcon' });

    return point;
  }
});

/* json.content = json.content.replace("\n\r","<br><br>"); */
SBApp.NewsItemView = Marionette.ItemView.extend({
  className: 'sb-news-list__item',

  template: JST['news:item'],

  templateHelpers: function () {
    return {
      dateAsString: function () {
        return SBApp.getDateAsString( this.cdate, '', true);
      }
    }
  }
});

SBApp.NewsView = Marionette.CompositeView.extend({
  className: 'sb-news',

  template: JST['news'],

  childView: SBApp.NewsItemView,

  childViewContainer: '.sb-news-list'
});

SBApp.NotFoundView = Marionette.ItemView.extend({
  className: 'sb-page',

  template: JST['page:not:found'],

  onRender: function () {
    var text = this.options.text || 'Такой страницы не существует';

    this.$el.find('h2').text(text);
  }
});

SBApp.NavView = Backbone.View.extend({
  el: '#sb-header',

  events: {
    'click a[data-alias]'    : 'changePage',
    'mouseenter #cart-badge' : 'showCartInfo',
    'mouseleave #cart-badge' : 'hideCartInfo'
  },

  changePage: function (e) {
    var alias = $(e.currentTarget).data('alias');

    if(alias === 'cart'){
      ga && ga('send','event','button','click','cart');
    }

    this.state.set('activePage', alias);
  },

  activateItem: function (model, page) {
    var $menu = this.$el.find('#nav-menu'),
        cssClass;

    if( _.isEmpty(page) ){
      this.deactivateItem();
      return;
    }

    cssClass = '.menu__item-'.concat(page);
    $menu.find('.active').removeClass('active');
    $menu.find(cssClass).addClass('active');
    /* TODO: вынести в событие для вьюхи категорий */
    $('#categories').find('.active').removeClass('active');
  },

  deactivateItem: function () {
    this.$el.find('.active').removeClass('active');
  },

  showCartInfo: function () {
    if(SBApp.cart.isEmpty()){
      return;
    }

    SBApp.Popover.show();
  },

  hideCartInfo: function () {
    if(SBApp.cart.isEmpty()){
      return;
    }

    SBApp.Popover.destroy();
  },

  updateCartAmount: function ( cart_model ) {
    var amount = cart_model.totalAmount();

    this.$el.find('#sb-menu__badge').html( SBApp.getPriceWithCurrency(amount) );
  },

  initialize: function () {
    var searchView;

    this.state = new Backbone.Model({
      defaults: {
        'activePage' : ''
      }
    });

    this.listenTo(this.state, 'change:activePage', this.activateItem);
    this.listenTo(SBApp.cart, 'sync', this.updateCartAmount);

    /* Создание формы поиска */
    searchView = new SBApp.SearchFormView();
    /* пудра */
    if(SBApp.shop.id === 17) {
      this.$el.find('#nav-menu').before(searchView.render().$el.removeClass('pull-right').addClass('pull-left'));
    }
    else{
      this.$el.find('#nav-menu').after( searchView.render().el );
    }
  }
});

//======================
//    Пагинатор
//======================
SBApp.PaginationView = Backbone.View.extend({
  container: '#products-pagination',

  tagName: 'ul',

  titleTempl: JST['pagination:title'],
  nextTempl: JST['pagination:next'],
  prevTempl: JST['pagination:prev'],
  dotsTempl: JST['pagination:dots'],

  events: {
    'click #previous-page' : 'goToPrevious',
    'click #next-page'     : 'goToNext'
  },

  isPageDisabled: function($page){
    return $page.parent().hasClass('disabled');
  },

  goToPrevious: function(event){
    var $page = $(event.target);
    var prevPage = this.currentPage - 1;
    event.preventDefault();

    if(this.isPageDisabled($page) && prevPage === 0){
      return;
    }

    this.$el.find('#page-' + prevPage).trigger('click');
  },

  goToNext: function(event){
    var $page = $(event.target);
    var nextPage = this.currentPage + 1;
    var lastPage = this.numOfPages;
    event.preventDefault();

    if(this.isPageDisabled($page) && nextPage !== lastPage){
      return;
    }

    this.$el.find('#page-' + nextPage).trigger('click');
  },

  /* Стрелочка "Вперед" */
  renderNext: function(){
    var className = (this.currentPage === this.numOfPages) ? 'disabled' : '',
        $next = this.nextTempl({ className: className });

    this.$el.append($next);
  },

  /* Cтрелочка "Назад" */
  renderPrevious: function(){
    var className = (this.currentPage === 1) ? 'disabled' : '',
        $previous = this.prevTempl({className: className});

    this.$el.prepend($previous);
  },

  renderPage: function (page, currentPage, limit, totalCount, numOfPages){
    var self = this;
    var pageView = new SBApp.PageView({page: page, currentPage: currentPage, limit: limit, totalCount: totalCount, numOfPages: numOfPages, parentView: self});

    this.$el.append(pageView.$el);
  },

  renderTitle: function () {
    var totalCount = this.totalCount,
        $title = $( this.titleTempl({totalCount: totalCount}) );

    this.$container.prepend($title);
  },

  initialize: function (settings) {
    this.items = settings.items;
    this.offset = this.items.offset;
    this.limit = this.items.limit;
    this.totalCount = this.items.total_count;
    this.numOfPages = Math.ceil(this.totalCount / this.limit);
    this.currentPage = Math.ceil(this.offset / this.limit) + 1;
    this.$container = (this.items.container) ? $(this.items.parent) : $(this.container);

    this.render();
  },

   renderFirstPage: function(){
    var currentPage = this.currentPage;
    var limit = this.limit;
    var totalCount = this.totalCount;
    var lastPage = this.numOfPages;

    this.renderPage(1, currentPage, limit, totalCount, lastPage);
   },

   renderLastPage: function (){
    var lastPage = this.numOfPages;
    var currentPage = this.currentPage;
    var limit = this.limit;
    var totalCount = this.totalCount;
    var self = this;
    var pageView = new SBApp.PageView({page: lastPage, currentPage: currentPage, limit: limit, totalCount: totalCount, numOfPages: lastPage, parentView: self});

    this.$el.append(pageView.$el);
   },

   renderThreeDots: function(){
    this.$el.append( this.dotsTempl() );
   },

   /* Рисуется страницы между 1-й и последней
    * Например: если страниц 100, рисуется со 2-ой по 99 страницу
     * */
   renderMiddlePages: function(currentPage, limit, totalCount, numOfPages){
    var prevPage = currentPage - 1;
    var nextPage = currentPage + 1;
    var lastPage = numOfPages;

    /* Пример: 2 (Рисуется 1 страница с номер 2)*/
    if (numOfPages === 3) {
      this.renderPage(2, currentPage, limit, totalCount, lastPage);

      return;
    }

    /* Пример: 2 ... */
    if (currentPage === 1) {
      this.renderPage(nextPage, currentPage, limit, totalCount, lastPage);
      this.renderThreeDots();
    }

    /* Пример: (2) 3 */
    else if (currentPage === 2) {
      this.renderPage(currentPage, currentPage, limit, totalCount, lastPage);
      this.renderPage(nextPage, currentPage, limit, totalCount, lastPage);

      /* Пример: (2) 3 ... */
      if (lastPage > 4) {
        this.renderThreeDots();
      }
    }
    /* Пример: ... 3 (4) 5 ... */
    else if (currentPage > 2 && (lastPage - currentPage) >= 2) {
      /* ... */
      if (currentPage > 3) {
        this.renderThreeDots();
      }

      /* 3 (4) 5 */
      this.renderPage(prevPage, currentPage, limit, totalCount, lastPage);
      this.renderPage(currentPage, currentPage, limit, totalCount, lastPage);
      this.renderPage(nextPage, currentPage, limit, totalCount, lastPage);

      /* ... */
      if((lastPage - currentPage) > 2){
        this.renderThreeDots();
      }
    }

    /*  ... 98 (99) */
    else if( (lastPage - currentPage) < 3 && currentPage !== numOfPages ) {
      /* ... */
      if(lastPage > 4){
        this.renderThreeDots();
      }
      /* 98 (99) */
      this.renderPage(prevPage, currentPage, limit, totalCount, lastPage);
      this.renderPage(currentPage, currentPage, limit, totalCount, lastPage);
    }

    /* ... 99 */
    else if(currentPage === numOfPages){
      this.renderThreeDots();
      this.renderPage(prevPage, currentPage, limit, totalCount, numOfPages);
    }
   },

  renderPages: function(){
    var limit = this.limit;
    var totalCount = this.totalCount;
    var currentPage = this.currentPage;
    var lastPage = this.numOfPages;

    /* Первая страничка */
    this.renderFirstPage();

    /* Если страниц > 2-x -> блок страниц между первой и последней */
    if (lastPage > 2) {
      this.renderMiddlePages(currentPage, limit, totalCount, lastPage);
    }

    /* Последняя страница */
    if (lastPage > 1) {
      this.renderLastPage();
    }
  },

  render: function(){
    this.renderPrevious();
    this.renderPages();
    this.renderNext();
    this.$container.html(this.$el);
    this.renderTitle();
  }
});

SBApp.PageView = Backbone.View.extend({
  tagName: 'li',

  template: JST['pagination:item'],

  events: {
    'click [id^="page-"]' : 'goTo'
  },

  goTo: function(event){
    var link = $(event.target),
        offset = link.data('offset');

    event.preventDefault();

    if(this.isActive()){
      return;
    }

    this.items.offset = offset;
    this.items.fetch({reset: true});
  },

  isActive: function () {
    this.$el.hasClass('active');
  },

  initialize: function(settings){
    this.items = settings.parentView.items;

    this.render();
  },

  render: function(){
    var limit = this.options.limit;
    var page = this.options.page;
    var currentPage = this.options.currentPage;
    var offset = (page - 1) * limit;
    var pageTemplate = this.template({offset: offset, page: page});

    if (currentPage === page){
      this.$el.addClass('active');
    }

    this.$el.append(pageTemplate);
  }
});

//======================
//    Заказ
//======================
SBApp.OrderView = Marionette.ItemView.extend({

  initialize: function () {
    var status = this.options.status || 'order-send';

    if (status === 'order-send') {
      this.template = JST['orders:item:send'];
    }
    else if (status === 'order-failed') {
      this.template = JST['orders:item:failed'];
    }
  }
});

/* TODO: Старая реализац. представления, переделать в OrderApp */
SBApp.OldOrderView = Marionette.ItemView.extend({
  el: '.SBcontent',

  renderSuccess: function (orderId) {
    var order = new SBApp.Order({id: orderId});

    var tmpl = _.template(SBApp.Templates.OrderView.orderSuccess);
    this.$el.empty().html( $(tmpl({ orderId: orderId })) );
  }
});

/* Форма поиска в шапке магазина */
SBApp.SearchFormView = Marionette.ItemView.extend({
  id: 'search',

  className: 'search pull-right',

  template: JST['search:form'],

  ui: {
    searchInput : '#search__input'
  },

  events: {
    'typeahead:selected @ui.searchInput' : 'onItemSelect',
    'keypress           @ui.searchInput' : 'onKeypress'
  },

  isTypeaheadEnabled: false,

  onRender: function () {
    this.initTypeahead();
  },

  initTypeahead: function () {
    var limit = 6;

    this.ui.searchInput.typeahead({
      name: 'products',
      valueKey: 'name',
      template: JST['typeahead:item'],
      limit: limit,
      remote: {
        url: '/api/v1/search/product?limit=' + limit +  '&query=%QUERY',
        dataType: 'json',
        filter: function (response){
          return response.objects;
        }
      }
    });

    this.isTypeaheadEnabled = true;
  },

  /* Выбор продукта в suggestions поиска */
  onItemSelect: function (obj, datum) {
    var pid = datum.id;

    SBApp.router.changeURL('product/' + pid, true);

    this.destroyTypeahead();
  },

  destroyTypeahead: function () {
    this.isTypeaheadEnabled = false;

    this.ui.searchInput.val('');
    this.ui.searchInput.typeahead('destroy');
  },

  enterPressed: function (keyCode) {
    var ENTER_KEY = 13;

    return keyCode === ENTER_KEY;
  },

  onKeypress: function (e) {
    var keyCode = e.which,
        queryString = $.trim( this.ui.searchInput.val() );

    SBApp.navView.deactivateItem();

    // Прерываем, если пуста строка
    if( _.isEmpty(queryString) ) {
      return;
    }

    if ( !this.isTypeaheadEnabled ) {
      this.initTypeahead();
      this.ui.searchInput.focus();
    }

    /* Кнопка Enter */
    if ( this.enterPressed(keyCode) ) {
      if (this.isTypeaheadEnabled) {
        this.destroyTypeahead();
      }

      this.search(queryString);
    }
  },

  search: function (queryString) {
    var url = 'search/'.concat(queryString).concat('/results');

    SBApp.router.changeURL(url, true);
  }
});

SBApp.SearchItemView = Backbone.View.extend({
  className: 'search-results__item',
  template: JST['search:results:item'],
  events: {
    /* SBApp.Mixins.Product.productProfile */
    'click' : 'productProfile'
  },
  render: function () {
    var model = this.model.toJSON();
    this.$el.append( this.template(model) );

    return this;
  }
});

SBApp.SearchView = Backbone.View.extend({
  className : 'search-results',
  template: JST['search:results:title'],

  initialize: function () {
    this.listenTo(this.collection, 'sync', this.render);
  },

  render: function () {
    var $content = $('#content');
    var pages;

    this.$el.empty();
    $content.empty();

    if(this.collection.getResultsCount() > this.collection.getLimit()){
      pages = new SBApp.PaginationView({container: '.search-results', items: this.collection});
      pages = $('<div class="pagination pagination-centered" />').append(pages.$el);
    }

    this.$el.append(this.template({query: this.collection}));

    this.collection.each(this.addSearchItem, this);

    $content.append(this.$el);
    $content.append(pages);
  },

  addSearchItem: function (model) {
    var itemView = new SBApp.SearchItemView({ model: model });
    this.$el.append( itemView.render().el );
  }
});

SBApp.OrderListView = Backbone.View.extend({
  tagName: 'ul',
  className: 'order-list unstyled',

  initialize: function () {
    this.orderId = this.options.orderId;
    this.listenTo(this.collection, 'sync', this.render);
    this.collection.fetch();
  },

  render: function () {
    var orderItemView;

    this.$el.empty();
    this.$el.append('<h1>Мои заказы</h1>');
    this.collection.each(function (model) {
      orderItemView = new SBApp.OrderItemView({ model: model });
      this.$el.append( orderItemView.render().el );
    }, this);

    if( this.orderId ) {
      this.$el.find( '.order-'.concat(this.orderId)).find('.order-item__content').show();
    }

    return this;
  }
});

SBApp.OrderItemView = Backbone.View.extend({
  tagName: 'li',
  className: 'order-list__item',
  template: JST['orders:list:item'],

  events: {
    'click .order-item__title': 'collapseContent'
  },

  render: function () {
    var json = this.model.toJSON();

    this.$el.append( $(this.template(json)) );
    this.$el.addClass('order-'.concat(this.model.get('id')));

    return this;
  },

  collapseContent: function () {
    var $content = this.$el.find('.order-item__content');

    if ($content.is(':visible')) {
      $content.hide();
      return;
    }

    $content.show();
  }
});

SBApp.InfinitePaginationView = Backbone.View.extend({
  className: 'sb-pagination__infinite text-center',
  template: JST['pagination:infinite'],

  events: {
    'click .js-show-more' : 'showMore'
  },

  showMore: function (e) {
    var that = this;

    e.preventDefault();
    e.stopPropagation();

    this.$el.find(e.currentTarget).text('Загрузка...');

    this.collection.offset += this.collection.limit;

    this.collection.fetch({
      remove: false,

      success: function (collection, resp, options) {
        var total_count = that.collection.total_count,
            offset = that.collection.offset,
            limit = that.collection.limit;


        if( total_count === that.collection.length ) {
          that.stopListening();
          that.remove();
        }

         that.$el.find(e.currentTarget).text(JST['pagination:infinite:title']);
      }
    });
  },

  initialize: function (options) {
    this.$container = $(options.container);
  },

  render: function () {
    this.$el.append( this.template() );

    return this;
  }
});

SBApp.StaticPageView = Marionette.ItemView.extend({
  className: 'page',

  template: JST['staticpage']
});

/* Пока вью виджета "Подписаться на новости группы" в ФБ.
 * TODO: Возможно сделать представлением для любого виджета ФБ
  * */
SBApp.FBWidgetView = Marionette.ItemView.extend({
  template: _.template('<fb:follow href="<%= fb_group %>" colorscheme="light" layout="standard" show_faces="false"></fb:follow>')
});

/* Наследуем базовые методы продукта */
_.extend(SBApp.ProductView.prototype, SBApp.Mixins.Product);
_.extend(SBApp.CartItemView.prototype, SBApp.Mixins.Product);
_.extend(SBApp.SearchItemView.prototype, SBApp.Mixins.Product);
_.extend(SBApp.BreadcrumbItemViewLink.prototype, SBApp.Mixins.VK);
