var div = document.createElement('DIV');
div.id = 'copblock';
document.body.appendChild(div);

function order_checkout(id) {
	var copFormId = id;
	var cophost = 'platform.checkout.ru';
	var copFormAction = '//'+cophost+'/shop/checkout';
	if(copFormId == null || typeof(copFormId) != 'string') return;
	var formsIds = copFormId.split(',');
	for(var i = 0; i < formsIds.length; i++){
		var form = document.getElementById(formsIds[i]);
		if(form == null) continue;
		form.action = copFormAction;
		form.target = 'copIframe';
		form.method = 'post';
		copAddEvent(form, 'submit', function(event){
			document.getElementById('copblock').style.display='block';
		});
	}
	var div = document.getElementById('copblock');
	div.innerHTML = '<div class="copshadow"></div><div class="copcontent"><span id="closecop" onclick="copIframe.location.href=\'about:blank\';document.getElementById(\'copblock\').style.display=\'none\';"></span><iframe name="copIframe" id="copIframe" frameborder="0"></iframe></div>';
	$("#"+id).find("[type='submit']").trigger("click");
}

function copAddEvent(element, eventName, handler){
	if (element.addEventListener) {
		element.addEventListener(eventName, handler, false);
	} else {
		element.attachEvent("on" + eventName, handler);
	}
}
