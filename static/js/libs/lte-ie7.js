/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'IconsFamily\'">' + entity + '</span>' + html;
	}
	var icons = {
			'sb-icon-undo' : '&#xe000;',
			'sb-icon-strikethrough' : '&#xe002;',
			'sb-icon-list-ol' : '&#xe003;',
			'sb-icon-list-ul' : '&#xe004;',
			'sb-icon-reorder' : '&#xe005;',
			'sb-icon-cut' : '&#xe006;',
			'sb-icon-link' : '&#xe007;',
			'sb-icon-picture' : '&#xe008;',
			'sb-icon-indent-right' : '&#xe009;',
			'sb-icon-indent-left' : '&#xe00a;',
			'sb-icon-list' : '&#xe00b;',
			'sb-icon-align-justify' : '&#xe00c;',
			'sb-icon-align-right' : '&#xe00d;',
			'sb-icon-align-center' : '&#xe00e;',
			'sb-icon-align-left' : '&#xe00f;',
			'sb-icon-text-width' : '&#xe010;',
			'sb-icon-text-height' : '&#xe011;',
			'sb-icon-italic' : '&#xe012;',
			'sb-icon-bold' : '&#xe013;',
			'sb-icon-font' : '&#xe014;',
			'sb-icon-repeat' : '&#xe015;',
			'sb-icon-chevron-left' : '&#xf053;',
			'sb-icon-chevron-right' : '&#xf054;',
			'sb-icon-underline' : '&#xe001;',
			'sb-icon-cart' : '&#xe017;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/sb-icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};