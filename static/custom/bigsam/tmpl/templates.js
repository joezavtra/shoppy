
/* Товар в списке */
/* TODO: Избавиться от SBproductHead */
SBApp.Templates.ProductView = {
  className: function(){ return this[ SBApp.productListType ].className; },
  grid: {
    className: 'span3 SBproductGrid',
    body: '<div class="product-item">\
             <% if (SBApp.fast_order) { %>\
               <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
             <% } %>\
             <h5 class="SBproductHead"><%= name %></h5>\
             <div style="background-image: url(\'<%= cover %>\')" class="SBproductCover img-rounded"></div>\
             <div class="product-item__desc"><% if (!_.isEmpty( desc )) { print(desc) } else { %><i>Без описания.</i><% } %></div>\
             <div class="product-item__toolbar g-clearfix">\
               <% if(!this.model.hasVariants()) { %>\
                 <a class="SBcartAdd btn"><i class="sb-icon-cart"></i> Купить</a>\
               <% } else { %>\
                 <a class="btn pull-right product__details"><i class="sb-icon-search"></i> Детали</a>\
               <% } %>\
               <div class="product-item__price"><%= price_str %></div>\
             </div>\
          </div>'
  },
  detail: {
    className: 'span9 SBproductDetail',
    body: '<div class="product-item">\
             <div class="row">\
               <div class="span3"><img src="<%= cover %>"></div>\
               <div class="span5">\
                 <h4 class="SBproductHead"><%= name %></h4>\
                 <div class="product-item__desc"><% if (!_.isEmpty( desc )) { print(desc) %><% } else { %><i>Без описания.</i><% } %></div>\
                 <div class="product-item__toolbar g-clearfix">\
                   <% if(!this.model.hasVariants()) { %>\
                     <a class="SBcartAdd btn"><i class="sb-icon-cart"></i> Купить</a>\
                   <% } else { %>\
                     <a class="btn pull-right product__details"><i class="sb-icon-search"></i> Детали</a>\
                   <% } %>\
                   <div class="product-item__price"><%= price_str %></div>\
                 </div>\
               </div>\
             </div>\
           </div>'
  }
};


/* Карточка товара: Блок Загловок, цена, описание */
JST['product:profile:info'] = _.template('<h3 class="product-profile__title"><%= product.name %></h3>\
<h3 class="product-profile__price"><%= price_str %></h3>\
<% if( product.model_oid ) { %>\
  <p>Артикул товара: <%= product.model_oid %></p>\
<% } %>\
<% if( product.url ) { %>\
  <p><a href="<%= product.url %>" target="_blank" rel="220-volt"><span class="volt-icon"></span>Посмотреть на сайте</a></p>\
<% } %>\
<p><%= product.desc %></p>');


/* Айтем категории: ссылка */
JST['categories:item'] = _.template('<a href="#category/<%= id %>" data-alias="category" title="<%= name %>" class="categories__link">&mdash;&nbsp;<%= name %>&nbsp;<small class="products-count"><%= products_count %></small></a>');
