SBApp.productsOrderBy = "price";
SBApp.hook.afterInit = function() {

  var appType = SBApp.shop.app_type;

  var customPosition = function () {
    $('#search').appendTo($('#categories')).removeClass('pull-right');
    $('<h3>Каталог одежды</h3>').prependTo($('#categories'));
    $('.menu__item-cart').appendTo($('.navbar-content')).addClass('pull-right');
  };

  /* ВК или домен */
  if (SBApp.isLayout('two-columns') && (appType === 'vk' || appType === 'www')) {
    customPosition();
  }

  /* Facebook canvas-приложение */
  else if(appType === 'fb') {
    customPosition();
  }
};