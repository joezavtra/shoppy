SBApp.Templates.ProductView.grid.body = '<div class="product-item">\
   <div class="product-item__cover SBproductCover img-rounded" style="background-image: url(\'<%= cover %>\');"></div>\
   <h4 class="SBproductHead"><%= name %></h4>\
   <div class="product-item__price"><%= price_str %></div>\
   <div class="product-item__toolbar g-clearfix">\
     <% if(!this.model.hasVariants()) { %>\
       <a class="SBcartAdd btn" style="float: none;"><i class="sb-icon-cart"></i> Купить</a>\
     <% } else { %>\
       <a class="btn product__details"><i class="sb-icon-search"></i> Купить</a>\
     <% } %>\
   </div>\
   <% if (SBApp.fast_order) { %>\
     <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
   <% } %>\
</div>';

/* Карточка товара: Блок Загловок, цена, описание */
JST['product:profile:info'] = _.template('<h3 class="product-profile__title"><%= product.name %></h3>\
<h3 class="product-profile__price"><span class="price"><%= price_str %></span></h3>\
<% if( product.model_oid ) { %>\
  <p class="articulus">Артикул товара: <span class="articulus__value"><%= product.model_oid %></span></p>\
<% } %>\
<p class="product-profile__desc"><%= product.desc %></p>');

/* Заголовк кнопки бесконечной подгрузки */
JST['pagination:infinite:title'] = 'Показать еще';