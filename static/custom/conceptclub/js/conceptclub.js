SBApp.productsOrderTypes = [
  {title: 'От А до Я',           type: 'name'},
  {title: 'По возрастанию цены', type: 'price'},
  {title: 'По убыванию цены',    type: '-price'},
  {title: 'По новинкам',          type: '-mtime'}
];