/* Товар в списке */
SBApp.Templates.ProductView = {
  tagName: 'div',
  className: function(){ return this[ SBApp.productListType ].className; },
  grid: {
    className: 'span3 SBproductGrid',
    body: '<div class="product-item">\
              <% if (SBApp.fast_order) { %>\
               <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
             <% } %>\
              <h3 class="SBproductHead"><%= name %></h3>\
              <div style="background-image: url(\'<%= cover %>\')" class="SBproductCover img-rounded"></div>\
              <div class="product-item__toolbar g-clearfix">\
                <% if(!this.model.hasVariants()) { %>\
                  <a class="SBcartAdd btn" title="В корзину"><i class="sb-icon-cart"></i></a>\
                <% } %>\
                <div class="product-item__price"><%= price_str %></div>\
              </div>\
              <div class="product-item__desc"><% if (!_.isEmpty( desc )) { print(desc) } else { %><i>Без описания.</i><% } %></div>\
            </div>'
  },
  detail: {
    className: 'span9 SBproductDetail',
    body: '<div class="product-item">\
            <div class="row">\
              <div class="span3"><img src="<%= cover %>"></div>\
                <div class="span5">\
                  <h3 class="SBproductHead"><%= name %></h3>\
                  <div class="product-item__toolbar g-clearfix">\
                    <% if(!this.model.hasVariants()) { %><a class="SBcartAdd btn"><i class="sb-icon-cart"></i> В корзину</a><% } %>\
                    <div class="product-item__price"><%= price_str %></div>\
                  </div>\
                  <div class="product-item__desc"><% if (!_.isEmpty( desc )) { print(desc) } else { %><i>Без описания.</i><% } %></div>\
                </div>\
              </div>\
            </div>'
  }
};

/* Карточка товара: Блок заголовок, цена, описание */
JST['product:profile:info'] = _.template('<h2 class="product-profile__title"><%= product.name %></h2>\
<div class="well well-large">\
  <h1 class="product-profile__price"><%= price_str %></h1>\
</div>\
<% if( product.model_oid ) { %>\
  <p>Артикул товара: <%= product.model_oid %></p>\
<% } %>\
<p><%= product.desc %></p>');

/* Кнопка: Добавить в корзину */
JST['button:add-to-cart'] = _.template('<button class="btn btn-block SBcartAdd" id="SBcartAdd" <% if (disabled) { %> disabled="disabled" <% } %> data-oid="<%= product.oid %>" data-pid="<%= product.id %>" type="button"><i class="sb-icon-cart"></i> В корзину</button>');

/* Корзина: форма заказа. С формой доставки также HEREANDNOW */
JST['cart:order:form'] = _.template('<form class="form-horizontal order-form" style="display: none" id="SBorderForm">\
  <h3>Оформление заказа</h3>\
  <div class="control-group <% if( valid_username) { %>success<% } %>">\
    <label class="control-label control-required" for="SBorderName">Получатель:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderName" placeholder="Имен Фамилёв" data-required="true" data-type="text" data-valid="<%= valid_username %>" value="<%= username %>">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderPhone">Телефон:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderPhone" placeholder="8 495 111-11-11" data-required="true" data-type="phone" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label" for="SBorderEmail">Email:</label>\
    <div class="controls">\
      <input type="email" class="span5 сontrol-input" id="SBorderEmail" placeholder="name@surname.ru" data-type="email" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderAddress">Адрес доставки:</label>\
    <div class="controls">\
      <span class="SBdeliveryAddress"></span>\
      <a class="SBdeliveryChoose" href="#" onclick="javascript: return false;">выбрать</a>\
      <a class="SBdeliveryChange" href="#" onclick="javascript: return false;">изменить</a>\
      <input type="text" class="span5 сontrol-input SBorderAddress" id="SBorderAddress" placeholder="Улица Доставочная, дом 1" data-required="true" data-type="text" data-valid="false">\
      <input type="hidden" class="SBdeliveryPoint" id="SBdeliveryPoint" data-required="true" data-valid="true">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="SBdelivery" id="SBdelivery"></div>\
  <div class="control-group">\
    <label class="control-label" for="SBorderComment">Комментарий:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderComment" placeholder="" data-required="false">\
    </div>\
  </div>\
  <div class="control-group">\
    <div class="controls-row">\
      <div class="controls">\
        <span class="required-control">*</span>&nbsp;поле, обязательное для заполнения\
      </div>\
    </div>\
  </div>\
  <div class="controls-row">\
    <button class="btn pull-right SBcartOrder" id="SBcartOrder" type="button" disabled="disabled"><i class="sb-icon-ok icon-white"></i> Отправить</button>\
  </div>\
</form>');