JST['cart:order:form'] = _.template('<form class="form-horizontal order-form" style="display: none" id="SBorderForm">\
  <h2>Оформление заказа</h2>\
  <div class="control-group <% if( valid_username) { %>success<% } %>">\
    <label class="control-label control-required" for="SBorderName">Получатель:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderName" placeholder="Имен Фамилёв" data-required="true" data-type="text" data-valid="<%= valid_username %>" value="<%= username %>">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderPhone">Телефон:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderPhone" placeholder="8 495 111-11-11" data-required="true" data-type="phone" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label control-required" for="SBorderEmail">Email:</label>\
    <div class="controls">\
      <input type="email" class="span5 сontrol-input" id="SBorderEmail" placeholder="name@surname.ru" data-required="true" data-type="email" data-valid="false">\
      <span class="help-inline"></span>\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="control-label" for="SBorderComment">Комментарий:</label>\
    <div class="controls">\
      <input type="text" class="span5 сontrol-input" id="SBorderComment" placeholder="" data-required="false">\
    </div>\
  </div>\
  <div class="control-group">\
    <div class="controls-row">\
      <div class="controls">\
        <span class="required-control">*</span>&nbsp;поле, обязательное для заполнения\
      </div>\
    </div>\
  </div>\
  <div class="controls-row">\
    <button class="btn btn-primary pull-right SBcartOrder" id="SBcartOrder" type="button" disabled="disabled"><i class="sb-icon-ok icon-white"></i> Заказ оформлен</button>\
  </div>\
</form>');