/* Товар в списке */
SBApp.Templates.ProductView = {
  className: function(){
    return this[ SBApp.productListType ].className;
  },

  grid: {
    className: 'span3 SBproductGrid',
    body: '<div class="product-item">\
             <div style="background-image: url(\'<%= cover %>\')" class="SBproductCover img-rounded"></div>\
             <h5 class="SBproductHead"><%= name %></h5>\
             <div class="product-item__price"><%= price_str %></div>\
             <div class="product-item__toolbar g-clearfix">\
               <% if(!this.model.hasVariants()) { %>\
                 <a class="SBcartAdd btn" style="float: none;"><i class="sb-icon-cart"></i> Купить</a>\
               <% } else { %>\
                 <a class="btn product__details"><i class="sb-icon-search"></i> Детали</a>\
               <% } %>\
             </div>\
             <div class="product-item__art">Артикул: <%= model_oid %></div>\
             <% if (SBApp.fast_order) { %>\
               <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
             <% } %>\
          </div>'
  },
  detail: {
    className: '',
    body: ''
  }
};

JST['pagination:infinite'] = _.template('<button class="btn js-show-more"><%= JST["pagination:infinite:title"] %></button>');