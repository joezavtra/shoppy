/* Товар в списке */
SBApp.Templates.ProductView = {
  className: function(){ return this[ SBApp.productListType ].className; },
  grid: {
    className: 'SBproductGrid g-inline-block',
    body: '<div class="product-item">\
             <% if (SBApp.fast_order) { %>\
               <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
             <% } %>\
             <div class="product-item__pic SBproductCover img-rounded">\
               <img src="<%= cover %>" />\
             </div>\
             <div class="SBproductHead product-item__head"><%= name %></div>\
             <div class="desc product-item__desc">\
               <% if (!_.isEmpty( desc )) { %><%= desc %><% } \
               else{ %><i>Без описания.</i><% } %>\
             </div>\
             <p class="lead product-item__price"><%= price_str %></p>\
             <% if( !this.model.hasVariants() ) { %>\
               <div style="text-align: center;">\
                 <a class="SBcartAdd btn btn-small" title="В корзину"><i class="sb-icon-cart"></i> В корзину</a>\
               </div>\
             <% } %>\
            </div>'
  },
  detail: {
    className: 'SBproductDetail',
    body: '<div class="product-item">\
             <div class="row">\
               <div class="span2" style="text-align: center;">\
                 <img src="<%= cover %>">\
                 <% if(!this.model.hasVariants()) { %>\
                   <a class="SBcartAdd btn"><i class="sb-icon-cart"></i> В корзину</a>\
                 <% } %>\
               </div>\
               <div class="span7">\
                 <h3 class="SBproductHead"><%= name %></h3>\
                 <p class="lead"><%= price_str %></p>\
                 <p class="desc"><% if (!_.isEmpty( desc )) { print(desc) } else { %><i>Без описания.</i><% } %></p>\
               </div>\
            </div>\
           </div>'
  }
};

JST["product:profile"] = _.template('<div class="row-fluid">\
  <div class="span10"><ul id="product__breadcrumb" class="breadcrumb"></ul></div></div>\
  <div class="row-fluid">\
  <h2 class="product-profile__title"><%= product.name %></h2>\
  <!-- Левая колонка -->\
  <div class="span4">\
    <div class="tabbable tabs-below">\
      <% if( productView.model.has("images") ) { %>\
        <!-- JST["product:profile:images"] -->\
        <%= renderImages({ images: productView.model.get("images")} ) %>\
      <% } %>\
      <% if ( productView.model.hasVariants() ) { %>\
        <!-- JST["product:profile:offers"] -->\
        <%= renderVariants({ variants: productView.model.get("variants") })%>\
      <% } %>\
      <!-- JST["product:profiles:variants"] -->\
      <%= renderSocials()  %>\
      <%= buttonAddToCart({ product: product, disabled: product.variants.length > 0}) %>\
    </div>\
  </div>\
  <!-- Правая колонка  -->\
  <div class="span6">\
    <!-- JST["product:profile:info"] -->\
    <%= productInfo({ product: product, price_str: price_str }) %>\
    <ul class="nav nav-tabs product-tabs" id="product-tabs">\
      <% if(!_.isEmpty(JSON.parse(product.misc))) { %>\
        <li class="active"><a href="#product-desc" data-toggle="tab">Характеристики</a></li>\
      <% } %>\
      <% if( !SBApp.Platform.isDomain() ) { %>\
      <li>\
        <a href="#product-comments" data-toggle="tab">Обсуждение <% if( commentsCount !== 0 ) { %>(<% print(commentsCount) %>)<% } %></a>\
      </li>\
      <% } %>\
    </ul>\
    <div class="tab-content">\
      <% if(!_.isEmpty(JSON.parse(product.misc))) { %>\
        <div class="tab-pane active" id="product-desc">\
          <!-- JST["product:profile:characteristics"] -->\
          <%= renderChars({misc: product.misc}) %>\
        </div>\
      <% } %>\
    <div class="tab-pane" id="product-comments">\
      <!-- Форма добавления комментария -->\
      <!-- Выводится только в приложении ВК и FB -->\
      <% if( !SBApp.Platform.isDomain() ) { %>\
        <!-- JST["comments:form:add"] -->\
        <%= formAddComment({ userPhoto: userPhoto }) %>\
        <!-- Вывод комментариев -->\
        <% if(!productView.model.hasComments()) { %>\
          <i>Пока никто ничего не написал.</i>\
        <% } else { %>\
          <!-- JST["comments"] -->\
          <%= renderComments({ comments: productView.model.get("comments"), renderComment: renderComment, reply: false }) %>\
        <% } %>\
       <% } %>\
    </div>\
  </div>\
</div>');

/* Комментарии: один комментарий */
JST["comments:item:user:auth"] = _.template('<li class="comments__item">\
  <% if( comment.customer && comment.customer.profile ) { %>\
  <div class="row-fluid">\
      <div class="span1 comments__item-photo">\
        <% if (SBApp.Platform.isVK()) { %>\
          <% if (comment.customer.profile.photo_50) { %>\
            <img src="<%= comment.customer.profile.photo_50 %>" /><br />\
          <% } %>\
        <% } %>\
        <% if (SBApp.Platform.isFacebook()) { %>\
          <img src="//graph.facebook.com/<%= comment.customer.profile.id %>/picture" /><br />\
        <% } %>\
        <% if (SBApp.Platform.isOK()) { %>\
          <img src="<%= comment.customer.profile.pic50x50%>" /><br />\
        <% } %>\
      </div>\
      <div class="span6">\
        <p class="comments__item-info">\
          <% if (SBApp.Platform.isVK()) { %>\
            <a class="comments__item-user" href="//vk.com/id<%= comment.customer.profile.id %>" target="_blank"><%= comment.customer.profile.first_name %>&nbsp;<%= comment.customer.profile.last_name %></a><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small></p>\
          <% } %>\
          <% if (SBApp.Platform.isFacebook()) { %>\
            <a class="comments__item-user" href="//facebook.com/<%= comment.customer.profile.id %>" target="_blank"><%= comment.customer.profile.name %></a><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small></p>\
          <% } %>\
          <% if (SBApp.Platform.isOK()) { %>\
            <a class="comments__item-user" href="//www.odnoklassniki.ru/profile/<%= comment.customer.profile.uid %>" target="_blank"><%= comment.customer.profile.name %></a><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small></p>\
          <% } %>\
        <p class="comments__item-text"><%= comment.text %></p>\
      </div>\
  </div>\
  <% } %>\
  <% if ( comment.reply_list.length > 0 && comment.customer && comment.customer.profile ) { %>\
    <%= JST["comments"]({ comments: comment.reply_list, renderComment:  JST["comments:item:user:admin"], reply: true }) %>\
  <% } %>\
</li>');

JST["comments:item:user:admin"] = _.template('<li class="comments__item">\
  <div class="row-fluid">\
      <div class="span1 comments__item-photo">\
        <img src="" /><br />\
      </div>\
      <div class="span6">\
        <p class="comments__item-info">\
          <span class="comments__item-user">Администрация</span><small class="comments__item-create-at"><%= SBApp.getDateAsString(comment.created, "short", true) %></small>\
        </p>\
        <p class="comments__item-text"><%= comment.text %></p>\
      </div>\
  </div>\
</li>');

/* Кнопка: Добавить в корзину */
JST['button:add-to-cart'] = _.template('<button class="btn btn-block SBcartAdd" id="SBcartAdd" <% if (disabled) { %> disabled="disabled" <% } %> data-oid="<%= product.oid %>" data-pid="<%= product.id %>" type="button"><i class="sb-icon-cart"></i> В корзину</button>');

/* Карточка товара: Блок Загловок, цена, описание */
JST['product:profile:info'] = _.template('<h3 class="product-profile__price"><%= price_str %></h3>\
<% if( product.model_oid ) { %>\
  <p>Артикул товара: <%= product.model_oid %></p>\
<% } %>\
<p><%= product.desc %></p><br />');

