/* Слайдер */
JST['slider:item'] = _.template('<div class="row sb-banner__item">\
  <div class="span3"><img src="<%= cover %>"></div>\
  <div class="span6">\
    <h3 class="sb-banner__item-title"><%= name %></h3> \
    <p class="sb-banner__item-price"><%= SBApp.getPriceWithCurrency(price) %></p>\
    <div class="sb-banner__item-desc"><% if (!_.isEmpty(jQuery.trim(desc))) { print(desc) }else { %><i>Без описания.</i><% } %></div>\
  </div>\
</div>');

SBApp.Templates.ProductView.grid.body = '<div class="product-item">\
   <% if (SBApp.fast_order) { %>\
     <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
   <% } %>\
   <div style="background-image: url(\'<%= cover %>\')" class="SBproductCover img-rounded"></div>\
   <h5 class="SBproductHead"><%= name %></h5>\
   <div class="product-item__toolbar g-clearfix">\
     <% if(!this.model.hasVariants()) { %>\
       <a class="SBcartAdd btn"><i class="sb-icon-cart"></i> Купить</a>\
     <% } else { %>\
       <a class="btn product__details"><i class="sb-icon-search"></i> Детали</a>\
     <% } %>\
   </div>\
   <div class="product-item__price"><%= price_str %></div>\
</div>';

SBApp.Templates.ProductView.detail.body = '<div class="product-item">\
<div class="row">\
  <div class="span3"><img src="<%= cover %>"></div>\
  <div class="span5">\
    <h4 class="SBproductHead"><%= name %></h4>\
    <div class="product-item__toolbar g-clearfix">\
      <% if(!this.model.hasVariants()) { %>\
        <a class="SBcartAdd btn"><i class="sb-icon-cart"></i> Купить</a>\
      <% } %>\
      <div class="product-item__price"><%= price_str %></div>\
     </div>\
     <div class="product-item__desc"><% if (!_.isEmpty( desc )) { print(desc) %><% } else { %><i>Без описания.</i><% } %></div>\
   </div>\
 </div>\
</div>';

JST["product:profile"] = _.template('<div class="row">\
  <div class="span8"><ul id="product__breadcrumb" class="breadcrumb"></ul></div></div>\
  <div class="row">\
  <!-- Левая колонка -->\
  <div class="span3">\
    <div class="tabbable tabs-below" style="text-align: center">\
      <% if( productView.model.has("images") ) { %>\
        <!-- JST["product:profile:images"] -->\
        <%= renderImages({ images: productView.model.get("images")} ) %>\
      <% } %>\
      <% if ( productView.model.hasVariants() ) { %>\
        <!-- JST["product:profile:variants"] -->\
        <%= renderVariants({ variants: productView.model.get("variants") })%>\
      <% } %>\
    </div>\
  </div>\
  <!-- Правая колонка  -->\
  <div class="span5">\
    <!-- JST["product:profile:info"] -->\
    <%= productInfo({ product: product, price_str: price_str }) %>\
    <ul class="nav nav-tabs product-tabs" id="product-tabs">\
      <% if( !_.isEmpty(JSON.parse(product.misc)) ) { %>\
        <li class="tabs__title-1"><a href="#product-desc" data-toggle="tab">Характеристики</a></li>\
      <% } %>\
      <% if( !SBApp.Platform.isDomain() ) { %>\
      <li class="product-tabs__item-2 active">\
        <a href="#product-comments" data-toggle="tab" class="product-discuss">Обсуждение <% if( commentsCount !== 0 ) { %>(<% print( commentsCount ) %>)<% } %></a>\
      </li>\
      <% } %>\
    </ul>\
    <div class="tab-content">\
      <% if( !_.isEmpty(JSON.parse(product.misc)) ) { %>\
        <div class="tab-pane tabs__desc-1" id="product-desc">\
          <!-- JST["product:profile:characteristics"] -->\
          <%= renderChars({misc: product.misc}) %>\
        </div>\
      <% } %>\
    <div class="tab-pane active" id="product-comments">\
      <!-- Форма добавления комментария -->\
      <!-- Выводится только в приложении ВК и FB -->\
      <% if( !SBApp.Platform.isDomain() ) { %>\
        <!-- JST["comments:form:add"] -->\
        <%= formAddComment({ userPhoto: userPhoto }) %>\
        <!-- Вывод комментариев -->\
        <% if(!productView.model.hasComments()) { %>\
          <i>Пока никто ничего не написал.</i>\
        <% } else { %>\
          <!-- JST["comments"] -->\
          <%= renderComments({ comments: productView.model.get("comments"), renderComment: renderComment, reply: false }) %>\
        <% } %>\
      <% } %>\
    </div>\
  </div>\
</div>\
</div>\
<div style="clear:both;"></div>\
<div style="text-align: center">\
  <!-- JST["product:profiles:socials"] -->\
  <%= renderSocials()  %>\
</div>');

/* Карточка товара: Блок Загловок, цена, описание */
JST['product:profile:info'] = _.template('<h3 class="product-profile__title"><%= product.name %></h3>\
<div class="product-profile__price">\
  <span class="price"><%= price_str %></span>\
  <button class="btn SBcartAdd" id="SBcartAdd" <% if (product.variants.length > 0) { %> disabled="disabled" <% } %> data-oid="<%= product.oid %>" data-pid="<%= product.id %>" type="button"><i class="sb-icon-cart"></i> Купить</button>\
</div>\
<% if( product.model_oid ) { %>\
  <p class="articulus">Артикул товара: <span class="articulus__value"><%= product.model_oid %></span></p>\
<% } %>\
<p class="product-profile__desc"><%= product.desc %></p>');

SBApp.Templates.ProductListView = {
  toolbar: '<div class="toolbar clearfix">\
      <div class="toolbar__item pull-left">\
        <span class="toolbar__item-title">Сортировать:</span>\
        <div class="g-inline-block styled-select">\
        <% if (!_.isEmpty(SBApp.productsOrderTypes)) { %>\
          <select class="toolbar__item-select" id="toolbar__item-select">\
            <% _.each(SBApp.productsOrderTypes, function (orderType) { %>\
              <option value="<%= orderType.type %>" <% if (orderType.type === SBApp.productsOrderBy) { %> selected="selected" <% } %>><%= orderType.title %></option>\
            <% }) %>\
          </select>\
          <i class="icon-chevron-down select__arrow"></i>\
        <% } %>\
        </div>\
      </div>\
      <div class="toolbar__item pull-right">\
        <span class="js-toolbar__link-type-switcher toolbar__link <% if (SBApp.productListType === "grid") { %> toolbar__link_active <% } %>" data-list-type="grid"><i class="sb-icon-th toolbar__item-icon"></i>сеткой</span>\
        <span class="js-toolbar__link-type-switcher toolbar__link <% if (SBApp.productListType === "detail") { %> toolbar__link_active <% } %>" data-list-type="detail"><i class="sb-icon-th-list toolbar__item-icon"></i>списком</span>\
      </div>\
  </div>',

  type: 'grid'
};