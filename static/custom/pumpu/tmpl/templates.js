/* Карточка товара: заголовк, описание, цена */
JST['product:profile:info'] = _.template('<h1 class="product-profile__title"><%= product.name %></h1>\
<div class="well well-large">\
  <h1 class="product-profile__price"><%= price_str %></h1>\
</div>\
<% if( product.model_oid ) { %>\
  <p>Артикул товара: <%= product.model_oid %></p>\
<% } %>\
<p><%= product.desc %></p>');

/* Кнопка: Добавить в корзину */
JST['button:add-to-cart'] = _.template('<button class="btn btn-block SBcartAdd" id="SBcartAdd" <% if (disabled) { %> disabled="disabled" <% } %> data-oid="<%= product.oid %>" data-pid="<%= product.id %>" type="button"><i class="sb-icon-cart"></i> В корзину</button>');

SBApp.Templates.ProductView = {
  className: function(){ return this[ SBApp.productListType ].className; },
  grid: {
    className: 'span3 SBproductGrid',
    body: '<div class="product-item">\
              <% if (SBApp.fast_order) { %>\
                <a class="js-show-modal fast-order__link" title="Обратный звонок"><i class="sb-icon-phone"></i></a>\
              <% } %>\
              <h5 class="SBproductHead"><%= name %></h5>\
              <div style="background-image: url(\'<%= cover %>\')" class="SBproductCover img-rounded"></div>\
                <div class="product-item__toolbar g-clearfix">\
                <% if(!this.model.hasVariants()) { %>\
                  <a class="SBcartAdd">В корзину</a>\
                <% } %>\
                <p class="product-item__price"><%= price_str %></p>\
              </div>\
            </div>'
  },
  detail: {
    className: 'span9 SBproductDetail',
    body: '<div class="product-item">\
             <div class="row">\
               <div class="span3"><img class="img-rounded" src="<%= cover %>"></div>\
               <div class="span5">\
                 <h3 class="SBproductHead"><%= name %></h3>\
                 <div class="product-item__toolbar g-clearfix">\
                    <% if(!this.model.hasVariants()) { %> <a class="SBcartAdd">В корзину</a> <% } %>\
                    <div class="product-item__price"><%= price_str %></div>\
                 </div>\
                 <div class="product-item__desc"><% if (!_.isEmpty(jQuery.trim(desc))) { print(desc) } else { %><i>Без описания.</i><% } %></div>\
               </div>\
             </div>\
           </div>'
  }
};