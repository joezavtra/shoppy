# coding: utf-8
import os
from datetime import datetime
from fabric.api import run, local, env, cd, lcd, roles, settings, sudo
from fabric.contrib.files import exists
from fabric.operations import open_shell
from fabric.utils import puts, abort

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

DROP_CREATE_DB = '''
    SET storage_engine=InnoDB;
    SET NAMES "utf8";
    SET CHARACTER SET utf8;
    SET CHARACTER_SET_CLIENT = utf8;
    SET CHARACTER_SET_CONNECTION = utf8;
    SET CHARACTER_SET_DATABASE = utf8;
    SET CHARACTER_SET_RESULTS = utf8;
    SET CHARACTER_SET_SERVER = utf8;

    DROP DATABASE IF EXISTS {db};
    CREATE DATABASE {db} DEFAULT CHARACTER SET utf8;
'''

env.roledefs['production'] = ['kazhuravlev@shoppyboom.ru']


def production_env():
    # Локальный путь до файла с ключами
    env.key_filename = [os.path.join(os.environ['HOME'], '.ssh', 'id_rsa.pub')]
    # На сервере будем работать из под пользователя "git"
    env.user = 'kazhuravlev'
    # Путь до каталога проекта (на сервере)
    env.project_root = '/home/kazhuravlev/shoppy'
    # Используем шелл отличный от умолчательного (на сервере)
    env.shell = '/bin/bash -c'
    # Путь до python (на сервере)
    env.python = '/usr/bin/python'

@roles('production')
def _test():
    production_env()
    with cd(env.project_root):
        run('git status')
        run('git pull')
        run('pip install -r requirements-pip.txt')
        run('python manage.py migrate')
        run('kill -HUP $(cat /tmp/gunicorn.pid)')



@roles('production')
def _deploy():
    production_env()  # Инициализация окружения
    with cd(env.project_root):  # Заходим в директорию с проектом на сервере
        run('git pull origin master')  # Пуляемся из репозитория
        run('find . -name "*.mo" -print -delete')  # Чистим старые скомпиленные файлы gettext'а
        run('{} manage.py compilemessages'.format(env.python))  # Собираем новые файлы gettext'а
        run('{} manage.py collectstatic --noinput'.format(env.python))  # Собираем статику

@roles('production')
def _pip_install():
    production_env()
    run('{pip} install --upgrade -r {filepath}'.format(pip=env.pip,
        filepath=os.path.join(env.project_root, 'requirements.txt')))


def test():
    env.hosts = ['test.shoppyboom.ru']
    env.project_root = '/home/sketcher/shoppy_production'
    env.branch = 'deploy/test'


def prod():
    env.hosts = ['shoppyboom.ru']
    env.project_root = '/home/shoppy/shoppy_prod'
    env.branch = 'master'

def deploy():
    """Deploy project to host.

    `hosts`, `project_root` and `branch` variables should be set before
    running this. Use `test` or `prod` task for this.

    Usage:

        Deploy to test server:

            $ fab test deploy

        Deploy to prod server:

            $ fab prod deploy
    """
    with cd(env.project_root):
        run('git pull origin ' + env.branch)
        run('./env/bin/pip install -r requirements-pip.txt')
        sudo('./env/bin/python manage.py migrate --merge')
        sudo('./env/bin/python manage.py collectstatic --noinput')

    sudo('supervisorctl restart shoppy_prod')


def dump(path='~/dumps', db='shoppyboom_dev', root='.'):
    """Dump and zip database.

    Options:
        path    - path to dump directory
        db      - database name
        root    - project root
    """
    now_str = datetime.now().isoformat()

    with lcd(root):
        sha_str = local(
                'git log -1 --abbrev-commit --pretty=format:"%h"',
                capture=True
        )

    prefix = '{now} {sha}'.format(now=now_str, sha=sha_str)

    local('mkdir -p {0}'.format(path))
    with lcd(path):
        dump_cmd = (
            'mysqldump -u $(whoami) {db} > ' +
            '"{prefix}.sql"'
        )
        local(
            dump_cmd.format(
                prefix=prefix,
                db=db
            )
        )

        local('zip "{0}.zip" "{0}.sql"'.format(prefix))
        local('rm "{0}.sql"'.format(prefix))

    puts(
        "Dump ready at\n\t{path}.zip".format(
            path=os.path.join(path, prefix)
        )
    )


def restore(path='~/dumps', db='shoppyboom_dev', commit=None, time=None):
    """Restore database from dump, matching criteria.
    If `commit` and `time` not given, restore latest dump.

    Options:
        path    - path to dump directory
        db      - database to restore to
        commit  - prefix of git commit hash of dump
        time    - part of date or time of dump in iso format

    Usage:

        To restore database from latest dump in `path`:

            $ fab restore

        To restore latest dump to database with `db_name`:

            $ fab restore:db=db_name

        To restore dump from 26th of April, 2014:

            $ fab restore:time=2014-04-26

        To restore dump with commit hash, that starts with `e43a`:

            $ fab restore:commit=e43a
    """
    zip_list = []
    zip = None
    sql = None

    if not commit and not time:
        zip_list = local(
            'ls {0} | grep "\.zip$" | sort -r'.format(path),
            capture=True
        ).split('\n')

        if not zip_list or not zip_list[0]:
            abort('Could not find file to restore from!')
            return

        zip = zip_list[0]

    if commit or time:
        zip_list  = local(
            'ls {0} | grep "\.zip$"'.format(path),
            capture=True
        ).split('\n')

        if time:
            zip_list = [ name for name in zip_list if time in name ]

        if commit:
            commit_str = ' ' + commit
            zip_list = [ name for name in zip_list if commit_str in name ]

        if not zip_list:
            abort(
                'Could not find file to restore from!'
                '\n\tdate: {0}'
                '\n\tcommit: {1}'.format(time, commit)
            )
            return

        if len(zip_list) > 1:
            abort(
                'More than one dump match criteria!'
                '\n\ttime: {0}'
                '\n\tcommit: {1}'
                '\ndumps:'
                '\n\t{2}'.format(
                    time,
                    commit,
                    '\n\t'.join(zip_list)
                )
            )
            return

        zip = zip_list[0]

    puts("About to restore dump: {0}".format(zip))

    with lcd(path):
        sql = local('unzip -Z -1 "{0}"'.format(zip), capture=True)
        local('unzip -o "{0}"'.format(zip))

        local(
            'echo "{cmd}" | mysql'.format(
                cmd=DROP_CREATE_DB.format(db=db)
            )
        )

        local(
            'mysql -u $(whoami) {db} < "{dump}"'.format(
                dump=sql,
                db=db
            )
        )

        local('rm "{0}"'.format(sql))

