import hashlib
import urllib
import requests

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class _Odnoklassniki(object):
    def __init__(self, app_key, app_secret, token=None):
        self.app_key = app_key
        self.app_secret = app_secret
        self.token = token
        self.base_url = 'http://api.odnoklassniki.ru/fb.do?'
        self.format = 'json'
        self._method = None

    def __getattr__(self, method):
        api = _Odnoklassniki(self.app_key, self.app_secret, self.token)
        if self._method:
            api._method = self._method + '.' + method
        else:
            api._method = method
        return api

    def get_sign(self, params):
        res = []
        for kv in sorted(params.items()):
            res.append('='.join(kv))
        return hashlib.md5(''.join(res) + self.app_secret).hexdigest().lower()

    def build_url(self, method, params=None):
        if params is None:
            params = {}
        query_dict = dict(
            method=method,
            format=self.format,
            application_key=self.app_key
        )
        query_dict.update(params)
        query_dict['sig'] = self.get_sign(query_dict)
        qs = urllib.urlencode(query_dict)
        return self.base_url + qs

    def get(self, url):
        r = requests.get(url)
        return r.json()

    def __call__(self, **kwargs):
        url = self.build_url(method=self._method, params=kwargs)
        return self.get(url)


class Odnoklassniki(_Odnoklassniki):
    def __init__(self, app_key, app_secret, token=None):
        _Odnoklassniki.__init__(self, app_key, app_secret, token)
