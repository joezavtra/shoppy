# coding=utf-8
import hashlib
from operator import attrgetter, itemgetter
from operator import itemgetter
import string
from django import forms
from django.db import transaction
from django.core.exceptions import ValidationError
from django.forms import widgets
from django.forms.util import ErrorList
from django.forms.models import BaseModelFormSet, BaseInlineFormSet

import re
from django.forms.models import modelformset_factory
import vkontakte
from Cabinet.widgets import HelpWidget, DateTimePickerWidget, DatePickerWidget
from Core.models import *
from Core.utils import clean_str
from PIL import Image


def render(pre_html):
    def renderer(fun):
        def wrapper(*args, **kwargs):
            output = fun(*args, **kwargs)
            return output + type(output)(pre_html)
        return wrapper
    return renderer


class HelpForm(object):
    class Media(object):
        js = ('js/cabinet/help_form.js', )

    def __init__(self, *args, **kwargs):
        super(HelpForm, self).__init__(*args, **kwargs)
        form_class = self.__class__
        form_name = form_class.__module__ + '.' + form_class.__name__
        form_name = form_name.replace('Admin','User')
        f_descs = FormDescription.objects.filter(
            field_name__startswith=form_name)

        descriptions = dict()
        for obj in f_descs:
            descriptions[obj.get_local_name()] = obj

        for f_name, field in self.fields.iteritems():
            if f_name in descriptions:
                desc = descriptions[f_name]
                edit_link = reverse('admin:Core_formdescription_change', args=(desc.pk,))
                help_button_class = ' btn-warning' if desc.help_text is None else ''
                field.widget.render = render(u'<a href="%s" class="btn btn-mini form-help-data%s" target="_blank"><i class="icon-pencil"></i> править подсказку</a>' % (edit_link,help_button_class))(field.widget.render)
                help_text = desc.help_text or ''
                field.help_text = "<br />".join(help_text.split("\n"))
            else:
                field.widget.attrs['style'] = 'border: 1px solid red'

class BaseCabinetModelForm(HelpForm, forms.ModelForm):
    def as_p(self):
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            # help_text_html='<button type="button" data-title="Подсказка" data-content="%s" class="btn btn-primary btn-mini"><i class="icon-white icon-question-sign"></i></button>',
            help_text_html=' <p class="helptext">%s</p>',
            errors_on_separate_row=True)


class ClientForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = Client
        fields = (
            'name', 'client_type', 'l_permission', 'l_doverennost_number',
            'l_doverennost_date', 'l_fio_gendir', 'l_org_name',
            'l_inn', 'l_kpp', 'l_okved', 'l_okpo', 'l_ogrn', 'l_bank', 'l_bik',
            'l_corr_account', 'l_current_account', 'l_legal_addr',
            'l_fact_addr', 'l_tel', 'l_email', )
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'l_fio_gendir': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'client_type': widgets.Select(attrs={'class': 'input-block-level'}),
            'l_permission': widgets.Select(attrs={'class': 'input-block-level'}),
            'l_org_name': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_doverennost_number': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_doverennost_date': DatePickerWidget(attrs={'class': 'input-block-level'}),
            'l_inn': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_kpp': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_okved': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_okpo': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_ogrn': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_bank': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_bik': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_corr_account': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_current_account': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_legal_addr': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_fact_addr': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_tel': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'l_email': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }

    class Media(object):
        js = ('js/cabinet/client_form.js', )

    readonly_fields = [
        'client_type', 'l_fio_gendir', 'l_permission', 'l_doverennost_number',
        'l_doverennost_date', 'l_org_name', 'l_inn', 'l_kpp', 'l_okved',
        'l_okpo', 'l_ogrn', 'l_bank', 'l_bik', 'l_corr_account',
        'l_current_account', 'l_legal_addr', 'l_fact_addr', 'l_tel', 'l_email'
    ]

    def __init__(self, *args, **kwargs):
        super(ClientForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            instance = kwargs['instance']
            if instance.pk and instance.is_validated:
                for field in self.readonly_fields:
                    self.fields[field].widget.attrs['disabled'] = 'disabled'

    def clean(self):
        instance = getattr(self, 'instance', None)
        cleaned_data = self.cleaned_data
        if instance and instance.pk and instance.is_validated:
            for field in self.readonly_fields:
                cleaned_data[field] = getattr(instance, field)
        return cleaned_data


class ShopForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = Shop
        exclude = (
            'cdate',
            'status',
            'users',
            'description',
        )
        fields = (
            'name',
            'alias',
            'client',
            'email',
            'domain',
            'user_reply',
            'user_from',
            'user_subject',
            'user_sign',
            'hello_text',
            'paid_till',
            'catalog',
            'email_banner_frequency',
            'currency',
        )
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'alias': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'catalog': widgets.Select(attrs={'class': 'input-block-level'}),
            'email_banner_frequency': widgets.Select(attrs={'class': 'input-block-level'}),
            'users': widgets.SelectMultiple(attrs={'class': 'input-block-level'}),
            'email': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'user_reply': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'user_from': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'user_subject': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'user_sign': widgets.Textarea(attrs={'class': 'input-block-level', 'rows': 3}),
            'hello_text': widgets.Textarea(attrs={'class': 'input-block-level', 'rows': 3}),
            'domain': HelpWidget(help_text='.shoppyboom.ru', attrs={'class': 'input-block-level'}),
            'paid_till': DateTimePickerWidget(attrs={'class': 'input-block-level'}),
            'currency': widgets.Select(attrs={'class': 'input-block-level'}),
        }

    def __init__(self, *args, **kwargs):
        current_user_obj = kwargs.pop('user_obj')
        super(ShopForm, self).__init__(*args, **kwargs)
        if not current_user_obj.is_superuser:
            if 'users' in self.fields:
                self.fields['users'].queryset = User.objects.filter(
                    clients__in=current_user_obj.clients.all()
                ).distinct()
            if 'client' in self.fields:
                self.fields['client'].queryset = current_user_obj.clients.all()

    def is_valid(self):
        if super(ShopForm, self).is_valid():
            if 'domain' in self.cleaned_data:
                domain = self.cleaned_data['domain']
                valid_chars = string.ascii_lowercase + string.digits + '-'
                for c in domain.lower():
                    if c not in valid_chars:
                        self.errors['domain'] = ErrorList([
                            u'Разрешены только символы латинского алфавита и "-"'.encode('utf-8')
                        ])
                        return False
            return True
        return False


class ShopNewForm(ShopForm):
    class Meta(ShopForm.Meta):
        exclude = ShopForm.Meta.exclude + ('client',)


class DsField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return u'%s (%d товаров, %d категорий)' % (
            obj.name, obj.products.count(), obj.categories.count())


class NewShopBase(ShopForm):
    MODE_AUTO = '1'
    MODE_MANUAL = '2'
    MODE_CHOICES = (
        (MODE_MANUAL, u'Ручной'),
        (MODE_AUTO, u'Автоматический'),
    )
    FROM_DS_YES = '1'
    FROM_DS_NO = '2'
    FROM_DS_CHOICES = (
        (FROM_DS_YES, u'Да'),
        (FROM_DS_NO, u'Нет'),
    )
    client = forms.ModelChoiceField(
        queryset=Client.objects.none(), required=False, label=u'клиент')
    assortment = DsField(
        queryset=DataSource.objects.none(), label=u'Ассортимент',
        required=False, empty_label=u'Новый')
    mode = forms.ChoiceField(
        choices=MODE_CHOICES, required=True, label=u'Режим',
        widget=widgets.Select(attrs={'class': 'input-block-level'}))
    yml_url = forms.URLField(
        label=u'Полный путь (URL) до YML файла вашего магазина',
        required=False,
        widget=widgets.TextInput(attrs={'class': 'input-block-level'}))

    class Meta(ShopForm.Meta):
        fields = ('name', 'domain', 'currency', 'client', 'assortment', 'mode', 'yml_url', )

    class Media(object):
        js = ('js/cabinet/form_shop_base.js',)

    def __init__(self, *args, **kwargs):
        if 'initial' in kwargs:
            initial = kwargs['initial']
            clients = initial.pop('clients', None)
            user = initial.pop('user', None)
        else:
            user = None
            clients = None
        super(NewShopBase, self).__init__(*args, **kwargs)
        if user.is_superuser:
            self.fields['client'].queryset = Client.objects.all()
        else:
            del self.fields['client']
        if clients and user:
            if user.is_superuser:
                self.fields['assortment'].queryset = DataSource.objects.all()
            else:
                self.fields['assortment'].queryset = DataSource.objects.filter(
                    client__in=clients)

    def is_valid(self):
        if super(NewShopBase, self).is_valid():
            domain = self.cleaned_data['domain']
            valid_chars = string.ascii_lowercase + string.digits + '-'
            for c in domain.lower():
                if c not in valid_chars:
                    self.errors['domain'] = ErrorList([
                        u'Разрешены только символы латинского алфавита и "-"'.encode('utf-8')
                    ])
                    return False
            if self.cleaned_data['mode'] == self.MODE_AUTO and self.cleaned_data['yml_url'] == '':
                self.errors['yml_url'] = ErrorList([
                    u'Магазин в автоматическом режиме должен иметь ссылку на YML файл'
                ])
                return False
            return True
        return False


class NewShopEmail1(HelpForm, forms.Form):
    email = forms.EmailField(
        required=True,
        label=u'Email для уведомлений',
        widget=widgets.TextInput(attrs={'class': 'input-block-level'})
    )


class NewShopEmail2(ShopForm):
    class Meta(ShopForm.Meta):
        fields = ('user_from', 'user_subject', 'hello_text', 'user_sign')


class ShopFormBaseUser(ShopForm):
    class Meta(ShopForm.Meta):
        fields = ('name', 'domain', 'currency', 'users', )


class ShopFormBaseAdmin(ShopForm):
    class Meta(ShopForm.Meta):
        fields = ('name', 'alias', 'domain', 'currency', 'client', 'users', 'paid_till', 'catalog', )


class ShopFormEmailUser(ShopForm):
    class Meta(ShopForm.Meta):
        fields = ('email', 'user_reply', 'user_from', 'user_subject', 'hello_text', 'user_sign')


class ShopFormEmailAdmin(ShopForm):
    class Meta(ShopForm.Meta):
        fields = ('email', 'user_reply', 'user_from', 'user_subject', 'hello_text', 'user_sign')


class ShopProfileForm(HelpForm, forms.ModelForm):
    vk_groupid = forms.ChoiceField(widget=widgets.Select(attrs={'class': 'input-block-level'}))

    class Meta(object):
        model = ShopProfile
        field = (
            'vk_url', 'vk_app_id', 'vk_app_secret', 'ga_vk', 'fb_url',
            'fb_taburl', 'fb_app_id', 'fb_app_secret', 'ga_fb', 'fb_groupid',
            'vk_groupname', )
        exclude = (
            'shop',
        )
        widgets = {
            'fb_app_id': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'fb_url': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'fb_taburl': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'fb_app_secret': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'ga_fb': widgets.Textarea(
                attrs={'class': 'input-block-level'}),
            'vk_app_id': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'vk_url': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'vk_app_secret': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'ok_app_id': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'ok_url': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'ok_app_secret': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'ok_app_public': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'ga_vk': widgets.Textarea(
                attrs={'class': 'input-block-level'}),
            'ga_ok': widgets.Textarea(
                attrs={'class': 'input-block-level'}),
            'fb_groupid': widgets.TextInput(
                attrs={'class': 'input-block-level'}),
            'vk_groupname': widgets.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        shopprofile = kwargs.get('instance')
        groups = [(0, u'-- Не выбрано --')]

        if shopprofile.vk_groupid and int(shopprofile.vk_groupid) != 0:
            groups.append((
                int(shopprofile.vk_groupid), shopprofile.vk_groupname))

        if self.user.shoppyuser.vk_token_valid():

            user_vk_token = self.user.shoppyuser.vk_token
            if user_vk_token:
                vk = vkontakte.API(
                    settings.VK_APP_ID, settings.VK_APP_SECRET, user_vk_token)
                vk_groups = vk.groups.get(extended=1,filter='admin')[1:]
                groups += map(itemgetter('gid', 'name'), vk_groups)

        super(ShopProfileForm, self).__init__(*args, **kwargs)
        self.fields['vk_groupid'].choices = sorted(
            set(groups), key=itemgetter(1))

    def clean_vk_groupname(self):
        token = self.user.shoppyuser.vk_token
        group_id = self.cleaned_data.get('vk_groupid',None)
        group_name = self.cleaned_data['vk_groupname']

        if group_id:
            if int(group_id) == 0:
                return ''

            if (not group_name or 'vk_groupid' in self.changed_data) and token:
                vk = vkontakte.API(
                    settings.VK_APP_ID, settings.VK_APP_SECRET, token)
                group = vk.groups.getById(group_id=group_id)[0]
                return group['name']
        return group_name


class ShopCategoryForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = Category
        fields = ('name', 'status',)
        exclude = ('shop', 'oid', 'active_products_count', 'parent')
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'order': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'status': widgets.CheckboxInput(attrs={'class': 'switcher'}),
        }

    def __init__(self, *args, **kwargs):
        datasource = kwargs.pop('datasource')
        super(ShopCategoryForm, self).__init__(*args, **kwargs)

    def formset__init__(self, *args, **kwargs):
        datasource = kwargs.pop('datasource')
        super(ShopCategoryForm, self).__init__(*args, **kwargs)
        # q = self.fields['parent'].queryset.filter(shop=shop_obj)
        # if 'instance' in kwargs:
        #     self.fields['parent'].queryset = q.exclude(pk=kwargs['instance'].pk)
        # else:
        #     self.fields['parent'].queryset = q


class ShopNewsForm(HelpForm, forms.ModelForm):
    class Meta:
        model = News
        # exclude = ()
        hidden_fields = ('shop', 'cdate',)
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'content': widgets.Textarea(attrs={'class': 'wysiwyg', 'id': 'source-view'}),
        }



class ShopPageForm(HelpForm, forms.ModelForm):
    class Meta:
        model = Page
        #exclude = ('order')
        hidden_fields = ('shop')
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'alias': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'order': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'content': widgets.Textarea(attrs={'class': 'wysiwyg', 'id': 'source-view'}),
        }


class ShopProductForm(HelpForm, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.datasource = kwargs.pop('datasource')
        super(ShopProductForm, self).__init__(*args, **kwargs)
        self.fields['categories'].queryset = self.fields['categories'].queryset.filter(datasource=self.datasource)

    def is_valid(self):
        if super(ShopProductForm, self).is_valid():
            products = Product.objects.filter(
                datasource=self.datasource,
                model_oid=self.cleaned_data['oid']
            )
            if self.instance and self.instance.pk:
                products = products.exclude(pk=self.instance.pk)
            if products.count() != 0:
                self.errors['oid'] = ErrorList(
                    [u'Артикул товара должен быть уникальным']
                )
                return False
            return True
        return False

    class Meta(object):
        model = Product
        fields = ('name', 'oid', 'desc', 'categories', 'status', 'url', 'price')
        hidden = ('datasource', )
        exclude = ('misc', 'images', 'model_oid', 'mtime')
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'desc': widgets.Textarea(attrs={'class': 'wysiwyg form-text text-editor source-view', 'id': 'source-view'}),
            'status': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'price': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'categories': widgets.SelectMultiple(attrs={'class': 'input-block-level'}),
            'oid': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'url': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }


#class ShopDeliveryForm(HelpForm, forms.ModelForm):
#    class Meta:
#        model = Shop
#        fields = ('delivery',)


#class ShopContactsForm(forms.ModelForm):
#    class Meta:
#        model = Shop
#        fields = ('contacts',)


class LoginForm(HelpForm, forms.Form):
    username = forms.CharField(
        max_length=255,
        widget=widgets.TextInput(attrs={'class': 'span12 title'}),
        label=u'Имя пользователя',
        required=True
    )
    password = forms.CharField(
        widget=widgets.PasswordInput(attrs={'class': 'span12 title'}),
        label=u'Пароль',
        required=True
    )


class DiscountForm(HelpForm, forms.ModelForm):
    products_id = forms.CharField(
        widget=widgets.HiddenInput(),
        required=False
    )

    categories_id = forms.CharField(
        widget=widgets.HiddenInput(),
        required=False
    )

    coupon_discount = forms.BooleanField(
        label="Скидка активируется купоном",
        widget=widgets.CheckboxInput(attrs={'class': 'switcher'}),
        required=False
    )

    show_additional = forms.BooleanField(
        label="Показать дополнительные настройки",
        widget=widgets.CheckboxInput(attrs={'class': 'switcher'}),
        required=False
    )

    class Meta(object):
        model = Discount
        exclude = ('shop', 'products', 'type', 'threshold',
                   'cur_activation_count')
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'title'}),
            'value': widgets.TextInput(),
            'value_type': widgets.Select(),
            'application': widgets.RadioSelect(),
            'min_cart_price': widgets.TextInput(),
            'status': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'time_from': DateTimePickerWidget(),
            'time_to': DateTimePickerWidget(),
        }

    class Media(object):
        css = {
            'all': (
                'css/cabinet/backbone.autocomplete.css',
            )
        }
        js = (
            'js/cabinet/backbone.autocomplete.js',
            'js/cabinet/discount_form.js',
        )

    def __init__(self, *args, **kwargs):
        shop = kwargs.pop('shop')

        if 'instance' not in kwargs:
            kwargs['initial'] = {
                'value_type': Discount.VT_PERCENT
            }
            return super(DiscountForm, self).__init__(*args, **kwargs)

        instance = kwargs['instance']

        show_coupon = instance.coupon_key
        categories_count = instance.categories.count()
        products_count = instance.products.count()
        show_additional = (
            products_count > 0 or
            categories_count > 0 or
            instance.min_cart_price > 0 or
            show_coupon
        )


        products_id = ','.join([
            str(id)
            for id
            in instance.products.all().values_list('pk', flat=True)
        ])
        categories_id = ','.join([
            str(id)
            for id
            in instance.categories.all().values_list('pk', flat=True)
        ])

        kwargs['initial'] = {
            "products_id": products_id,
            "categories_id": categories_id,
            "coupon_discount": show_coupon,
            "show_additional": show_additional
        }

        return super(DiscountForm, self).__init__(*args, **kwargs)


class BannerForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = Banner
        exclude = ('shop', 'cdate', )
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'content': widgets.Textarea(attrs={'class': 'wysiwyg form-text text-editor source-view', 'id': 'source-view'}),
            'slot': widgets.Select(attrs={'class': 'input-block-level'}),
            'status': widgets.CheckboxInput(attrs={'class': 'switcher'}),
        }


class ShopExtraForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = ShopExtra
        fields = ('key', 'value')


class ShopExtraFormSet(BaseModelFormSet):
    def is_valid(self):
        if super(ShopExtraFormSet, self).is_valid():
            keys = []
            for form in self.forms:
                if form.cleaned_data:
                    key = form.cleaned_data['key'].lower()
                    if key in keys:
                        form.errors['key'] = ErrorList([u'Значение поля "ключ" должно быть уникальным'])
                        return False
                    else:
                        keys.append(key)
            return True
        else:
            return False


class NotificationForm(BaseCabinetModelForm):
    class Meta(object):
        model = Notification
        fields = ('text', 'type', 'date')
        widgets = {
            'text': widgets.Textarea(attrs={'rows': '3', 'class': 'input-block-level'}),
            'type': widgets.Select(attrs={'class': 'input-block-level'}),
            'date': widgets.DateTimeInput(attrs={'class': 'input-block-level'})
        }


class ChangePasswordForm(HelpForm, forms.Form):
    password_old = forms.CharField(widget=widgets.PasswordInput(attrs={'class': 'input-block-level'}), label=u'Текущий пароль')
    password1 = forms.CharField(widget=widgets.PasswordInput(attrs={'class': 'input-block-level'}), label=u'Пароль')
    password2 = forms.CharField(widget=widgets.PasswordInput(attrs={'class': 'input-block-level'}), label=u'Повторите ввод пароля')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def is_valid(self):
        if super(ChangePasswordForm, self).is_valid():
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                self.errors['password2'] = ErrorList([u'Пароли не совпадают'])
                return False
            if not self.user.check_password(self.cleaned_data['password_old']):
                self.errors['password_old'] = ErrorList([u'Текущий пароль введен неверно'])
                return False
            return True
        return False


class UserForm(HelpForm, forms.ModelForm):
    roles = forms.ModelMultipleChoiceField(
        queryset=Role.objects.all(), label=u'Роли', widget=widgets.CheckboxSelectMultiple,
        required=True)
    tel = forms.CharField(
        label=u'Номер телефона', required=False,
        widget=widgets.TextInput(attrs={'class': 'input-block-level'}),
        validators=[field_validators.telephone_number])

    class Meta(object):
        model = User
        fields = ('first_name', 'email', 'tel', 'roles')
        widgets = {
            'email': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'first_name': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            self.fields['roles'].initial = kwargs['instance'].shoppyuser.roles.all()
            self.fields['tel'].initial = kwargs['instance'].shoppyuser.tel

    def clean_tel(self):
        tel = self.cleaned_data['tel'].strip()
        if tel:
            return clean_str(tel, string.digits + '+')
        return tel

    def is_valid(self):
        if super(UserForm, self).is_valid():
            email = self.cleaned_data['email']
            first_name = self.cleaned_data['first_name']
            if not email:
                self.errors['email'] = ErrorList([u'Обязательное поле'])
                return False
            if not first_name:
                self.errors['first_name'] = ErrorList([u'Обязательное поле'])
                return False
            return True
        return False


class RobokassaForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = RobokassaPlatform
        fields = ('login', 'password1', 'password2')
        widgets = {
            'login': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'password1': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'password2': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }


class CheckoutForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = CheckoutPlatform
        fields = ('api_key', )
        widgets = {
            'api_key': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }


class DatasourceForm(HelpForm, forms.ModelForm):
    class Meta(object):
        model = DataSource
        fields = ('name', 'mode', 'yml_url', 'root_cat_oid')
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level title'}),
            'mode': widgets.Select(attrs={'class': 'input-block-level'}),
            'yml_url': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'root_cat_oid': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }

    def is_valid(self):
        if super(DatasourceForm, self).is_valid():
            if self.cleaned_data['mode'] == DataSource.MODE_AUTO and self.cleaned_data['yml_url'] == '':
                self.errors['yml_url'] = ErrorList([
                    u'Ссылка на YML файл обязательна для источников, '
                    u'работающих в автоматическом режиме'
                ])
                return False
            return True
        return False


class OfferFakeForm(HelpForm, forms.ModelForm):
    value = forms.CharField(max_length=100, required=True, label=u'Размер')

    class Meta(object):
        model = Offer
        fields = ('oid', 'value')

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        initial = kwargs.get('initial', {})
        if instance:
            value = instance.get_params().get(u'Размер', '')
            initial.update({'value': value})
            kwargs['initial'] = initial
        super(OfferFakeForm, self).__init__(*args, **kwargs)


class PasswordRecoveryForm(forms.Form):
    email = forms.EmailField(
        required=True,
        widget=widgets.TextInput(attrs={'class': 'span12 title'})
    )

    def is_valid(self):
        if super(PasswordRecoveryForm, self).is_valid():
            email = self.cleaned_data['email']
            if not ShoppyUser.objects.filter(sys_user__email=email):
                self.errors['email'] = ErrorList([u'Пользователя с таким email не существует'])
                return False
            return True
        return False


class RecoveryPassword2Form(forms.Form):
    password1 = forms.CharField(widget=widgets.PasswordInput(attrs={'class': 'input-block-level'}), label=u'Пароль')
    password2 = forms.CharField(widget=widgets.PasswordInput(attrs={'class': 'input-block-level'}), label=u'Повторите ввод пароля')

    def is_valid(self):
        if super(RecoveryPassword2Form, self).is_valid():
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                self.errors['password2'] = ErrorList([u'Пароли не совпадают'])
                return False
            return True
        return False


class ProductManualDescForm(forms.ModelForm):
    class Meta(object):
        model = Product
        fields = ('manual_desc', )
        widgets = {
            'manual_desc': widgets.Textarea(attrs={
                'rows': 3,
                'class': 'wysiwyg form-text text-editor source-view',
                'id': 'source-view'})
        }

    def is_valid(self):
        if super(ProductManualDescForm, self).is_valid():
            if not self.cleaned_data['manual_desc']:
                self.errors['manual_desc'] = ErrorList([
                    u'Нельзя использовать пустое описание для товара'])
                return False
            return True
        return False


class ProductGroupForm(forms.ModelForm):
    products_id = forms.CharField(
        widget=widgets.HiddenInput(
            attrs={'class': 'input-block-level js-products'}), required=False)

    class Media(object):
        css = {
            'all': (
                'css/cabinet/backbone.autocomplete.css',
            )
        }
        js = (
            'js/cabinet/backbone.autocomplete.js',
            'js/cabinet/product_group_form.js',
        )

    class Meta(object):
        model = ProductGroup
        fields = ('name', 'use_for_slider', 'show_in_cart', )
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'use_for_slider': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'show_in_cart': widgets.CheckboxInput(attrs={'class': 'switcher'}),
        }

    def __init__(self, *args, **kwargs):
        self.shop = kwargs.pop('shop')
        if 'instance' in kwargs:
            instance = kwargs['instance']
            initial = ','.join(map(str, instance.products.all().values_list('pk', flat=True)))
            kwargs['initial'] = dict(products_id=initial)
        super(ProductGroupForm, self).__init__(*args, **kwargs)

    def is_valid(self):
        if super(ProductGroupForm, self).is_valid():
            if self.cleaned_data['use_for_slider']:
                groups = ProductGroup.objects.filter(use_for_slider=True, shop=self.shop)
                if self.instance.pk:
                    groups = groups.exclude(pk=self.instance.pk)
                if groups:
                    self.errors['use_for_slider'] = ErrorList([u'Только одна группа товаров может отображаться в слайдере. Отключите группу "%s" от слайдера и выберите новую группу для слайдера' % groups[0].name])
                    return False
            if self.cleaned_data['show_in_cart']:
                groups = ProductGroup.objects.filter(show_in_cart=True, shop=self.shop)
                if self.instance.pk:
                    groups = groups.exclude(pk=self.instance.pk)
                if groups:
                    self.errors['show_in_cart'] = ErrorList([u'Только одна группа товаров может отображаться в корзине. Отключите группу "%s" от корзины и выберите новую группу для корзины' % groups[0].name])
                    return False
            return True
        return False


class SelectBannerFrequencyForm(forms.ModelForm):
    class Meta(object):
        model = Shop
        fields = ('email_banner_frequency', )

class LandingSectionFormMixin(forms.ModelForm):
    # order = forms.ChoiceField(widget=widgets.Select(attrs={'class': 'input-block-level'}), label=u'Позиция')

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        super(LandingSectionFormMixin, self).__init__(*args, **kwargs)
        # if instance:
        #     self.fields['order'].choices = self.get_order_choices(instance.landing)

    def get_order_choices(self, landing):
        res = []
        sections = sorted(landing.get_sections(), key=lambda section: section.order)
        for section in sections:
            res.append((section.order, section.order))
        return res

    # def is_valid(self):
    #     if super(LandingSectionFormMixin, self).is_valid():
    #         instance = getattr(self, 'instance', None)
    #         if instance:
    #             sections = instance.landing.get_sections()
    #             sections.remove(instance)
    #             order2section = dict(map(lambda s: (s.order, s), sections))
    #             cur_order = int(self.cleaned_data['order'])
    #             if cur_order in order2section:
    #                 self.errors['order'] = ErrorList([u'Эта позиция занята разделом "%s"' % order2section[cur_order]])
    #                 return False
    #         return True
    #     return False

    # def save(self, *args, **kwargs):
    #     instance = self.instance
    #     cur_order = int(self.cleaned_data['order'])
    #     sections = instance.landing.get_sections()
    #     sections.remove(instance)
    #     orders = map(lambda s: (s.order, s), sections)
    #     if cur_order in orders:
    #         sections_for_reorder = filter(
    #             lambda s: s.order >= cur_order, sections)
    #         sorted_sections = sorted(
    #             sections_for_reorder, reverse=True, key=lambda s: s.order)
    #         with transaction.commit_on_success():
    #             for section in sorted_sections:
    #                 print section, section.order
    #                 # section.order += 1
    #                 # section.save()
    #     obj = super(LandingSectionFormMixin, self).save(*args, **kwargs)
    #     return obj


class LandingSectionMainForm(LandingSectionFormMixin):
    class Meta(object):
        model = LSectionMain
        fields = ('is_active', 'title', 'desc', 'custom_style')
        widgets = {
            'is_active': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'title': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg-title'}),
            'desc': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg'}),
            'custom_style': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }


class LandingSectionCounterForm(LandingSectionFormMixin):
    class Meta(object):
        model = LSectionCounter
        fields = ('is_active', 'title', 'date', 'show_in_footer', 'custom_style', )
        widgets = {
            'is_active': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'show_in_footer': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'title': widgets.Textarea(attrs={'class': 'wysiwyg-title input-block-level'}),
            'date': widgets.DateTimeInput(attrs={'class': 'input-block-level'}),
            'custom_style': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }


class LandingSectionProductsForm(LandingSectionFormMixin):
    class Meta(object):
        model = LSectionProducts
        fields = ('is_active', 'title', 'product_group', 'custom_style', )
        widgets = {
            'is_active': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'title': widgets.Textarea(attrs={'class': 'wysiwyg-title input-block-level'}),
            'product_group': widgets.Select(attrs={'class': 'input-block-level'}),
            'custom_style': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }

    def __init__(self, *args, **kwargs):
        shop_obj = kwargs.pop('shop')
        super(LandingSectionProductsForm, self).__init__(*args, **kwargs)
        self.fields['product_group'].queryset = shop_obj.product_groups.all()


class LandingSectionCompareForm(LandingSectionFormMixin):
    class Meta(object):
        model = LSectionCompare
        fields = (
            'is_active', 'left_title', 'left_text', 'right_title',
            'right_text', 'custom_style', )
        widgets = {
            'is_active': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'left_title': widgets.Textarea(attrs={'class': 'wysiwyg-title input-block-level'}),
            'right_title': widgets.Textarea(attrs={'class': 'wysiwyg-title input-block-level'}),
            'left_text': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg'}),
            'right_text': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg'}),
            'custom_style': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }


class LandingSectionCommentsForm(LandingSectionFormMixin):
    class Meta(object):
        model = LSectionComments
        fields = ('is_active', 'title', 'custom_style', )
        widgets = {
            'is_active': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'title': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg-title'}),
            'custom_style': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }


class LandingSectionCommentForm(forms.ModelForm):
    class Meta(object):
        model = LSectionComment
        fields = ('author', 'text', )
        widgets = {
            'author': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'text': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg'}),
        }


class LandingSectionSimpleForm(LandingSectionFormMixin):
    class Meta(object):
        model = LSectionSimple
        fields = ('is_active', 'title', 'text', 'custom_style',)
        widgets = {
            'is_active': widgets.CheckboxInput(attrs={'class': 'switcher'}),
            'title': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg-title'}),
            'text': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg'}),
            'custom_style': widgets.TextInput(attrs={'class': 'input-block-level'}),
        }

class BannerBaseForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.shop = kwargs.pop('shop')
        super(BannerBaseForm, self).__init__(*args, **kwargs)


class BannerTextForm(BannerBaseForm):
    product_name = forms.CharField(
        required=False, max_length=200,
        widget=widgets.TextInput(attrs={
            'class': 'input-block-level '
                     'js-product-auto-complete '
                     'banner-item_input'}))

    class Media(object):
        js = (
            'js/libs/backbone.marionette.min.js',
            'js/libs/typeahead.min.js',
            'js/cabinet/banners/views.js',
        )

    class Meta(object):
        model = BannerText
        fields = ('slot', 'name', 'status', 'text', 'product', )
        widgets = {
            'slot': widgets.Select(attrs={'class': 'input-block-level'}),
            'text': widgets.Textarea(attrs={'class': 'input-block-level wysiwyg'}),
            'product': widgets.HiddenInput(
                attrs={'class': 'input-block-level product-id'}),
            #'product': widgets.Select(attrs={'class': 'input-block-level select2-select'}),
            'name': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'status': widgets.CheckboxInput(attrs={'class': 'switcher'}),
        }

    def __init__(self, *args, **kwargs):
        super(BannerTextForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance', None)
        if instance and instance.product:
            self.fields['product'].product_object = instance.product
            self.fields['product_name'].initial = instance.product.name

        #self.fields['product'].queryset = Product.objects.none() #.filter(datasource__in=self.shop.datasources.all())


class BannerCarouselForm(BannerBaseForm):
    class Meta(object):
        model = BannerCarousel
        fields = ('slot', 'name', 'status', 'product_group', )
        widgets = {
            'slot': widgets.Select(attrs={'class': 'input-block-level'}),
            'product_group': widgets.Select(attrs={'class': 'input-block-level'}),
            'name': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'status': widgets.CheckboxInput(attrs={'class': 'switcher'}),
        }

    def __init__(self, *args, **kwargs):
        super(BannerCarouselForm, self).__init__(*args, **kwargs)
        self.fields['product_group'].queryset = self.shop.product_groups.all()


class BannerGraphicForm(BannerBaseForm):
    class Meta(object):
        model = BannerGraphic
        fields = ('slot', 'name', 'status', )
        widgets = {
            'slot': widgets.Select(attrs={'class': 'input-block-level'}),
            'name': widgets.TextInput(attrs={'class': 'input-block-level'}),
            'status': widgets.CheckboxInput(attrs={'class': 'switcher'}),
        }

    class Media(object):
        js = (
            'js/libs/backbone.marionette.min.js',
            'js/libs/typeahead.min.js',
            'js/cabinet/banners/views.js',
        )


class BannerGraphicImageForm(forms.ModelForm):
    product_name = forms.CharField(
        required=False, max_length=200,
        widget=widgets.TextInput(attrs={
            'class': 'input-block-level '
                     'js-product-auto-complete '
                     'banner-item_input'}))

    class Meta(object):
        model = BannerGraphicImage
        fields = ('image', 'product', )
        widgets = {
            'image': widgets.FileInput(attrs={'class': 'input-block-level'}),
            'product': widgets.HiddenInput(
                attrs={'class': 'input-block-level product-id'}),
        }

    def clean_image(self):
        slot_pk = self.data.get('slot')
        slot_obj = BannerSlot.objects.get(pk=slot_pk)
        image = self.cleaned_data['image']
        im = Image.open(image)
        width, height = im.size
        if slot_obj:
            w, h = slot_obj.size_width, slot_obj.size_height
            if w and h:
                if w != width or h != height:
                    raise ValidationError(u'Размеры изображения должны быть '
                                          u'строго равны %dpx x %dpx' % (w, h))
        return image

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        initial_data = kwargs.get('initial', {})
        if initial_data:
            shop = initial_data.pop('shop')
            kwargs['initial'] = initial_data
        else:
            shop = None
        super(BannerGraphicImageForm, self).__init__(*args, **kwargs)
        if instance and instance.product:
            self.fields['product'].product_object = instance.product
            self.fields['product_name'].initial = instance.product.name
        if shop:
            self.fields['product'].queryset = Product.objects.filter(
                datasource__in=shop.datasources.all())
