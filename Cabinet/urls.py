from django.conf.urls import patterns, include
from tastypie.api import Api
from Cabinet.api.resources import *

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

v1_api = Api(api_name='v1')
v1_api.register(CabinetCategoryResource())
v1_api.register(CabinetCommentResource())
v1_api.register(CabinetLandingResource())
v1_api.register(CabinetOrderResource())
v1_api.register(CabinetProductResource())
v1_api.register(Group2ProductResource())
v1_api.register(ProductGroupResource())


urlpatterns = patterns('',
    (r'^', include(v1_api.urls)),
)
