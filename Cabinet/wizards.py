from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from Cabinet.forms import *

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

FORMS = [
    ('base', NewShopBase),
    ('email', NewShopEmail1),
    ('email2', NewShopEmail2),
]

class ShopNewWizard(SessionWizardView):
    def get_template_names(self):
        return ['Cabinet/wizard/shop_create/wizard_base.html']

    def get_form_initial(self, step=None):
        initial = super(ShopNewWizard, self).get_form_initial(step=step)
        if step == 'base':
            initial['clients'] = self.request.user.clients.all()
            initial['user'] = self.request.user
        if step == 'email':
            initial['email'] = self.request.user.email
        return initial

    def get_form_kwargs(self, step=None):
        if step is not None:
            if step != 'email':
                return dict(user_obj=self.request.user)
        return {}

    def done(self, form_list, **kwargs):
        kwargs = {}
        client = form_list[0].cleaned_data.pop('client', None)
        assortment = form_list[0].cleaned_data.pop('assortment')
        mode = form_list[0].cleaned_data.pop('mode')
        yml_url = form_list[0].cleaned_data.pop('yml_url')
        kwargs.update(form_list[0].cleaned_data)
        kwargs.update(dict(
            email=form_list[1].cleaned_data['email'],
            user_reply=form_list[1].cleaned_data['email']
        ))
        kwargs.update(form_list[2].cleaned_data)
        if not client:
            kwargs['client'] = self.request.user.clients.all()[0]
        else:
            kwargs['client'] = client
        shop_obj = Shop(**kwargs)
        shop_obj.save()
        if assortment:
            ds = assortment
        else:
            ds = DataSource.objects.create(
                client=shop_obj.client,
                name=shop_obj.name,
                yml_url=yml_url or None,
                mode=int(mode)
            )
        shop_obj.datasources.add(ds)
        return HttpResponseRedirect(
            reverse('shop', kwargs=dict(shop_id=shop_obj.pk))
        )
