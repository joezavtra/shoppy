# coding=utf-8
from Core.utils import update_get_params

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'
from django import template

register = template.Library()


@register.filter
def multiply(arg1, arg2):
    return arg1 * arg2


@register.filter
def division(arg1, arg2):
    return float(arg1) / float(arg2)


@register.simple_tag
def pagination_link(request, page_param, page_num):
    url = '%s?%s' % (request.path, request.META['QUERY_STRING'])
    return update_get_params(url, {page_param: page_num})


@register.simple_tag(name='call_method')
def call_method_tag(obj, method_name, *args, **kwargs):
    if hasattr(obj, method_name):
        method = getattr(obj, method_name)
        if callable(method):
            return method(*args, **kwargs)
    return None


@register.filter
def get_attr(obj, method_name):
    if hasattr(obj, method_name):
        method = getattr(obj, method_name)
        return method
    return None


@register.filter
def call_attr(method, arg):
    return method(arg)


@register.filter
def help_text_for_model_filed(model_obj, field_name):
    """
    Возвращает значение help_text для указанного поля модели
    :param model_obj: models.Model
    :param field_name: text
    """
    return model_obj._meta.get_field(field_name).help_text


DANGER = 50
INFO = 40
IMPORTANT = 30
WARNING = 20
SUCCESS = 10
DEFAULT = 0
levels = {
    DANGER: {
        'class': 'danger',
        'desc': u'Критично'
    },
    INFO: {
        'class': 'info',
        'desc': u'Информация'
    },
    IMPORTANT: {
        'class': 'important',
        'desc': u'Важно'
    },
    WARNING: {
        'class': 'warning',
        'desc': u'Предупреждение'
    },
    SUCCESS: {
        'class': 'success',
        'desc': u'Удачно'
    },
    DEFAULT: {
        'class': 'default',
        'desc': u'Без статуса'
    },
}


@register.filter
def code2bootstrap_alert(code, field=None):
    code = int(code)
    if not field:
        field = 'desc'
    return levels.get(code, levels[DEFAULT])[field]