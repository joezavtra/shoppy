# -*- coding: utf-8 -*-
from collections import defaultdict
from django.conf.urls import url
from django.db import transaction
from tastypie import http
from tastypie.utils import trailing_slash
from django.db.models import Q
from Core.models import *
from tastypie import fields
from tastypie.authentication import Authentication
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource


__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class CabinetCategoryResource(ModelResource):
    datasource = fields.IntegerField('datasource_id')
    parent_name = fields.CharField('parent__name', readonly=True, blank=True, null=True)

    class Meta(object):
        resource_name = 'category'
        queryset = Category.objects.all().select_related('parent', 'datasource')
        filtering = {
            'name': 'icontains',
            'datasource': 'exact',
            'id': 'in'
        }
        limit = 0
        allowed_methods = ['get', 'post', 'patch']
        authentication = Authentication()
        authorization = Authorization()


    def build_filters(self, filters=None):
        res = super(CabinetCategoryResource, self).build_filters(filters)
        query = filters.get('query', None)
        shop_id = filters.get('shop_id', None)
        if query and query.isdigit():
            res.update({
                'id': query
            })
        elif query:
            res.update({
                'name__icontains': query
            })
        if shop_id:
            ds_id = Shop.objects.get(pk=shop_id).datasources.all().values_list('id', flat=True)
            res.update({
                'datasource__id__in': ds_id
            })
        return res


class CabinetOrderResource(ModelResource):

    class Meta(object):
        resource_name = 'order'
        queryset = Order.objects.all()
        allowed_methods = ['get', 'delete', 'patch']
        authentication = Authentication()
        authorization = Authorization()

    def obj_delete(self, bundle, **kwargs):
        if bundle.request.user.is_superuser:
            super(CabinetOrderResource, self).obj_delete(bundle, **kwargs)

    def obj_update(self, bundle, skip_errors=False, **kwargs):
        order_bundle = super(CabinetOrderResource, self).obj_update(bundle, skip_errors=skip_errors, **kwargs)
        OrderHistory.objects.create(
            order=order_bundle.obj,
            new_status=order_bundle.obj.status,
            author=order_bundle.request.user
        )
        return order_bundle


class CabinetOfferResource(ModelResource):
    product_id = fields.IntegerField('product_id')

    class Meta(object):
        resource_name = 'offer'
        queryset = Offer.objects.all()
        allowed_methods = ['get', 'delete', 'patch']
        authentication = Authentication()
        authorization = Authorization()


class CabinetCommentResource(ModelResource):
    shop_id = fields.IntegerField('shop_id')
    product_id = fields.IntegerField('product_id')
    reply_to_id = fields.IntegerField('reply_to_id', blank=True, null=True)

    class Meta(object):
        resource_name = 'comments'
        queryset = Comment.objects.all()
        allowed_methods = ['get', 'delete', 'post']
        always_return_data = True
        authentication = Authentication()
        authorization = Authorization()

    def obj_create(self, bundle, **kwargs):
        bundle.data['shop_id'] = Comment.objects.get(pk=bundle.data['reply_to_id']).shop_id
        return super(CabinetCommentResource, self).obj_create(bundle, **kwargs)



class CabinetProductResource(ModelResource):

    class Meta(object):
        resource_name = 'product'
        queryset = Product.objects.all()
        allowed_methods = ['get']
        filtering = {'id': 'in'}
        always_return_data = True
        authentication = Authentication()
        authorization = Authorization()

    def get_object_list(self, request):
        objects = super(CabinetProductResource, self).get_object_list(request)
        query = request.GET.get('query', None)
        shop_id = request.GET.get('shop_id', None)
        args, kwargs = [], {}
        if query:
            args.append(
                Q(oid=query) | Q(model_oid=query) | Q(name__icontains=query))
            try:
                args[0] = args[0] | Q(id=int(query))
            except:
                pass

        if shop_id:
            ds_id = Shop.objects.get(pk=shop_id).datasources.all().values_list(
                'id', flat=True)
            kwargs['datasource__id__in'] = list(ds_id)
        return objects.filter(*args, **kwargs)

    def dehydrate(self, bundle):
        discount_pk = bundle.request.GET.get('discount_pk', None)
        if discount_pk:
            discount = Discount.objects.get(pk=discount_pk)
            bundle.data['price_with_discount'] = discount.apply(bundle.obj.price)
        bundle.data['cover'] = bundle.obj.cover()
        return bundle


class ProductGroupResource(ModelResource):
    shop_id = fields.IntegerField('shop_id')

    class Meta(object):
        resource_name = 'product-group'
        queryset = ProductGroup.objects.all()
        allowed_methods = ['get']
        filtering = {'shop_id': 'exact'}


class Group2ProductResource(ModelResource):
    product_id = fields.IntegerField('product_id')
    group_id = fields.IntegerField('group_id')

    class Meta(object):
        resource_name = 'group2product'
        queryset = Group2Product.objects.all()
        allowed_methods = ['get', 'post', 'delete']
        filtering = {
            'product_id': 'exact',
            'group_id': 'exact'
        }
        authentication = Authentication()
        authorization = Authorization()


class CabinetLandingResource(ModelResource):
    class Meta(object):
        resource_name = 'landing'
        model = Landing
        queryset = Landing.objects.all()
        allowed_methods = ['get']
        always_return_data = True
        authorization = Authorization()

    def prepend_urls(self):
        return [
            url(r'^(?P<resource_name>%s)/(?P<landing_pk>\d+)/section-move%s$' % (self._meta.resource_name, trailing_slash()), self.wrap_view('move_section'), name='api_move_section'),
        ]

    def move_section(self, request, landing_pk, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        self.throttle_check(request)

        user = request.user
        clients = user.clients.all()
        if not user.is_staff:
            shops = Shop.objects.filter(client__in=clients)
            landing = Landing.objects.get(pk=landing_pk, shop__in=shops)

        if user.is_staff:
            landing = Landing.objects.get(pk=landing_pk)

        sections = landing.get_sections()
        dict_sections = defaultdict(dict)

        for s_type, s in map(lambda s: (s.TYPE_CODE, s), sections):
            dict_sections[s_type][s.pk] = s

        data = self.deserialize(
            request, request.body,
            format=request.META.get('CONTENT_TYPE', 'application/json'))

        with transaction.commit_on_success():
            cur_order = 1
            for section_record in data:
                section_by_id = dict_sections[section_record['type']]
                section_id = long(section_record['id'])
                section = section_by_id.get(section_id, None)
                if section:
                    section.order = cur_order
                    section.save()
                    cur_order += 1

        self.log_throttled_access(request)
        return http.HttpAccepted()
