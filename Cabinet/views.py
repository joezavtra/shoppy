# -*- coding: utf-8 -*-
import datetime
import hashlib
import random
import shutil
from itertools import imap
from math import log10
import urllib
from django.core import management
from django.core.paginator import Paginator, EmptyPage
from django.core.mail import send_mail
from django.core.cache import cache

from django.db import transaction
from django.db.models import Count

from django.forms.models import modelformset_factory, formset_factory, \
    inlineformset_factory

from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.cache import cache_page
import math
import itertools
import vkontakte

from Core.amocrm_api import AmoManager
from Core.checkout import get_ticket

from Core.decorators import *
from Cabinet.forms import *
from Cabinet import analytics
from Core.queryset_sorter import SortQueryset
from Core.utils import random_password
# from app_walletone.models import WalletOnePayment
from social_alert.models import MessageText, Message

l = logging.getLogger('app')
PAGE_PARAM_NAME = 'page'


@login_required
@render_to('Cabinet/main.html')
@csrf_protect
def main(r):
    if r.user.is_superuser:
        try:
            shop_id = Shop.objects.values_list('id', flat=True)[0]
            return redirect(reverse('shop', kwargs=dict(shop_id=shop_id)))
        except IndexError:
            # No shop exists in system
            pass

    clients = r.user.clients.all()
    if not clients:
        return dict(
            err_msg=u'К вашей учетной записи не привязан ни один клиент'
        )
    shops = Shop.objects.filter(client__in=clients)
    if not shops:
        return HttpResponseRedirect(reverse('new_shop'))
    s = shops[0]
    return HttpResponseRedirect(reverse('shop', args=[s.id]))


@render_to('Cabinet/login.html')
def login_view(request):
    form = LoginForm(request.POST or None)
    res = True
    if form.is_valid():
        user = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password']
        )
        if user is not None and user.is_active:
            login(request, user)
            analytics.identify(user.id, {
                "email": user.email,
                "username": user.username,
                "app": "cabinet"
            })
            return HttpResponseRedirect(reverse('main'))
        res = False
    return {
        'form': form,
        'res': res
    }


@render_to('Cabinet/password_recovery.html')
def password_recovery(r):
    form = PasswordRecoveryForm(r.POST or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        customer_obj = ShoppyUser.objects.get(sys_user__email=email)
        customer_obj.create_recovery_token()
        url = 'http://shoppyboom.ru%s' % reverse(
            'password_recovery_step2',
            kwargs=dict(token=customer_obj.recovery_token)
        )
        data = dict(
            email=email,
            url=url,
            token_expired_hours=settings.PASSWORD_RECOVERY_TOKEN_EXPIRED_HOURS
        )
        passwd_recovery_html = render_to_string('email/password_recovery.html', data)
        EmailQueue.objects.create(
            email_from=u'ShoppyBoom <no-reply@shoppyboom.ru>',
            email_to=email,
            bcc='password.recovery@shoppyboom.ru',
            reply_to='no-reply@shoppyboom.ru',
            subject=u'ShoppyBoom - Восстановление пароля',
            message_type=EmailQueue.MTYPE_HTML,
            body=passwd_recovery_html
        )
        return dict(result=True)
    return dict(form=form)


@login_required
@only_clients
@render_to('Cabinet/shop_change_tariff.html')
def shop_change_tariff(r, shop_obj):
    current_tariff = shop_obj.tariff
    all_tariffs = Tariff.objects.all().order_by('order')
    available_tariffs = all_tariffs.filter(is_archive=False, is_default=False)
    resp_message = None

    if r.method == 'POST':
        action = r.POST.get('action', 'apply')
        tariff_pk = r.POST.get('tariff', current_tariff.pk)
        month_count = int(r.POST.get('month_count', 1))
        if action == 'apply':
            new_tariff = get_object_or_404(available_tariffs, pk=tariff_pk)
            shop_obj.tariff = new_tariff
            if not new_tariff.max_products:
                p_link = 'http://shoppyboom.ru/admin/Core/clientpayment/'
                textm = u'Магазин <a href="http://shoppyboom.ru%s">%s</a> перешел на безлимитный тариф "%s". <a href="%s">Перейти к счетам</a>' % (reverse('shop', kwargs=dict(shop_id=shop_obj.pk)), shop_obj.name, new_tariff.name, p_link)
                EmailQueue.objects.create(
                    email_from='payments@shoppyboom.ru',
                    email_to='info@shoppyboom.ru',
                    subject=u'Переход на безлимитный тариф',
                    message_type=EmailQueue.MTYPE_HTML,
                    body=textm
                )
            else:
                new_paid_till = shop_obj.calculate_tariff_migration(new_tariff)
                if new_paid_till:
                    shop_obj.paid_till = new_paid_till
            shop_obj.save(update_fields=['tariff', 'paid_till'])
        elif action == 'extend':
            if not current_tariff.max_products:
                p_link = 'http://shoppyboom.ru/admin/Core/clientpayment/'
                textm = u'Магазин <a href="http://shoppyboom.ru%s">%s</a> запрашивает продление тарифа "%s" на %d месяцев. <a href="%s">Перейти к счетам</a>' % (
                    reverse('shop', kwargs=dict(shop_id=shop_obj.pk)),
                    shop_obj.name,
                    current_tariff.name,
                    month_count,
                    p_link
                )
                EmailQueue.objects.create(
                    email_from='payments@shoppyboom.ru',
                    email_to='info@shoppyboom.ru',
                    subject=u'Продление безлимитного тарифа',
                    message_type=EmailQueue.MTYPE_HTML,
                    body=textm
                )
                messages.success(r,
                    u'Заявка на продление тарифа отправлена')
            else:
                cost = month_count * current_tariff.cost
                ClientPayment.objects.create(
                    client=shop_obj.client,
                    shop=shop_obj,
                    amount=cost,
                    days=month_count * 31,
                    desc=u'Оплата тарифа "%s". Количество месяцев: %d' % (
                        current_tariff.name, month_count)
                )
                resp_message = (u'Вам выставлен счет на оплату продления '
                                u'тарифа. Для продолжения перейдите '
                                u'на <a href="%s">страницу</a> оплаты счета')
                resp_message = resp_message % reverse(
                    'shop_payment_list', kwargs=dict(shop_id=shop_obj.pk))
                return dict(
                    tariff=current_tariff,
                    tariffs=all_tariffs,
                    available_tariffs=available_tariffs,
                    shop=shop_obj,
                    resp_message=resp_message
                )
                # messages.success(r,
                #     u'Перейдите на страницу списка счетов для оплаты выбранного тарифа')
        else:
            raise
        return redirect(
            reverse('shop_change_tariff', kwargs=dict(shop_id=shop_obj.pk)))
    return dict(
        current_tariff=current_tariff,
        tariffs=all_tariffs,
        available_tariffs=available_tariffs,
        shop=shop_obj,
        resp_message=resp_message
    )


@render_to('Cabinet/password_recovery.html')
def password_recovery_step2(r, token):
    shoppy_user_obj = get_object_or_404(ShoppyUser, recovery_token=token)
    if not shoppy_user_obj.recovery_token_is_valid():
        return HttpResponseServerError(
            u'Кто-то уже перешел по этой ссылке или время ее жизни исчерпано')
    user = shoppy_user_obj.sys_user
    form = RecoveryPassword2Form(r.POST or None)
    if form.is_valid():
        password = form.cleaned_data['password1']
        user.set_password(password)
        user.save()
        messages.success(r, u'Пароль успешно изменен')
        shoppy_user_obj.destroy_recovery_token()

        user = authenticate(username=user.username, password=password)
        if user is not None and user.is_active:
            login(r, user)
            return HttpResponseRedirect(reverse('main'))

    return dict(
        form=form
    )


# @login_required
def logout_view(request):
    logout(request)
    request.session.flush()
    request.session.clear()
    return HttpResponseRedirect(reverse('main'))


@to_json
def signup(request):
    is_partner = 'partner' in request.GET
    promocode = request.GET.get('coupon', request.POST.get('coupon', ''))
    email = request.GET.get('email', request.POST.get('email', None))
    name = request.GET.get('name', request.POST.get('name', None))
    site = request.GET.get('site', request.POST.get('site', None))
    phone = request.GET.get('phone', request.POST.get('phone', None))
    referer = request.GET.get('referer', request.POST.get('referer', None))
    reg_link = None
    try:
        user_obj, created = User.objects.get_or_create(
            username=email,
            first_name=email,
            is_staff=False,
            is_active=False,
            email=email
        )
        user_obj.save()

        client_obj = Client(name=name or email)
        try:
            client_obj.partner = Partner.objects.get(code=promocode)
        except:
            pass
        client_obj.save()
        client_obj.users.add(user_obj)

        reg_link = 'http://shoppyboom.ru' + reverse(
            'account_verify', kwargs=dict(token=user_obj.shoppyuser.token)
        )
        email_text = render_to_string(
            'email/client_user_verification.html',
            dict(reg_link=reg_link)
        )
        EmailQueue.objects.create(
            email_from=u'Служба заботы о пользователях ShoppyBoom.Ru <signup@shoppyboom.ru>',
            email_to=email,
            bcc='signup@shoppyboom.ru',
            reply_to=u'Команда ShoppyBoom.Ru <team@shoppyboom.ru>',
            subject=u'Shoppyboom - Подтверждение регистрации',
            message_type=EmailQueue.MTYPE_HTML,
            body=email_text
        )
    except Exception as e:
        logger.error('Error on registration: "%s"' % str(e))

    params = []
    for k, v in request.GET.iteritems():
        params.append(u'"%s" = "%s"' % (k, v))
    for k, v in request.POST.iteritems():
        params.append(u'"%s" = "%s"' % (k, v))

    try:
        manager = AmoManager()
        manager.auth()

        lead = {
            'name': 'From Site',
            'status_id': 7467884,
        }
        lead_obj = manager.lead_add([lead])
        lead_id = lead_obj['response']['leads']['add'][0]['id']
        extended_data = u'Ссылка регистрации: %s' % reg_link
        fields = []
        if phone:
            fields.append({
                'id': 574594,
                'values': [{'value': str(phone), 'enum': 'WORK'}]
            })
        if email:
            fields.append({
                "id": 574596,
                'values': [{'value': email, 'enum': 'WORK'}]
            })
        if site:
            fields.append({
                'id': 574598,
                'values': [
                    {'value': site or u'сайт не указан'},
                ]
            })
        if extended_data:
            fields.append({
                'id': 574602,
                'values': [
                    {'value': extended_data}
                ]
            })
        if referer:
            fields.append({
                'id': 647036,
                'values': [
                    {'value': referer or ''}
                ]
            })

        fields.append({
            'id': 580846,
            'values': [
                {'value': 1272324 if is_partner else 1272322}
            ]
        })
        contact = {
            'name': name,
            'responsible_user_id': settings.AMOCRM_USER_BASE_ID,
            'linked_leads_id': [lead_id],
            'custom_fields': fields,
            'tags': promocode
        }
        manager.contact_add([contact])
    except Exception as e:
        logger.error('Cannot send data to AmoCRM: "%s"' % str(e))

    data = u"\n".join(params)
    try:
        send_mail(
            u'Новый подписчик',
            u'Привет, дорогая команда ShoppyBoom!\n\nСпешу сообщить, что у нас '
            u'появился новый подписчик "%s"\n'
            u'Дпо. данные: \n%s\nC уважением,\nВаш верный писатель '
            u'Чукча' % (email, data),
            'Чукча писатель ShoppyBoom.Ru <no-reply@shoppyboom.ru>',
            ['signup@shoppyboom.ru']
        )
    except:
        pass

    return {'status': 1}


@render_to('Cabinet/account_verify.html')
def account_verify(r, token):
    if not token:
        raise HttpResponseServerError
    try:
        token_expired = timezone.now() - timezone.timedelta(days=10)
        shoppyuser_obj = ShoppyUser.objects.get(
            token=token
        )
    except ObjectDoesNotExist:
        return dict(
            status=False,
            response=u'Время жизни ссылки истекло'
        )
    if shoppyuser_obj.token_created < token_expired:
        shoppyuser_obj.sys_user.clients.all().delete()
        shoppyuser_obj.sys_user.delete()
        return dict(
            status=False,
            response=u'Время жизни ссылки истекло'
        )
    shoppyuser_obj.sys_user.is_active = True
    password = ''.join([str(random.randint(0, 9)) for i in xrange(5)])
    shoppyuser_obj.sys_user.set_password(password)
    shoppyuser_obj.sys_user.save()
    try:
        email_text = render_to_string(
            'email/client_user_registration.html',
            dict(username=shoppyuser_obj.sys_user.username, password=password)
        )
        EmailQueue.objects.create(
            email_from=u'Служба заботы о пользователях ShoppyBoom.Ru <signup@shoppyboom.ru>',
            email_to=shoppyuser_obj.sys_user.email,
            bcc='signup@shoppyboom.ru',
            reply_to=u'Команда ShoppyBoom.Ru <team@shoppyboom.ru>',
            subject=u'Shoppyboom - Успешная регистрация',
            message_type=EmailQueue.MTYPE_HTML,
            body=email_text
        )

    except Exception as e:
        logger.error('Cannot send login/password email to user %d: "%s"' % (shoppyuser_obj.sys_user.pk, str(e)))
    shoppyuser_obj.sys_user.backend = 'django.contrib.auth.backends.ModelBackend'
    login(r, shoppyuser_obj.sys_user)
    shoppyuser_obj.token_created = token_expired
    shoppyuser_obj.save()
    messages.success(r, u'Email адрес подтвержден успешно')
    return HttpResponseRedirect(reverse('main'))


@login_required
@only_clients
@render_to('Cabinet/shop.html')
def shop(r, shop_obj):
    return dict(
        shop=shop_obj
    )


@login_required
@only_clients
@render_to('Cabinet/shop_datasources_list.html')
def shop_datasource_list(r, shop_obj):
    datasources = shop_obj.datasources.all()
    if not r.user.is_superuser:
        all_datasources = DataSource.objects.filter(client__in=r.user.clients.all())
    else:
        all_datasources = DataSource.objects.all()

    all_datasources = all_datasources.prefetch_related('client')

    add_ds = r.GET.get('add_datasource')
    del_ds = r.GET.get('del_datasource')
    if del_ds or add_ds:
        if add_ds:
            ds = get_object_or_404(all_datasources, pk=add_ds)
            if ds not in shop_obj.datasources.all():
                shop_obj.datasources.add(ds)
        if del_ds:
            ds = get_object_or_404(all_datasources, pk=del_ds)
            if ds in shop_obj.datasources.all():
                if shop_obj.datasources.count() > 1:
                    shop_obj.datasources.remove(ds)
                else:
                    messages.error(r, u'Невозможно отвязать источник от '
                                      u'магазина. Магазин должен иметь хотя бы '
                                      u'один иточник')

        return redirect('shop_datasource_list', shop_id=shop_obj.pk)


    return dict(
        shop=shop_obj,
        datasources=datasources,
        all_datasources=all_datasources
    )


@login_required
@only_clients
@render_to("Cabinet/shop_datasources_edit_or_create.html")
def shop_datasource_edit_or_create(r, shop_obj, datasource_pk=None):
    if datasource_pk:
        ds_obj = get_object_or_404(shop_obj.datasources.all(), pk=datasource_pk)
        form = DatasourceForm(r.POST or None, instance=ds_obj)
    else:
        ds_obj = None
        form = DatasourceForm(r.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.client = shop_obj.client
        obj.save()
        if obj not in shop_obj.datasources.all():
            shop_obj.datasources.add(obj)
        return redirect(
            reverse('shop_datasource_list', kwargs=dict(shop_id=shop_obj.pk)))
    return dict(
        shop=shop_obj,
        datasource=ds_obj,
        form=form
    )


@login_required
@only_clients
@render_to('Cabinet/shop_base.html')
def shop_base(r, shop_obj):
    if r.user.is_superuser:
        form = ShopFormBaseAdmin(r.POST or None, instance=shop_obj, user_obj=r.user)
    else:
        form = ShopFormBaseUser(r.POST or None, instance=shop_obj, user_obj=r.user)

    if form.is_valid():
        form.save()
        shop_logo = r.FILES.get('shop_logo', None)
        if shop_logo is not None:
            shop_obj.set_logo(shop_logo)
        if shop_obj.domain == 'shoppyboom.ru':
            shop_obj.domain = shop_obj.alias
            shop_obj.save()
        return HttpResponseRedirect(
            reverse('shop_base', kwargs=dict(shop_id=shop_obj.id))
        )
    return dict(shop=shop_obj, form=form)


@login_required
@only_clients
@render_to('Cabinet/shop_email.html')
def shop_email(r, shop_obj):
    if r.user.is_superuser:
        form = ShopFormEmailAdmin(r.POST or None, instance=shop_obj, user_obj=r.user)
    else:
        form = ShopFormEmailUser(r.POST or None, instance=shop_obj, user_obj=r.user)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(
            reverse('shop_email', kwargs=dict(shop_id=shop_obj.id))
        )
    return dict(shop=shop_obj, form=form)


@login_required
@only_clients
@render_to('Cabinet/shop_integration.html')
def shop_integration(r, shop_obj):
    form = ShopFormPayment(r.POST or None, instance=shop_obj, user_obj=r.user)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(
            reverse('shop_payment', kwargs=dict(shop_id=shop_obj.id))
        )
    return dict(shop=shop_obj, form=form)


@login_required
@only_clients
@render_to('Cabinet/shop_integration_list.html')
def shop_integration_list(r, shop_obj):
    shop_platforms = shop_obj.get_platforms()
    platforms = []
    for platform_obj in Platform.objects.all():
        platforms.append(dict(
            platform=platform_obj,
            shop_platform=shop_platforms[platform_obj.code]
        ))
    return dict(shop=shop_obj, platforms=platforms)


@login_required
@only_clients
@render_to('Cabinet/shop_integration_service.html')
def integration_service_delete(r, shop_obj, platform_code):
    shop_platforms = shop_obj.platforms

    try:
        if platform_code == 'robokassa':
            platform_obj = shop_platforms.robokassa
            shop_platforms.robokassa = None
            shop_platforms.save()
        elif platform_code == 'checkout':
            platform_obj = shop_platforms.checkout
            shop_platforms.checkout = None
            shop_platforms.save()
        else:
            return HttpResponseServerError
        platform_obj.delete()
        messages.success(r, u'Сервис успешно удален')
    except:
        messages.error(r, u'Невозможно удалить сервис')
    return redirect(
        reverse('shop_integration_list', kwargs={'shop_id': shop_obj.pk}))


@login_required
@only_clients
@render_to('Cabinet/shop_integration_service.html')
def shop_integration_service(r, shop_obj, platform_code):
    platform_obj = get_object_or_404(Platform, code=platform_code)
    args, kwargs = (r.POST or None, ), dict()
    platforms = shop_obj.get_platforms()
    if platform_code == 'robokassa':
        form_class = RobokassaForm
        if platforms['robokassa']:
            kwargs['instance'] = platforms['robokassa']
    elif platform_code == 'checkout':
        form_class = CheckoutForm
        if platforms['checkout']:
            kwargs['instance'] = platforms['checkout']
    else:
        return HttpResponseServerError()
    form = form_class(*args, **kwargs)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.platform = platform_obj
        obj.save()
        shop_platform, created = ShopPlatform.objects.get_or_create(shop=shop_obj)
        setattr(shop_platform, platform_code, obj)
        shop_platform.save()
        return redirect('.')
    return dict(shop=shop_obj, services=shop_obj.get_platforms(), platform=platform_obj, form=form)


@login_required
@only_clients
@render_to('Cabinet/goods.html')
def goods(r, shop_obj):
    category_id = r.GET.get('category', None)
    cat_obj = None
    datasources = shop_obj.datasources.all()

    ds_pk = r.GET.get('datasource_pk', None)
    if not ds_pk:
        if category_id:
            cat_obj = get_object_or_404(Category, pk=category_id)
            if cat_obj.datasource in datasources:
                goods_url = reverse('shop_goods', kwargs={'shop_id': shop_obj.pk})
                params = '?datasource_pk=%d&category=%d' % (
                    cat_obj.datasource.pk, cat_obj.pk)
                return redirect(goods_url + params)
        return redirect(
            reverse(
                'shop_goods', kwargs=dict(shop_id=shop_obj.pk)
            ) + '?datasource_pk=%d' % datasources[0].pk
        )

    current_ds = get_object_or_404(DataSource, pk=ds_pk)
    products = current_ds.products.all()

    if category_id is not None:
        cat_obj = get_object_or_404(Category, pk=category_id)
        products = products.filter(categories__in=cat_obj.get_nesting_id_list())

    products = products.prefetch_related('groups')

    per_page = 25
    p = Paginator(products, per_page)
    page_num = r.GET.get('page', 1)
    try:
        product_res = p.page(page_num)
    except EmptyPage:
        product_res = p.page(p.num_pages)

    res = dict(
        shop=shop_obj,
        groups=shop_obj.product_groups.all(),
        cat=cat_obj,
        goods=product_res,
        datasources=datasources,
        current_ds=current_ds,
        page_param_name=PAGE_PARAM_NAME,
        is_automatic_mode=current_ds.mode == DataSource.MODE_AUTO
    )
    if category_id is None:
        res['root_cats'] = current_ds.categories.extra(where=["parent_id is null"])#.filter(parent=None)
    return res


@login_required
@only_clients
@render_to('Cabinet/product_group_list.html')
def product_group_list(r, shop_obj):
    return dict(
        shop=shop_obj,
        groups=shop_obj.product_groups.all()
    )


@login_required
@only_clients
@render_to('Cabinet/product_group_edit_or_create.html')
def product_group_edit_or_create(r, shop_obj, group_pk=None):
    if group_pk:
        group_obj = get_object_or_404(shop_obj.product_groups.all(), pk=group_pk)
        form = ProductGroupForm(r.POST or None, instance=group_obj, shop=shop_obj)
    else:
        group_obj = None
        form = ProductGroupForm(r.POST or None, shop=shop_obj)
    if form.is_valid():
        new_group_obj = form.save(commit=False)
        new_group_obj.shop = shop_obj
        new_group_obj.save()
        products_id = form.cleaned_data['products_id']
        if products_id:
            new_group_obj.products = Product.objects.filter(id__in=products_id.split(','))
        else:
            new_group_obj.products = []
        if group_obj:
            messages.success(r, u'Группа "%s" изменена' % new_group_obj.name)
        else:
            messages.success(r, u'Группа "%s" успешно создана' % new_group_obj.name)
        return redirect(reverse(
            'product_group_list', kwargs={'shop_id': shop_obj.pk}))

    return dict(
        shop=shop_obj,
        form=form,
        group=group_obj
    )


@login_required
@only_clients
@render_to('Cabinet/goods_view.html')
def good_view(r, shop_obj, product_pk):
    product_obj = get_object_or_404(Product, pk=product_pk, datasource__in=shop_obj.datasources.all())
    current_ds = product_obj.datasource
    form = ProductManualDescForm(r.POST or None, instance=product_obj)
    use_default_desc = r.POST.get('clear_manual_desc', None)
    if use_default_desc:
        product_obj.manual_desc = None
        product_obj.save(update_fields=['manual_desc'])
        messages.info(
            r,
            u'Для товара "%s" используется описание из YML' % product_obj.name)
        return redirect(reverse(
            'good_view',
            kwargs={'shop_id': shop_obj.pk, 'product_pk': product_obj.pk}))
    else:
        if form.is_valid():
            product_obj = form.save(commit=False)
            product_obj.save(update_fields=['manual_desc'])
            messages.info(
                r,
                u'Описание для товара "%s" изменено' % product_obj.name)
            return redirect(reverse(
                'good_view',
                kwargs={'shop_id': shop_obj.pk, 'product_pk': product_obj.pk}))

    return dict(
        shop=shop_obj,
        product=product_obj,
        form=form,
        is_automatic_mode=current_ds.mode == DataSource.MODE_AUTO)


@login_required
@only_clients
@render_to('Cabinet/goods_edit.html')
def goods_edit_or_create(r, shop_obj, productid=None):
    ds_pk = r.GET.get('datasource_pk', None)
    if productid:
        product_obj = get_object_or_404(Product, pk=productid)
        current_ds = product_obj.datasource
        if current_ds.mode == DataSource.MODE_AUTO:
            messages.error(r, u'Редактирование товаров доступно только '
                              u'источникам, которые находятся в ручном режиме')
            return redirect(reverse(
                'good_view', kwargs=dict(shop_id=shop_obj.pk, product_pk=product_obj.pk)))
            # return redirect(reverse(
            #     'shop_goods', kwargs=dict(shop_id=shop_obj.pk)))
        form = ShopProductForm(
            r.POST or None, instance=product_obj, datasource=current_ds)
    else:
        product_obj = None
        current_ds = get_object_or_404(DataSource, pk=ds_pk)
        form = ShopProductForm(r.POST or None, datasource=current_ds)

    if form.is_valid():
        images = r.POST.getlist('images', [])
        product_obj = form.save(commit=False)
        images = filter(bool, images)
        del_prefix = 'del-img-'
        for k, v in r.POST.items():
            if k.startswith(del_prefix):
                img_path = k[len(del_prefix):]
                while img_path in images:
                    k_index = images.index(img_path)
                    if img_path.startswith(settings.MEDIA_URL):
                        img_path = img_path[len(settings.MEDIA_URL):]
                        img_filename = os.path.join(settings.MEDIA_ROOT, img_path)
                        try:
                            os.remove(img_filename)
                        except:
                            l.exception('Can not remove image for product. '
                                        'Image path "%s"' % img_filename)
                    del images[k_index]
        product_obj.images = Product.IMAGES_SEPARATOR.join(images)
        product_obj.shop = shop_obj
        product_obj.model_oid = product_obj.oid
        product_obj.datasource = current_ds
        product_obj.save()

        offers_oid = r.POST.getlist('offer_oid', [])
        offers_size = r.POST.getlist('offer_size', [])
        offers = []
        for oid, size in zip(offers_oid, offers_size):
            if oid == '':
                oid = product_obj.oid
            if size == '':
                continue
            offer = Offer(product=product_obj, oid=oid, price=product_obj.price)
            offer.set_size(size)
            offers.append(offer)
        Offer.objects.bulk_create(offers)

        offers_del = r.POST.getlist('remove_offer', [])
        if offers_del:
            product_obj.offers.filter(pk__in=offers_del).delete()

        images_for_download = r.FILES.getlist('images_download', [])
        shop_dir = shop_obj.get_img_dir()
        for image in images_for_download:
            uniq_name = str(hashlib.md5(image.name.encode('utf-8') + str(uuid.uuid4())).hexdigest())
            fname = os.path.join(
                shop_dir, uniq_name + os.path.splitext(image.name)[-1])
            filename = os.path.join(settings.MEDIA_ROOT, fname)
            url = os.path.join(settings.MEDIA_URL, fname)
            images.append(url)
            with open(filename, 'wb') as f:
                f.write(image.read())
        product_obj.images = Product.IMAGES_SEPARATOR.join(images)
        product_obj.save()
        #max_products = current_ds.shops.all().order_by('-tariff__max_products')[0].tariff.max_products
        #if max_products:
        #    ds_list = shop_obj.datasources.all()
        #    if Product.objects.filter(datasource__in=ds_list, status=True).exclude(pk=product_obj.pk).count() > max_products:
        #        product_obj.status = False
        #        product_obj.save()

        # Работа с категориями товара
        p_cats_objs = ProductCategories.objects.filter(product=product_obj)
        old_path_list = []
        for p_cat_obj in p_cats_objs:
            p_cat_obj.category.upd_product_cnt()
            old_path_list.append(p_cat_obj.category.get_category_path())
        p_cats_objs.delete()
        ProductCategories.objects.bulk_create([
            ProductCategories(product=product_obj, category=category)
            for category
            in form.cleaned_data['categories']
        ])
        with transaction.commit_on_success():
            for cat in product_obj.categories.all():
                new_path = cat.get_category_path()
                if new_path:
                    new_path[0].upd_product_cnt()
                else:
                    cat.upd_product_cnt()
            for old_path in old_path_list:
                if not old_path:
                    continue
                old_path[0].upd_product_cnt()

        #чистим кеш категорий для приложения
        shop_obj.clear_category_cache()
        return redirect(reverse(
            'shop_good_edit',
            kwargs=dict(shop_id=shop_obj.pk, productid=product_obj.pk)))
    return dict(form=form, shop=shop_obj, product=product_obj)


@login_required
@only_clients
@render_to('Cabinet/news.html')
def news(request, shop_obj):
    news_objects = News(shop=shop_obj, cdate=datetime.now())

    if request.method == 'POST': # If the form has been submitted...
        form = ShopNewsForm(request.POST,instance=news_objects) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            form.save()
            # news.save()
            return HttpResponseRedirect(reverse('shop_news', kwargs=dict(shop_id=shop_obj.pk)))

    else:
        form = ShopNewsForm(instance=news_objects) # An unbound form


    newslist = News.objects.filter(shop=shop_obj)
    ret = {
        'form': form,
        'shop': shop_obj,
        'newslist': newslist,
    }

    return ret


@login_required
@only_clients
@render_to('Cabinet/news_edit.html')
def news_edit(request, shop_obj, newsid=0):
    news_objects = News.objects.get(pk=newsid, shop=shop_obj)

    delete = int(request.GET.get('delete', 0))

    if delete == 1:
        news_objects.delete()
        return HttpResponseRedirect(reverse('shop_news', kwargs=dict(shop_id=shop_obj.pk)))

    if request.method == 'POST': # If the form has been submitted...
        form = ShopNewsForm(request.POST,instance=news_objects) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            form.save()
            return HttpResponseRedirect(reverse('shop_news', kwargs=dict(shop_id=shop_obj.pk)))

    else:
        form = ShopNewsForm(instance=news_objects) # An unbound form

    ret = {
        'form': form,
        'shop': shop_obj,
    }

    return ret


@login_required
@only_clients
@render_to('Cabinet/pages.html')
def pages(request, shop_obj):
    pages_objects = Page(shop=shop_obj)

    if request.method == 'POST': # If the form has been submitted...
        form = ShopPageForm(request.POST,instance=pages_objects) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            form.save()
            # pages.save()
            return HttpResponseRedirect(shop_obj.get_cabinet_pages_url())

    else:
        form = ShopPageForm(instance=pages_objects) # An unbound form


    pageslist = Page.objects.filter(shop=shop_obj).order_by('order')
    ret = {
        'form': form,
        'shop': shop_obj,
        'pageslist': pageslist,
    }

    return ret


@login_required
@only_clients
@render_to('Cabinet/pages_edit.html')
def pages_edit(request, shop_obj, pageid=0):
    page = Page.objects.get(pk=pageid, shop=shop_obj)

    delete = int(request.GET.get('delete', 0))

    if delete == 1:
        page.delete()
        return HttpResponseRedirect(shop_obj.get_cabinet_pages_url())

    if request.method == 'POST': # If the form has been submitted...
        form = ShopPageForm(request.POST,instance=page) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            form.save()
            # renumber new list
            pages = shop_obj.pages.exclude(pk=page.id).order_by('order')
            i = 0
            for p in pages:
                if i == page.order:
                    i += 1
                p.order = i
                p.save()
                i += 1
            return HttpResponseRedirect(shop_obj.get_cabinet_pages_url())

    else:
        form = ShopPageForm(instance=page) # An unbound form

    ret = {
        'form': form,
        'shop': shop_obj,
    }

    return ret


@login_required
@only_clients
@to_json
def category_move(request, shop_obj):
    cid = int(request.GET.get('cid', 0))
    pcid = int(request.GET.get('pcid', 0))
    index = int(request.GET.get('index', 0))
    dsid = int(request.GET.get('dsid', 0))
    ret = {'ok': 1}

    datasource = DataSource.objects.get(pk=dsid)
    cat = datasource.categories.get(pk=cid)
    old_parent = cat.parent
    old_path = cat.get_category_path()

    #for c in datasource.categories.filter(parent=None):
    #    Category.objects.partial_rebuild(c.tree_id)
    #
    try:
        new_parent = datasource.categories.get(pk=pcid)
    except:
        new_parent = None

    # move category
    cat.parent = new_parent
    cat.order = index
    cat.save()
    cat.upd_lvl()

    new_path = cat.get_category_path()

    if new_parent != old_parent:
        if new_path:
            new_path[0].upd_product_cnt()
        if old_path:
            old_path[0].upd_product_cnt()
        if not (old_path or new_path):
            cat.upd_product_cnt()

        # renumber old list
        subcats = datasource.categories.filter(parent=old_parent).order_by('order')
        i = 0
        for c in subcats:
            c.order = i
            c.save()
            i += 1
        #print "---"

    # renumber new list
    subcats = datasource.categories.filter(parent=new_parent).exclude(pk=cat.id).order_by('order')
    i = 0
    for c in subcats:
        if i == index:
            i += 1
        c.order = i
        c.save()
        i += 1

    for c in datasource.categories.filter(parent=None):
        Category.objects.partial_rebuild(c.tree_id)

    #чистим кеш категорий для приложения
    shop_obj.clear_category_cache()

    return ret


@login_required
@only_clients
@render_to('Cabinet/index.html')
def look(request, shop_obj):
    return {}


def get_catlist(categories_list, prefix=None, exclude_pk=None):
    if prefix is None:
        prefix = '</ul></li>'
    if exclude_pk is not None:
        categories_list = categories_list.exclude(pk=exclude_pk)
    catlist = list(categories_list.order_by('order').values())
    catlist.sort(cmp=lambda a, b: cmp(a['parent_id'], b['parent_id']))
    pow_step = 0
    if len(catlist) > 0:
        pow_step = round(log10(len(catlist)) + 0.5)

    # prepare order
    for i in range(0, len(catlist)):
        catlist[i]['order'] += 1

    #fill dict
    catdict = dict(imap(lambda c: (c['id'], c), catlist))

    loops = 0
    min_level = 0
    max_level = 0
    max_products = 0

    #calculation keys
    while True:
        for i in range(0, len(catlist)):
            cat = catlist.pop(0)
            c = catdict[cat['id']]

            if c['parent_id'] is not None and c['parent_id'] in catdict:
                cp = catdict[c['parent_id']]
                catdict[c['parent_id']]['foldable'] = True
                if 'level' in cp:
                    c['level'] = cp['level'] + 1
                    max_level = max(max_level, c['level'])
                    max_products = max(max_products, c['active_products_count'])
                    c['key'] = cp['key']*pow(10, pow_step) + c['order']
                    c['key_str'] = u"%s-%s" % (cp['key_str'], c['order'])
                    #print u"l:%02d o:%02d [%d] -> %d" % (c['level'],c['order'],pow_step,c['key'])
                else:
                    catlist.append(cat)
            else:
                c['level'] = min_level
                c['key'] = c['order']
                c['key_str'] = u"%s" % (c['order'])
                max_products = max(max_products, c['active_products_count'])
                #print u"l:%02d o:%02d [%d] -> %d" % (c['level'],c['order'],pow_step,c['key'])

        loops += 1
        if loops > 10 or len(catlist) == 0:
            break

    #print "~~~~~"
    #normalize int keys
    for i in sorted(catdict.keys(), key=lambda x: catdict[x]['key']):
        c = catdict[i]
        c['key'] *= pow(10, pow_step * (max_level - c['level'] + 1))
        #print u"l:%02d o:%02d [%d] -> %s %d (%d)" % (c['level'], c['order'], pow_step, c['key_str'].ljust(10), c['key'], oldkey)

    catlist = catdict.values()
    catlist.sort(key=lambda x: x['key'])

    # postfix for last category closing ul
    level = min_level-1
    if max_products == 0:
        max_products = 1
    for c in catlist:
        c['color'] = int(255*((float(c['active_products_count'])/max_products)**0.3))
        c['ccolor'] = 255 - c['color']
        c['prefix'] = prefix
        if c['level'] > level:
            c['prefix'] = ""
        if c['level'] < level:
            c['prefix'] = prefix * (level-c['level']+1)
        level = c['level']
    return level, catlist


@login_required
@only_clients
def category_delete(r, shop_obj, category_pk):
    cats = Category.objects.filter(datasource__in=shop_obj.datasources.all())
    category_obj = get_object_or_404(cats, pk=category_pk)
    redirect_url = reverse('shop_categories', kwargs={'shop_id': shop_obj.pk})
    category_name = category_obj.name
    if category_obj.datasource.mode != DataSource.MODE_MANUAL:
        messages.error(r, u'Невозможно удалить категорию "%s", '
                          u'так как источник, в котором находится категория '
                          u'не в ручном режиме' % category_name)
        return redirect(redirect_url)
    subcats = category_obj.get_all_subcats()
    products = (
        category_obj.products.all() |
        Product.objects.filter(categories__in=subcats))
    for product in products.annotate(cats_cnt=Count('categories')):
        if product.cats_cnt == 1 and product.categories.filter(pk=category_pk):
            product.delete()
    subcats.delete()
    category_obj.delete()
    messages.success(r, u'Категория "%s" успешно удалена' % category_name)
    return redirect(redirect_url)


@login_required
@only_clients
@render_to('Cabinet/categories.html')
def categories(r, shop_obj):
    datasources = shop_obj.datasources.all()
    ds_pk = r.GET.get('datasource_pk', None)
    if not ds_pk:
        return redirect(
            reverse(
                'shop_categories', kwargs=dict(shop_id=shop_obj.pk)
            ) + '?datasource_pk=%d' % datasources[0].pk
        )
    current_ds = get_object_or_404(DataSource, pk=ds_pk)

    return dict(
        datasources=datasources,
        current_ds=current_ds,
        shop=shop_obj,
        nodes=current_ds.categories.all().order_by('order'),
        is_automatic_mode=current_ds.mode == DataSource.MODE_AUTO
    )

@login_required
@only_clients
@render_to('Cabinet/shop_social.html')
def shop_social(r, shop_obj):
    user = r.user
    form = ShopProfileForm(
        r.POST or None, instance=shop_obj.shopprofile, user=user)
    if form.is_valid():
        form.save()
        return redirect(reverse('shop_profile', kwargs={'shop_id': shop_obj.pk}))

    user_has_vk_token = False
    if user.shoppyuser.vk_token_valid():
        user_has_vk_token = True

    return dict(
        form=form,
        shop=shop_obj,
        client=user.clients.all()[0],
        user=user,
        user_has_vk_token=user_has_vk_token
    )


@login_required
@only_clients
@render_to('Cabinet/category_create.html')
def shop_category_create(r, shop_obj):
    parent_id = r.GET.get('parent',r.POST.get('parent', None))
    if parent_id == 'None':
        parent_id = None
    if parent_id is not None:
        parent = Category.objects.get(pk=parent_id)
        current_ds = parent.datasource
    else:
        parent = None
        current_ds = get_object_or_404(DataSource, pk=r.GET.get('datasource_pk'))

    if current_ds.mode == DataSource.MODE_AUTO:
        messages.add_message(
            r,
            messages.ERROR,
            u'Создавать категории можно только в ручном режиме'
        )
        return HttpResponseRedirect(
            reverse('shop_categories', kwargs=dict(shop_id=shop_obj.pk))
        )
    form = ShopCategoryForm(
        r.POST or None, datasource=current_ds
    )
    if form.is_valid():
        cat_obj = form.save(commit=False)
        cat_obj.parent = parent
        cat_obj.datasource = current_ds
        cat_obj.save()
        if parent is not None:
            parent.renumber_subcats()
        return HttpResponseRedirect(
            reverse(
                'shop_categories',
                kwargs=dict(shop_id=shop_obj.pk)
            ) + '?datasource_pk=%d' % current_ds.pk
        )
    categories_list = Category.objects.filter(datasource=current_ds)
    level, catlist = get_catlist(categories_list)

    #чистим кеш категорий для приложения
    shop_obj.clear_category_cache()

    return dict(
        parent=int(r.GET.get('parent', 0)),
        catlist=catlist,
        shop=shop_obj,
        form=form
    )


@login_required
@only_clients
@render_to('Cabinet/category_edit.html')
def shop_category_edit(r, shop_obj, category_id):
    cat_obj = get_object_or_404(Category, pk=category_id)
    cat_name = cat_obj.name
    current_ds = cat_obj.datasource
    form = ShopCategoryForm(r.POST or None, instance=cat_obj, datasource=current_ds)
    if form.is_valid():
        cat_obj = form.save(commit=False)
        if current_ds.mode == DataSource.MODE_AUTO:
            messages.add_message(
                r,
                messages.ERROR,
                u'Переименовывать категории можно только в ручном режиме'
            )
            cat_obj.name = cat_name
        parent_id = r.POST.get('parent', None)
        old_path = cat_obj.get_category_path()
        if parent_id is not None and parent_id != 'None':
            cat_obj.parent = Category.objects.get(pk=parent_id)
        else:
            cat_obj.parent = None
        cat_obj.save()
        new_path = cat_obj.get_category_path()
        if new_path:
            new_path[0].upd_product_cnt()
        if old_path:
            old_path[0].upd_product_cnt()
        if not (old_path or new_path):
            cat_obj.upd_product_cnt()
        return HttpResponseRedirect(
            reverse('shop_categories', kwargs=dict(shop_id=shop_obj.pk))
        )

    cat_exclude = cat_obj.get_descendants(
        include_self=False)
    cat_exclude_pk = [int(cat.pk) for cat in cat_exclude]
    categories_list = Category.objects.filter(
        datasource=current_ds).exclude(pk__in=cat_exclude_pk)
    level, catlist = get_catlist(categories_list, exclude_pk=cat_obj.pk)

    #чистим кеш категорий для приложения
    shop_obj.clear_category_cache()

    return dict(
        parent=cat_obj.parent_id,
        catlist=catlist,
        shop=shop_obj,
        form=form
    )


@login_required
@only_clients
@render_to('Cabinet/shop_discount_list.html')
def shop_discount_list(r, shop_obj):
    discount_list = Discount.objects.filter(shop=shop_obj)
    return dict(
        shop=shop_obj,
        discounts=discount_list
    )


@login_required
@only_clients
@render_to('Cabinet/shop_discount_edit_or_create.html')
def shop_discount_edit_or_create(r, shop_obj, discount_id=None):
    if discount_id is not None:
        discount = get_object_or_404(Discount, id=discount_id)
        form = DiscountForm(r.POST or None, instance=discount, shop=shop_obj)
    else:
        discount = None
        form = DiscountForm(r.POST or None, shop=shop_obj)

    if form.is_valid():
        d_obj = form.save(commit=False)

        is_coupon_discount = form.cleaned_data['coupon_discount']
        if not is_coupon_discount:
            d_obj.coupon_key = None
            d_obj.max_activation_count = 0

        d_obj.shop = shop_obj
        d_obj.save()

        products_str = form.cleaned_data['products_id']
        categories_str = form.cleaned_data['categories_id']

        if products_str:
            p = products_str.split(',')
            pp = Product.objects.filter(
                    datasource__in=shop_obj.datasources.all(),
                    pk__in=p
            )
            d_obj.products = pp
        else:
            d_obj.products = []

        if categories_str:
            cat_ids = categories_str.split(',')
            categories = Category.objects.filter(
                    datasource__in=shop_obj.datasources.all(),
                    pk__in=cat_ids
            )
            d_obj.categories = categories
        else:
            d_obj.categories = []



        track_action = "Created a discount"
        if discount:
            track_action = "Updated a discount"

        analytics.track(r.user.id, track_action, {
            "discount_id": d_obj.id,
            "shop_id": d_obj.shop.id,
            "client_id": d_obj.shop.client.id,
            "name": d_obj.name,
            "status": d_obj.status,
            "application": d_obj.application,
            "min_cart_price": d_obj.min_cart_price,
            "coupon_key": d_obj.coupon_key,
            "max_activation_count": d_obj.max_activation_count
        })

        return HttpResponseRedirect(
            reverse('shop_discount_list', kwargs=dict(shop_id=shop_obj.id))
        )

    return dict(
        shop=shop_obj,
        form=form,
        discount=discount
    )


@login_required
@only_clients
@render_to('Cabinet/shop_banner_list.html')
def shop_banner_list(r, shop_obj):
    banners = Banner.objects.filter(
        shop=shop_obj).select_related('slot').order_by('-status')
    return dict(
        shop=shop_obj,
        banners=banners
    )


@login_required
@only_clients
def shop_banner_delete(r, shop_obj, banner_pk):
    banner_obj = get_object_or_404(shop_obj.banners.all(), pk=banner_pk)
    banner_obj.delete()
    messages.success(r, u'Баннер "%s" успешно удален' % banner_obj.name)
    return redirect(reverse('shop_banner_list', kwargs={'shop_id': shop_obj.pk}))


@login_required
@only_clients
@render_to('Cabinet/shop_banner_edit_or_create.html')
def shop_banner_edit_or_create(r, shop_obj, banner_id=None):
    def _init_for_type(banner_type):
        name = dict(Banner.BANNER_TYPE_CHOICES)[banner_type]
        if banner_type == Banner.BANNER_TYPE_TEXT:
            return name, BannerTextForm
        elif banner_type == Banner.BANNER_TYPE_GRAPHIC:
            return name, BannerGraphicForm
        elif banner_type == Banner.BANNER_TYPE_CAROUSEL:
            return name, BannerCarouselForm
        else:
            return HttpResponseServerError

    if banner_id is not None:
        banner_obj = get_object_or_404(Banner, id=banner_id)
        b_type = banner_obj.banner_type
        if b_type == Banner.BANNER_TYPE_TEXT:
            banner_obj = banner_obj.bannertext
        elif b_type == Banner.BANNER_TYPE_GRAPHIC:
            banner_obj = banner_obj.bannergraphic
        elif b_type == Banner.BANNER_TYPE_CAROUSEL:
            banner_obj = banner_obj.bannercarousel
        else:
            raise

        banner_obj.get_banner_type_display()
        banner_type_name, form_cls = _init_for_type(banner_obj.banner_type)
        form = form_cls(r.POST or None, r.FILES or None, shop=shop_obj, instance=banner_obj)
    else:
        b_type = int(r.GET['type'])
        banner_type_name, form_cls = _init_for_type(b_type)
        banner_obj = None
        form = form_cls(r.POST or None, r.FILES or None, shop=shop_obj)

    if b_type == Banner.BANNER_TYPE_GRAPHIC:
        ImgFormSet = inlineformset_factory(BannerGraphic, BannerGraphicImage, form=BannerGraphicImageForm, extra=1)
        if banner_obj:
            images_formset = ImgFormSet(r.POST or None, r.FILES or None, instance=banner_obj.bannergraphic, initial=[{'shop': shop_obj}])
        else:
            images_formset = ImgFormSet(r.POST or None, r.FILES or None, initial=[{'shop': shop_obj}])
    else:
        images_formset = None
    if (images_formset and images_formset.is_valid() and form.is_valid()) or not images_formset and form.is_valid():
        obj = form.save(commit=False)
        obj.shop = shop_obj
        obj.banner_type = b_type
        obj_pk = obj.pk
        obj.save()
        if not obj_pk and images_formset:
            images_formset = ImgFormSet(r.POST or None, r.FILES or None, instance=obj.bannergraphic, initial=[{'shop': shop_obj}])
        if images_formset and images_formset.is_valid():
            images_formset.save()
        return redirect(reverse(
            'shop_banner_list', kwargs={'shop_id': shop_obj.pk}))
    return dict(
        shop=shop_obj,
        form=form,
        banner_type_name=banner_type_name,
        banner=banner_obj,
        images_formset=images_formset
    )


@login_required
@only_clients
@render_to('Cabinet/order_view.html')
def order_view(r, shop_obj, order_pk):
    order_obj = get_object_or_404(shop_obj.orders.all(), pk=order_pk)
    history = order_obj.history.all().order_by('timestamp')
    return dict(shop=shop_obj, order=order_obj, history=history)


@login_required
@only_clients
@render_to('Cabinet/orders.html')
def orders(request, shop_obj):
    # import here because:
    #   OrderFilter attributes are ORM-dependant
    #   It crushes first run of South migrations
    from Cabinet.filters import OrderFilter

    order_objs = Order.objects.filter(shop=shop_obj).exclude(cart=None).order_by('-cdate')
    order_filter = OrderFilter(request.GET, queryset=order_objs)

    per_page = 10
    p = Paginator(order_filter, per_page)
    page_num = request.GET.get('page', 1)
    try:
        orders_res = p.page(page_num)
    except EmptyPage:
        orders_res = p.page(p.num_pages)

    for o in orders_res:
        if o.cart.discount:
            discount = {
                'name': o.cart.discount.name,
                'value': o.cart.discount.get_value(),
                'price': o.cart.discount.apply(o.cart.total())
            }
            setattr(o, 'discount', discount)

    return dict(
        shop=shop_obj,
        orders=orders_res,
        filter=order_filter,
        page_param_name=PAGE_PARAM_NAME
    )


@login_required
@only_clients
@render_to('Cabinet/callback_orders.html')
def callback_orders(r, shop_obj):
    # import here because:
    #   CallbackOrderFilter attributes are ORM-dependant
    #   It crushes first run of South migrations
    from Cabinet.filters import CallbackOrderFilter

    orders_list = shop_obj.callback_orders.all()
    order_filter = CallbackOrderFilter(r.GET, queryset=orders_list)

    per_page = 10
    p = Paginator(order_filter, per_page)
    page_num = r.GET.get('page', 1)
    try:
        orders_res = p.page(page_num)
    except EmptyPage:
        orders_res = p.page(p.num_pages)
    return dict(
        orders=orders_res,
        filter=order_filter,
        shop=shop_obj,
        page_param_name=PAGE_PARAM_NAME
    )


@login_required
@only_clients
@render_to('email/user_notice.html')
def order_notice_preview(request, shop_obj, orderid):
    order = Order.objects.get(shop=shop_obj,pk=orderid)

    return order.prepare_usernotice_data()


@login_required
@render_to('Cabinet/client_profile_list.html')
def client_profile_list(r):
    if r.user.is_superuser:
        clients = Client.objects.all()
    else:
        client = r.user.clients.all()[0]
        return redirect(reverse('client_profile', kwargs={'client_pk': client.pk}))

    return dict(clients=clients)


@login_required
@render_to('Cabinet/client_profile.html')
def client_profile(r, client_pk):
    if r.user.is_superuser:
        clients = Client.objects.all()
    else:
        clients = r.user.clients.all()
    client = get_object_or_404(clients, pk=client_pk)
    if r.user.is_superuser:
        if 'is_validated' in r.POST:
            client.is_validated = True
            client.save()
            messages.success(
                r, u'Клиент "%s" помечен как "Проверенный"' % client.name)
            return redirect(
                reverse('client_profile', kwargs={'client_pk': client.pk}))
    form = ClientForm(r.POST or None, instance=client)
    if form.is_valid():
        form.save()
        return redirect(
            reverse('client_profile', kwargs=dict(client_pk=client_pk)))
    return dict(client=client, form=form)


@login_required
@render_to('Cabinet/client_shop_list.html')
def client_shop_list(r, client_pk):
    client_obj = get_object_or_404(Client, pk=client_pk)
    shops = Shop.objects.filter(client=client_obj)
    return dict(
        client=client_obj,
        shops=shops
    )


@login_required
@render_to('Cabinet/client_user_edit_or_create.html')
def client_user_edit_or_create(r, client_pk, user_pk=None):
    client_obj = get_object_or_404(Client, pk=client_pk)
    user_obj = None
    if user_pk is not None:
        user_obj = get_object_or_404(client_obj.users.all(), pk=user_pk)
        form = UserForm(r.POST or None, instance=user_obj)
    else:
        form = UserForm(r.POST or None)

    if form.is_valid():
        email = form.cleaned_data['email']
        users = User.objects.filter(username=email)
        if users:
            user = users[0]
            user_obj = user
            form = UserForm(r.POST or None, instance=user_obj)
        user_obj = form.save(commit=False)

        user_obj.username = user_obj.email
        user_obj.is_active = True
        user_obj.save()
        user_obj.shoppyuser.tel = form.cleaned_data['tel']
        user_obj.shoppyuser.save()
        user_obj.shoppyuser.roles = form.cleaned_data['roles']
        client_obj.users.add(user_obj)
        if user_pk:
            messages.success(r, u'Пользователь "%s" успешно изменен' % user_obj.email)
        else:
            password = random_password()
            user_obj.set_password(password)
            user_obj.save()
            try:
                message_html = render_to_string(
                    'email/client_user_add.html',
                    dict(username=user_obj.username, password=password, client=client_obj.name)
                ).replace("\n", '')
                EmailQueue.objects.create(
                    email_from=u'Служба заботы о пользователях ShoppyBoom.Ru <signup@shoppyboom.ru>',
                    email_to=user_obj.email,
                    bcc='signup@shoppyboom.ru',
                    reply_to=u'Команда ShoppyBoom.Ru <team@shoppyboom.ru>',
                    subject=u'Shoppyboom - Успешная регистрация',
                    message_type=EmailQueue.MTYPE_HTML,
                    body=message_html
                )
            except Exception as e:
                logger.error('Can not send email after user creation. Exception: %s' % str(e))

            messages.success(
                r,
                u'Пользователь "%s" успешно создан. Данные для входа высланы '
                u'на указанный электронный адрес' % user_obj.username
            )
        return redirect(reverse('client_user_list', kwargs={'client_pk': client_pk}))

    # client_obj.shops.all()[0].shopprofile.vk_app_id
    # client_obj.shops.all()[0].shopprofile.vk_app_secret
    url = 'https://oauth.vk.com/authorize?'
    params = dict(
        client_id=settings.VK_APP_ID,
        scope='photos',
        redirect_uri='http://%s/cabinet/client/user/vk-auth/' % settings.BASE_HOST_NAME,
        display='page',
        v='5.24',
        response_type='code'
    )
    vk_auth_url = url + urllib.urlencode(params)
    return dict(
        form=form, user=user_obj, client=client_obj, vk_auth_url=vk_auth_url)


@login_required
def client_user_vk_auth(r):
    user = r.user
    s_user = user.shoppyuser
    code = r.GET['code']
    url = 'https://oauth.vk.com/access_token?'
    params = dict(
        client_id=settings.VK_APP_ID,
        client_secret=settings.VK_APP_SECRET,
        code=code,
        redirect_uri='http://%s/cabinet/client/user/vk-auth/' % settings.BASE_HOST_NAME
    )
    url = url + urllib.urlencode(params)
    r = requests.get(url)
    data = r.json()
    s_user.vk_token = data['access_token']
    s_user.vk_token_expired = timezone.now() + timezone.timedelta(seconds=data['expires_in'])
    s_user.save()
    return redirect(reverse('client_user_edit',
        kwargs={'client_pk': user.clients.all()[0].pk, 'user_pk': user.pk}))


@login_required
def client_user_delete(r, client_pk, user_pk):
    client_obj = get_object_or_404(Client, pk=client_pk)
    user_obj = get_object_or_404(User, pk=user_pk)
    if user_obj == r.user:
        messages.success(r, u'Вы не можете удалить сами себя')
        return redirect(reverse('client_profile', kwargs={'client_pk': client_obj.pk}))
    if user_obj in client_obj.users.all():
        user_name = user_obj.username
        user_obj.delete()
        messages.success(r, u'Пользователь "%s" успешно удален' % user_name)
        return redirect(reverse('client_user_list', kwargs={'client_pk': client_obj.pk}))
    return HttpResponseServerError()


@login_required
@only_clients
@render_to('Cabinet/shop_comment_list.html')
def shop_comment_list(r, shop_obj):
    comments = Comment.objects.filter(shop=shop_obj, reply_to=None).select_related('reply_to', 'customer', 'product__shop__shopprofile').prefetch_related('product__categories').order_by('-created')
    per_page = 10
    p = Paginator(comments, per_page)
    page_num = r.GET.get(PAGE_PARAM_NAME, 1)
    try:
        comments_res = p.page(page_num)
    except EmptyPage:
        comments_res = p.page(p.num_pages)

    for c in comments_res:
        # FIXME: Это бред
        setattr(c, 'fb_url', c.product.get_fb_url(shop=shop_obj))
        setattr(c, 'vk_url', c.product.get_vk_url(shop=shop_obj))

    return dict(
        comments=comments_res,
        shop=shop_obj,
        page_param_name=PAGE_PARAM_NAME
    )


@login_required
@only_clients
@render_to('Cabinet/shop_extra.html')
def shop_advanced(r, shop_obj):
    ExtraFormset = modelformset_factory(ShopExtra, extra=1, formset=ShopExtraFormSet, form=ShopExtraForm)
    formset = ExtraFormset(r.POST or None, queryset=ShopExtra.objects.filter(shop=shop_obj))
    if formset.is_valid():
        for obj in formset.save(commit=False):
            obj.shop = shop_obj
            obj.save()
        return redirect(reverse('shop_extra', kwargs=dict(shop_id=shop_obj.pk)))
    return dict(shop=shop_obj, formset=formset)



@login_required
@only_clients
def shop_datasource_truncate_data(r, shop_obj, datasource_pk):
    datasource_obj = get_object_or_404(shop_obj.datasources.all(), pk=datasource_pk)
    datasource_obj.categories.all().delete()
    datasource_obj.products.all().delete()
    # datasource_obj.orders.all().delete()
    # datasource_obj.stocks.all().delete()
    return HttpResponseRedirect(
        reverse(
            'shop_datasource_edit',
            kwargs=dict(shop_id=shop_obj.pk, datasource_pk=datasource_pk)
        )
    )


@login_required
@only_clients
@render_to('Cabinet/subscription_list.html')
def subscription_list(r, shop_obj):
    customers = Customer.objects.all()\
        .exclude(email='')\
        .exclude(email=None)\
        .filter(carts__in=shop_obj.cart_set.all())\
        .distinct()
    form = SelectBannerFrequencyForm(r.POST or None, instance=shop_obj)
    if form.is_valid():
        form.save()
        messages.success(
            r, u'Частота показа баннера с просьбой ввести email изменена')
        return redirect(
            reverse('subscription_list', kwargs={'shop_id': shop_obj.pk}))
    return dict(
        shop=shop_obj,
        form=form,
        customers=customers
    )


@login_required
@only_clients
def shop_calculate_cat_product_count(r, shop_obj):
    for d in shop_obj.datasources.all():
        for cat in d.categories.filter(parent=None):
            cat.upd_product_cnt()
    messages.success(r, u'Счетчики товаров для всех категорий обновлены')

    #чистим кеш категорий для приложения
    shop_obj.clear_category_cache()

    return HttpResponseRedirect(reverse('shop', kwargs=dict(shop_id=shop_obj.pk)))


@login_required
@only_clients
@to_json
def catetgory_status_toggle(r, shop_obj, category_id):
    ret = {
        'status': None,
        'ids': []
    }
    try:
        cat_obj = Category.objects.get(pk=category_id)
        cat_obj.status = not cat_obj.status
        cat_obj.save()
        ret['status'] = cat_obj.status
        #ret['ids'] = [cat_obj.id]

        n_ids = cat_obj.get_nesting_id_list()
        ret['ids'] += n_ids
        nesting = Category.objects.filter(id__in=n_ids)
        for n in nesting:
            n.status = cat_obj.status
            n.save()

        if cat_obj.status:
            p_ids = cat_obj.get_parent_id_list()
            ret['ids'] += p_ids
            parents = Category.objects.filter(id__in=p_ids)
            for p in parents:
                p.status = cat_obj.status
                p.save()

        #чистим кеш категорий для приложения
        shop_obj.clear_category_cache()
        return ret
    except:
        return dict(status=False)


@login_required
@only_clients
@render_to('Cabinet/shop_notification_list.html')
def shop_notification_list(r, shop_obj):
    notifications = shop_obj.notifications.all()
    return dict(
        shop=shop_obj,
        notifications=notifications
    )


@login_required
@only_clients
@render_to('Cabinet/shop_notification_add.html')
def shop_notification_add(r, shop_obj):
    form = NotificationForm(r.POST or None)
    if form.is_valid():
        notification = form.save(commit=False)
        notification.shop = shop_obj
        notification.save()
        customers_vk_ids = Customer.objects.exclude(vk_id=None).exclude(vk_id='').distinct().values_list('vk_id', flat=True)
        shop_profile = shop_obj.shopprofile
        texts = MessageText.objects.filter(text=notification.text)
        if texts:
            text_obj = texts[0]
        else:
            text_obj = MessageText(text=notification.text)
            text_obj.save()
        messages = []
        m_type = notification.type
        for c_id in customers_vk_ids:
            messages.append(Message(
                customer=c_id,
                date=notification.date,
                text=text_obj,
                shop_profile=shop_profile,
                platform='vk',
                type=m_type
            ))
        Message.objects.bulk_create(messages)

        return redirect(reverse('shop_notification_list', kwargs=dict(shop_id=shop_obj.pk)))
    return dict(
        shop=shop_obj,
        form=form
    )


@login_required
@render_to('Cabinet/change_password.html')
def change_password(r):
    form = ChangePasswordForm(r.POST or None, user=r.user)
    if form.is_valid():
        password = form.cleaned_data['password1']
        r.user.set_password(password)
        r.user.save()
        messages.success(r, u'Пароль успешно изменен')
        return redirect(reverse('main'))
    return dict(
        form=form
    )


@login_required
@only_clients
@render_to('Cabinet/payment_list.html')
def shop_payment_list(r, shop_obj):
    payments = shop_obj.payments.all()
    return dict(
        shop=shop_obj,
        payments=payments
    )


@login_required
@only_clients
@render_to('Cabinet/offer_list.html')
def shop_offer_list(r, shop_obj, product_pk):
    product_obj = get_object_or_404(Product, pk=product_pk)
    offers = product_obj.offers.all()

    offerFormset = modelformset_factory(Offer, extra=1, form=OfferFakeForm)  #  formset=ShopExtraFormSet,
    formset = offerFormset(r.POST or None, queryset=offers)

    if formset.is_valid():
        for form in formset.forms:
            if not form.cleaned_data:
                continue
            obj = form.save(commit=False)
            obj.product = product_obj
            obj.params = json.dumps({u'Размер': form.cleaned_data['value']})
            obj.save()
        return redirect(reverse('shop_offer_list', kwargs=dict(shop_id=shop_obj.pk, product_pk=product_obj.pk)))
    return dict(
        shop=shop_obj,
        product=product_obj,
        formset=formset,
        is_automatic_mode=product_obj.datasource.mode == DataSource.MODE_AUTO
    )


@login_required
@only_clients
@to_json
def shop_datasource_force_update(r, shop_obj, datasource_pk):
    datasource_obj = get_object_or_404(
        shop_obj.datasources.all(), pk=datasource_pk)
    management.call_command(
        'fileimport',
        ds_id=datasource_obj.pk,
        shop_id=shop_obj.pk,
        force=True,
        auto=True
    )
    messages.success(
        r, u'Данные источника "%s" успешно обновлены' % datasource_obj.name)
    return dict(result=True)


@login_required
@only_clients
def delete_payment(r, shop_obj, payment_pk):
    payment_obj = get_object_or_404(shop_obj.payments.all(), pk=payment_pk)
    if not payment_obj.is_paid():
        messages.success(r, u'Счет успешно удален')
        payment_obj.delete()
    else:
        messages.error(r, u'Невозможно удалить оплаченный счет')
    return redirect(
        reverse('shop_payment_list', kwargs=dict(shop_id=shop_obj.pk)))


@login_required
@render_to('Cabinet/client_user_list.html')
def client_user_list(r, client_pk):
    if r.user.is_superuser:
        clients = Client.objects.all()
    else:
        clients = r.user.clients.all()
    client_obj = get_object_or_404(clients, pk=client_pk)
    roles = Role.objects.all()
    form = ClientForm(r.POST or None, instance=client_obj)
    return dict(
        client=client_obj,
        form=form,
        roles=roles
    )


@render_to('email/client_new_callback_order.html')
def test_view(r):
    return dict(
        order=CallbackOrder.objects.all()[0]
    )


@login_required
@render_to('Cabinet/customer_view.html')
def customer_view(r, customer_pk):
    customer_obj = get_object_or_404(Customer, pk=customer_pk)
    clients = r.user.clients.all()
    try:
        shop = Shop.objects.filter(client__in=clients)[0]
    except:
        shop = None
    return dict(customer=customer_obj, shop=shop)


@login_required
@only_clients
@render_to('Cabinet/landing_list.html')
def landing_list(r, shop_obj):
    landings = shop_obj.landings.all()
    if landings:
        landing = landings[0]
        return redirect(reverse('landing_view', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing.pk}))
    else:
        landing = None
    return dict(
        shop=shop_obj,
        landing=landing
    )


@login_required
@only_clients
@render_to('Cabinet/landing_view.html')
def landing_view(r, shop_obj, landing_pk):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    return dict(
        shop=shop_obj,
        landing=landing_obj,
        sections=sorted(landing_obj.get_sections(), key=lambda s: s.order)
    )


@login_required
@only_clients
@render_to('Cabinet/landing_section_edit_or_create.html')
def landing_section_main(r, shop_obj, landing_pk):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    section_obj = landing_obj.section_main
    form = LandingSectionMainForm(r.POST or None, instance=section_obj)
    if form.is_valid():
        section_obj = form.save(commit=False)
        section_obj.landing = landing_obj
        section_obj.save()
        messages.success(r, u'Раздел успешно изменен')
        return redirect(reverse('lsection_main', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk}))
    return dict(
        shop=shop_obj,
        landing=landing_obj,
        form=form,
        section_obj=section_obj,
        section='main'
    )


@login_required
@only_clients
@render_to('Cabinet/landing_section_edit_or_create.html')
def landing_section_counter(r, shop_obj, landing_pk):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    section_obj = landing_obj.section_counter
    form = LandingSectionCounterForm(r.POST or None, instance=section_obj)
    if form.is_valid():
        section_obj = form.save(commit=False)
        section_obj.landing = landing_obj
        section_obj.save()
        messages.success(r, u'Раздел успешно изменен')
        return redirect(reverse('lsection_counter', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk}))
    return dict(
        shop=shop_obj,
        landing=landing_obj,
        form=form,
        section_obj=section_obj,
        section='counter'
    )


@login_required
@only_clients
@render_to('Cabinet/landing_section_edit_or_create.html')
def landing_section_products(r, shop_obj, landing_pk):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    section_obj = landing_obj.section_products
    form = LandingSectionProductsForm(r.POST or None, instance=section_obj, shop=shop_obj)
    if form.is_valid():
        section_obj = form.save(commit=False)
        section_obj.landing = landing_obj
        section_obj.save()
        messages.success(r, u'Раздел успешно изменен')
        return redirect(reverse('lsection_products', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk}))
    return dict(
        shop=shop_obj,
        landing=landing_obj,
        form=form,
        section_obj=section_obj,
        section='products'
    )


@login_required
@only_clients
@render_to('Cabinet/landing_section_edit_or_create.html')
def landing_section_compare(r, shop_obj, landing_pk):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    section_obj = landing_obj.section_compare
    form = LandingSectionCompareForm(r.POST or None, instance=section_obj)
    if form.is_valid():
        section_obj = form.save(commit=False)
        section_obj.landing = landing_obj
        section_obj.save()
        messages.success(r, u'Раздел успешно изменен')
        return redirect(reverse('lsection_compare', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk}))
    return dict(
        shop=shop_obj,
        landing=landing_obj,
        form=form,
        section_obj=section_obj,
        section='compare'
    )


@login_required
@only_clients
@render_to('Cabinet/landing_section_edit_or_create.html')
def landing_section_comments(r, shop_obj, landing_pk):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    section_obj = landing_obj.section_comments
    form = LandingSectionCommentsForm(r.POST or None, instance=section_obj)
    base_comments_formset = inlineformset_factory(LSectionComments, LSectionComment, form=LandingSectionCommentForm, extra=1)
    comments_formset = base_comments_formset(r.POST or None, instance=section_obj)
    if form.is_valid() and comments_formset.is_valid():
        section_obj = form.save(commit=False)
        section_obj.landing = landing_obj
        section_obj.save()
        comments_formset.save()
        messages.success(r, u'Раздел успешно изменен')
        return redirect(reverse('lsection_comments', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk}))
    return dict(
        shop=shop_obj,
        landing=landing_obj,
        form=form,
        section_obj=section_obj,
        comments_formset=comments_formset,
        section='comments'
    )


@login_required
@only_clients
@render_to('Cabinet/landing_section_edit_or_create.html')
def landing_section_simple(r, shop_obj, landing_pk, section_pk=None):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    if section_pk:
        section_obj = get_object_or_404(landing_obj.sections_simple, pk=section_pk)
        form = LandingSectionSimpleForm(r.POST or None, instance=section_obj)
    else:
        section_obj = landing_obj.sections_simple.create(
            title=u'Свободный раздел', text='')
        return redirect(reverse('lsection_simple_edit', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk,
            'section_pk': section_obj.pk}))
        # form = LandingSectionSimpleForm(r.POST or None)
    if form.is_valid():
        section_obj = form.save(commit=False)
        section_obj.landing = landing_obj
        section_obj.save()
        messages.success(r, u'Раздел успешно изменен')
        return redirect(reverse('lsection_simple_edit', kwargs={
            'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk,
            'section_pk': section_obj.pk}))
    return dict(
        shop=shop_obj,
        landing=landing_obj,
        form=form,
        section_obj=section_obj,
        section='simple'
    )


@login_required
@only_clients
@render_to('Cabinet/landing_section_edit_or_create.html')
def landing_section_simple_delete(r, shop_obj, landing_pk, section_pk):
    landing_obj = get_object_or_404(shop_obj.landings.all(), pk=landing_pk)
    section_simple_obj = get_object_or_404(landing_obj.sections_simple, pk=section_pk)
    section_simple_obj.delete()
    messages.success(r, u'Раздел успешно удален')
    return redirect(reverse('landing_view', kwargs={
        'shop_id': shop_obj.pk, 'landing_pk': landing_obj.pk}))


@login_required
@only_clients
def landing_create(r, shop_obj):
    if not shop_obj.landings.all():
        l = Landing.objects.create(shop=shop_obj)
        product_groups = shop_obj.product_groups.all()
        if product_groups:
            product_group = product_groups[0]
        else:
            product_group = ProductGroup.objects.create(
                name=u'Подборка товаров для посадочной страницы',
                shop=shop_obj
            )
        LSectionMain.objects.create(
            landing=l, order=1, title=u'Основной раздел',
            desc=u'Текст основного раздела')
        LSectionCounter.objects.create(
            landing=l, order=2, title=u'Счетчик',
            date=timezone.now() + timezone.timedelta(days=7),
            show_in_footer=False)
        LSectionProducts.objects.create(
            landing=l, order=3, title=u'Продукты',
            product_group=product_group)
        LSectionCompare.objects.create(
            landing=l, order=4, left_title=u'Проблемы', left_text=u'Проблемы покупателей',
            right_title=u'Решения', right_text=u'Ваши достоинства')
        c = LSectionComments.objects.create(
            landing=l, order=5, title=u'Комментарии пользователей')
        LSectionComment.objects.create(
            section_comments=c, author=u'Иван Петров',
            text=u'Спасибо за отличный сервис')
        LSectionSimple.objects.create(
            landing=l, order=6, title=u'Свободный раздел', text=u'')
        messages.success(r, u'Посадочная страница успешно создана')
    return redirect(reverse('landing_list', kwargs={'shop_id': shop_obj.pk}))
