# coding=utf-8
from __future__ import unicode_literals
import copy
from Core import models
from Core.models import CallbackOrder
from django.forms import widgets
from django.forms.util import flatatt
from django.utils.encoding import force_text
from django.utils.html import format_html, format_html_join
from itertools import chain
import calendar

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

import django_filters


class CallbackOrderFilter(django_filters.FilterSet):
    app_type = django_filters.ChoiceFilter(
        choices=(('', u'Любая'), ) + tuple(models.AppType.objects.all().values_list('id', 'name')),
        widget=widgets.Select(attrs={'class': 'span3'}))

    class Meta(object):
        model = CallbackOrder
        fields = ['app_type']


class OrderFilter(django_filters.FilterSet):
    cdate_from = django_filters.DateFilter(name='cdate', lookup_type='gte',
        widget=widgets.TextInput(attrs={'placeholder': u'От даты', 'class': 'span2'}))
    cdate_to = django_filters.DateFilter(name='cdate', lookup_type='lte',
        widget=widgets.TextInput(attrs={'placeholder': u'До даты', 'class': 'span2'}))
    status = django_filters.ChoiceFilter(
        choices=(('', u'Любой'), ) + models.Order.STATUS_CHOICES,
        widget=widgets.Select(attrs={'class': 'span3'}))

    class Meta(object):
        model = models.Order
        fields = ['cdate_from', 'cdate_to', 'status']


class ButtonRadioInput(widgets.RadioInput):
    def __init__(self, *args, **kwargs):
        self.button_attrs = copy.deepcopy(kwargs.pop('button_attrs', None))
        super(ButtonRadioInput, self).__init__(*args, **kwargs)

    def render(self, name=None, value=None, attrs=None, choices=()):
        name = name or self.name
        value = value or self.value
        attrs = attrs or self.attrs
        choice_label = force_text(self.choice_label)
        final_attrs = dict(name=name, value=self.choice_value)
        button_attrs = self.button_attrs
        if button_attrs:
            button_attrs.update(final_attrs)
        if self.is_checked():
            klass = button_attrs.get('class', '')
            klass += ' disabled'
            button_attrs['class'] = klass

        return format_html('<button {0}>{1}</button>',
                           flatatt(button_attrs), choice_label)


class ButtonRadioSelectRenderer(widgets.RadioFieldRenderer):
    def __init__(self, name, value, attrs, choices, button_attrs=None):
        self.button_attrs = button_attrs
        super(ButtonRadioSelectRenderer, self).__init__(name, value, attrs, choices)

    def __iter__(self):
        for i, choice in enumerate(self.choices):
            yield ButtonRadioInput(self.name, self.value, self.attrs.copy(), choice, i, button_attrs=self.button_attrs)

    def render(self):
        return format_html('<div>\n{0}\n</div>',
                           format_html_join('\n', '{0}', [(force_text(w),) for w in self]))


class WidgetButtonRadioSelect(widgets.RadioSelect):
    renderer = ButtonRadioSelectRenderer

    def __init__(self, *args, **kwargs):
        self.button_attrs = kwargs.pop('button_attrs', None)
        super(WidgetButtonRadioSelect, self).__init__(*args, **kwargs)

    def get_renderer(self, name, value, attrs=None, choices=()):
        if value is None: value = ''
        str_value = force_text(value) # Normalize to string.
        final_attrs = self.build_attrs(attrs)
        choices = list(chain(self.choices, choices))
        return self.renderer(name, str_value, final_attrs, choices, button_attrs=self.button_attrs)


class ReportByClientFilter(django_filters.FilterSet):
    MONTH_CHOICES = [('', u'Все')] + map(
        lambda d: (d.month, calendar.month_name[d.month]),
        models.Client.objects.all().dates('cdate', 'month'))
    month = django_filters.ChoiceFilter(
        name='cdate', lookup_type='month', choices=MONTH_CHOICES,
        widget=WidgetButtonRadioSelect(attrs={'class': 'span6'}, button_attrs={'class': 'btn btn-success'}))

    class Meta(object):
        model = models.Client
        fields = ['month']


class ReportByShopFilter(django_filters.FilterSet):
    MONTH_CHOICES = [('', u'Все')] + map(
        lambda d: (d.month, calendar.month_name[d.month]),
        models.Shop.objects.all().dates('cdate', 'month'))
    month = django_filters.ChoiceFilter(
        name='cdate', lookup_type='month', choices=MONTH_CHOICES,
        widget=WidgetButtonRadioSelect(attrs={'class': 'span6'}, button_attrs={'class': 'btn btn-success'}))

    class Meta(object):
        model = models.Shop
        fields = ['month']
