# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.timesince import timesince, timeuntil
from Cabinet.templatetags import cabinet
from Core.models import *


top_menu = [
    (u'Магазины', 'main'),
    (u'Профиль', 'client_profile_list'),
]

menu_sub_elements = [
    # Текст ссылки, Имя URL в файле urls.py
    (u'Профиль', 'shop_profile'),
    (u'Меню', 'shop_pages'),
    (u'Категории', 'shop_categories'),
    (u'Товары', 'shop_goods'),
    (u'Отзывы', 'shop_comments'),
    (u'Заказы', 'shop_orders'),
    (u'Акции', 'shop_stock'),
]


def get_subs(shop_id, cur_path):
    data = {}
    for index, (sub_name, url_name) in enumerate(menu_sub_elements):
        url = reverse(url_name, kwargs=dict(shop_id=shop_id))
        data[index] = {
            'name': sub_name,
            'link': url,
            'active': cur_path.startswith(url)
        }
    return data


def menu(request):
    shops = Shop.objects.none()
    if request.user.is_superuser:
        shops = Shop.objects.all().order_by('client','id').select_related('client')
    else:
        try:
            shops = Shop.objects.filter(client__in=request.user.clients.all()).select_related('client')
        except:
            pass

    menu_items = {}
    cur_path = request.path
    path = cur_path.split('/')[2:]

    #for shop in shops:
    #    shop_id = shop.id
    #    shop_url = reverse('shop', kwargs=dict(shop_id=shop_id))
    #    menu_items[shop_id] = {
    #        'info': shop,
    #        'name': shop.name,
    #        'link': shop_url,
    #        'active': cur_path.startswith(shop_url),
    #        'subs': get_subs(shop_id, cur_path)
    #    }
    top_menu_items = []
    for link_name, url_name in top_menu:
        url = reverse(url_name)
        top_menu_items.append(dict(
            title=link_name,
            url=url,
            active=cur_path.startswith(url)
        ))

    return dict(top_menu_items=top_menu_items, menu=menu_items, path=path, shops=shops, request=request)


def get_shop_notifications4clients(clients):
    n = []
    for shop in Shop.objects.filter(client__in=clients):
        if timezone.now() <= shop.paid_till:
            n.append({
                'level': cabinet.INFO,
                'text': u'Для магазина "<a href="%s">%s</a>" тестовый период заканчивается через %s!' % (reverse('shop', kwargs={'shop_id': shop.pk}), shop.name, timeuntil(shop.paid_till))
            })
        elif timezone.now() > shop.paid_till:
            n.append({
                'level': cabinet.WARNING,
                'text': u'Для магазина "<a href="%s">%s</a>" истек тестовый период!'  % (reverse('shop', kwargs={'shop_id': shop.pk}), shop.name,)
            })
    return n


def get_notification4ds(ds_list, clients):
    n = []
    for ds in ds_list.all():
        last_import = ds.get_last_fileimport_log()
        if last_import and last_import.text_err:
            ds_shop = ds.shops.filter(client__in=clients)[0]
            ds_url = reverse('shop_datasource_edit', kwargs={
                'shop_id': ds_shop.pk, 'datasource_pk': ds.pk})
            n.append({
                'level': cabinet.DANGER,
                'text': u'Ошибка при импортировании '
                        u'данных для источника "<a href="%s">%s</a>"' % (
                            ds_url, ds.name)
            })
    return n


def get_notification4client(clients):
    n = []
    for client in clients:
        if not client.is_validated:
            url = reverse('client_profile', kwargs={'client_pk': client.pk})
            n.append({
                'level': cabinet.WARNING,
                'text': u'<a href="%s">Укажите</a> реквизиты для клиента "%s"' % (url, client.name)
            })
    return n


def get_notification4user(user_list):
    n = []
    for user in user_list:
        try:
            client = user.clients.all()[0]
        except:
            return []
        if not user.shoppyuser.tel:
            url = reverse('client_user_edit', kwargs={
                'client_pk': client.pk, 'user_pk': user.pk})
            n.append({
                'level': cabinet.INFO,
                'text': u'<a href="%s">Введите</a> контактный номер '
                        u'телефона для сотрудника "%s"' % (
                    url, user.shoppyuser.get_user_name())})
        if not user.first_name:
            url = reverse('client_user_edit', kwargs={
                'client_pk': client.pk, 'user_pk': user.pk})
            n.append({
                'level': cabinet.INFO,
                'text': u'<a href="%s">Укажите</a> имя для сотрудника "%s"' % (
                    url, user.shoppyuser.get_user_name())
            })
    return n


def get_notifications_for_user(user):
    n = []
    clients = user.clients.all()
    if user.shoppyuser.roles.filter(alias=Role.ALIAS_ADMIN):
        for client in clients:
            n += get_notification4user(client.users.all().exclude(pk=user.pk))
    ds_list = DataSource.objects.filter(client__in=clients)
    if clients:
        n += get_notification4user([user])
    n += get_notification4ds(ds_list, clients)
    n += get_shop_notifications4clients(clients)
    n += get_notification4client(clients)
    return n


def notifications(r):
    n = []
    p = r.path
    if not p.startswith('/cabinet/') or r.shop or not r.user.is_authenticated():
        return {'notifications': []}
    if not r.user.is_superuser:
        try:
            n = get_notifications_for_user(r.user)
        except:
            logger.exception('Error on get user notification for user')
    else:
        n = []
        try:
            if p.startswith('/cabinet/client/'):
                client_pk = int(p[16:].split('/')[0])
                client_obj = Client.objects.get(pk=client_pk)
                shops = client_obj.shops.all()
            else:
                shop_pk = int(r.path[9:].split('/')[0])
                shops = [Shop.objects.get(pk=shop_pk)]

            for shop_obj in shops:
                n += get_notification4user(shop_obj.client.users.all())
                n += get_notification4ds(shop_obj.datasources.all(), [shop_obj.client])
                n += get_shop_notifications4clients([shop_obj.client])
                n += get_notification4client([shop_obj.client])
        except:
            logger.exception('Error on get user notification for superadmin')
    return {'notifications': n}
