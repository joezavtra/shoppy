from datetime import datetime
from django.forms import widgets
from django.forms.util import flatatt
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe


class HelpWidget(widgets.TextInput):
    def __init__(self, *args, **kwargs):
        self.help_text = kwargs.pop('help_text')
        super(HelpWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            final_attrs['value'] = force_text(self._format_value(value))
        return format_html(
            u'<div class="input-append"><input{0} /><span class="add-on">{1}</span></div>',
            flatatt(final_attrs),
            self.help_text
        )


class DSSelect(widgets.Select):
    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        return format_html('<option value="{0}"{1}>{2}</option>',
                           option_value,
                           selected_html,
                           force_text(option_label))


class DateTimePickerWidget(widgets.TextInput):

    def _format_value(self, value):
        if isinstance(value, basestring):
            return value
        return datetime.strftime(value, '%d.%m.%Y %H:%M')

    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            final_attrs['value'] = force_text(self._format_value(value))
        js_script = mark_safe((
            u'<script>'
            u'$(function(){'
            u'$("#' + final_attrs['id'] + u'").datetimepicker({'
            u'format:"d.m.Y H:i"'
            u'});'
            u'});'
            u'</script>'
        ))
        return format_html(
            u'<input{0} />',
            flatatt(final_attrs)
        ) + js_script


class DatePickerWidget(widgets.TextInput):

    def _format_value(self, value):
        if isinstance(value, basestring):
            return value
        return datetime.strftime(value, '%d.%m.%Y')

    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            final_attrs['value'] = force_text(self._format_value(value))
        js_script = mark_safe((
            u'<script>'
            u'$(function(){'
            u'$("#' + final_attrs['id'] + u'").datetimepicker({'
            u'format:"d.m.Y",'
            u'scrollInput: false,'
            u'scrollTime: false,'
            u'scrollMonth: false,'
            u'timepicker:false,'
            u'});'
            u'});'
            u'</script>'
        ))
        return format_html(
            u'<input{0} />',
            flatatt(final_attrs)
        ) + js_script
