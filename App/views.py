# -*- coding: utf-8 -*-
import Core
import facebook
import re

from urlparse import urlparse, parse_qs, parse_qsl
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from Core.api.resources import *
from Core.decorators import *
from Core.models import ShopExtra


T = 265


@required_shop_status
@render_to('App/odnoklassniki.html')
def shop_odnoklassniki(r):
    shop_obj = r.shop
    platforms = shop_obj.get_platforms()
    user_id = r.app_customer['user_id']
    customer = r.app_customer['customer']
    redirect = r.app_customer['redirect']
    sex_dict = { 1: 'female', 2: 'male' }

    Core.analytics.identify(customer.id, {
        "name": customer.name,
        "last_name": customer.last_name,
        "nickname": customer.nickname,
        "sex": sex_dict.get(customer.sex),
        "email": customer.email,
        "city": customer.city
    })

    Core.analytics.track(customer.id, "Entered a shop", {
        "shop_domain": shop_obj.domain,
        "shop_id": shop_obj.id,
        "shop_type": "odnoklassniki"
    })

    slider_group = shop_obj.product_groups.filter(use_for_slider=True)
    if slider_group and slider_group[0].products.all().count():
        has_favourites = True
    else:
        has_favourites = False

    landings = shop_obj.landings.all()
    if landings and landings[0].is_active():
        has_landings = True
    else:
        has_landings = False

    ret = dict(
        has_landing=has_landings,
        shop=shop_obj,
        has_favourites=has_favourites,
        show_subscribe_form=customer.is_need_show_banner_for_shop(shop_obj=shop_obj),
        app_type='ok',
        services=dict(
            checkout=bool(platforms['checkout']),
            robokassa=bool(platforms['robokassa'])
        ),
        redirect=redirect,
        platform='ok',
        user=dict(
            id=user_id,
            customer=customer,
            cart=customer.get_cart_for_shop(shop_obj)
        ),
        currency={
            'text': shop_obj.currency.text,
            'code': shop_obj.currency.code,
            # доступные значения text, code, symbol
            'display_type': 'text'
        },
        config={
            'show_news': int(
                ShopExtra().get_param(shop_obj.pk, 'APP_SECTION_NEWS') or 0),
        },
        utm={
            'utm_source': '',
            'utm_medium': '',
            'utm_campaign': '',
            'utm_custom': '',
            'utm_prefix': ''
        },
        layout='two-columns',
        fast_order=True,
        t=T
    )

    if 'custom_args' in r.GET:
        query_string = r.GET['custom_args']
        for k, v in parse_qsl(query_string):
            if re.match(r'^utm_', k):
                ret['utm'][k] = v

    return ret


@required_shop_status
@render_to('App/vk.html')
def shopVK(r):
    customer = r.app_customer['customer']
    viewer_id = r.app_customer['user_id']
    shop_obj = r.shop
    sex_dict = { 1: 'female', 2: 'male' }

    Core.analytics.identify(customer.id, {
        "name": customer.name,
        "last_name": customer.last_name,
        "nickname": customer.nickname,
        "sex": sex_dict.get(customer.sex),
        "email": customer.email,
        "city": customer.city
    })

    Core.analytics.track(customer.id, "Entered a shop", {
        "shop_domain": shop_obj.domain,
        "shop_type": "vk",
        "shop_id": shop_obj.id
    })

    platforms = shop_obj.get_platforms()
    slider_group = shop_obj.product_groups.filter(use_for_slider=True)
    if slider_group and slider_group[0].products.all().count():
        has_favourites = True
    else:
        has_favourites = False

    landings = shop_obj.landings.all()
    if landings and landings[0].is_active():
        has_landings = True
    else:
        has_landings = False

    ret = dict(
        has_landing=has_landings,
        shop=shop_obj,
        has_favourites=has_favourites,
        show_subscribe_form=customer.is_need_show_banner_for_shop(shop_obj=shop_obj),
        app_type='vk',
        services={
            'checkout': bool(platforms['checkout']),
            'robokassa': bool(platforms['robokassa']),
        },
        redirect='',
        hash=r.app_customer['params']['hash'],
        platform='vk',
        user={
            'id': viewer_id,
            'customer': customer,
            'cart': customer.get_cart_for_shop(shop_obj),
        },
        currency={
            'text': shop_obj.currency.text,
            'code': shop_obj.currency.code,
            # доступные значения text, code, symbol
            'display_type': 'text'
        },
        config={
            'show_news': int(
                ShopExtra().get_param(shop_obj.pk, 'APP_SECTION_NEWS') or 0),
        },
        customer=customer,
        utm={
            'utm_source': '',
            'utm_medium': '',
            'utm_campaign': '',
            'utm_custom': '',
            'utm_prefix': ''
        },
        layout='two-columns',
        fast_order=True,
        t=T
    )

    offer = dict()
    query_string = urlparse.urlparse(
        r.META.get('HTTP_REFERER', 'referer')).query
    if query_string:
        for k, v in parse_qs(query_string, True).iteritems():
            if re.match(r'^utm_', k):
                try:
                    ret['utm'][k] = v[0]
                except IndexError:
                    ret['utm'][k] = v
            elif re.match(r'^vk_', k):
                try:
                    offer[k] = v[0]
                except IndexError:
                    offer[k] = v

    if offer and VKOffer.check(**offer):
        r.session['vk_offer_sid'] = offer['vk_sid']
        r.session['vk_offer_lead_id'] = offer['vk_lead_id']
        r.session.modified = True

    return ret


@csrf_exempt
@required_shop_status
@render_to('App/facebook.html')
def shopFB(r, tab, redirect=''):
    shop_obj = r.shop
    sex_dict = { 1: 'female', 2: 'male' }

    app_type = 'fb'
    if tab:
        app_type = 'fb_tab'

    platforms = shop_obj.get_platforms()
    customer = r.app_customer['customer']

    Core.analytics.identify(customer.id, {
        "name": customer.name,
        "last_name": customer.last_name,
        "nickname": customer.nickname,
        "sex": sex_dict.get(customer.sex),
        "email": customer.email,
        "city": customer.city
    })

    Core.analytics.track(customer.id, "Entered a shop", {
        "shop_domain": shop_obj.domain,
        "shop_type": "facebook",
        "shop_id": shop_obj.id
    })

    slider_group = shop_obj.product_groups.filter(use_for_slider=True)
    if slider_group and slider_group[0].products.all().count():
        has_favourites = True
    else:
        has_favourites = False

    landings = shop_obj.landings.all()
    if landings and landings[0].is_active():
        has_landings = True
    else:
        has_landings = False

    ret = dict(
        has_landing=has_landings,
        shop=shop_obj,
        show_subscribe_form=customer.is_need_show_banner_for_shop(shop_obj=shop_obj),
        has_favourites=has_favourites,
        app_type=app_type,
        services={
            'checkout': bool(platforms['checkout']),
            'robokassa': bool(platforms['robokassa']),
        },
        redirect=r.app_customer['redirect'] or redirect,
        platform='fb',
        user={
            'id': r.app_customer['user_id'],
            'customer': customer,
            'cart': customer.get_cart_for_shop(shop_obj),
            'name': u''
        },
        currency={
            'text': shop_obj.currency.text,
            'code': shop_obj.currency.code,
            # доступные значения text, code, symbol
            'display_type': 'text'
        },
        config={
            'show_news': int(
                 ShopExtra().get_param(shop_obj.pk, 'APP_SECTION_NEWS') or 0),
        },
        tab=tab,
        layout='two-columns',
        fast_order=True,
        t=T
    )
    if tab:
        ret['layout'] = 'one-column'

    return ret


@required_shop_status
@render_to('App/index.html')
def shop(r):
    shop_obj = r.shop
    platforms = shop_obj.get_platforms()
    customer = r.app_customer['customer']

    sex_dict = { 1: 'female', 2: 'male' }

    Core.analytics.identify(customer.id, {
        "name": customer.name,
        "last_name": customer.last_name,
        "nickname": customer.nickname,
        "sex": sex_dict.get(customer.sex),
        "email": customer.email,
        "city": customer.city
    })

    Core.analytics.track(customer.id, "Entered a shop", {
        "shop_domain": shop_obj.domain,
        "shop_id": shop_obj.id,
        "shop_type": "domain"
    })

    slider_group = shop_obj.product_groups.filter(use_for_slider=True)
    if slider_group and slider_group[0].products.all().count():
        has_favourites = True
    else:
        has_favourites = False

    landings = shop_obj.landings.all()
    if landings and landings[0].is_active():
        has_landings = True
    else:
        has_landings = False


    return dict(
        has_landing=has_landings,
        shop=shop_obj,
        show_subscribe_form=customer.is_need_show_banner_for_shop(shop_obj=shop_obj),
        has_favourites=has_favourites,
        app_type='www',
        services={
            'checkout': bool(platforms['checkout']),
            'robokassa': bool(platforms['robokassa']),
        },
        redirect='',
        platform='',
        user={
            'id': 0,
            'customer': customer,
            'cart': customer.get_cart_for_shop(shop_obj),
            'name': u''
        },
        currency={
            'text': shop_obj.currency.text,
            'code': shop_obj.currency.code,
            # доступные значения text, code, symbol
            'display_type': 'text'
        },
        config={
            'show_news': int(
                ShopExtra().get_param(shop_obj.pk, 'APP_SECTION_NEWS') or 0),
        },
        layout='two-columns',
        fast_order=True,
        t=T
    )


@to_json
def app_specification(r):
    """
    Возвращает спецификацию приложения для указанного магазина и в зависимости
    от прочих параметров

    :param r: объект запроса
    """
    return {}
    # shop_obj = r.shop
    # ds_list = shop_obj.datasources.all()
    # platforms = shop_obj.get_platforms()
    # return {
    #     'shop': {
    #         'name': shop_obj.name,  # название магазина
    #         'alias': shop_obj.alias,  # алиас магазина
    #         'domain': shop_obj.domain,  # домен магазина
    #         'pk': shop_obj.pk,  # иденентификатор магазина
    #     },
    #     'services': {
    #         'checkout': bool(platforms['checkout']),
    #         'robokassa': bool(platforms['robokassa']),
    #     },
    #     'customer': {
    #
    #     },
    #     'theme': {  # параметры отображения (стили, логотипы, etc)
    #         'base_path': '/static/themes/%d' % shop_obj.pk,  # базовый путь до
    #         # стилей (относительно этого пути будут формироваться ссылки на все
    #         # статичные файлы, а так же можно генерировать ссылки и на
    #         # изображеиня магазина)
    #         'logo_url': shop_obj.get_logo_url()  # логотип магазина
    #     },
    #     'modules': {  # модули. Каждый модуль обслуживается некоторым
    #         # обработчиком (представлением на клиентской стороне), который знает
    #         # что делать с переданными ему данными
    #         'categories': {  # список категорий. Должен значть где взять список
    #             # категорий, какая категория будет актиына по-умолчанию
    #             'is_active': True,
    #             'defaults': {
    #                 'first_pk': Category.objects.filter(datasource__in=ds_list, status=True)[0].pk,
    #                 'order_by': 'name',
    #                 'expands': False  # все категории по умолчанию развернуты?
    #             },
    #             'data_url': reverse(
    #                 'api_dispatch_list',
    #                 kwargs=CategoryResource().resource_uri_kwargs()
    #             )
    #         },
    #         'products': {
    #             'is_active': True,
    #             'defaults': {
    #                 'display': 'grid',
    #                 'order_by': 'name'
    #             },
    #             'grid_columns': 3,
    #             'items_on_page': 9,
    #             'data_url': reverse(
    #                 'api_dispatch_list',
    #                 kwargs=ProductResource().resource_uri_kwargs()
    #             )
    #         },
    #         'pages': {
    #             'is_active': True,
    #             'items': [
    #                 {
    #                     'title': page.name,
    #                     'alias': page.alias,
    #                     'content': page.content
    #                 }
    #                 for page in shop_obj.pages.all().order_by('-order')
    #             ]
    #         },
    #         'slider': {
    #             'is_active': False,
    #             'items': [
    #                 {
    #                     'title': fav.product.name.strip(),
    #                     'img': fav.product.abs_cover(),
    #                     'text': fav.product.desc
    #                 }
    #                 for fav in shop_obj.favs.all()
    #             ]
    #         },
    #         'search': {
    #             'is_active': True,
    #             'suggest_data_url': reverse(
    #                 'api_dispatch_list',
    #                 kwargs=SearchProductResource().resource_uri_kwargs()
    #             ),
    #             'full_data_url': reverse(
    #                 'api_dispatch_list',
    #                 kwargs=SearchProductFullResource().resource_uri_kwargs()
    #             ),
    #         },
    #         'menu_top': {
    #             'is_active': True,
    #             'checked': 'catalog',
    #             'items': [
    #                 {
    #                     'title': u'Каталог',
    #                     'id': 'catalog'
    #                 },
    #                 {
    #                     'title': u'Новости',
    #                     'id': 'news'
    #                 },
    #                 {
    #                     'title': u'О нас',
    #                     'id': 'about'
    #                 },
    #                 {
    #                     'title': u'Корзина',
    #                     'id': 'cart'
    #                 }
    #             ]
    #         }
    #     }
    # }
