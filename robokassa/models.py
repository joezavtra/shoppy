# coding=utf-8
from django.conf import settings
from Core.models import ClientPayment
from robokassa.api_robokassa import Robokassa

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'
from django.db import models


# class RobokassaPayment(models.Model):
#     STATUS_OK = 1
#     STATUS_NO = 2
#     STATUS_CHOICES = (
#         (STATUS_OK, u'Оплачен'),
#         (STATUS_NO, u'Не оплачен'),
#     )
#     payment = models.ForeignKey(
#         ClientPayment, related_name='robokassa_payments')
#     status = models.PositiveIntegerField(
#         choices=STATUS_CHOICES, default=STATUS_NO)
#
#
