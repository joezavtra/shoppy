# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'RobokassaPayment'
        db.delete_table(u'robokassa_robokassapayment')


    def backwards(self, orm):
        # Adding model 'RobokassaPayment'
        db.create_table(u'robokassa_robokassapayment', (
            ('status', self.gf('django.db.models.fields.PositiveIntegerField')(default=2)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('payment', self.gf('django.db.models.fields.related.ForeignKey')(related_name='robokassa_payments', to=orm['Core.ClientPayment'])),
        ))
        db.send_create_signal(u'robokassa', ['RobokassaPayment'])


    models = {
        
    }

    complete_apps = ['robokassa']