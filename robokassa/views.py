# coding=utf-8
import hashlib
import json
import logging
from django.conf import settings
from django.contrib import messages
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils import timezone
import requests
from Core.decorators import render_to
from Core.models import Order, ClientPayment, EmailQueue
from app_checkout.utils import notify_checkout
from robokassa.api_robokassa import Robokassa
from django.http import HttpResponseRedirect



l = logging.getLogger('app')


def fail(r):
    data = r.GET
    res = dict(order_sum=data['OutSum'], order_id=data['InvId'])
    order_obj = Order.objects.get(pk=res['order_id'])
    order_obj.status = Order.STATUS_NOT_PAID
    order_obj.save()
    return HttpResponseRedirect(order_obj.get_failed_url())


# http://0.0.0.0:8000/robokassa/success/?InvId=96&OutSum=100.00&SignatureValue=bab5a6b100a943d0ce313a7235a2ddaf&Culture=ru-RU
def success(r):
    data = r.GET
    res = dict(
        order_sum=data['OutSum'],
        order_id=data['InvId'],
        checksum=data['SignatureValue'].upper()
    )
    order_obj = Order.objects.get(pk=res['order_id'])
    shop = order_obj.cart.shop
    platforms = shop.get_platforms()
    checkout_platform = platforms['checkout']
    rk_platform = platforms['robokassa']
    if rk_platform:
        rk = Robokassa(
            m_login=rk_platform.login,
            m_pass1=rk_platform.password1,
            m_pass2=rk_platform.password2,
            order_id=res['order_id'],
            order_sum=res['order_sum']
        )

        if rk.get_checksum_success() == res['checksum']:
            order_obj.status = Order.STATUS_PAID
            order_obj.save()
            order_obj.send_notice()

            if shop.pk == 5:
                fname, lname = (order_obj.name + ' - -').split()[0:2]
                url = 'https://letarium.ru/w/make-order.html'
                pid = getattr(settings, 'LETARIUM_PID', None)
                if not pid:
                    l.error('Please set settings.LETARIUM_PID key')
                parthner = getattr(settings, 'LETARIUM_PARTHNER', None)
                if not parthner:
                    l.error('Please set settings.LETARIUM_PARTHNER key')
                else:
                    for item in order_obj.cart.items.all():
                        for i in xrange(item.count):
                            try:
                                check = hashlib.md5(pid + item.product.oid + order_obj.email).hexdigest()
                                params = dict(
                                    pn=item.product.oid,
                                    cert=1,
                                    count=1,
                                    email=order_obj.email,
                                    fullname=order_obj.name,
                                    phone=order_obj.phone,
                                    drv_firstname=fname,
                                    drv_lastname=lname,
                                    parthner=parthner,
                                    check=check
                                )
                                l.info('Call "%s" with GET: "%s"' % (
                                            url, json.dumps(params)))
                                r = requests.post(url, params=params, verify=False)
                                if r.status_code != 200:
                                    l.error(
                                        'Error on letarium hook '
                                        'on order "%d"' % order_obj.pk)
                                    body = 'Order for cart: %d' % order_obj.cart_id
                                    body += "\nServer response:\n"
                                    body += r.content
                                    EmailQueue.objects.create(
                                        email_from='system@shoppyboom.ru',
                                        email_to='dev@shoppyboom.ru',
                                        subject=u'Ошибка при выполнении hook`а',
                                        body=body
                                    )
                                else:
                                    res = r.json()
                                    if 'error' in res:
                                        l.error(
                                            'Bad result for call Letarium hook. '
                                            'Letarium response "%s"' % (str(res))
                                        )
                            except Exception as e:
                                l.exception('Error on call Letarium hook '
                                            'for order: "%d"' % order_obj.pk)
            if checkout_platform:
                for checkout_obj in order_obj.checkouts.all():
                    l.info(
                        'Run "notify_checkout" for order "%d"' % order_obj.pk)
                    notify_checkout(
                        api_key=checkout_platform.api_key,
                        checkout_order_pk=checkout_obj.orderId,
                        local_order_pk=order_obj.pk,
                        payment_method='prepay')

            return HttpResponseRedirect(order_obj.get_success_url())
    return HttpResponseRedirect(order_obj.get_app_url())


# http://0.0.0.0:8000/robokassa/result/?OutSum=100.00&InvId=96&SignatureValue=5103C011F0E7ABD2AEC990807F83F1B0
@render_to('robokassa/result.html')
def result(r):
    res = dict(
        order_sum=str(r.GET['OutSum']),
        order_id=str(r.GET['InvId']),
        checksum=str(r.GET['SignatureValue'].upper())
    )
    order_obj = Order.objects.get(pk=res['order_id'])
    shop = order_obj.cart.shop
    rk_platform = shop.get_platforms()['robokassa']
    if rk_platform:
        rk = Robokassa(
            m_login=rk_platform.login,
            m_pass1=rk_platform.password1,
            m_pass2=rk_platform.password2,
            order_id=res['order_id'],
            order_sum=res['order_sum']
        )
        if rk.get_checksum_result() == res['checksum']:
            # order_obj.status = Order.
            # order_obj.save()
            return dict(status='OK%d' % order_obj.pk)
    return dict(status='FAIL%d' % order_obj.pk)



@transaction.commit_on_success
@render_to('robokassa/fail.html')
def callback_fail(r):
    res = dict(
        order_sum=r.GET['OutSum'], order_id=r.GET['InvId'])
    rk_payment_obj = get_object_or_404(ClientPayment, pk=res['order_id'])
    rk_payment_obj.status = ClientPayment.STATUS_NO
    rk_payment_obj.save()
    return dict()


@transaction.commit_on_success
@render_to('robokassa/success.html')
def callback_success(r):
    res = dict(
        order_sum=r.GET['OutSum'],
        order_id=r.GET['InvId'],
        checksum=r.GET['SignatureValue'].upper()
    )
    payment_obj = get_object_or_404(ClientPayment, pk=res['order_id'])

    rk = Robokassa(
        m_login=settings.ROBOKASSA_LOGIN,
        m_pass1=settings.ROBOKASSA_PASSWORD_1,
        m_pass2=settings.ROBOKASSA_PASSWORD_2,
        order_id=res['order_id'],
        order_sum=res['order_sum']
    )

    if rk.get_checksum_success() == res['checksum']:
        payment_obj.status = ClientPayment.STATUS_OK
        payment_obj.save()
        delta = timezone.timedelta(
            days=payment_obj.days if payment_obj.days else 0)
        payment_obj.shop.paid_till = payment_obj.shop.paid_till + delta
        payment_obj.shop.save()
        messages.success(r, u'Платеж проведен успешно')
        try:
            email_body = render_to_string(
                'email/client_new_payment.html',
                dict(payment=payment_obj)
            )
            EmailQueue.objects.create(
                email_from=u'ShoppyBoom <payments@shoppyboom.ru>',
                email_to='finance@shoppyboom.ru',
                subject=u'ShoppyBoom: оплата услуг через кабинет',
                message_type=EmailQueue.MTYPE_HTML,
                body=email_body
            )
            email_body = render_to_string('email/payment_paid.html', dict(
                shop_name=payment_obj.shop.name,
                payment_desc=payment_obj.desc,
                payment_amount=payment_obj.get_amount(),
                shop_pk=payment_obj.shop.pk
            ))
            print email_body
            EmailQueue.objects.create(
                email_from='payments@shoppyboom.ru',
                email_to=payment_obj.shop.email,
                bcc='info@shoppyboom.ru',
                subject=u'Счет оплачен',
                message_type=EmailQueue.MTYPE_HTML,
                body=email_body
            )
        except:
            l.exception('Cannot create email queue for robokassa success callback')
        return dict(status=True, payment=payment_obj)
    messages.success(r, u'Платеж не проведен')
    return dict(status=False, payment=payment_obj)


@transaction.commit_on_success
@render_to('robokassa/result.html')
def callback_result(r):
    res = dict(
        order_sum=str(r.GET['OutSum']),
        order_id=str(r.GET['InvId']),
        checksum=str(r.GET['SignatureValue'].upper())
    )
    rk_payment_obj = get_object_or_404(ClientPayment, pk=res['order_id'])

    rk = Robokassa(
        m_login=settings.ROBOKASSA_LOGIN,
        m_pass1=settings.ROBOKASSA_PASSWORD_1,
        m_pass2=settings.ROBOKASSA_PASSWORD_2,
        order_id=res['order_id'],
        order_sum=res['order_sum']
    )
    if rk.get_checksum_result() == res['checksum']:
        rk_payment_obj.status = ClientPayment.STATUS_IN_PROCESS
        rk_payment_obj.save()
        return dict(status='OK%d' % rk_payment_obj.pk)
    return dict(status='FAIL%d' % rk_payment_obj.pk)

