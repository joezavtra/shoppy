from django.conf.urls import patterns, url
from robokassa.views import *

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


urlpatterns = patterns('',
    url(r'^result/$', result, name='robokassa_result'),
    url(r'^success/$', success, name='robokassa_success'),
    url(r'^fail/$', fail, name='robokassa_fail'),

    url(r'^callback/result/$', callback_result, name='robokassa_callback_result'),
    url(r'^callback/success/$', callback_success, name='robokassa_callback_success'),
    url(r'^callback/fail/$', callback_fail, name='robokassa_callback_fail'),
)