from django.conf import settings

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

from hashlib import md5


class RobokassaException(Exception):
    pass


class Robokassa(object):
    """
        http://test.robokassa.ru/Index.aspx?
        MrchLogin=test_100&
        OutSum=100.20&
        InvId=1000&
        Desc=sInvDesc&
        SignatureValue=7c84935aa0c7ea5badc8bbb74cc28b43&
        Culture=ru
    """
    if settings.DEBUG:
        base_url = 'http://test.robokassa.ru/Index.aspx?'
    else:
        base_url = 'https://auth.robokassa.ru/Merchant/Index.aspx?'
    params = {
        'MrchLogin': None,
        'OutSum': None,
        'InvId': None,
        'Desc': None,
        'SignatureValue': None,
        'Culture': 'ru',
    }
    user_params = {}
    m_login = ''
    m_pass1 = ''
    m_pass2 = ''

    def __init__(
        self,
        m_login,
        m_pass1,
        m_pass2,
        order_id,
        order_sum,
        user_params=None,
        order_desc=None
    ):
        self.m_login = str(m_login)
        self.m_pass1 = str(m_pass1)
        self.m_pass2 = str(m_pass2)
        self.params['MrchLogin'] = str(m_login)
        self.params['InvId'] = str(order_id)
        self.params['OutSum'] = str(order_sum)
        self.params['Desc'] = str(order_desc) if order_desc else None
        self.params['SignatureValue'] = self.get_checksum_request()
        if user_params is not None:
            self.user_params = user_params
        for key in self.user_params.iterkeys():
            if not key.startswith('sph'):
                raise RobokassaException(
                    'All keys in user_params must starts with "sph" symbols'
                )

    def get_link_to_payment(self):
        pair_param = [k + '=' + v for k, v in self.params.iteritems() if v]
        return self.base_url + '&'.join(pair_param)

    def get_checksum_request(self):
        p_list = [
            self.m_login,
            self.params['OutSum'],
            self.params['InvId'],
            self.m_pass1,
        ] + [k + '=' + v for k, v in self.user_params.iteritems()]
        return md5(':'.join(p_list)).hexdigest().upper()

    def get_checksum_result(self):
        p_list = [
            self.params['OutSum'],
            self.params['InvId'],
            self.m_pass2,
        ] + [k + '=' + v for k, v in self.user_params.iteritems()]
        return md5(':'.join(p_list)).hexdigest().upper()

    def get_checksum_success(self):
        p_list = [
            self.params['OutSum'],
            self.params['InvId'],
            self.m_pass1,
        ] + [k + '=' + v for k, v in self.user_params.iteritems()]
        return md5(':'.join(p_list)).hexdigest().upper()


