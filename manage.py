#!/usr/bin/env python
import os
import site
import socket
import sys


if socket.getfqdn() == 'cs12589.selectel.ru':
    project_root = os.path.abspath(os.path.curdir)
    site.addsitedir(os.path.join(project_root, 'env/lib/python2.6/site-packages'))
    site.addsitedir(os.path.join(project_root, 'env/lib/python2.7/site-packages'))


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
