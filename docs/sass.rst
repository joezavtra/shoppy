SASS
====

Шаблонный тег sass
------------------

Получение ссылки на CSS файл на основе SASS файла

.. code-block:: html

    {% load superstatic %}
    <link src="{% sass 'sass/index.sass' %}" type="text/css" />


Получение содержимого готового CSS файла на основе SASS

.. code-block:: html

    {% load superstatic %}
    <style>
        {% sass 'sass/index.sass' css='yes' %}
    </style>


Тег может принимать неогрниченное кол-во ссылок на файлы. При этом результат его
работы будет конкатенация полученного результата

.. code-block:: html

    {% load superstatic %}
    <style>
        {% sass 'sass/reset.sass' 'sass/index.sass' 'sass/custom.sass' css='yes' %}
    </style>
