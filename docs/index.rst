.. Shoppy documentation master file, created by
   sphinx-quickstart on Thu Nov 21 00:45:19 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Shoppy's documentation!
==================================

Contents:

.. toctree::
    :maxdepth: 2

    fileimport
    virtualenv
    south
    deploy
    sphinx-search
    sass
    user_middleware



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

