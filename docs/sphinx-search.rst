Поисковая система Sphinx
========================


Установка Sphinx
----------------

.. code-block:: bash

    sudo yum install -y mysql-devel
    wget http://sphinxsearch.com/files/sphinx-2.1.2-release.tar.gz
    tar xzf sphinx-2.1.2-release.tar.gz
    cd sphinx-2.1.2-release
    ./configure --with-mysql
    make install
    cd /usr/local/etc/
    cp sphinx.conf.dist sphinx.conf
    vi sphinx.conf
    ln -s /usr/local/bin/indexer /usr/sbin/indexer
    ln -s /usr/local/bin/searchd /usr/sbin/searchd
    mkdir -p /var/data/
    indexer --all --config /usr/local/etc/sphinx.conf
    echo 'CREATE USER "sphinxd"@"localhost" IDENTIFIED BY "sphinxd";' | mysql -uroot shoppyboom
    echo 'GRANT ALL PRIVILEGES ON *.* TO sphinxd@'localhost' WITH GRANT OPTION;' | mysql -uroot shoppyboom

Деплой Sphinx
-------------

В текущей конфигурации Sphinx использует два индекса: Полный индекс и дельта
индекс. Оба индекса при обновлении полностью перестраиваются, что может занять
достаточно продолжительное время и ресурсы. Для того, что бы не пересчитвать
индекс по все мзаписям можно поийти другим путем - использовать некоторвй дельта
индекс, который будет хранить всебе только те записи, которые были добавлены с
момента последнего обновления инлекса. Это, соответсвенно, займет
непродолжительное время, и не настолько много ресурсов. После обновления дельта
индекса его можно "смерджить" в основным индексом. Это операция намного быстрее
чем полный пересчет всего индекса. У данного подхода есть один недостаток - при
слиянии индексов происходит только добавление новых записей, но не происзодит
изменение старых.