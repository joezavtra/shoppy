Деплой
======

Первоначальная настройка
------------------------

Зависимости системы
~~~~~~~~~~~~~~~~~~~

Необходимо установить все пакеты, перечисленные в файле
shoppy/requirements-centos.txt

Python
~~~~~~

Для начала необходимо устранить системные зависимости. А именно - использовать
одну и ту же версию интерпретатора Python как на машинах разработчиков так и на
сервере.

Например это будет версия 2.7

Установка Python 2.7(текущая версия остается актуальной)

.. code-block:: bash

    wget http://python.org/ftp/python/2.7/Python-2.7.tar.bz2
    tar xf Python-2.7.tar.bz2
    cd Python-2.7
    ./configure --prefix=/usr/local
    make && make altinstall
    wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
    /usr/local/bin/python2.7 ez_setup.py
    /usr/local/bin/easy_install-2.7 pip
    /usr/local/bin/pip-2.7 install virtualenv


NGINX
~~~~~

С сервером все просто - можно использовать просто актуальные данные из
официальных репозиториев

Файл /etc/yum.repos.d/nginx.repo

Файл с конфигурацией находится в shoppy/deploy/yum-nginx.repo

Установка

.. code-block:: bash

    yum install -y nginx
    chkconfig nginx on

Конфигурация NGINX (/etc/nginx/conf.d/default.conf)

Файл с конфигурацией находится в shoppy/deploy/nginx-config.conf. Ноебходимо
либо скопировать либо слинковать файл в /etc/nginx/conf.d/default.conf

Запускаем NGINX

.. code-block:: bash

    service nginx start

Клонируем проект в директорию /var/www/shoppy/

.. code-block:: bash

    git clone ..... /var/www/shoppy
    cd /var/www/shoppy
    git checkout master
    /usr/local/bin/virtualenv --no-site-packages -p /usr/local/bin/python2.7 env
    source env/bin/activate
    pip install -r requirements-pip.txt


Запускаем проект
----------------

.. code-block:: bash

    ./manage.py run_gunicorn

