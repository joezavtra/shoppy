# coding=utf-8
import logging
from django.core.management.base import BaseCommand
from django.utils import timezone
import itertools
import math
import time
import vkontakte
from Core.models import ShopProfile
from Core.utils import split_list
from social_alert.models import MessageText, Message

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

logger = logging.getLogger('app.social_alert')


class Command(BaseCommand):

    def send_message(self, shop_profile, platform, message_text, type_msg, user_ids):
        logger.debug('Send Messages')
        if platform == 'vk':
            logger.debug('Platform: VK')
            app_id = shop_profile.vk_app_id
            app_secret = shop_profile.vk_app_secret
            vk = vkontakte.API(
                api_id=app_id,
                api_secret=app_secret
            )
            logger.debug('app_id: "%s"' % app_id)
            logger.debug('app_secret: "%s"' % app_secret)
            users_count = len(user_ids)
            logger.debug('users: "%d"' % users_count)
            if type_msg == 'sms':
                for uid in user_ids:
                    try:
                        vk.secure.sendSMSNotification(uid=uid, message=message_text)
                    except Exception as e:
                        logger.error('[ERROR] Error on send notifications: "%s"' % str(e))
            else:
                i = 0
                for iter_group in split_list(user_ids, 100):
                    if i == 3:
                        time.sleep(1)
                        i = 0
                    i += 1
                    user_uids = map(str, iter_group)
                    user_uids = ','.join(user_uids)
                    try:
                        logger.debug('Try to send...')
                        vk.secure.sendNotification(
                            user_ids=user_uids,
                            message=message_text
                        )
                        logger.debug('...[OK]')
                    except Exception as e:
                        logger.error('[ERROR] Error on send notifications: "%s"' % str(e))
        elif shop_profile.platform == 'fb':
            pass
        else:
            pass

    def handle(self, *args, **options):
        shop_profiles = ShopProfile.objects.all()
        platforms = Message.objects.all().values_list('platform', flat=True).distinct()
        for shop_profile in shop_profiles:
            for message_text_obj in MessageText.objects.all():
                for platform in platforms:
                    for type_msg in (Message.TYPE_TEXT, Message.TYPE_SMS):
                        messages = Message.objects.filter(
                            text=message_text_obj,
                            shop_profile=shop_profile,
                            platform=platform,
                            date__lte=timezone.now(),
                            type=type_msg
                        )
                        if messages:
                            self.send_message(
                                shop_profile,
                                platform,
                                message_text_obj.text,
                                type_msg,
                                messages.values_list('customer', flat=True)
                            )
                            messages.delete()
