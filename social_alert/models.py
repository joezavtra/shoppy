# coding=utf-8
from django.db import models
from Core.models import ShopProfile


class MessageText(models.Model):
    text = models.TextField()


class Message(models.Model):
    TYPE_SMS = 'sms'
    TYPE_TEXT = 'text'
    TYPE_CHOICES = (
        (TYPE_SMS, u'SMS'),
        (TYPE_TEXT, u'Текст'),
    )
    customer = models.CharField(max_length=100)
    shop_profile = models.ForeignKey(ShopProfile, blank=True, null=True)
    date = models.DateTimeField(db_index=True)
    text = models.ForeignKey(MessageText, db_index=True)
    platform = models.CharField(max_length=10)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES, default=TYPE_TEXT)

    class Meta(object):
        unique_together = ('customer', 'date', 'text')
