# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Account', fields ['platform', 'app_id', 'app_secret']
        db.create_unique(u'social_alert_account', ['platform', 'app_id', 'app_secret'])


    def backwards(self, orm):
        # Removing unique constraint on 'Account', fields ['platform', 'app_id', 'app_secret']
        db.delete_unique(u'social_alert_account', ['platform', 'app_id', 'app_secret'])


    models = {
        u'social_alert.account': {
            'Meta': {'unique_together': "(('platform', 'app_id', 'app_secret'),)", 'object_name': 'Account'},
            'app_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'app_secret': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'social_alert.message': {
            'Meta': {'object_name': 'Message'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['social_alert.Account']", 'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'text': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['social_alert.MessageText']"})
        },
        u'social_alert.messagetext': {
            'Meta': {'object_name': 'MessageText'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['social_alert']