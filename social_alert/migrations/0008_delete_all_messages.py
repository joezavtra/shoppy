# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        orm['social_alert.Message'].objects.all().delete()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'social_alert.account': {
            'Meta': {'unique_together': "(('platform', 'app_id', 'app_secret'),)", 'object_name': 'Account'},
            'app_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'app_secret': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'social_alert.message': {
            'Meta': {'object_name': 'Message'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['social_alert.Account']"}),
            'customer': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['social_alert.MessageText']"})
        },
        u'social_alert.messagetext': {
            'Meta': {'object_name': 'MessageText'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['social_alert']
    symmetrical = True
