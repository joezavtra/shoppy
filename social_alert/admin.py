# -*- coding: utf-8 -*-
from django.contrib import admin
from social_alert.models import *

admin.site.register(Message)

admin.site.register(MessageText)