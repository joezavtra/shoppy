from django.conf import settings
import requests

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


def get_ticket(api_key):
    url = 'http://platform.checkout.ru/service/login/ticket/%s' % api_key
    r = requests.get(url)
    if r.status_code == 200:
        return r.json()['ticket']
    else:
        return False