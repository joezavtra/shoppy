from django.db import models

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class CategoryManager(models.Manager):
    def get_query_set(self):
        return super(CategoryManager, self).get_query_set().annotate(
            products_cnt=models.Count('product')
        )
