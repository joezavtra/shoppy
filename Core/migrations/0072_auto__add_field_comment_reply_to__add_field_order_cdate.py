# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Comment.reply_to'
        db.add_column(u'Core_comment', 'reply_to',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Comment'], null=True, blank=True),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting field 'Comment.reply_to'
        db.delete_column(u'Core_comment', 'reply_to_id')


    models = {
        u'Core.cart': {
            'Meta': {'object_name': 'Cart'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'}),
            'coupon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'carts'", 'null': 'True', 'to': u"orm['Core.Coupon']"}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'carts'", 'to': u"orm['Core.Customer']"}),
            'discount': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'carts'", 'null': 'True', 'to': u"orm['Core.Discount']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mdate': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Core.cartitem': {
            'Meta': {'object_name': 'CartItem'},
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': u"orm['Core.Cart']"}),
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Product']"})
        },
        u'Core.category': {
            'Meta': {'ordering': "['order']", 'unique_together': "(('shop', 'oid'),)", 'object_name': 'Category'},
            'active_products_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'oid': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'subcats'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['Core.Category']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'categories'", 'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'})
        },
        u'Core.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'lng': ('django.db.models.fields.FloatField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'Core.client': {
            'Meta': {'object_name': 'Client'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'clients'", 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.comment': {
            'Meta': {'object_name': 'Comment'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'comments'", 'null': 'True', 'to': u"orm['Core.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['Core.Product']"}),
            'rating': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'reply_to': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Comment']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'Core.coupon': {
            'Meta': {'unique_together': "(('stock', 'key'),)", 'object_name': 'Coupon'},
            'cur_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'max_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'coupons'", 'to': u"orm['Core.Stock']"}),
            'time_from': ('django.db.models.fields.DateTimeField', [], {}),
            'time_to': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'Core.customer': {
            'Meta': {'unique_together': "(('vk_id', 'fb_id'),)", 'object_name': 'Customer'},
            'fb_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'vk_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'Core.deliverypoint': {
            'Meta': {'unique_together': "(('shop', 'oid'),)", 'object_name': 'DeliveryPoint'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.City']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'json_data': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'lng': ('django.db.models.fields.FloatField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'delivery_points'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.discount': {
            'Meta': {'object_name': 'Discount'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Core.favorite': {
            'Meta': {'object_name': 'Favorite'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fav'", 'to': u"orm['Core.Product']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'favs'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.fileimportlog': {
            'Meta': {'object_name': 'FileImportLog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report_text': ('django.db.models.fields.TextField', [], {}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'})
        },
        u'Core.news': {
            'Meta': {'object_name': 'News'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {}),
            'content': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"})
        },
        u'Core.offer': {
            'Meta': {'unique_together': "(('product', 'oid'),)", 'object_name': 'Offer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {'default': "'http://shoppyboom.ru/images/design/main_bg.gif'"}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'params': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'offers'", 'to': u"orm['Core.Product']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'Core.order': {
            'Meta': {'object_name': 'Order'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'unique': 'True', 'to': u"orm['Core.Cart']"}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': u"orm['Core.Customer']"}),
            'deliverypoint': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.DeliveryPoint']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'Core.page': {
            'Meta': {'object_name': 'Page'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pages'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.product': {
            'Meta': {'unique_together': "(('shop', 'model_oid'),)", 'object_name': 'Product'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'products'", 'symmetrical': 'False', 'through': u"orm['Core.ProductCategories']", 'to': u"orm['Core.Category']"}),
            'desc': ('django.db.models.fields.TextField', [], {'db_column': "'description'", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {'default': "'http://shoppyboom.ru/images/design/main_bg.gif'"}),
            'misc': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'model_oid': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'mtime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'Core.productcategories': {
            'Meta': {'object_name': 'ProductCategories', 'db_table': "'Core_product_categories'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Product']"})
        },
        u'Core.shop': {
            'Meta': {'object_name': 'Shop'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'shops'", 'to': u"orm['Core.Client']"}),
            'contacts': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'data_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'delivery': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'}),
            'mode': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'next_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'}),
            'notice': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'processing': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'robokassa_login': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'robokassa_passwd_1': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'robokassa_passwd_2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'user_from': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_notice': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user_reply': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_subject': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'shops'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.shopextra': {
            'Meta': {'unique_together': "(('shop', 'key'),)", 'object_name': 'ShopExtra'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra'", 'to': u"orm['Core.Shop']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'Core.shopprofile': {
            'Meta': {'object_name': 'ShopProfile'},
            'fb_app_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'fb_app_secret': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fb_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'ga_fb': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ga_vk': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'shop': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Core.Shop']", 'unique': 'True', 'primary_key': 'True'}),
            'vk_app_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'vk_app_secret': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'vk_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'Core.shoppyuser': {
            'Meta': {'object_name': 'ShoppyUser'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sys_user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'Core.sphinxupdatecnt': {
            'Meta': {'object_name': 'SphinxUpdateCnt'},
            'max_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'table_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        u'Core.stock': {
            'Meta': {'object_name': 'Stock'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 2, 21, 0, 0)'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'discount': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stocks'", 'to': u"orm['Core.Discount']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stocks'", 'to': u"orm['Core.Shop']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Core']