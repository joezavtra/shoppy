# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Client'
        db.create_table(u'Core_client', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('cdate', self.gf('django.db.models.fields.DateTimeField')()),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'Core', ['Client'])

        # Adding M2M table for field users on 'Client'
        m2m_table_name = db.shorten_name(u'Core_client_users')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('client', models.ForeignKey(orm[u'Core.client'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['client_id', 'user_id'])

        # Adding model 'Shop'
        db.create_table(u'Core_shop', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='shops', to=orm['Core.Client'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('alias', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('cdate', self.gf('django.db.models.fields.DateTimeField')()),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('data_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('delivery', self.gf('django.db.models.fields.TextField')(max_length=10000)),
            ('contacts', self.gf('django.db.models.fields.TextField')(max_length=10000)),
            ('last_fetch', self.gf('django.db.models.fields.DateTimeField')()),
            ('next_fetch', self.gf('django.db.models.fields.DateTimeField')()),
            ('mode', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('notice', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('user_from', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('user_reply', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('user_subject', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('user_notice', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'Core', ['Shop'])

        # Adding M2M table for field users on 'Shop'
        m2m_table_name = db.shorten_name(u'Core_shop_users')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('shop', models.ForeignKey(orm[u'Core.shop'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['shop_id', 'user_id'])

        # Adding model 'News'
        db.create_table(u'Core_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Shop'])),
            ('cdate', self.gf('django.db.models.fields.DateTimeField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('content', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'Core', ['News'])

        # Adding model 'Category'
        db.create_table(u'Core_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Shop'])),
            ('oid', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='subcats', null=True, on_delete=models.SET_NULL, to=orm['Core.Category'])),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'Core', ['Category'])

        # Adding unique constraint on 'Category', fields ['shop', 'oid']
        db.create_unique(u'Core_category', ['shop_id', 'oid'])

        # Adding model 'Product'
        db.create_table(u'Core_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Category'], on_delete=models.DO_NOTHING)),
            ('oid', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')(db_column='description', blank=True)),
            ('misc', self.gf('django.db.models.fields.TextField')(default='')),
            ('images', self.gf('django.db.models.fields.TextField')(default='http://shoppyboom.ru/images/design/main_bg.gif')),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('price', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'Core', ['Product'])

        # Adding model 'Favorite'
        db.create_table(u'Core_favorite', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(related_name='favs', to=orm['Core.Shop'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name='fav', to=orm['Core.Product'])),
        ))
        db.send_create_signal(u'Core', ['Favorite'])

        # Adding model 'Cart'
        db.create_table(u'Core_cart', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vk', self.gf('django.db.models.fields.IntegerField')()),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Shop'])),
            ('cdate', self.gf('django.db.models.fields.DateTimeField')()),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Core', ['Cart'])

        # Adding model 'Order'
        db.create_table(u'Core_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cart', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Cart'])),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Shop'])),
            ('vk', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('comment', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'Core', ['Order'])

        # Adding model 'CartItem'
        db.create_table(u'Core_cartitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cart', self.gf('django.db.models.fields.related.ForeignKey')(related_name='items', to=orm['Core.Cart'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Product'])),
            ('count', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'Core', ['CartItem'])

        # Adding model 'Discount'
        db.create_table(u'Core_discount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Core.Shop'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('type', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'Core', ['Discount'])


    def backwards(self, orm):
        # Removing unique constraint on 'Category', fields ['shop', 'oid']
        db.delete_unique(u'Core_category', ['shop_id', 'oid'])

        # Deleting model 'Client'
        db.delete_table(u'Core_client')

        # Removing M2M table for field users on 'Client'
        db.delete_table(db.shorten_name(u'Core_client_users'))

        # Deleting model 'Shop'
        db.delete_table(u'Core_shop')

        # Removing M2M table for field users on 'Shop'
        db.delete_table(db.shorten_name(u'Core_shop_users'))

        # Deleting model 'News'
        db.delete_table(u'Core_news')

        # Deleting model 'Category'
        db.delete_table(u'Core_category')

        # Deleting model 'Product'
        db.delete_table(u'Core_product')

        # Deleting model 'Favorite'
        db.delete_table(u'Core_favorite')

        # Deleting model 'Cart'
        db.delete_table(u'Core_cart')

        # Deleting model 'Order'
        db.delete_table(u'Core_order')

        # Deleting model 'CartItem'
        db.delete_table(u'Core_cartitem')

        # Deleting model 'Discount'
        db.delete_table(u'Core_discount')


    models = {
        u'Core.cart': {
            'Meta': {'object_name': 'Cart'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'vk': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Core.cartitem': {
            'Meta': {'object_name': 'CartItem'},
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': u"orm['Core.Cart']"}),
            'count': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Product']"})
        },
        u'Core.category': {
            'Meta': {'ordering': "['order']", 'unique_together': "(('shop', 'oid'),)", 'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'subcats'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['Core.Category']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'Core.client': {
            'Meta': {'object_name': 'Client'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'clients'", 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.discount': {
            'Meta': {'object_name': 'Discount'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Core.favorite': {
            'Meta': {'object_name': 'Favorite'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fav'", 'to': u"orm['Core.Product']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'favs'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.news': {
            'Meta': {'object_name': 'News'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {}),
            'content': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"})
        },
        u'Core.order': {
            'Meta': {'object_name': 'Order'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Cart']"}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'vk': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Core.product': {
            'Meta': {'object_name': 'Product'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Category']", 'on_delete': 'models.DO_NOTHING'}),
            'desc': ('django.db.models.fields.TextField', [], {'db_column': "'description'", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {'default': "'http://shoppyboom.ru/images/design/main_bg.gif'"}),
            'misc': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'Core.shop': {
            'Meta': {'object_name': 'Shop'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'shops'", 'to': u"orm['Core.Client']"}),
            'contacts': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'data_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'delivery': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_fetch': ('django.db.models.fields.DateTimeField', [], {}),
            'mode': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'next_fetch': ('django.db.models.fields.DateTimeField', [], {}),
            'notice': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'user_from': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_notice': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user_reply': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_subject': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'shops'", 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Core']