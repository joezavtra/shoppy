# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        for banner in orm['Core.Banner'].objects.all():
            if not banner.slot:
                banner.slot = orm['Core.BannerSlot'].objects.all()[0]
                banner.save()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'Core.apptype': {
            'Meta': {'object_name': 'AppType'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'Core.banner': {
            'Meta': {'object_name': 'Banner'},
            'banner_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'banners'", 'to': u"orm['Core.Shop']"}),
            'slot': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'banners'", 'null': 'True', 'to': u"orm['Core.BannerSlot']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'Core.bannercarousel': {
            'Meta': {'object_name': 'BannerCarousel', '_ormbases': [u'Core.Banner']},
            u'banner_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Core.Banner']", 'unique': 'True', 'primary_key': 'True'}),
            'product_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'banners_carousel'", 'to': u"orm['Core.ProductGroup']"})
        },
        u'Core.bannergraphic': {
            'Meta': {'object_name': 'BannerGraphic', '_ormbases': [u'Core.Banner']},
            u'banner_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Core.Banner']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Core.bannergraphicimage': {
            'Meta': {'object_name': 'BannerGraphicImage'},
            'banner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['Core.BannerGraphic']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'banner_images'", 'to': u"orm['Core.Product']"})
        },
        u'Core.bannerslot': {
            'Meta': {'object_name': 'BannerSlot'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'size_height': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'size_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        },
        u'Core.bannertext': {
            'Meta': {'object_name': 'BannerText', '_ormbases': [u'Core.Banner']},
            u'banner_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Core.Banner']", 'unique': 'True', 'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'banners_text'", 'null': 'True', 'to': u"orm['Core.Product']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Core.callbackorder': {
            'Meta': {'object_name': 'CallbackOrder'},
            'app_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'callback_orders'", 'to': u"orm['Core.AppType']"}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'callback_orders'", 'to': u"orm['Core.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'callback_orders'", 'to': u"orm['Core.Product']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'callback_orders'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.cart': {
            'Meta': {'object_name': 'Cart'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'coupon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'carts'", 'null': 'True', 'to': u"orm['Core.Coupon']"}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'carts'", 'to': u"orm['Core.Customer']"}),
            'discount': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'carts'", 'null': 'True', 'to': u"orm['Core.Discount']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Core.cartitem': {
            'Meta': {'object_name': 'CartItem'},
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': u"orm['Core.Cart']"}),
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'offer': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['Core.Offer']", 'null': 'True', 'blank': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Product']"})
        },
        u'Core.category': {
            'Meta': {'ordering': "['order']", 'unique_together': "(('datasource', 'oid'),)", 'object_name': 'Category'},
            'active_products_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'datasource': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'categories'", 'to': u"orm['Core.DataSource']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lvl': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'mtime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'oid': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'subcats'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['Core.Category']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'})
        },
        u'Core.checkoutplatform': {
            'Meta': {'object_name': 'CheckoutPlatform'},
            'api_key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'checkout_platforms'", 'to': u"orm['Core.Platform']"})
        },
        u'Core.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'lng': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'Core.client': {
            'Meta': {'object_name': 'Client'},
            'balance': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'client_type': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_validated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'l_bank': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'l_bik': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'l_corr_account': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'l_current_account': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'l_doverennost_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'l_doverennost_number': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'l_email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'l_fact_addr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'l_fio_gendir': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'l_inn': ('django.db.models.fields.CharField', [], {'max_length': '12', 'null': 'True', 'blank': 'True'}),
            'l_kpp': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'l_legal_addr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'l_ogrn': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'l_okpo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'l_okved': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'l_org_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'l_permission': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'l_tel': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'clients'", 'null': 'True', 'to': u"orm['Core.Partner']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'clients'", 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.clientpayment': {
            'Meta': {'object_name': 'ClientPayment'},
            'amount': ('django.db.models.fields.FloatField', [], {}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payments'", 'to': u"orm['Core.Client']"}),
            'ctime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'days': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.TextField', [], {'default': "''"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mtime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payments'", 'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2'})
        },
        u'Core.comment': {
            'Meta': {'object_name': 'Comment'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'comments'", 'null': 'True', 'to': u"orm['Core.Customer']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['Core.Product']"}),
            'rating': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'reply_to': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'reply_list'", 'null': 'True', 'blank': 'True', 'to': u"orm['Core.Comment']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['Core.Shop']"}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'Core.coupon': {
            'Meta': {'unique_together': "(('stock', 'key'),)", 'object_name': 'Coupon'},
            'cur_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'max_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'coupons'", 'to': u"orm['Core.Stock']"}),
            'time_from': ('django.db.models.fields.DateTimeField', [], {}),
            'time_to': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'Core.currency': {
            'Meta': {'object_name': 'Currency'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'Core.customer': {
            'Meta': {'object_name': 'Customer'},
            'bdate_d': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'bdate_m': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'bdate_y': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.City']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'email_last_update': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'fb_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ok_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'relation': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sex': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vk_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'db_index': 'True', 'blank': 'True'})
        },
        u'Core.datasource': {
            'Meta': {'object_name': 'DataSource'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Client']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'mode': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u'Default'", 'max_length': '100'}),
            'next_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'processing': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'root_cat_oid': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'yml_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'Core.deliverypoint': {
            'Meta': {'unique_together': "(('shop', 'oid'),)", 'object_name': 'DeliveryPoint'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.City']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'json_data': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'lng': ('django.db.models.fields.FloatField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'delivery_points'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.discount': {
            'Meta': {'object_name': 'Discount'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['Core.Product']", 'null': 'True', 'blank': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'threshold': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'value': ('django.db.models.fields.IntegerField', [], {}),
            'value_type': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        },
        u'Core.emailqueue': {
            'Meta': {'object_name': 'EmailQueue'},
            'bcc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'body': ('django.db.models.fields.TextField', [], {}),
            'email_from': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'email_to': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'message_type': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'reply_to': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'Core.fileimportlog': {
            'Meta': {'object_name': 'FileImportLog'},
            'categories_created': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'categories_deactivated': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'categories_total': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'datasource': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fileimport_logs'", 'to': u"orm['Core.DataSource']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'products_created': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'products_deactivated': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'products_total': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'report_text': ('django.db.models.fields.TextField', [], {}),
            'text_err': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 8, 14, 0, 0)'})
        },
        u'Core.formdescription': {
            'Meta': {'object_name': 'FormDescription'},
            'field_label': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'field_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'help_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'Core.group2product': {
            'Meta': {'unique_together': "(('group', 'product'),)", 'object_name': 'Group2Product', 'db_table': "'Core_productgroup_products'"},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'group2product_rel'", 'db_column': "'productgroup_id'", 'to': u"orm['Core.ProductGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'group2product_rel'", 'db_column': "'product_id'", 'to': u"orm['Core.Product']"})
        },
        u'Core.news': {
            'Meta': {'object_name': 'News'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {}),
            'content': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"})
        },
        u'Core.notification': {
            'Meta': {'object_name': 'Notification'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'notifications'", 'to': u"orm['Core.Shop']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'text'", 'max_length': '10'})
        },
        u'Core.offer': {
            'Meta': {'object_name': 'Offer'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {'default': "'http://shoppyboom.ru/images/design/main_bg.gif'"}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'params': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'offers'", 'to': u"orm['Core.Product']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'Core.order': {
            'Meta': {'object_name': 'Order'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'app_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'orders'", 'null': 'True', 'to': u"orm['Core.AppType']"}),
            'approved_comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'unique': 'True', 'to': u"orm['Core.Cart']"}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': u"orm['Core.Customer']"}),
            'deliverypoint': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.DeliveryPoint']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'ext_order_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        },
        u'Core.orderhistory': {
            'Meta': {'object_name': 'OrderHistory'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new_status': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'history'", 'to': u"orm['Core.Order']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'Core.page': {
            'Meta': {'object_name': 'Page'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pages'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.partner': {
            'Meta': {'object_name': 'Partner'},
            'benefits': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'Core.platform': {
            'Meta': {'object_name': 'Platform'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'Core.product': {
            'Meta': {'object_name': 'Product'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'products'", 'symmetrical': 'False', 'through': u"orm['Core.ProductCategories']", 'to': u"orm['Core.Category']"}),
            'datasource': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['Core.DataSource']"}),
            'desc': ('django.db.models.fields.TextField', [], {'db_column': "'description'", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.TextField', [], {'default': "'http://shoppyboom.ru/images/design/main_bg.gif'"}),
            'manual_desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'misc': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'model_oid': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'mtime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'Core.productcategories': {
            'Meta': {'object_name': 'ProductCategories', 'db_table': "'Core_product_categories'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Product']"})
        },
        u'Core.productgroup': {
            'Meta': {'object_name': 'ProductGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'groups'", 'symmetrical': 'False', 'to': u"orm['Core.Product']"}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product_groups'", 'to': u"orm['Core.Shop']"}),
            'show_in_cart': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'use_for_slider': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'})
        },
        u'Core.robokassaplatform': {
            'Meta': {'object_name': 'RobokassaPlatform'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password1': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'password2': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'robokassa_platforms'", 'to': u"orm['Core.Platform']"})
        },
        u'Core.role': {
            'Meta': {'object_name': 'Role'},
            'alias': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'Core.shop': {
            'Meta': {'object_name': 'Shop'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'catalog': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['Core.ShopCatalog']", 'null': 'True', 'blank': 'True'}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'shops'", 'to': u"orm['Core.Client']"}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Currency']"}),
            'datasources': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'shops'", 'symmetrical': 'False', 'to': u"orm['Core.DataSource']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'email_banner_frequency': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'hello_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'paid_till': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tariff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Tariff']", 'null': 'True', 'blank': 'True'}),
            'user_from': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_reply': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_sign': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user_subject': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'shops'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.shopcatalog': {
            'Meta': {'object_name': 'ShopCatalog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'themes'", 'null': 'True', 'to': u"orm['Core.ShopCatalog']"})
        },
        u'Core.shopextra': {
            'Meta': {'unique_together': "(('shop', 'key'),)", 'object_name': 'ShopExtra'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra'", 'to': u"orm['Core.Shop']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'Core.shopplatform': {
            'Meta': {'object_name': 'ShopPlatform'},
            'checkout': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'shop_platform'", 'unique': 'True', 'null': 'True', 'to': u"orm['Core.CheckoutPlatform']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'robokassa': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'shop_platform'", 'unique': 'True', 'null': 'True', 'to': u"orm['Core.RobokassaPlatform']"}),
            'shop': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'platforms'", 'unique': 'True', 'to': u"orm['Core.Shop']"})
        },
        u'Core.shopprofile': {
            'Meta': {'object_name': 'ShopProfile'},
            'fb_app_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'fb_app_secret': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fb_groupid': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fb_taburl': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'fb_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'ga_fb': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ga_ok': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ga_vk': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'ok_app_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'ok_app_public': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ok_app_secret': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ok_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'shop': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Core.Shop']", 'unique': 'True', 'primary_key': 'True'}),
            'vk_app_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'vk_app_secret': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'vk_groupid': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'vk_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'Core.shoppyuser': {
            'Meta': {'object_name': 'ShoppyUser'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'recovery_token': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'recovery_token_expired': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'roles': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'users'", 'symmetrical': 'False', 'to': u"orm['Core.Role']"}),
            'sys_user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'shoppyuser'", 'unique': 'True', 'to': u"orm['auth.User']"}),
            'tel': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'default': "'d9532a17-61c1-40d5-8573-3ede0f50c3c2'", 'unique': 'True', 'max_length': '100'}),
            'token_created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'Core.sphinxupdatecnt': {
            'Meta': {'object_name': 'SphinxUpdateCnt'},
            'max_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'table_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        u'Core.stock': {
            'Meta': {'object_name': 'Stock'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'discount': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stocks'", 'to': u"orm['Core.Discount']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stocks'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.tariff': {
            'Meta': {'object_name': 'Tariff'},
            'cost': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'fb_enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'has_whitelabel': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_archive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_products': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ok_enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'update_period': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10'}),
            'vk_enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Core']
    symmetrical = True
