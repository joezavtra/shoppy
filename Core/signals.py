from Core.models import Product

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'
from django.db.models.signals import pre_save
from django.dispatch import receiver


@receiver(pre_save, sender=Product)
def add_score(instance, **kwargs):
    pass
    # profile = instance.user_profile
    # profile.score += 1
    # profile.save()