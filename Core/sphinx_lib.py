# coding=utf-8
__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

from sphinxit.core.helpers import BaseSearchConfig


class SphinxItConfig(BaseSearchConfig):
    """Конфигурация для Sphinx Search Engine"""

    DEBUG = False
    WITH_META = False
    WITH_STATUS = False
    POOL_SIZE = 5
    SEARCHD_CONNECTION = {
        'host': '127.0.0.1',
        'port': 9306,
    }