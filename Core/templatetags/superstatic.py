import os
import cStringIO
from django.conf import settings
import sassc
from django import template
from hashlib import md5

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


register = template.Library()

@register.simple_tag
def sass(*files, **options):
    css = options.get('css', False)
    result = cStringIO.StringIO()

    for sass_file in files:
        for static_dir in settings.STATICFILES_DIRS:
            abs_file = os.path.join(static_dir, sass_file)
            if os.path.isfile(abs_file):
                result.write(sassc.compile(filename=str(abs_file)).replace("\n", ''))
        result.write(' ')

    if css:
        return result.getvalue()

    md5_of_files = md5(''.join(files)).hexdigest()
    res_file = '_s/css/%s.css' % str(md5_of_files)
    with open(os.path.join(settings.MEDIA_ROOT, res_file), 'wb') as f:
        f.write(result.getvalue())
    return os.path.join(settings.MEDIA_URL, res_file)
