# coding=utf-8
from collections import defaultdict
import logging
import random
import string
import urllib
import urlparse
import base64
import hashlib
import hmac
import json

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


logger = logging.getLogger('fileimport')


def fast_iter(context, func, kwargs, max_elements=None):
    if not max_elements:
        max_elements = -1
    for i, (event, elem) in enumerate(context, start=1):
        if i == max_elements:
            break
        try:
            func(elem, **kwargs)
        except Exception as e:
            logger.error('Error: "%s"' % e.message)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    del context


def update_get_params(url, params):
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urllib.urlencode(query)
    return urlparse.urlunparse(url_parts)


def abs_url(url, base_host):
    url_parts = list(urlparse.urlparse(url))
    if not url_parts[0]:
        url_parts[0] = 'http'
    if not url_parts[1]:
        url_parts[1] = base_host

    url = urlparse.urlunparse(url_parts)
    return url


def split_list(some_list, chunk_len):
    """
    Разбивает список some_list на чанки длинной chunk_len
    """
    for item in xrange(0, len(some_list), chunk_len):
        yield some_list[item:item + chunk_len]


def random_password(length=6):
    res = ''
    symbols = string.ascii_lowercase + string.digits
    for i in xrange(length):
        res += random.choice(symbols)
    return res


def get_similar_items(collection_list):
    """
    Возвращает список (key, value) на основе переданного списка списков
    (key, value).
    Результат будет состоять из элементов (key, value), (key, value) которых
    находится в каждом списке
    :param collection_list:
    :return:
    """
    counter = defaultdict(int)
    res = list()
    collections_cnt = len(collection_list)
    for collection in collection_list:
        collection = set(collection)
        for k, v in collection:
            counter[str((k, v))] += 1
            if counter[str((k, v))] == collections_cnt:
                res.append((k, v))
    return res


def clean_str(some_str, allowed_chars):
    res = []
    for char in some_str:
        if char in allowed_chars:
            res.append(char)
    return type(some_str)().join(res)