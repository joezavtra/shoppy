# -*- coding: utf-8 -*-
import Core
import json
from datetime import datetime
from tastypie import http
from itertools import imap
import traceback
import urllib

from django.conf import settings
from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist

import tastypie.constants
from django.core.urlresolvers import NoReverseMatch
from django.db.models import Count
from sphinxit.core.processor import Search
from tastypie.authorization import DjangoAuthorization, Authorization
from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.bundle import Bundle
from tastypie.exceptions import NotFound
from tastypie.resources import Resource
from tastypie.cache import SimpleCache
from tastypie.utils import dict_strip_unicode_keys

from Core.checkout import get_ticket

from Core.models import *
from Core.sphinx_lib import SphinxItConfig
from django.core.mail import send_mail
from app_checkout.models import CheckoutOrder
from robokassa.api_robokassa import Robokassa


class ClientResource(ModelResource):
    class Meta(object):
        resource_name = 'client'
        queryset = Client.objects.all()
        allowed_methods = ['get']


class ShopResource(ModelResource):
    client = fields.ForeignKey(ClientResource, 'client')

    class Meta(object):
        resource_name = 'shop'
        queryset = Shop.objects.all()
        allowed_methods = ['get']


class NewsResource(ModelResource):
    shop = fields.ForeignKey(ShopResource, 'shop')

    class Meta(object):
        resource_name = 'news'
        filtering = {}
        queryset = News.objects.all()
        allowed_methods = ['get']

    def get_object_list(self, request):
        return super(NewsResource, self).get_object_list(request).filter(
            shop=request.shop)


class CategoryResource(ModelResource):
    datasource = fields.IntegerField('datasource_id')
    subcats = fields.ListField()
    children = fields.ListField()
    parent = fields.IntegerField('parent_id', null=True)
    products_count = fields.IntegerField(
        'active_products_count',
        null=True,
        blank=True
    )
    level = fields.IntegerField('mptt_level')

    class Meta(object):
        resource_name = 'category'
        filtering = {}
        limit = 0
        max_limit = 0
        queryset = Category.objects.filter(
            status=True,
            active_products_count__gt=0
        ).order_by('datasource', 'order')
        allowed_methods = ['get']
        excludes = ['mtime','mptt_level','lft','rght','status','tree_id']
        cache = SimpleCache()

    def get_object_list(self, request, data=None):
        return super(CategoryResource, self).get_object_list(request).filter(
            datasource__in=request.shop.datasources.all())

    def get_list(self, request=None, **kwargs):
        cache_key = request.shop.get_category_cache_key()
        objects = self._meta.cache.get(cache_key)

        if objects is None:
            objects = super(CategoryResource, self).get_list(request,**kwargs)
            self._meta.cache.set(cache_key, objects)

        return objects

    def alter_list_data_to_serialize(self, request, to_be_serialized):
        cats = dict( imap( lambda c: (c.data['id'], c), to_be_serialized['objects']) )
        oids = {}

        # fill subcats
        for i,c in cats.items():
            c.data['level'] += 1
            if c.data['oid']:
                if c.data['oid'] in oids:
                    cats[oids[c.data['oid']]].data['products_count'] += c.data['products_count']
                    del cats[i]
                    continue
            else:
                oids[c.data['oid']] = i

            if c.data['parent']:
                try:
                    #cats[c.data['parent']].data['subcats'].append(c)
                    cats[c.data['parent']].data['children'].append(c.data['id'])
                except:
                    if c.data['parent'] in cats:
                        #cats[c.data['parent']].data['subcats'] = [c]
                        cats[c.data['parent']].data['children'] = [c.data['id']]
            else:
                pass

        ### sort subcats
        for i in cats:
            if cats[i].data['children']:
                cats[i].data['children'].sort(cmp=lambda a,b: cmp(cats[a].data['order'],cats[b].data['order']))

        to_be_serialized['objects'] = sorted(cats.values(), cmp=lambda a,b: cmp(a.data['order'],b.data['order']))

        return to_be_serialized


class OfferResource(ModelResource):
    class Meta(object):
        resource_name = 'offer'
        queryset = Offer.objects.all()
        allowed_methods = ['get']


class PageResource(ModelResource):
    class Meta(object):
        model = Page
        queryset = Page.objects.all()
        resource_name = 'page'

    def get_object_list(self, request):
        return super(PageResource, self).get_object_list(request).filter(
            shop=request.shop)


class ProductResourceMixin(object):
    def dehydrate_desc(self, bundle):
        if bundle.obj.manual_desc:
            return bundle.obj.manual_desc
        return bundle.obj.desc

    def dehydrate_cover(self, bundle):
        return bundle.obj.cover()

    def dehydrate_images(self, bundle):
        return bundle.obj.image_list()


class ProductResource(ModelResource, ProductResourceMixin):
    category = fields.ToManyField(CategoryResource, 'categories')
    offers = fields.ToManyField(OfferResource, 'offers', full=True)
    comments = fields.ToManyField(
        'Core.api.resources.CommentResource',
        'comments',
        full=True
    )
    cover = fields.CharField(null=True, readonly=True)

    class Meta(object):
        resource_name = 'product'
        filtering = {
            'category': ('exact', ),
        }
        excludes = ('manual_desc', )
        queryset = Product.objects.filter(status=True).prefetch_related('comments', 'categories', 'offers').order_by('-mtime')
        allowed_methods = ['get']
        ordering = ['name', 'price', 'mtime']

    def apply_filters(self, request, applicable_filters):
        if 'categories__exact' in applicable_filters:
            try:
                c = Category.objects.get(pk=int(applicable_filters['categories__exact']))
                applicable_filters.pop('categories__exact', None)
                applicable_filters['categories__id__in'] = [id for id in c.get_nesting_id_list()]
                # убираем мердж источников на лету
                #applicable_filters['datasource__id__in'] = [ds.id for ds in request.shop.datasources.all()]
                #if c.oid:
                #    cats = Category.objects.filter(datasource__id__in=request.shop.datasources.all(),oid=c.oid).exclude(pk=c.id)
                #    for cat in cats:
                #        applicable_filters['categories__id__in'] += cat.get_nesting_id_list()

            except:
                pass
        return self.get_object_list(request).filter(**applicable_filters)

    def get_object_list(self, request, data=None):
        return super(ProductResource, self).get_object_list(request).filter(
            datasource__in=[ds for ds in request.shop.datasources.all()])

    def dehydrate_offers(self, bundle):
        o_r = OfferResource()
        map_fun = lambda offer: o_r.full_dehydrate(o_r.build_bundle(obj=offer))
        offers = bundle.obj.offers.filter(available=True, status=True)
        return map(map_fun, offers)

    def dehydrate(self, bundle):
        try:
            for i, comment in enumerate(bundle.data['comments'][:]):
                if comment.obj.reply_to is not None:
                    del bundle.data['comments'][i]
        except:
            pass
        bundle.data['comments'] = sorted(
            bundle.data['comments'],
            key=lambda x: x.data['created'],
            reverse=True
        )
        bundle.data['variants'] = []
        variants = {}
        for o in bundle.data['offers']:
            params = json.loads(o.data['params'])
            for k in params:
                try:
                    variants[k]['items'][params[k]] = o.data['id']
                except:
                    variants[k] = {
                        'name': k,
                        'title': k,
                        'items': {
                            params[k]: o.data['id']
                        }
                    }
        exclude_misc = []
        for v, vv in variants.items():
            if len(vv['items']) > 1:
                tmp = {
                    'name': v,
                    'title': v,
                    'items': []
                }
                for i, vvv in vv['items'].items():
                    tmp['items'].append({'oid': vvv, 'value': i})
                    exclude_misc.append(v)
                bundle.data['variants'].append(tmp)
        try:
            misc = json.loads(bundle.data['misc'])
        except:
            misc = {}
        for k in exclude_misc:
            misc.pop(k, None)

        bundle.data['misc'] = json.dumps(misc)
        return bundle


class SlotProductsResource(ModelResource, ProductResourceMixin):
    """
    Use cases:
        "slot-products/?slot=slider" - список товаров, которые должны быть
        отображены в блоке списка товаров
        "slot-products/?slot=cart" - список товаров, которые должны быть
        отбражены в корзине
        "slot-products/" - алиас для "slot-products/?slot=slider"
    """
    cover = fields.CharField(blank=True, null=True, readonly=True)

    class Meta(object):
        resource_name = 'slot-products'
        queryset = Product.objects.none()
        excludes = (
            'manual_desc', 'misc', 'model_oid', 'oid', 'status', 'url', 'mtime')
        allowed_methods = ['get']

    def get_object_list(self, request):
        slot = request.GET.get('slot', 'slider')
        if slot == 'slider':
            prod_groups = ProductGroup.objects.filter(
                shop=request.shop, use_for_slider=True).select_related(
                    'product', 'shop')
        elif slot == 'cart':
            prod_groups = ProductGroup.objects.filter(
                shop=request.shop, show_in_cart=True).select_related(
                    'product', 'shop')
        else:
            prod_groups = ProductGroup.objects.none()

        products = Product.objects.none()
        if prod_groups:
            products = prod_groups[0].products.filter(status=True)

        customer_obj = request.app_customer['customer']
        carts = Cart.objects.filter(
            customer=customer_obj, shop=request.shop, status=False)

        if carts:
            cart = carts[0]
            products = products.exclude(id__in=[i.product_id for i in cart.items.all()])

        return products


class CartResource(ModelResource):
    shop = fields.ForeignKey(ShopResource, 'shop')
    items = fields.ToManyField('Core.api.resources.CartItemResource', 'items',
        full=True)
    id = fields.IntegerField('id')
    total_sum = fields.IntegerField(default=0)

    class Meta(object):
        resource_name = 'cart'
        filtering = {}
        limit = 0
        queryset = Cart.objects.filter(status=False)
        allowed_methods = ['get', 'put']
        always_return_data = True

    def get_object_list(self, request):
        customer_obj = request.app_customer['customer']
        carts = Cart.objects.filter(
            customer=customer_obj,
            shop=request.shop,
            status=False
        )

        if carts:
            #if carts.count() > 1:
            #    carts = carts.filter(pk=carts[0].pk)
            pass
        else:
            c = Cart.objects.create(
                customer=customer_obj,
                shop=request.shop,
                cdate=timezone.now()
            )
            carts = Cart.objects.filter(pk=c.pk)

        cart = carts[0]
        cart.set_max_available_discount()

        if cart.discount:
            discount = cart.discount
            Core.analytics.track(
                cart.customer.id,
                "Has discount for cart", {
                    "shop_domain": cart.shop.domain,
                    "shop_id": cart.shop.id,
                    "cart_id": cart.id,
                    "cart_total": cart.total(),
                    "cart_price_with_discount": cart.price_with_discount(),
                    "discount_id": discount.id,
                    "discount_name": discount.name,
                    "discount_application": discount.application,
                    "discount_value": discount.value,
                    "discount_value_type": discount.value_type,
                    "discount_min_cart_price": discount.min_cart_price,
                    "discount_coupon_key": discount.coupon_key
                }
            )


        return carts


    def obj_update(self, bundle, **kwargs):
        cart = super(CartResource, self).obj_get(bundle, **kwargs)

        coupon = bundle.data['coupon']

        if not coupon:
            return bundle

        # Если купон...
        try:
            discount = Discount.objects.get(
                Discount.active_only_filter(),
                coupon_key=coupon,
                shop=cart.shop
            )

            price_before_coupon = cart.price_with_discount()
            price_after_coupon = cart.price_with_discount(discount)

            # compare current price with coupon price
            if (price_before_coupon > price_after_coupon):

                Core.analytics.track(
                    cart.customer.id,
                    "Entered valid coupon", {
                        "cart_id": cart.id,
                        "shop_domain": cart.shop.domain,
                        "coupon_key": coupon,
                        "price_before_coupon": price_before_coupon,
                        "price_after_coupon": price_after_coupon,
                        "coupon_discount_id": discount.id
                    }
                )
                cart.discount = discount
                cart.save()
        except ObjectDoesNotExist:
            Core.analytics.track(
                cart.customer.id,
                "Entered invalid coupon", {
                    "cart_id": cart.id,
                    "shop_domain": cart.shop.domain,
                    "coupon_key": coupon
            })

        return bundle

    def dehydrate(self, bundle):
        bundle.data['total_sum'] = round(bundle.obj.total(), 2)
        cart = bundle.obj

        if cart.discount:
            discount = cart.discount
            bundle.data['discount'] = {
                'name': discount.name,
                'value': discount.get_value(),
                'price': cart.price_with_discount()
            }

        bundle.data['order_id'] = None
        orders = Order.objects.filter(cart=bundle.obj)
        if orders:
            order = orders[0]
            bundle.data['order_id'] = order.pk
        return bundle


class CartItemResource(ModelResource):
    cart = fields.ForeignKey(CartResource, 'cart')
    product = fields.ForeignKey(ProductResource, 'product', full=True)
    offer = fields.ForeignKey(OfferResource, 'offer', full=True, null=True)
    count = fields.IntegerField('count')
    product_id = fields.IntegerField('product_id')

    class Meta(object):
        resource_name = 'cartitem'
        filtering = {
            'user': ('exact', ),
        }
        queryset = CartItem.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete', 'patch']
        # authorization = Authorization()

    def obj_create(self, bundle, **kwargs):
        cart = Cart.objects.get(pk=bundle.data['cart_id'])
        product = Product.objects.get(pk=bundle.data['product_id'])
        try:
            offer = product.offers.get(pk=bundle.data['original_id'])
        except:
            offer = None

        try:
            ci = CartItem.objects.get(cart=cart, product=product, offer=offer)
            ci.count += 1
            ci.save()
        except:
            ci = CartItem(cart=cart, product=product, offer=offer, count=1)
            ci.save()
        return

    def obj_update(self, bundle, **kwargs):
        try:
            ci = CartItem.objects.get(pk=kwargs['pk'])
            if bundle.data['count'] > 0:
                ci.count = bundle.data['count']
                ci.save()
            else:
                self.obj_delete(pk=kwargs['pk'])
        except:
            pass
        return

    def obj_delete(self, request=None, **kwargs):
        item = CartItem.objects.get(pk=kwargs['pk'])
        discount = item.cart.discount
        if discount:
            item.cart.set_max_available_discount()
        item.delete()

    def dehydrate(self, bundle):
        bundle.data['price_with_discount'] = bundle.obj.price_with_discount()
        if bundle.data['offer'] is not None:
            offer_bundle = bundle.data['offer']
            offer_bundle.data['size'] = offer_bundle.obj.get_size()
        return bundle


class CheckoutResource(ModelResource):
    class Meta(object):
        resource_name = 'checkout_order'
        queryset = CheckoutOrder.objects.all()
        allowed_methods = ['get']
        authorization = Authorization()
        always_return_data = True


class UserResource(ModelResource):
    class Meta(object):
        model = User
        queryset = User.objects.all()


class OrderHistoryResource(ModelResource):
    order = fields.ForeignKey('Core.api.resources.OrderResource', 'order')
    author = fields.ForeignKey(
        UserResource, 'author', full=True, null=True, blank=True)

    class Meta(object):
        resource_name = 'order_history'
        model = OrderHistory
        queryset = OrderHistory.objects.all()

def send_biggon(tel,fio,addr,site,timezone,ip,offer,cnt=1):
    try:
        baseurl = 'https://api.biggon.net/v3/orders/submit.json?'
        data = dict(
            tel=tel,
            fio=fio,
            addr=addr,
            site=site,
            timezone=timezone,
            ip=ip,
            offer=offer,
            cnt=1
        )
        url = baseurl + urllib.urlencode(data)
        r = requests.get(url)

        logger.info('Call url with GET: "%s"' % (
                    url))

        if r.status_code != 200:
            body = 'Order for cart: %d' % order.cart_id
            body += "\nTraceback:\n"
            body += r.content
            EmailQueue.objects.create(
                email_from='system@shoppyboom.ru',
                email_to='dev@shoppyboom.ru',
                subject=u'Ошибка при выполнении hook`а BIGGON',
                body=body
            )
        else:
            res = r.json()
            if 'fail' in res and res['fail'] == True:
                logger.warning(
                    'Bad result for call BIGGON hook. '
                    'BIGGON response "%s"' % (res['message'])
                )
    except Exception as e:
        logger.error('Ошибка при выполнении hook`а BIGGON: "%s"' % str(e))


class OrderResource(ModelResource):
    cart = fields.ForeignKey(CartResource, 'cart', full=True)
    shop = fields.ForeignKey(ShopResource, 'shop')
    customer = fields.ForeignKey(
        'Core.api.resources.CustomerResource',
        'customer'
    )
    checkouts = fields.ToManyField(CheckoutResource, 'checkouts', full=True)
    history = fields.ToManyField(OrderHistoryResource, 'history', full=True,
        use_in='detail')

    class Meta(object):
        resource_name = 'order'
        filtering = {'customer': 'exact'}
        queryset = Order.objects.all().select_related('cart', 'shop').prefetch_related('shop__platforms', 'cart__items')
        allowed_methods = ['get', 'post', 'put', 'delete', 'patch']
        authorization = Authorization()
        always_return_data = True

    def get_object_list(self, request, data=None):
        return super(OrderResource, self).get_object_list(request).filter(
            shop=request.shop)

    def alter_list_data_to_serialize(self, r, data):
        data['meta']['status_choices'] = dict(Order.STATUS_CHOICES)
        return data

    def obj_create(self, bundle, request=None, **kwargs):
        app_type = bundle.data.get('app_type', None)
        name = bundle.data['name']
        phone = bundle.data['phone']
        email = bundle.data['email']
        address = bundle.data.get('address', '')
        comment = bundle.data['comment']
        delivery = bundle.data.get('deliveryPoint', 0)
        cart_id = bundle.data['cart']

        cart = Cart.objects.get(pk=cart_id)

        track_dict = {
            "cart_id": cart.id,
            "shop_id": cart.shop.id,
            "shop_domain": cart.shop.domain,
            "cart_total": cart.total()
        }

        try:
            dp_obj = DeliveryPoint.objects.get(pk=delivery)
        except ObjectDoesNotExist:
            dp_obj = None

        if not cart.discount:
            cart.set_max_available_discount()

        if cart.discount:
            discount = cart.discount
            track_dict.update({
                "discount_id": discount.id,
                "discount_name": discount.name,
                "discount_application": discount.application,
                "discount_value": discount.value,
                "discount_value_type": discount.value_type,
                "discount_min_cart_price": discount.min_cart_price,
                "discount_coupon_key": discount.coupon_key,
                "cart_price_with_discount": cart.price_with_discount(),
            })

        if cart.discount and cart.discount.coupon_key:
            cart.discount.cur_activation_count += 1
            cart.discount.save()

        ##############################################
        ####  Завершение VK offer                 ####
        ####  https://vk.com/dev.php?method=leads ####
        ##############################################
        VKOffer.complete(bundle.request)


        cart.status = True
        cart.save()

        order = Order(
            app_type=AppType.objects.get(code=app_type),
            cart=cart,
            shop=cart.shop,
            customer=cart.customer,
            name=name,
            phone=phone,
            email=email,
            address=address,
            comment=comment,
            deliverypoint=dp_obj
        )
        order.save()
        OrderHistory.objects.create(
            order=order, new_status=order.status, author=None)
        platforms = cart.shop.get_platforms()
        if not platforms['checkout']:
            order.send_notice()

        if order.shop.client_id in (8, 225):
            try:
                url = 'http://www.e5.ru/order/quickOrder/'
                promocode = ''
                if order.cart.discount:
                    promocode = order.cart.discount.coupon_key

                products = dict()
                for ci in order.cart.items.all():
                    products[str(ci.product.oid)] = ci.count
                data = dict(
                    phone=order.phone,
                    name=order.name,
                    email=order.email,
                    shopId=order.deliverypoint.oid if order.deliverypoint else '',
                    comment=order.comment,
                    promocode=promocode,
                    source=82,
                    products=products
                )
                data_json = json.dumps(data)
                logger.info('Call "%s" with POST: "%s"' % (
                            url, data_json))
                r = requests.post(url, data=data_json)
                if r.status_code != 200:
                    body = 'Order for cart: %d' % order.cart_id
                    body += "\nTraceback:\n"
                    body += r.content
                    EmailQueue.objects.create(
                        email_from='system@shoppyboom.ru',
                        email_to='dev@shoppyboom.ru',
                        subject=u'Ошибка при выполнении hook`а',
                        body=body
                    )
                else:
                    res = r.json()
                    if 'error' in res:
                        logger.warning(
                            'Bad result for call e5 hook. '
                            'E5 response "%s"' % (str(res))
                        )
                    else:
                        order.ext_order_id = res['orderNumber']
                        order.save()
            except Exception as e:
                logger.error('Error on call e5 hook: "%s"' % str(e))

        bundle.obj = order

        biggon_site = ShopExtra().get_param(order.shop.pk, 'BIGGON_SITE')
        if biggon_site:
            for i in order.cart.items.all():
                send_biggon(
                    order.phone,
                    order.name,
                    order.address,
                    biggon_site,
                    bundle.request.COOKIES.get('SBTZO'),
                    bundle.request.META['HTTP_X_REAL_IP'],
                    i.product.oid,
                    i.count,
                )

        Core.analytics.track(cart.customer.id, 'Made an order', track_dict)

        return bundle

    def dehydrate(self, bundle):
        platforms = bundle.obj.shop.get_platforms()
        if platforms['checkout']:
            bundle.data['checkout_ticket'] = get_ticket(platforms['checkout'].api_key)
        else:
            bundle.data['checkout_ticket'] = None

        total_sum = bundle.obj.cart.total()
        if bundle.obj.cart.discount:
            discount_summ = bundle.obj.cart.discount.apply(total_sum)
            if discount_summ != total_sum:
                total_sum = discount_summ
        if bundle.data['checkouts']:
            total_sum += bundle.data['checkouts'][0].data['deliveryCost']
        if platforms['robokassa']:
            bundle.data['robokassa_url'] = Robokassa(
                m_login=platforms['robokassa'].login,
                m_pass1=platforms['robokassa'].password1,
                m_pass2=platforms['robokassa'].password2,
                order_id=bundle.obj.pk,
                order_sum=total_sum
            ).get_link_to_payment()
        else:
            bundle.data['robokassa_url'] = None

        next_statuses = bundle.obj.get_next_statuses(values_only=True) or None
        bundle.data['next_statuses'] = next_statuses
        return bundle


class FastOrderResource(ModelResource):
    class Meta(object):
        resource_name = 'fast-order'
        model = Order
        queryset = Order.objects.all()
        allowed_methods = ['post']
        authorization = Authorization()
        always_return_data = True

    def obj_create(self, bundle, **kwargs):
        pid = bundle.data.get('product_id', None)
        oid = bundle.data.get('original_id', None)
        app_type = bundle.data.get('app_type', None)
        name = bundle.data['name']
        phone = bundle.data['phone']
        email = bundle.data['email']
        address = bundle.data.get('address', '')
        comment = bundle.data['comment']

        product = Product.objects.get(pk=pid)
        offer = product.offers.filter(oid=oid)[0]

        if product:
            cart = Cart.objects.create(
                customer=bundle.request.app_customer['customer'],
                shop=bundle.request.shop,
                status=True
            )
            cartitem = CartItem.objects.create(
                cart=cart,
                product=product,
                offer=offer,
            )
            order = Order.objects.create(
                app_type=AppType.objects.get(code=app_type),
                cart=cart,
                shop=cart.shop,
                customer=cart.customer,
                name=name,
                phone=phone,
                email=email,
                address=address,
                comment=comment
            )
            order.send_notice()

            biggon_site = ShopExtra().get_param(order.shop.pk, 'BIGGON_SITE')
            if biggon_site:
                send_biggon(order.phone,order.name,order.address,biggon_site,bundle.request.COOKIES.get('SBTZO'),bundle.request.META['HTTP_X_REAL_IP'],offer.oid)

            bundle.obj = order

        return bundle


class SearchProductResourceRow(object):
    id = None
    name = ''
    datasource_id = None


class SearchProductResource(Resource):
    id = fields.IntegerField(attribute='id')
    name = fields.CharField(attribute='name')

    class Meta(object):
        resource_name = 'search/product'
        map_class = SearchProductResourceRow
        allowed_methods = ['get']
        #authentication = Authentication()
        #authorization = Authorization()

    def _mapper(self, data):
        c = self._meta.map_class()
        c.id = data['id']
        c.name = data['name']
        return c

    def get_resource_uri(self, bundle_or_obj=None, url_name='api_dispatch_list'):
        kwargs = {
            'resource_name': self._meta.resource_name,
        }
        if bundle_or_obj is not None:
            if isinstance(bundle_or_obj, Bundle):
                kwargs['pk'] = bundle_or_obj.obj.id
            else:
                kwargs['pk'] = bundle_or_obj.id

        if self._meta.api_name is not None:
            kwargs['api_name'] = self._meta.api_name
        try:
            return self._build_reverse_url('api_dispatch_detail', kwargs=kwargs)
        except NoReverseMatch:
            return ''

    @staticmethod
    def search(data):
        return (
            Search(indexes=settings.SPHINX_INDEXES_MIN, config=SphinxItConfig)
            .match(data['query'] + '*')
            .limit(0, 10)
            .select('id')
            .filter(shop_id__eq=data['shop_id'])
            .options(field_weights={'name': 100, 'description': 80})
            .order_by('name', 'description')
        )

    @staticmethod
    def get_query_filter(bundle):
        return dict(
            query=bundle.request.GET.get('query', None),
            shop_id=bundle.request.shop.pk
        )

    def get_object_list(self, request, data=None):
        if data is not None:
            p_ids = map(
                lambda x: x['id'], self.search(data).ask()['result']['items'])
            products_qs = Product.objects.filter(id__in=p_ids, status=True)
            return map(self._mapper, products_qs.values('id', 'name'))
        else:
            return []

    def obj_get_list(self, request=None, **kwargs):
        data = self.get_query_filter(kwargs['bundle'])
        return self.get_object_list(request, data=data)

    def obj_get(self, request=None, **kwargs):
        pk = int(kwargs['pk'])
        data = self.get_query_filter(kwargs['bundle'])
        search = self.search(data)
        try:
            return map(self._mapper, search.filter(id__eq=pk).limit(0, 1).ask()['result']['items'])[0]
        except KeyError:
            raise NotFound('Object not found')


class SearchProductFullResource(ModelResource):
    category = fields.ToManyField(CategoryResource, 'categories')
    offers = fields.ToManyField(OfferResource, 'offers')
    images = fields.ListField('image_list')
    comments = fields.ToManyField(
        'Core.api.resources.CommentResource',
        'comments',
        full=True
    )

    class Meta(object):
        resource_name = 'search/product-full'
        filtering = {
            'category': ('exact', ),
            'name': ('search', ),
        }
        queryset = Product.objects.filter(status=True)
        allowed_methods = ['get']
        ordering = ['name', 'price']

    @staticmethod
    def get_sphinx_filters(filters):
        return dict(
            query=filters.pop('query', [None])[0],
            shop_id=filters.pop('shop', [None])[0]
        )

    def get_object_list(self, request):
        data = {'query': request.GET['query'],
                'shop_id': request.shop.pk}
        cat_id = request.GET.get('category', None)
        if cat_id is not None:
            cat_ids = Category.objects.get(id=cat_id).get_nesting_id_list()
        else:
            cat_ids = None

        p_ids = map(
            lambda item: item['id'],
            self.search(data).ask()['result']['items']
        )
        base_qs = super(SearchProductFullResource, self).get_object_list(
            request).filter(pk__in=p_ids, status=True)
        if cat_ids:
            return base_qs.filter(category__in=base_qs)
        else:
            return base_qs

    def dehydrate(self, bundle):
        bundle.data['cover'] = bundle.data['images'][0]
        return bundle

    @staticmethod
    def search(data):
        return (
            Search(
                indexes=settings.SPHINX_INDEXES_FULL, config=SphinxItConfig
            )
            .match(data['query'] + '*')
            .limit(0, 1000)
            .filter(shop_id__eq=data['shop_id'])
            .select('id')
            .options(
                field_weights={'name': 100, 'description': 80},
            )
            .order_by('name', 'desc')
        )


class CustomerResource(ModelResource):
    class Meta(object):
        resource_name = 'customer'
        allowed_methods = ['get', 'patch']
        queryset = Customer.objects.all()
        filtering = {
            'vk_id': ('exact', ),
            'fb_id': ('exact', ),
            'ok_id': ('exact', )
        }
        authorization = Authorization()


class CommentResource(ModelResource):
    shop = fields.ForeignKey(ShopResource, 'shop')
    product = fields.ForeignKey(ProductResource, 'product')
    customer = fields.ForeignKey(CustomerResource, 'customer', full=True,
        null=True)
    reply_list = fields.ToManyField('self', 'reply_list', blank=True,
        readonly=True, full=True)

    class Meta(object):
        resource_name = 'comments'
        allowed_methods = ['get', 'post', 'put', 'delete', 'patch']
        queryset = Comment.objects.filter(reply_to=None)
        authorization = Authorization()

    def alter_list_data_to_serialize(self, request, data):
        data['meta']['max_rating'] = Comment.MAX_RATING
        return data

    def get_object_list(self, request, data=None):
        return super(CommentResource, self).get_object_list(request).filter(
            shop=request.shop)

    def obj_create(self, bundle, **kwargs):
        customer_name = bundle.data.get('customer_name', None)
        customer_obj = bundle.request.app_customer['customer']
        if customer_name and customer_obj.name != customer_name:
            customer_obj.name = customer_name
            customer_obj.save()
        bundle.data['customer'] = CustomerResource().get_resource_uri(
            customer_obj)
        bundle.data['product'] = ProductResource().get_resource_uri(
            Product.objects.get(pk=bundle.data['product']))
        bundle.data['shop'] = ShopResource().get_resource_uri(bundle.request.shop)
        bundle_res = super(CommentResource, self).obj_create(bundle, **kwargs)
        comment_obj = bundle_res.obj
        try:
            message_html = render_to_string(
                'email/new_comment.html',
                dict(comment=comment_obj, shop=bundle.request.shop)
            )
            EmailQueue.objects.create(
                email_from=u'ShoppyBoom.Ru <no-reply@shoppyboom.ru>',
                email_to=bundle.request.shop.email,
                bcc='orders@shoppyboom.ru',
                reply_to=u'Команда ShoppyBoom.Ru <team@shoppyboom.ru>',
                subject=u'ShoppyBoom: Новый комментарий',
                message_type=EmailQueue.MTYPE_HTML,
                body=message_html
            )
        except Exception as e:
            logger.error('Error on send email about new comment& Error message:"%s"' % str(e))
        return bundle_res


class CityResource(ModelResource):
    class Meta(object):
        resource_name = 'city'
        allowed_methods = ['get']
        queryset = City.objects.all().annotate(d_count=Count('deliverypoint')).order_by('name').select_related('deliverypoint')
        limit = 0

    def alter_list_data_to_serialize(self, r, data):
        min_dp_count = 20
        top_dp = filter(lambda bundle: bundle.obj.d_count >= min_dp_count, data['objects'])
        top_dp.sort(key=lambda x: x.obj.d_count, reverse=True)
        data['objects'] = top_dp + data['objects']
        data['meta']['highlight'] = len(top_dp)
        return super(CityResource, self).alter_list_data_to_serialize(r, data)

    def get_object_list(self, request):
        return super(CityResource, self).get_object_list(request).filter(
            deliverypoint__in=request.shop.delivery_points.all())


class DeliveryPointResource(ModelResource):
    shop = fields.ForeignKey(ShopResource, 'shop')
    city = fields.ForeignKey(CityResource, 'city', null=True)

    class Meta(object):
        resource_name = 'deliverypoint'
        filtering = {'city': ('exact', )}
        allowed_methods = ['get']
        queryset = DeliveryPoint.objects.all().select_related('shop')
        limit = 0

    def get_object_list(self, request):
        return super(DeliveryPointResource, self).get_object_list(request).filter(shop=request.shop)

    @staticmethod
    def dehydrate_json_data(bundle):
        return json.loads(bundle.data['json_data'])


class CallbackOrderResource(ModelResource):
    pid = fields.IntegerField('product_id')
    app_type = fields.CharField('app_type__code')

    class Meta(object):
        resource_name = 'callback-order'
        model = CallbackOrder
        queryset = CallbackOrder.objects.all()
        allowed_methods = ['post', 'get']
        authorization = Authorization()
        #always_return_data = True

    def obj_create(self, bundle, **kwargs):
        obj = CallbackOrder.objects.create(
            customer=bundle.request.app_customer['customer'],
            shop=bundle.request.shop,
            app_type=AppType.objects.get(code=bundle.data['app_type']),
            product=Product.objects.get(pk=bundle.data['pid']),
            name=bundle.data['name'],
            phone=bundle.data['phone']
        )
        obj.send_notice()


def handle_section(section):
    section_type = section.TYPE_CODE
    if section_type == 'main':
        return {
            'landing_id': section.pk,
            'order': section.order,
            'type': 'main',
            'title': section.title,
            'desc': section.desc,
            'custom_style': section.custom_style,
        }
    elif section_type == 'counter':
        return {
            'landing_id': section.pk,
            'order': section.order,
            'type': 'counter',
            'title': section.title,
            'date': section.date,
            'show_in_footer': section.show_in_footer,
            'custom_style': section.custom_style,
        }
    elif section_type == 'products':
        p = ProductResource()
        map_fun = lambda prod: p.full_dehydrate(p.build_bundle(obj=prod))
        prods_data = map(map_fun, section.product_group.products.filter(status=True))
        return {
            'landing_id': section.pk,
            'order': section.order,
            'type': 'products',
            'title': section.title,
            'products': prods_data,
            'custom_style': section.custom_style,
        }
    elif section_type == 'compare':
        return {
            'landing_id': section.pk,
            'order': section.order,
            'type': 'compare',
            'left': {
                'title': section.left_title,
                'text': section.left_text
            },
            'right': {
                'title': section.right_title,
                'text': section.right_text
            },
            'custom_style': section.custom_style,
        }
    elif section_type == 'comments':
        comments = []
        for comment in section.comments.all():
            comments.append({
                'author': comment.author,
                'text': comment.text
            })
        return {
            'landing_id': section.pk,
            'order': section.order,
            'type': 'comments',
            'title': section.title,
            'comments': comments,
            'custom_style': section.custom_style,
        }
    elif section_type == 'simple':
        return {
            'landing_id': section.pk,
            'order': section.order,
            'type': 'simple',
            'title': section.title,
            'text': section.text,
            'custom_style': section.custom_style,
        }


class LandingResource(ModelResource):
    sections = fields.ListField(default=[])

    class Meta(object):
        resource_name = 'landing'
        model = Landing
        queryset = Landing.objects.all()

    def get_object_list(self, request):
        return super(LandingResource, self).get_object_list(request).filter(shop=request.shop)

    @staticmethod
    def dehydrate_sections(bundle):
        sections = []
        for section in sorted(bundle.obj.get_sections(), key=lambda s: s.order):
            if section.is_active:
              sections.append(handle_section(section))
        return sections


class BannerResource(ModelResource):
    data = fields.DictField()
    slot = fields.CharField(attribute='slot__place')
    type_code = fields.CharField()
    type = fields.IntegerField(attribute='banner_type')

    class Meta(object):
        resource_name = 'banner'
        model = Banner
        queryset = Banner.objects.filter(status=True).select_related(
            'bannertext', 'bannergraphic', 'bannercarousel', 'slot')
        allowed_methods = ['get']
        filtering = {
            'slot': tastypie.constants.ALL
        }
        excludes = ['cdate', 'status', 'banner_type']
        authorization = Authorization()

    def get_object_list(self, request):
        objects = super(BannerResource, self).get_object_list(request).filter(
                shop=request.shop)
        if 'slot' in request.GET:
            objects = objects.filter(slot__place=request.GET['slot'])
            if objects.count():
                r = random.randint(1, objects.count())
                return objects.filter(pk=objects[r-1].pk)
        return objects

    @staticmethod
    def dehydrate_data(bundle):
        p = ProductResource()
        map_fun = lambda product: p.full_dehydrate(p.build_bundle(obj=product))

        if bundle.obj.banner_type == Banner.BANNER_TYPE_TEXT:
            return {
                'text': bundle.obj.bannertext.text,
                'product': map_fun(bundle.obj.bannertext.product) if bundle.obj.bannertext.product else None
            }
        elif bundle.obj.banner_type == Banner.BANNER_TYPE_CAROUSEL:
            products = bundle.obj.bannercarousel.product_group.products.all()
            return map(map_fun, products)
        elif bundle.obj.banner_type == Banner.BANNER_TYPE_GRAPHIC:
            res = []
            for image_obj in bundle.obj.bannergraphic.images.all():
                res.append({
                    'product': map_fun(image_obj.product),
                    'image_url': image_obj.image.url
                })
            return res
        return {}

    @staticmethod
    def dehydrate_type_code(bundle):
        return dict(Banner.BANNER_TYPE_CODES)[bundle.obj.banner_type]
