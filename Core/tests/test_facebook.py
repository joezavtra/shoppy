# coding=utf-8
from urllib2 import HTTPError
from django_nose import FastFixtureTestCase
import facebook

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class FacebookTestCase(FastFixtureTestCase):

    def setUp(self):
        self.app_id = '195142764008078'
        self.invalid_app_id = '195142764008078ss'
        self.app_secret = 'ce368a8a95e2f266487773c7ce7d3932'
        self.user_id = '100001020583521'

    def test_auth(self):
        token = facebook.get_app_access_token(self.app_id, self.app_secret)
        self.assertTrue(isinstance(token, str) or isinstance(token, unicode))

    def test_fail_auth(self):
        self.assertRaises(HTTPError, facebook.get_app_access_token, self.invalid_app_id, self.app_secret)

    def test_fql_user_data(self):
        token = facebook.get_app_access_token(self.app_id, self.app_secret)
        fb = facebook.GraphAPI(access_token=token)
        fields = ['uid', 'name', 'first_name', 'last_name', 'username', 'sex']
        users = fb.fql('select %s from user where uid in (%s)' % (','.join(fields), self.user_id))
        self.assertTrue(type(users) == list)
        self.assertTrue(len(users) == 1)
        user = users[0]
        self.assertTrue(set(user.keys()) == set(fields))

    def test_user_get_data(self):
        token = facebook.get_app_access_token(self.app_id, self.app_secret)
        fb = facebook.GraphAPI(access_token=token)
        fields = ['first_name', 'last_name', 'gender', 'username', 'hometown', 'relationship_status']
        user = fb.get_object(self.user_id, fields=','.join(fields))
        self.assertTrue(type(user) == dict and 'id' in user)
