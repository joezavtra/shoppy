# coding=utf-8
import os
from django.conf import settings
from django_nose import FastFixtureTestCase
import sassc
import time
from Core.templatetags import superstatic

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class SasscTestCase(FastFixtureTestCase):
    def test_sassc_speed(self):
        sass_file = os.path.join(
            settings.PROJECT_ROOT, 'Core/tests/data/sass/test.sass'
        )
        start = time.time()
        for i in xrange(1000):
            sassc.compile(filename=sass_file)
        res = time.time() - start

        self.assertLessEqual(res, 1.5, 'Time for render sass file "%s" to css '
                                       'is grater than 1.5' % sass_file)


class SassTemplateTagTestCase(FastFixtureTestCase):
    def test_sass_css(self):
        self.assertLessEqual(
            superstatic.sass('test/sass/test.sass', css='yes'),
            'body a {  text-decoration: underline; } '
        )
