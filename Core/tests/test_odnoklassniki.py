from django_nose import FastFixtureTestCase
from odnoklassniki_api import Odnoklassniki

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class OdnoklassnikiTestCase(FastFixtureTestCase):
    def setUp(self):
        self.ok = Odnoklassniki(
            app_key='CBALFHLBEBABABABA',
            app_secret='2528397628B0BA43E1F0FC8C'
        )

    def test_user_get(self):
        res = self.ok.users.getInfo(
            uids='575993327671,204505246625', fields='uid,birthday,first_name,last_name,age,location,gender'
        )
        self.assertEqual(type(res), list)
        self.assertEqual(len(res), 2)
