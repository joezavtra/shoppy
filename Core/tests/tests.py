# coding=utf-8
from django.core.management import call_command
from django_nose import FastFixtureTestCase
from Core.models import *


class UtilsTestCase(FastFixtureTestCase):
    def setUp(self):
        self.base_host = 'shoppyboom.ru'

    def test_abs_url1(self):
        url = '/path/to/file.png'
        res = abs_url(url, self.base_host)
        self.assertEqual(res, 'http://%s%s' % (self.base_host, url))

    def test_abs_url2(self):
        url = 'http://ya.ru/path/to/file.png'
        res = abs_url(url, self.base_host)
        self.assertEqual(res, url)


class DiscountTestCase(FastFixtureTestCase):
    fixtures = ['shops.json']

    def setUp(self):
        self.discount = Discount(
            shop=Shop.objects.all()[0],
            name='test_name'
        )

    def test_get_discount_rub(self):
        self.discount.type = Discount.SPECIAL
        self.discount.threshold = 1000
        self.discount.value = 80
        self.discount.value_type = Discount.VT_RUBLES
        self.discount.save()
        res = self.discount.apply(100)
        self.assertEqual(res, 100)
        res = self.discount.apply(1000)
        self.assertEqual(res, 920)

    def test_get_discount_percent(self):
        self.discount.type = Discount.SPECIAL
        self.discount.threshold = 1000
        self.discount.value = 80
        self.discount.value_type = Discount.VT_PERCENT
        self.discount.save()
        res = self.discount.apply(100)
        self.assertEqual(res, 100)
        res = self.discount.apply(1000)
        self.assertEqual(res, 200)

    def test_get_discount_percent_simple(self):
        self.discount.type = Discount.SIMPLE
        self.discount.value = 80
        self.discount.value_type = Discount.VT_PERCENT
        self.discount.save()
        res = self.discount.apply(10)
        self.assertEqual(res, 2)
        res = self.discount.apply(100)
        self.assertEqual(res, 20)
        res = self.discount.apply(1000)
        self.assertEqual(res, 200)

    def test_get_discount_rub_simple(self):
        self.discount.type = Discount.SIMPLE
        self.discount.value = 80
        self.discount.value_type = Discount.VT_RUBLES
        self.discount.save()
        res = self.discount.apply(10)
        self.assertEqual(res, 0)
        res = self.discount.apply(100)
        self.assertEqual(res, 20)
        res = self.discount.apply(1000)
        self.assertEqual(res, 920)


class FileimportTestCase(FastFixtureTestCase):
    fixtures = ['shops.json']

    def test_hello(self):
        self.assertEqual(Category.objects.all().count(), 0)
        self.assertEqual(Offer.objects.all().count(), 0)
        self.assertEqual(Product.objects.all().count(), 0)
        self.assertEqual(Shop.objects.all().count(), 1)

        filename = os.path.join(
            settings.PROJECT_ROOT, 'Core/tests/data/yml/1.first_import.yml'
        )
        call_command('fileimport', filename=filename, shop_id=4)

        self.assertEqual(Category.objects.all().count(), 2)
        self.assertEqual(Offer.objects.all().count(), 2)
        self.assertEqual(Product.objects.all().count(), 2)
        self.assertEqual(Product.objects.filter(status=True).count(), 2)
        self.assertEqual(Shop.objects.all().count(), 1)

        filename = os.path.join(
            settings.PROJECT_ROOT, 'Core/tests/data/yml/2.disable-1-product.yml'
        )
        call_command('fileimport', filename=filename, shop_id=4)

        self.assertEqual(Category.objects.all().count(), 2)
        self.assertEqual(Offer.objects.all().count(), 2)
        self.assertEqual(Product.objects.all().count(), 2)
        self.assertEqual(Product.objects.filter(status=True).count(), 1)
        self.assertEqual(Shop.objects.all().count(), 1)

        filename = os.path.join(
            settings.PROJECT_ROOT, 'Core/tests/data/yml/1.first_import.yml'
        )
        call_command('fileimport', filename=filename, shop_id=4)

        self.assertEqual(Category.objects.all().count(), 2)
        self.assertEqual(Offer.objects.all().count(), 2)
        self.assertEqual(Product.objects.all().count(), 2)
        self.assertEqual(Product.objects.filter(status=True).count(), 2)
        self.assertEqual(Shop.objects.all().count(), 1)

        filename = os.path.join(
            settings.PROJECT_ROOT, 'Core/tests/data/yml/3.delete-product.yml'
        )
        call_command('fileimport', filename=filename, shop_id=4)

        self.assertEqual(Category.objects.all().count(), 2)
        self.assertEqual(Offer.objects.all().count(), 2)
        self.assertEqual(Product.objects.all().count(), 2)
        self.assertEqual(Product.objects.filter(status=True).count(), 1)
        self.assertEqual(Shop.objects.all().count(), 1)
