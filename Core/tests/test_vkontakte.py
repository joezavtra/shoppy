# coding=utf-8
from django_nose import FastFixtureTestCase
import vkontakte
import vkontakte.api

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class VkontakteTestCase(FastFixtureTestCase):
    # fixtures = []

    def setUp(self):
        self.vk = vkontakte.API(api_id=4056146, api_secret='xhgsaxnrvcZLqvUBRRZ4')
        self.uid = 19945483
        self.uids = [str(self.uid), '19945483']

    # def test_connect(self):
    #     res = self.vk.getServerTime()
    #     self.assertGreaterEqual(res, 0)
    #
    # def test_seng_message(self):
    #     try:
    #         res = self.vk.secure.sendNotification(user_id=self.uid, message=u'Привет, бро. зайди в аппликэйшн!!!')
    #         self.assertEqual(str(res), str(self.uid))
    #     except vkontakte.api.VKError as e:
    #         self.assertIn('Access denied: too frequently', e.message)
    #
    # def test_seng_mass_message(self):
    #     try:
    #         res = self.vk.secure.sendNotification(user_ids=','.join(self.uids), message=u'Привет, бро. зайди в аппликэйшн!!!')
    #         self.assertEqual(str(res), str(self.uid))
    #     except vkontakte.api.VKError as e:
    #         self.assertIn('Access denied: too frequently', e.message)

    def _test_send_sms(self):
        res = self.vk.secure.sendSMSNotification(uid=str(self.uid), message=u"Tol'ko do 8 marta! Pri pokupke v prilozhenii magazina E5 VKontakte www.vk.com/e5_ru poluchite skidku 150 rublej! Skidku mozhno poluchit' v prilozhenii!")
        self.assertEqual(res, 1)

    # def test_users_get(self):
    #     res = self.vk.users.get(user_ids=self.uid, fields='sex,bdate,city,country,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max,photo_max_orig,online,online_mobile,lists,domain,has_mobile,contacts,connections,site,education,universities,schools,can_post,can_see_all_posts,can_see_audio,can_write_private_message,status,last_seen,relation,relatives,counters')
    #     self.assertEqual(type(res), list)
    #
    # def test_sms_hist(self):
    #     res = self.vk. secure.getSMSHistory()
    #     self.assertEqual(type(res), list)
    #
    # def test_transaction(self):
    #     res = self.vk. secure.getTransactionsHistory()
    #     self.assertEqual(type(res), list)
    #
    # def test_app_balance(self):
    #     res = self.vk. secure.getAppBalance()
    #     self.assertGreater(res, -1)
    #
    # # def test_app_votes(self):
    # #     res = self.vk.secure.withdrawVotes(uid=self.uid, votes=1)
    # #     print res
    # #     self.assertEqual(1, 0)
    #
    # def test_set_counter(self):
    #     try:
    #         res = self.vk.setCounter(user_id=self.uid, counter=10)
    #         self.assertEqual(res, 1)
    #         res = self.vk.setCounter(user_id=self.uid, counter=0)
    #         self.assertEqual(res, 1)
    #     except vkontakte.api.VKError as e:
    #         self.assertIn('Access to the menu of the user denied', e.message)

    def _test_app_permissions(self):
        res = self.vk.account.getAppPermissions(uid=self.uid)
        print res
        self.assertEqual(1, 0)

    # def test_is_app_user(self):
    #     res = self.vk.users.isAppUser(user_id=self.uid)
    #     self.assertEqual(res, str(1))