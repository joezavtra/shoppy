import string
from django_nose.testcases import FastFixtureTestCase
from Core.utils import clean_str

__author__ = 'Prosto Chewey <prosto.chewey@gmail.com>'


class CleanStrTestCase(FastFixtureTestCase):
    def test_clean_str(self):
        allowed_chars = string.digits + '+'
        some_str = '+7 (929) 563-11 - 00'
        self.assertEqual(clean_str(some_str, allowed_chars), '+79295631100')