from django_nose import FastFixtureTestCase
from Core import field_validators

__author__ = 'Prosto Chewey <prosto.chewey@gmail.com>'


class TelephoneNumberTestCase(FastFixtureTestCase):
    def test_numbers(self):
        tel_list = [
            '+79261234567',
            '89261234567',
            '79261234567',
            '+7 926 123 45 67',
            '8(926)123-45-67',
            '123-45-67',
            '9261234567',
            '79261234567',
            '(495)1234567',
            '(495) 123 45 67',
            '89261234567',
            '8-926-123-45-67',
            '8 927 1234 234',
            '8 927 12 12 888',
            '8 927 12 555 12',
            '8 927 123 8 123',
        ]
        for tel in tel_list:
            self.assertIsNone(field_validators.telephone_number(tel))