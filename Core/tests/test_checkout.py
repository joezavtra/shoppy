from django_nose import FastFixtureTestCase
from Core.checkout import get_ticket

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class GetTicketTestCase(FastFixtureTestCase):
    def test_get_ticket(self):
        api_key = 'oQZMgKL7m75NUVb1W3O5LR0L'
        ticket = get_ticket(api_key)
        self.assertNotEqual(ticket, False)
        self.assertTrue(issubclass(type(ticket), basestring))
        self.assertEqual(len(ticket), 32)
