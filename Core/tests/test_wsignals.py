from django_nose import FastFixtureTestCase
from Core.models import Cart, Customer, Shop, CartItem, Product

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class UpdateCartMtimeTestCase(FastFixtureTestCase):
    fixtures = ['test_signals_CartTest.json']

    def setUp(self):
        try:
            super(UpdateCartMtimeTestCase, self).setUp()
            self.shop = Shop.objects.all()[0]
            self.product = Product.objects.all()[0]
            self.customer = Customer(
                name='Peter',
                vk_id='19945483'
            )
            self.customer.save()
            self.cart = Cart(
                customer=self.customer,
                shop=self.shop
            )
            self.cart.save()
        except:
            pass

    def test_cart_create(self):
        self.assertEqual(self.cart.mdate, None)

    def test_cart_update_notice(self):
        self.assertEqual(self.cart.mdate, None)
        self.cart.update_notice_time()
        self.assertNotEqual(self.cart.mdate, None)

    def test_cart_mtime_signal(self):
        self.assertEqual(self.cart.mdate, None)
        item = CartItem(
            cart=self.cart,
            product=self.product,
            count=1
        )
        item.save()
        cart = Cart.objects.all()[0]
        self.assertNotEqual(cart.mdate, None)
