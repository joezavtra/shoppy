__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'
from django_nose import FastFixtureTestCase
from Core.models import *


class ShopExtraTestCase(FastFixtureTestCase):
    fixtures = ['shops.json']

    def setUp(self):
        self.shop = Shop.objects.all()[0]
        self.key = 'my_key'
        self.value = 'my_value'
        self.e = ShopExtra(
            shop=self.shop,
            key=self.key,
            value=self.value
        )

    def test_key_upper(self):
        self.e.save()
        self.assertEqual(self.e.key, self.key.upper())

    def test_cache(self):
        self.e.save()
        cache_key = ShopExtra.get_cache_key(self.shop.pk, self.key)
        self.assertEqual(cache.get(cache_key, None), self.value)

    def test_get_param(self):
        self.e.save()
        res_value = self.e.get_param(self.shop.pk, self.key)
        self.assertEqual(res_value, self.value)