# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group
from tastypie.models import ApiAccess
from django.contrib import admin
from Core.models import *


class FileImportLogAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'datasource', 'categories_created', 'categories_deactivated', 'categories_total', 'products_created', 'products_deactivated', 'products_total', )
    list_filter = ['datasource']


class PageAdmin(admin.ModelAdmin):
    list_display = ('name', 'order', 'shop')
    list_filter = ['shop']

class ClientAdmin(admin.ModelAdmin):
    def shops_count(self, obj):
        return obj.shops.count()

    def users_count(self, obj):
        return obj.users.count()

    list_display = ('name', 'cdate', 'shops_count', 'users_count', 'status')
    list_filter = ['status']


class ShopAdmin(admin.ModelAdmin):
    list_display = ('name', 'cdate', 'cats_count', 'status', 'tariff')
    list_filter = ('status', 'tariff')


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ('oid', 'name',)
    list_display = ('id', 'name', 'parent', 'active_products_count', 'status')
    list_filter = ['datasource', 'status']


class CategoryInline(admin.TabularInline):
    model = Product.categories.through
    extra = 1


class OfferInline(admin.TabularInline):
    model = Offer
    #extra = 1


class ProductAdmin(admin.ModelAdmin):
    inlines = (CategoryInline,OfferInline)
    list_display = ('name', 'status')
    list_filter = ['status', 'datasource', 'categories__name']


class OfferAdmin(admin.ModelAdmin):
    list_filter = ('status',)
    list_display = ('product', 'oid', 'price', 'status')


class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'last_name',
        'nickname',
        'bdate_y',
        'bdate_m',
        'bdate_d',
        'city',
        'sex',
        'relation',
        'vk_id',
        'fb_id',
        'ok_id',
    )
    list_filter = (
        'city',
        'sex',
        'relation',
    )

    def queryset(self, request):
        return super(CustomerAdmin, self).queryset(request).select_related(
            'city'
        )


class CommentAdmin(admin.ModelAdmin):
    list_display = (
        'product',
        'customer',
        'rating',
        'text',
        'created',
    )
    raw_id_fields = ('product', 'customer')


class DeliveryPointAdmin(admin.ModelAdmin):
    list_display = (
        'shop',
        'oid',
        'city',
        'point_name',
        'name',
        'lat',
        'lng',
    )


class DiscountAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'shop',
    )
    raw_id_fields = ('products', )

class EmailQueueAdmin(admin.ModelAdmin):
    list_display = (
        'email_from',
        'email_to',
        'subject',
        'body',
        'is_complete'
    )



class CartItemAdmin(admin.ModelAdmin):
    raw_id_fields = ('cart', 'product')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('shop', 'cdate', 'app_type', 'customer', 'status', )
    raw_id_fields = ('cart', 'customer', )


class ShoppyUserAdmin(admin.ModelAdmin):
    list_display = ('sys_user',)


class ShopPlatformAdmin(admin.ModelAdmin):
    list_display = ('shop', 'checkout', 'robokassa')


class RoleAdmin(admin.ModelAdmin):
    list_display = ('name', 'alias')


class DataSourceAdmin(admin.ModelAdmin):
    list_display = ('name', 'yml_url', 'mode', 'last_fetch', 'next_fetch', 'processing')
    list_filter = ['mode', 'processing', 'client']


class FormDescriptionAdmin(admin.ModelAdmin):
    list_display = ('field_name', 'field_label', 'help_text')
    ordering = ('help_text', )
    search_fields = ('field_name', 'field_label', 'help_text')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ClientPaymentAdmin(admin.ModelAdmin):
    list_display = ('desc', 'client', 'shop', 'amount', 'status',)
    list_filter = ('status', 'client', 'shop',)
    readonly_fields = ('status', )


class TariffAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'max_products', 'cost',
        'is_default', 'is_archive', 'vk_enable', 'fb_enable',
        'ok_enable', 'update_period', 'has_whitelabel', 'order', )
    list_editable = ('max_products', 'cost',
        'is_default', 'is_archive', 'vk_enable', 'fb_enable',
        'ok_enable', 'update_period', 'order', )


class PartnerAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'desc', 'contact_name', 'email', 'benefits', 'code', )

class AppTypeAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'code', )

class ShopCatalogAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'parent', )


class RobokassaPlatformAdmin(admin.ModelAdmin):
    list_display = ('login', 'password1', 'password2', )


class CheckoutPlatformAdmin(admin.ModelAdmin):
    list_display = ('api_key', )


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', )


class OrderHistoryAdmin(admin.ModelAdmin):
    list_display = ('order', 'author', 'new_status', 'timestamp')


class CallbackOrderAdmin(admin.ModelAdmin):
    list_display = ('shop', 'app_type', 'product', 'customer', 'name', 'phone')
    list_filter = ('shop', 'app_type', )
    raw_id_fields = ('product', 'customer')


class ProductGroupAdmin(admin.ModelAdmin):
    list_display = ('shop', 'name', )
    list_filter = ('shop', )
    raw_id_fields = ('products', )


class BannerSlotAdmin(admin.ModelAdmin):
    list_display = ('place', )


class BannerTextAdmin(admin.ModelAdmin):
    raw_id_fields = ('product', )


admin.site.register(ApiAccess)
admin.site.register(AppType, AppTypeAdmin)
admin.site.register(Banner)
admin.site.register(BannerCarousel)
admin.site.register(BannerGraphic)
admin.site.register(BannerSlot, BannerSlotAdmin)
admin.site.register(BannerText, BannerTextAdmin)
admin.site.register(CallbackOrder, CallbackOrderAdmin)
admin.site.register(Cart)
admin.site.register(CartItem, CartItemAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(CheckoutPlatform, CheckoutPlatformAdmin)
admin.site.register(City)
admin.site.register(Client, ClientAdmin)
admin.site.register(ClientPayment, ClientPaymentAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Currency, CurrencyAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(DataSource, DataSourceAdmin)
admin.site.register(DeliveryPoint, DeliveryPointAdmin)
admin.site.register(Discount, DiscountAdmin)
admin.site.register(EmailQueue, EmailQueueAdmin)
admin.site.register(FileImportLog, FileImportLogAdmin)
admin.site.register(FormDescription, FormDescriptionAdmin)
admin.site.register(Landing)
admin.site.register(LSectionComment)
admin.site.register(LSectionComments)
admin.site.register(LSectionCompare)
admin.site.register(LSectionCounter)
admin.site.register(LSectionMain)
admin.site.register(LSectionProducts)
admin.site.register(LSectionSimple)
admin.site.register(Offer, OfferAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderHistory, OrderHistoryAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(Partner, PartnerAdmin)
admin.site.register(Platform)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductCategories)
admin.site.register(ProductGroup, ProductGroupAdmin)
admin.site.register(RobokassaPlatform, RobokassaPlatformAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(ShopCatalog, ShopCatalogAdmin)
admin.site.register(ShopPlatform, ShopPlatformAdmin)
admin.site.register(ShopProfile)
admin.site.register(ShoppyUser, ShoppyUserAdmin)
admin.site.register(Tariff, TariffAdmin)
admin.site.register(VKOffer)
admin.site.unregister(Group)
