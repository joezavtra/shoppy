from django.conf import settings

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class SortQueryset(object):
    def __init__(self, qs, fields=None):
        self.qs = qs
        if fields is None:
            fields = self.qs.model._meta.get_all_field_names()
        self.fields = fields
        self.param_name = settings.SORT_PARAM_NAME

    def parse_request(self, request):
        sort_str = request.GET.get(self.param_name, None)
        if sort_str is None:
            return self.qs
        sort_params = []
        for s in sort_str.split(','):
            if s.startswith('-'):
                key = s[1:]
            else:
                key = s
            if key in self.fields:
                sort_params.append(s)
        return sort_params

    def sort_by_request(self, request):
        return self.qs.order_by(*self.parse_request(request))


