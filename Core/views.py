# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from Core.decorators import render_to
from Core.models import Product, Shop


def redirect(request):
    url = request.GET.get('q', '/')
    pid = request.GET.get('pid', None)
    sid = request.GET.get('sid', None)

    if pid != None:
        ret = {}

        try:
            product = Product.objects.get(pk=pid)
            if sid !=None:
                shop = Shop.objects.get(pk=sid)
            else:
                shop = product.datasource.shops.all()[0]

            ret = {
                'original_url' : "http://%s%s" % (request.META['HTTP_X_FORWARDED_HOST'], request.get_full_path() ),
                'url': url,
                'product': product,
                'shop': shop
            }

            return render_to_response(
                'Core/redirect.html',
                ret,
                context_instance=RequestContext(request)
            )

        except:
            pass

    return HttpResponseRedirect(url)


@render_to('Core/share.html')
def share(r):
    shop_pk = r.GET.get('sid', None)
    product_pk = r.GET.get('pid', None)
    app_type = r.GET.get('type', None)
    origin_url = 'http://%s%s' % (
        r.META.get('HTTP_X_FORWARDED_HOST', 'shoppyboom.ru'),
        r.get_full_path()
    )

    shop_obj = get_object_or_404(
        Shop.objects.select_related('shopprofile'), pk=shop_pk)
    products = Product.objects.filter(datasource__in=shop_obj.datasources.all())
    product_obj = get_object_or_404(products, pk=product_pk)

    if app_type == 'vk':
        url = shop_obj.shopprofile.vk_url + '#product/%d' % product_obj.pk
    elif app_type == 'fb':
        url = shop_obj.shopprofile.fb_url + '/product/%d' % product_obj.pk
    elif app_type == 'fb_tab':
        url = shop_obj.shopprofile.fb_taburl + '?app_data=/product/%s' % product_obj.pk
    elif app_type == 'ok':
        url = shop_obj.shopprofile.ok_url + '?custom_args=/product/%s' % product_obj.pk
    elif app_type == 'www':
        url = shop_obj.get_url() + '#product/%d' % product_obj.pk
    else:
        url = 'http://shoppyboom.ru'

    return dict(
        original_url=origin_url,
        url=url,
        product=product_obj,
        shop=shop_obj
    )
