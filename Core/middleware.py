# coding=utf-8
from collections import defaultdict
from urlparse import parse_qsl
import logging
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.middleware.common import CommonMiddleware
from django.shortcuts import redirect
import facebook
from Core.models import Shop, Customer

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


logger = logging.getLogger('app')


class ExceptionMiddleware(CommonMiddleware):
    def __init__(self, *args, **kwargs):
        super(ExceptionMiddleware, self).__init__(*args, **kwargs)

    def process_exception(self, request, exception):
        logger.exception(exception)


class HostsMiddleware(object):
    def __init__(self):
        self.base_host_name = settings.BASE_HOST_NAME
        self.allowed_base_hosts = settings.ALLOWED_BASE_HOSTS

    def get_shop(self, host):
        if host.endswith(self.base_host_name):
            try:
                return Shop.objects.get(
                    domain=host[:-(len(self.base_host_name) + 1)]
                )
            except ObjectDoesNotExist:
                pass
        try:
            return Shop.objects.get(domain=host)
        except ObjectDoesNotExist:
            return None

    def process_request(self, r):
        r.logger = logger

        cur_host = r.get_host().split(':')[0]

        shop = self.get_shop(cur_host)
        if shop is not None:
            r.shop = shop
        else:
            r.shop = None
            if cur_host not in self.allowed_base_hosts:
                return redirect('http://' + self.base_host_name)

    def process_response(self, request, response):
        response['P3P'] = 'CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"'
        return response




class UserMiddleware(object):
    """
    Получаем сессию пользователя
    Если это приложение
        Получаем пользователя из приложения
    Если не получили пользователя приложения - считаем что мы на домене
    Если есть пользователь приложения и пользователь сессии
        Мержим их
    Если нет ни того ни другого
        Берем пользователя сессии или пользователя приложения
    Если нет ни одного
        Создаем нового
    Возвращаем пользователя
    """
    def __init__(self):
        self.key_name = settings.USER_MIDDLEWARE_SESSION_KEY

    def get_session_customer(self, r):
        """
        Метод извлечения пользователя сессии
        :param r: request
        :return: Customer() | None
        """
        customer_pk = r.session.get(self.key_name, None)
        if customer_pk:
            try:
                return Customer.objects.get(pk=customer_pk)
            except Exception as e:
                del r.session[self.key_name]
                r.session.modified = True
                logger.exception(
                    'Unable to find Customer with pk = "%s". '
                    'Clear session key' % str(customer_pk))
        return None

    def auth_customer(self, r, app_customer):
        """
        Знает как авторизовать пользователя. Записываем ему сессию
        :param r: request
        :param app_customer: dict(customer=Customer(), ...)
        """
        if app_customer and app_customer['customer']:
            customer_pk = str(app_customer['customer'].pk)
            if r.session.get(self.key_name, None) != customer_pk:
                logger.debug('create session "%s"' % customer_pk)
                r.session[self.key_name] = customer_pk
                r.session.modified = True
            else:
                logger.debug('found session "%s"' % customer_pk)

    def get_customer_data(self, r):
        p = r.path
        session_customer = self.get_session_customer(r)

        base_data = {
            'platform': 'www',
            'user_id': None,
            'customer': None,
            'platform_key': None
        }
        if p == '/':
            base_data['platform'] = 'www'
            if session_customer:
                base_data['customer'] = session_customer
            else:
                base_data['customer'] = Customer.objects.create()
        elif p.startswith('/api'):
            base_data['platform'] = 'api'
            if session_customer:
                base_data['customer'] = session_customer
            else:
                base_data['customer'] = Customer.objects.create()
        elif p.startswith('/vk'):
            data_is_valid, data = self.parse_vk(r)
            if data_is_valid:
                data['platform_key'] = 'vk_id'
                data['platform'] = 'vk'
            base_data.update(data)
        # TODO: можно заменить условие на p.startswith('/fb'), но так нагляднее
        elif p.startswith('/fb/tab') or p.startswith('/fb'):
            data_is_valid, data = self.parse_fb(r)
            if data_is_valid:
                data['platform_key'] = 'fb_id'
                data['platform'] = 'fb'
            base_data.update(data)
        elif p.startswith('/ok'):
            data_is_valid, data = self.parse_ok(r)
            if data_is_valid:
                data['platform_key'] = 'ok_id'
                data['platform'] = 'ok'
            base_data.update(data)
        else:
            raise Exception('Unknown url')

        if base_data['platform'] and base_data['user_id']:
            user_id = str(base_data['user_id'])
            if session_customer:
                if getattr(session_customer, base_data['platform_key'], None) == user_id:
                    base_data['customer'] = session_customer
                else:
                    platform_customer = self.get_db_customer(
                        base_data['platform_key'], user_id)
                    if platform_customer:
                        base_data['customer'] = self.merge_customers(platform_customer, session_customer)
                    else:
                        setattr(session_customer, base_data['platform_key'], user_id)
                        session_customer.save()
                        base_data['customer'] = session_customer
            else:
                platform_customer = self.get_db_customer(
                    base_data['platform_key'], user_id)
                if platform_customer:
                    base_data['customer'] = platform_customer
                else:
                    base_data['customer'] = Customer.objects.create(**{base_data['platform_key']: user_id})
        else:
            if session_customer:
                base_data['customer'] = session_customer
            else:
                base_data['customer'] = Customer.objects.create()
        return base_data

    def get_db_customer(self, key_name, customer_id):
        """
        Ищет пользователя в базе данных по указанному ключу и идентфиикатору
        :param key_name: (vk_id | fb_id | ok_id)
        :param customer_id: string (значение поля key_name)
        :return: (Customer() | None)
        """
        customers = Customer.objects.filter(**{key_name: customer_id})
        if customers:
            customer = customers[0]
            for c1 in customers[1:]:
                customer = self.merge_customers(customer, c1)
            return customer
        return None

    def merge_carts(self, main_cart, cart):
        if main_cart == cart:
            return main_cart
        main_cartitems = main_cart.items.all()
        for cart_item in cart.items.all():
            existed_items = main_cartitems.filter(product=cart_item.product, offer=cart_item.offer)
            if existed_items:
                main_item = existed_items[0]
                main_item.count += 1
                main_item.save()
                cart_item.delete()
            else:
                cart_item.cart = main_cart
                cart_item.save()
        cart.delete()
        return main_cart

    def merge_customers(self, main_customer, to_customer):
        if main_customer == to_customer:
            return main_customer
        if not main_customer.pk:
            main_customer.save()
        need_save = False
        all_carts = to_customer.carts.all() | main_customer.carts.all()
        current_carts = all_carts.filter(status=False)
        closed_carts = all_carts.filter(status=True)

        cur_carts_by_shop = defaultdict(list)
        for cart in current_carts:
            cur_carts_by_shop[cart.shop_id].append(cart)
        for shop, current_carts in cur_carts_by_shop.iteritems():
            current_cart = None
            if len(current_carts) > 1:
                current_cart = current_carts[0]
                for cart in current_carts[1:]:
                    current_cart = self.merge_carts(current_cart, cart)
            elif len(current_carts) == 1:
                current_cart = current_carts[0]
            if current_cart:
                current_cart.customer = main_customer
                current_cart.save()
        for cart in closed_carts.all():
            cart.customer = main_customer
            cart.save()
            need_save = True
        for comment in to_customer.comments.all():
            comment.customer = main_customer
            comment.save()
            need_save = True
        for order in to_customer.orders.all():
            order.customer = main_customer
            order.save()
            need_save = True
        for field in main_customer._meta.fields:
            if field.name == 'id':
                continue
            main_field_data = getattr(main_customer, field.name)
            if not main_field_data:
                second_field_data = getattr(to_customer, field.name)
                if second_field_data:
                    setattr(main_customer, field.name, second_field_data)
                    need_save = True

        if need_save:
            main_customer.save()
        to_customer.delete()
        return main_customer

    def parse_ok(self, r):
        """
        Получает данные из объекта r(request) для одноклассников
        (то есть на url /ok/)

        :param r: request
        :return: dict()
        """
        data_is_valid = True
        user_id = r.GET.get('logged_user_id', None)
        if not user_id:
            data_is_valid = False
        redirect_str = ''
        if 'custom_args' in r.GET:
            query_string = r.GET['custom_args']
            for k, v in parse_qsl(query_string):
                if k == 'custom_args':
                    redirect_str = v
        return data_is_valid, {
            'user_id': user_id,
            'redirect': redirect_str
        }

    def parse_vk(self, r):
        """
        Получает данные из объекта r(request) для вконтактика
        (то есть на url /vk/)

        :param r: request
        :return: dict()
        """
        data_is_valid = True

        user_id = r.GET.get('user_id', None)
        api_url = r.GET.get('api_url', None)
        api_id = r.GET.get('api_id', None)
        access_token = r.GET.get('access_token', None)
        auth_key = r.GET.get('auth_key', None)
        secret = r.GET.get('secret', None)
        viewer_id = r.GET.get('viewer_id', None)
        viewer_type = r.GET.get('viewer_type', None)
        is_app_user = r.GET.get('is_app_user', None)
        sid = r.GET.get('sid', None)
        group_id = r.GET.get('group_id', None)
        language = r.GET.get('language', None)
        api_result = r.GET.get('api_result', None)
        api_settings = r.GET.get('api_settings', None)
        referrer = r.GET.get('referrer', None)
        vk_hash = r.GET.get('hash', None)
        if not viewer_id:
            data_is_valid = False

        return data_is_valid, {
            'user_id': viewer_id,
            'params': {
                'user_id': user_id,
                'api_url': api_url,
                'api_id': api_id,
                'access_token': access_token,
                'auth_key': auth_key,
                'secret': secret,
                'viewer_type': viewer_type,
                'is_app_user': is_app_user,
                'sid': sid,
                'group_id': group_id,
                'language': language,
                'api_result': api_result,
                'api_settings': api_settings,
                'referrer': referrer,
                'hash': vk_hash,
            }
        }

    def parse_fb(self, r):
        """
        Получает данные из объекта r(request) для фейсбук
        (то есть на url /fb/ и /fb/tab/)

        :param r: request
        :return: dict()
        """
        data_is_valid = False
        data = {
            'user_id': None,
            'redirect': ''
        }
        signed_request = r.POST.get('signed_request', None)
        if signed_request:
            signed_data = facebook.parse_signed_request(
                signed_request, r.shop.shopprofile.fb_app_secret
            )
            if signed_data:
                if 'user_id' in signed_data:
                    data['user_id'] = signed_data['user_id']
                    data_is_valid = True
                if 'app_data' in signed_data:
                    data['redirect'] = signed_data['app_data']
        return data_is_valid, data

    def guard(self, r):
        """
        Это некий guard, который разрешает или запрешает использование этого
        middleware для данной страницы

        :param r: request
        :return: (True|False)
        """
        p = r.path
        return r.shop and (
            p == '/' or
            p.startswith('/vk') or
            p.startswith('/fb/tab') or
            p.startswith('/fb') or
            p.startswith('/ok') or
            p.startswith('/api'))

    def process_request(self, r):
        if self.guard(r):
            app_customer = self.get_customer_data(r)
            self.auth_customer(r, app_customer)
            r.app_customer = app_customer


