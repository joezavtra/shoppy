# coding=utf-8
import sys
import traceback
import hotshot
import os
import tempfile
import functools

from django.shortcuts import render_to_response, get_object_or_404, render
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseServerError
from django.utils import simplejson
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.core.mail import mail_admins
from django.views import defaults

from Core.models import Shop
import settings


def render_to(tmpl):
    def renderer(func):

        @functools.wraps(func)
        def wrapper(request, *args, **kw):
            output = func(request, *args, **kw)
            if not isinstance(output, dict):
                return output
            return render_to_response(
                tmpl,
                output,
                context_instance=RequestContext(request)
            )
        return wrapper
    return renderer


def to_json(func):

    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        try:
            data = func(request, *args, **kwargs)
            ret = HttpResponse(mimetype='application/json')
            simplejson.dump(data, ret, ensure_ascii=False)
            return ret
        except ObjectDoesNotExist, e:
            raise Http404

    return wrapper


def profile_it(log_file):
    """
    Декоратор для профилирования функций
    """

    if not os.path.isabs(log_file):
        log_file = os.path.join(tempfile.gettempdir(), log_file)

    def _outer(f):

        @functools.wraps(f)
        def _inner(*args, **kwargs):
            # Add a timestamp to the profile output when the callable
            # is actually called.
            (base, ext) = os.path.splitext(log_file)
            #base = base + "-" + time.strftime("%Y%m%dT%H%M%S", time.gmtime())
            final_log_file = base + ext

            prof = hotshot.Profile(final_log_file)
            try:
                ret = prof.runcall(f, *args, **kwargs)
            finally:
                prof.close()
            return ret

        return _inner

    return _outer


def only_clients(fun):

    @functools.wraps(fun)
    def view(request, *args, **kwargs):
        if 'shop_id' not in kwargs:
            return HttpResponseServerError(request)
        shop = get_object_or_404(Shop, pk=kwargs.pop('shop_id'))
        kwargs['shop_obj'] = shop
        if request.user.is_superuser:
            return fun(request, *args, **kwargs)
        elif shop.client in request.user.clients.all():
            return fun(request, *args, **kwargs)
        return defaults.permission_denied(request)
    return view


def required_shop_status(fun):

    @functools.wraps(fun)
    def view(r, *args, **kwargs):
        if not r.shop.status:
            return render(r, 'App/deactivated.html')
        return fun(r, *args, **kwargs)
    return view
