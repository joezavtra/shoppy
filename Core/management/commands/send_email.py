# coding=utf-8
import logging
from django.core.mail import get_connection
from django.core.management.base import BaseCommand
from Core.models import EmailQueue


__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

logger = logging.getLogger('app')


class Command(BaseCommand):
    """
    Рассылка сообщений из очереди
    """
    def handle(self, *args, **options):
        connection = get_connection()
        for email in EmailQueue.objects.filter(is_complete=False):
            try:
                m = email.build_email()
                connection.send_messages([m])
                # email.send_email(connection=connection)
                email.is_complete = True
                email.save(update_fields=['is_complete'])
            except Exception as e:
                email.is_complete = False
                email.save(update_fields=['is_complete'])
                logger.error('Can not send email %d. Error: %s' % (email.pk, str(e)))