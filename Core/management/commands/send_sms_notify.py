# coding=utf-8
from django.core.management import BaseCommand
import time
import vkontakte
from Core.models import Shop, Customer

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class Command(BaseCommand):

    def handle(self, *args, **options):
        shop = Shop.objects.get(pk=4)
        vk = vkontakte.API(
            api_id=shop.shopprofile.vk_app_id, api_secret=shop.shopprofile.vk_app_secret
        )
        msg = u"Tol'ko do 8 marta! Pri pokupke v prilozhenii magazina E5 VKontakte www.vk.com/e5_ru poluchite skidku 150 rublej! Skidku mozhno poluchit' tol'ko v prilozhenii!"
        i = 0
        c_ids = shop.cart_set.all().values_list('customer_id', flat=True)
        customers = Customer.objects.filter(id__in=c_ids)
        e = ['102178835', '10397083', '102178835', '1207794', '122807971', '124738707', '124909605', '125169106', '1263837', '12933080', '1302431', '134784717', '134784717', ]
        for vk_id in customers.exclude(vk_id=None).exclude(vk_id='').values_list('vk_id', flat=True):
            if str(vk_id) not in e:
                if i == 3:
                    time.sleep(1)
                    i = 0
                i += 1
                try:
                    vk.secure.sendSMSNotification(uid=vk_id, message=msg)
                    print 1,
                except:
                    print 0,
                print vk_id