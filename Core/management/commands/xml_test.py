# coding=utf-8

from django.core.management.base import BaseCommand
from sabridge import Bridge

from Core.models import Shop


__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
    )

    def handle(self, *args, **options):
        brigde = Bridge()
        table = brigde[Shop]

        res = list(table.select(table.c.id == 4).execute())
        print res
        print table[0][table.c.category]


