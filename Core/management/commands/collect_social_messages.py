# coding=utf-8
import logging
from django.core.management.base import BaseCommand
from django.utils import timezone
from Core.models import Cart, Notification, Customer
from social_alert.models import MessageText, Message

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

logger = logging.getLogger('app.social_alert')


class Command(BaseCommand):

    def handle(self, *args, **options):
        current_date = timezone.now().replace(hour=0, minute=0, second=0)
        date_from = current_date - timezone.timedelta(days=2)
        date_to = current_date - timezone.timedelta(days=1)

        message_text = u'В вашей корзине остались товары. Не забудьте их!'
        texts = MessageText.objects.filter(text=message_text)
        if texts:
            text_obj = texts[0]
        else:
            text_obj = MessageText(text=message_text)
            text_obj.save()

        messages = []
        carts = Cart.objects.filter(
            mdate__gte=date_from,
            mdate__lte=date_to,
            status=False
        ).select_related('customer', 'shop__shopprofile')

        message_date = current_date + timezone.timedelta(days=1)
        for cart in carts:
            if cart.customer.get_platform() == 'vk':
                platform = 'vk'
                customer_id = cart.customer.vk_id
            elif cart.customer.get_platform() == 'fb':
                platform = 'fb'
                customer_id = cart.customer.fb_id
            elif cart.customer.get_platform() == 'ok':
                platform = 'ok'
                customer_id = cart.customer.ok_id
            else:
                logger.error('app_id and app_secret not found for cart "%d"' % cart.pk)
                continue

            try:
                Message.objects.create(
                    customer=customer_id,
                    date=message_date,
                    text=text_obj,
                    shop_profile=cart.shop.shopprofile,
                    platform=platform
                )
            except:
                pass
        Message.objects.bulk_create(messages)
