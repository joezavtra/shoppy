# coding=utf-8
import logging
from django.conf import settings
from django.core.management import BaseCommand
from django.template.loader import render_to_string
from django.utils import timezone
from Core.models import Shop, EmailQueue

l = logging.getLogger('app.email_notification_for_shops')
BCC_EMAIL = 'support@shoppyboom.ru'


class EmailNotificationException(Exception):
    pass


def handle_shop(shop_obj):
    if not shop_obj.email:
        raise EmailNotificationException(
            'Shop object has no email or email is blank')
    if not shop_obj.paid_till:
        raise EmailNotificationException(
            'Shop object has no paid_till or paid_till is blank')

    data = dict(shop=shop_obj)
    html_string = render_to_string('email/shop_notify_oldtimers.html', data)
#    EmailQueue.objects.create(
#        email_from=u"ShoppyBoom.ru <no-reply@shoppyboom.ru>",
#        email_to='pavel@shoppyboom.ru',#shop_obj.email,
#        #bcc=BCC_EMAIL,
#        reply_to=u'Служба заботы Shoppyboom.RU <support@shoppyboom.ru>',
#        subject=u'Только вы знаете, как нам стать лучше',
#        message_type=EmailQueue.MTYPE_HTML,
#        body=html_string
#    )
    print u"http://shoppyboom.ru%sshop/ [%s] %s %s" % (shop_obj.get_cabinet_url(), shop_obj.paid_till, shop_obj.email, shop_obj.name)


class Command(BaseCommand):
    def handle(self, *args, **options):

        # Округляем дату окончания действия платежа до 00:00 следующего дня
        limit_date = timezone.now() - timezone.timedelta(days=3)
        shops = Shop.objects.filter(paid_till__lt=limit_date, status=True, tariff_id=3)
        for shop_obj in shops:
            try:
                #l.debug(
                #    'Start handle shop_obj with pk: "%d"' % shop_obj.pk)
                handle_shop(shop_obj)
                #l.debug(
                #    'Success handle shop_obj with pk: "%d"' % shop_obj.pk)
            except Exception as e:
                l.exception(
                    'Error on handle shop_obj with pk: "%d"' % shop_obj.pk)
