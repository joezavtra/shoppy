# coding=utf-8
import logging
from django.core.mail import get_connection
from django.core.management.base import BaseCommand
from Core.models import *
from Core.amocrm_api import AmoManager

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

logger = logging.getLogger('app')


class Command(BaseCommand):
    """
    Рассылка сообщений из очереди
    """
    def handle(self, *args, **options):
        manager = AmoManager()
        manager.auth()

        for c in Client.objects.filter(cdate__gt='2014-09-01'):
            do_add = True
            print "---\n %s" % c.name
        
            for u in c.users.all():
                print u.email
                ret = manager.search_contact(u.email)
                if ret:
                    print json.dumps(ret, indent=4, ensure_ascii=False)
                    print "skip"
                    continue

                if do_add:
                    do_add = False

                    #lead = {
                    #     'name': u'From Site',
                    #     'status_id': '7467884',
                    #}
                    #print lead
                    #ret  = manager.lead_add([lead])
                    #print json.dumps(ret, indent=4, ensure_ascii=False)
                    #lead_id = ret['response']['leads']['add'][0]['id']
         
         
                    #company = {
                    #    'name': "%d_%s" % (c.id, c.name),
                    #    'responsible_user_id': settings.AMOCRM_USER_BASE_ID,
                    #    'linked_leads_id': [lead_id],
                    #}
                    #print company
                    #ret = manager.company_add([company])
                    #print json.dumps(ret, indent=4, ensure_ascii=False)
                    #company_id = ret['response']['contacts']['add'][0]['id']


                fields = []
                if u.shoppyuser.tel:
                    fields.append({
                        'id': 574594,
                        'values': [{'value': str(u.shoppyuser.tel), 'enum': 'WORK'}]
                    })
                if u.email:
                    fields.append({
                        "id": 574596,
                        'values': [{'value': u.email, 'enum': 'WORK'}]
                    })

                contact = {
                    'name': u.get_full_name(),
                    'responsible_user_id': settings.AMOCRM_USER_BASE_ID,
                    #'linked_leads_id': [lead_id],
                    #'linked_company_id': str(company_id),
                    'custom_fields': fields,
                    'company_name': "%d_%s" % (c.id, c.name),
                }
                print contact
                manager.contact_add([contact])


"""
            except Exception as e:
        logger.error('Cannot send data to AmoCRM: "%s"' % str(e))

    data = u"\n".join(params)
    try:
        send_mail(
            u'Новый подписчик',
            u'Привет, дорогая команда ShoppyBoom!\n\nСпешу сообщить, что у нас '
            u'появился новый подписчик "%s"\n'
            u'Дпо. данные: \n%s\nC уважением,\nВаш верный писатель '
            u'Чукча' % (email, data),
            'Чукча писатель ShoppyBoom.Ru <no-reply@shoppyboom.ru>',
            ['signup@shoppyboom.ru']
        )
    except:
        pass

"""
