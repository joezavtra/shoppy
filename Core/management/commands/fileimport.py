# coding=utf-8
from collections import defaultdict
import gzip
import json
import logging
import os
import subprocess
from datetime import datetime
import time
import traceback
import zipfile
import sys
from optparse import make_option
from HTMLParser import HTMLParser
from django.conf import settings

from django.utils import timezone
from lxml import etree
from django.core.exceptions import ObjectDoesNotExist
from lxml.etree import XMLSyntaxError
import magic
import requests
from django.core.management.base import CommandError
from django.core.management.base import BaseCommand
from Core import utils

from Core.models import Shop, Category, Product, ProductCategories, \
    FileImportLog, Offer, ShopExtra, DataSource
from Core.utils import fast_iter


__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class DBLogger(object):
    def __init__(self):
        self.logger = logging.getLogger('fileimport')
        self.messages = []

    def msg(self, message):
        self.logger.info(message)
        self.messages.append(message)

    def flush(self):
        self.messages = []

    def save(self, ds, categories_created, categories_deactivated, categories_total, products_created, products_deactivated, products_total):
        obj = FileImportLog(
            datasource=ds,
            timestamp=timezone.now(),
            report_text="\n".join(self.messages),
            categories_created=categories_created,
            categories_deactivated=categories_deactivated,
            categories_total=categories_total,
            products_created=products_created,
            products_deactivated=products_deactivated,
            products_total=products_total
        )
        obj.save()
        l.msg('Log: %d' % obj.id)
        self.flush()

    def error(self, ds, error_msg):
        obj = FileImportLog(
            datasource=ds,
            timestamp=timezone.now(),
            report_text="\n".join(self.messages),
            text_err=error_msg)
        obj.save()
        l.msg('Log: %d' % obj.id)
        self.flush()
        ds.processing = None
        ds.next_fetch = timezone.now() + timezone.timedelta(hours=2)
        ds.last_fetch = timezone.now()
        ds.save()

l = DBLogger()


# Получение данных (YML файл)
#   Подгружаем файл из интернета
#   Или по указанному пути













def get_offer_data(elem, parser):
    elem_id = str(elem.get('id'))
    data = {
        'oid': elem_id,
        'model_oid': elem_id,
        'images': '#|#'.join([pic.text for pic in elem.findall('picture') or []]),
        'name': '',
        'desc': '',
        'price': 0.0,
        'url': '',
        'available': True,
        'store': True,
        'delivery': True,
        'pickup': True,
        'categories': [],
        'params': '{}',
    }
    available = elem.get('available', None)
    if available and available == 'false':
        data['available'] = False
    p_name = elem.find('name')
    p_description = elem.find('description')
    p_url = elem.find('url')
    p_price = elem.find('price')
    model = elem.find('model')
    for el in ('store','delivery','pickup'):
        node = elem.find(el)
        if node is not None and node.text is not None:
            if node.text == 'false':
                data[el] = False
    if model is not None and model.text is not None:
        data['model_oid'] = model.text
    if p_name is not None and p_name.text is not None:
        data['name'] = parser.unescape(p_name.text)
    else:
        type_prefix = elem.find('typePrefix')
        vendor = elem.find('vendor')
        model = elem.find('model')
        if type_prefix is not None and type_prefix.text is not None:
            data['name'] = type_prefix.text
        if vendor is not None and vendor.text is not None:
            data['name'] += ' ' + vendor.text
        if model is not None and model.text is not None:
            data['name'] += ' ' + model.text
    if p_description is not None and p_description.text is not None:
        data['desc'] = parser.unescape(p_description.text)
    if p_url is not None and p_url.text is not None:
        data['url'] = p_url.text
    if p_price is not None and p_price.text is not None:
        data['price'] = float(p_price.text)

    for e in elem.findall('categoryId') + elem.findall('categoryID'):
        data['categories'].append(e.text)
    params = []
    for e in elem.findall('param'):
        params.append((e.get('name'), e.text))
    data['params'] = json.dumps(dict(params))
    return data


def category_handler_base(elem, categories):
    catId = elem.get('id')
    catParentId = elem.get('parentId', None)
    catName = elem.text

    if catParentId is not None and int(catParentId) == 0:
        catParentId = None

    categories.append((
        catId, catParentId, catName
    ))


class PidLock(object):
    def __init__(self, ds_pk, tmp_dir):
        self.pidfile = os.path.join(tmp_dir, str(ds_pk) + '.pid')

    def lock(self):
        pid = str(os.getpid())
        if os.path.isfile(self.pidfile):
            return False
        else:
            file(self.pidfile, 'w').write(pid)
            return True

    def release(self):
        os.unlink(self.pidfile)


def offer_handler_new(elem, parser, oid2offer, model2oid):
    data = get_offer_data(elem, parser)
    oid = str(data['oid'])
    oid2offer[oid] = data
    model2oid[data['model_oid']].append(oid)


def worker(ds, filename, options):
    ds = DataSource.objects.get(pk=ds.pk)
    try:
        l.msg('Start worker with datasource "%d"' % ds.pk)
        if options['force']:
            processing_ds = True
        else:
            processing_ds = (ds.processing is None) or (ds.processing is not None and ds.processing < timezone.now() - timezone.timedelta(minutes=15))
        if processing_ds:
            l.msg('Processing datasource "%d"' % ds.pk)
            tz_now = timezone.now()
            l.msg('Set processing time to "%s"' % str(tz_now))
            ds.processing = tz_now
            ds.save()
            counters = dict(
                cats_yml=0,
                cats_db=0,
                cats_created=0,
                cats_deactivated=0,
                p_yml=0,
                p_db=0,
                p_created=0,
                p_deactivated=0,
                p_name_blank=0,
                p_desc_blank=0
            )

            try:
                p = etree.XMLParser(remove_blank_text=True, resolve_entities=False)
                etree.parse(filename, p)
                l.msg('XML is valid.')
            except XMLSyntaxError as e:
                l.msg('Error on open xml file "%s": %s' % (filename, e))
                l.error(ds, u'Произошла ошибка при попытке прочитать YML. '
                            u'Текст ошибки: "%s"' % str(e))
                return
            # Категории
            doc_categories = etree.iterparse(
                filename, events=('end',), tag='category'
            )
            l.msg('Import categories from YML...')

            # categories = {}
            categories_base = []
            fast_iter(doc_categories, category_handler_base, dict(categories=categories_base))
            category_res = {}

            def get_category2(cat_id, categories):
                res = {}
                order = 0
                for category_id, parent_id, cat_name in filter(lambda item: item[1] == cat_id, categories):
                    order += 1
                    res[category_id] = {
                        'name': cat_name,
                        'order': order,
                        'child': get_category2(category_id, categories)
                    }
                return res

            order = 0
            l.msg('Create dict of categories')
            for cat_id, parent_id, cat_name in categories_base:
                if parent_id is None:
                    order += 1
                    category_res[cat_id] = {
                        'name': cat_name,
                        'order': order,
                        'child': get_category2(cat_id, categories_base)
                    }
            # print category_res
            root_cat_oid = ds.root_cat_oid
            if root_cat_oid:
                l.msg('Filter cats for datasource by root_cat_oid "%s"' % str(root_cat_oid))

                def filter_cats(categories):
                    if root_cat_oid in categories:
                        return categories[root_cat_oid]
                    # print categories
                    for category in categories.itervalues():
                        res = filter_cats(category['child'])
                        if res:
                            return res

                category_res = filter_cats(category_res)['child']

            def create_cats(cats, current_cats, cur_count, lvl, res=None, parent=None):
                if not res:
                    res = dict()
                cur_count.n += 1
                if cur_count.n % 10 == 0:
                    l.msg('--- Imported: %d' % cur_count.n)
                for cat_oid, cat_data in cats.items():
                    cat_obj = current_cats.get(cat_oid, None)
                    if cat_obj is None:
                        cat_obj = Category(
                            datasource=ds,
                            oid=cat_oid,
                            order=cat_data['order']
                        )
                    cat_obj.name = cat_data['name']
                    cat_obj.lvl = lvl
                    if parent is not None:
                        cat_obj.parent = parent
                    cat_obj.save()
                    res[cat_oid] = cat_obj
                    create_cats(cat_data['child'], current_cats, cur_count, lvl+1, res, cat_obj)
                return res

            l.msg('Create cats...')
            current_cats = dict()
            class N(object):
                n = 0
            for cat in Category.objects.filter(datasource=ds):
                current_cats[unicode(cat.oid)] = cat
            cur_count = N()
            lvl = 1
            categories = create_cats(category_res, current_cats=current_cats, cur_count=cur_count, lvl=lvl)
            l.msg('Cats created')
            l.msg('Deactivate cats...')
            all_cats = Category.objects.filter(datasource=ds)
            cats_for_deactivate = all_cats.exclude(pk__in=map(lambda x: x.pk, categories.values()))
            cats_for_activate = all_cats.exclude(pk__in=map(lambda x: x.pk, cats_for_deactivate.all()))
            cats_for_activate.update(status=True)
            cats_deactivated = cats_for_deactivate.count()
            cats_for_deactivate.update(status=False)
            l.msg('Cats deactivated')
            counters['cats_yml'] = len(categories)
            counters['cats_db'] = all_cats.count()

            l.msg('Categories is imported')

            l.msg('Import products from YML...')

            context = etree.iterparse(filename, events=('end',), tag='offer')
            parser = HTMLParser()

            counters['p_db'] = Product.objects.filter(datasource=ds).count()
            p_active_cnt = Product.objects.filter(
                datasource=ds, status=True
            ).count()

            t = time.time()

            products = Product.objects.filter(datasource=ds)
            offers = Offer.objects.filter(product__in=products).select_related('product')
            offers_dict = dict(map(lambda offer: (str(offer.oid), offer), offers))
            # fast_iter(
            #     context,
            #     offer_handler,
            #     dict(
            #         ds=ds,
            #         p_yml_oids=p_yml_oids,
            #         parser=parser,
            #         categories=categories,
            #         cur_position=cur_position,
            #         products=dict(map(lambda product: (unicode(product.model_oid).lower(), product), products)),
            #         offers=dict(map(lambda offer: (str(offer.oid), offer), offers))
            #     )
            # )
            oid2offer = dict()
            model2oid = defaultdict(list)
            fast_iter(
                context,
                offer_handler_new,
                dict(parser=parser, oid2offer=oid2offer, model2oid=model2oid)
            )
            for i, (oid, offer_data) in enumerate(oid2offer.iteritems()):
                if i % 100 == 0:
                    l.msg('--- Imported: %d' % i)
                if oid in offers_dict:
                    offer = offers_dict[oid]
                    product = offer.product
                else:
                    product = None
                    offer = Offer(
                        oid=offer_data['oid']
                    )

                if not product:
                    if offer_data['model_oid'] in model2oid:
                        for oid2 in model2oid[offer_data['model_oid']]:
                            if oid2 in offers_dict:
                                product = offers_dict[oid2].product
                                if product.model_oid != offer_data['model_oid']:
                                    product = None
                                break
                    if not product:
                        product = Product.objects.create(
                            datasource=ds,
                            model_oid=offer_data['model_oid']
                        )

                offer.product = product
                offer.params = offer_data['params']
                offer.url = offer_data['url']
                offer.price = offer_data['price']
                offer.images = offer_data['images']
                offer.status = offer_data['delivery'] or offer_data['pickup']
                offer.available = offer_data['available']
                offer.save()
                offers_dict[oid] = offer

                product = offer.product
                product.oid = offer_data['oid']
                product.name = offer_data['name']
                product.desc = offer_data['desc']
                product.misc = offer_data['params']
                product.images = offer_data['images']
                product.status = True
                product.url = offer_data['url']
                product.price = offer_data['price']
                if product.pk:
                    product_offer_params = list()
                    for product_offer_param in product.offers.all().values_list('params', flat=True):
                        try:
                            data = json.loads(product_offer_param)
                            product_offer_params.append(data.items())
                        except:
                            product_offer_params.append([])
                    if product_offer_params:
                        product_misc = utils.get_similar_items(product_offer_params)
                        product.misc = json.dumps(dict(product_misc))

                product.save()
                product_cat_for_create = []
                for cat_oid in offer_data['categories']:
                    category_obj = categories.get(cat_oid, None)
                    if category_obj:
                        product_cat_for_create.append(ProductCategories(
                            product=product,
                            category=category_obj
                        ))
                ProductCategories.objects.filter(product=product).delete()
                ProductCategories.objects.bulk_create(product_cat_for_create)
                offers_dict[offer_data['oid']] = offer


            l.msg('Products imported')
            l.msg('Deactivate products...')
            # Отключаем те Offer, oid которых нет в YML
            Offer.objects.all()\
                .select_related('product__datasource')\
                .filter(status=True, product__datasource=ds)\
                .exclude(oid__in=oid2offer.keys())\
                .update(status=False)

            # Отключаем те продукты, у которых нет активных offers
            products_with_active_offers_count = Product.objects\
                .filter(datasource=ds)\
                .extra(select={'active_offers_count': 'select count(*) from Core_offer where status=1 and available=1 and product_id=Core_product.id'})\
                .only('id')\
                .values_list('id', 'active_offers_count')
            products_with_0_active_offers = map(lambda x: x[0], filter(lambda x: x[1] == 0, products_with_active_offers_count))
            products_for_deactivate = Product.objects.filter(pk__in=products_with_0_active_offers)
            p_deactivated = products_for_deactivate.count()
            products_for_deactivate.update(status=False)

            l.msg('Products is deactivated')
            l.msg('Update category product_count...')
            for cat in Category.objects.filter(parent=None, datasource=ds):
                l.msg('--- Update category "%d"' % cat.id)
                cat.upd_product_cnt()
            l.msg('Category product_count is updated')
            l.msg('=' * 80)
            l.msg('Categories in YML: %d' % counters['cats_yml'])
            l.msg('Categories in DB: %d' % counters['cats_db'])
            l.msg('Categories created: %d' % counters['cats_created'])
            l.msg('Categories deactivated: %d' % cats_deactivated)
            l.msg('Offer tags in YML: %d' % len(oid2offer.keys()))
            l.msg('Products in DB: %d' % counters['p_db'])
            p_created = Product.objects.filter(
                datasource=ds
            ).count() - counters['p_db']
            l.msg('Products created: %d' % p_created or 0)
            l.msg('Products deactivated: %d' % p_deactivated or 0)
            l.msg('=' * 80)
            l.msg('Total:')
            categories_total = ds.categories.all().count()
            products_total = ds.products.all().count()
            l.msg('Categories: %d' % categories_total)
            l.msg('Products: %d' % products_total)
            #Слишком тяжело
            #l.msg('Offers: %d' % Offer.objects.filter(product__in=ds.products.all()).count())
            l.save(ds, counters['cats_created'], cats_deactivated, categories_total, p_created, p_deactivated, products_total)
            ds.processing = None
            ds.next_fetch = timezone.now() + timezone.timedelta(hours=2)
            ds.last_fetch = timezone.now()
            ds.save()

            for s in ds.shops.all():
                s.clear_category_cache()

    finally:
        pass


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            '--shop_id',
            action='store',
            help='Internal shop ID',
            dest='shop_id'),
        make_option(
            '--ds_id',
            action='store',
            help='Internal datasource ID',
            dest='ds_id'),
        make_option(
            '--filename',
            action='store',
            help='Path to file for parse',
            dest='filename'),
        make_option(
            '--auto',
            action='store_true',
            help='If this param is "1" - program will be scan all shops',
            dest='auto',
            default=False),
        make_option(
            '--force',
            action='store_true',
            help='If enabled - importer work with all shops',
            dest='force',
            default=False),
        make_option(
            '--tmp_dir',
            action='store',
            help='Temporary directory for store downloaded YML from '
                 'Datasource.yml_url. Default "/tmp"',
            dest='tmp_dir',
            default=settings.FILEIMPORT_TMP_DIR),
    )

    def handle_worker(self, *args, **options):
        l.msg('Start fileimport...')
        tmp_dir = options['tmp_dir']
        l.msg('Temp directory: ' + tmp_dir)
        if not os.path.exists(tmp_dir):
            l.msg('Temp directory "%s" does not exists. Create it...' % tmp_dir)
            try:
                os.makedirs(tmp_dir)
                l.msg('Temp dir "%s" is created' % tmp_dir)
            except os.error:
                l.msg('Cannot create temp directory "%s". Create this dir'
                      ' manually or choose another dir' % tmp_dir)
                raise CommandError('Cannot create tmp_dir')
        if options['auto'] is True:
            l.msg('Run in auto mode')

            datasources = DataSource.objects.filter(mode=DataSource.MODE_AUTO)
            if options['force']:
                l.msg('Use option "force". Update all automatic datasources')
            else:
                l.msg('Option "force" is not enable')
                tz_now = timezone.now()
                datasources = datasources.filter(next_fetch__lte=tz_now)
                datasources = datasources.filter(
                    processing=None
                ) | datasources.filter(
                    processing__lte=tz_now - timezone.timedelta(minutes=15)
                )

            if options['shop_id']:
                l.msg('Use option "shop_id". Shop id is "%s"' % options['shop_id'])
                try:
                    shop = Shop.objects.get(pk=options['shop_id'])
                except:
                    raise CommandError('Shop with shop_id="%s" is NOT found' % options['shop_id'])
                datasources = datasources.filter(id__in=[d.id for d in shop.datasources.all()])

            if options['ds_id']:
                datasources = datasources.filter(pk=options['ds_id'])

            for ds in datasources.all():
                l.flush()
                directory = os.path.join(
                    tmp_dir,
                    str(ds.pk),
                    datetime.now().strftime('%Y-%m-%d')
                )
                if not os.path.exists(directory):
                    os.makedirs(directory)
                l.msg('Directory to download YML: "%s"' % directory)
                filename = os.path.join(
                    directory, datetime.now().strftime('%H-%M') + '.tmp'
                )
                l.msg('Filename to save YML: "%s"' % filename)

                yml_url = ds.yml_url
                l.msg('Try to download YML from "%s"' % yml_url)
                try:
                    headers = {'User-Agent': 'ShoppyBoom.RU bot'}
                    r = requests.get(yml_url, stream=True, headers=headers)
                    l.msg('Server status on download YML: %d' % r.status_code)
                except Exception as e:
                    l.msg('Error! Exception on download file. Message: "%s"' % e.message)
                    l.msg('Ignore datasource "%d"' % ds.pk)
                    l.error(
                        ds, u'Произошла проблема '
                            u'при загрузке YML по ссылке "%s". '
                            u'Текст ошибки: "%s"' % (yml_url, str(e)))
                    continue
                if r.status_code not in settings.FILEIMPORT_VALID_STATUS_CODES:
                    valid_codes = ','.join(
                        map(str, settings.FILEIMPORT_VALID_STATUS_CODES))
                    l.msg(
                        'Error! Status code for URL "%s" is not in "%s". '
                        'Status is "%d"' % (
                            ds.yml_url, valid_codes, r.status_code)
                    )
                    l.msg('Ignore datasource "%d"' % ds.pk)
                    l.error(ds, u'При попытке загрузить YML "%s" сервер вернул '
                                u'код ответа: "%d". '
                                u'Код ответа должен быть одним из: "%s"' % (
                                    yml_url, r.status_code, valid_codes))
                    continue

                l.msg('Save data into "%s"...' % filename)
                with open(filename, 'wb') as f:
                    f.write(r.content)
                l.msg('Data saved. Check mimetype')

                mimetype = magic.from_file(filename, mime=True)

                if mimetype in ('application/gzip', 'application/x-gzip'):
                    l.msg('Data has mimetype "%s". Try to '
                          'decompress...' % mimetype)
                    try:
                        with gzip.open(filename, 'rb') as f_gz:
                            with open(filename + '.yml', 'wb') as f_yml:
                                f_yml.write(f_gz.read())
                        filename += '.yml'
                        l.msg('Decompressing is ok. '
                              'Unzip file "%s"' % filename)
                    except:
                        l.msg('Error on decompress file "%s". '
                              'Ignore datasource' % filename)
                        l.error(ds, u'Произошла проблема '
                                    u'при попытке разархивировать YML')
                        continue

                if mimetype == 'application/zip':
                    l.msg('Mimetype is "%s". Try to decompress' % mimetype)
                    z_file = zipfile.ZipFile(filename)
                    for name in z_file.namelist():
                        if os.path.splitext(name)[-1] in ['.xml', '.yml']:
                            filename += '.yml'
                            with open(filename, 'wb') as fd:
                                fd.write(z_file.read(name))
                                l.msg('Unzip is ok.')
                                break
                try:
                    worker(ds, filename, options)
                except Exception as e:
                    l.msg(str(traceback.format_exc()))
                    l.error(ds, u'Произошла непредвиденная ошибка')

        else:
            if options['filename'] is None:
                raise CommandError('Filename is required')
            if options['ds_id'] is None:
                raise CommandError('Datasource ID is required')
            filename = options['filename']
            ds_id = int(options['ds_id'])
            if not os.path.exists(filename):
                raise CommandError('File "%s" does not exists' % filename)
            try:
                ds = DataSource.objects.get(pk=ds_id)
            except ObjectDoesNotExist:
                raise CommandError('Datasource with pk "%d" not exists' % ds_id)

            if ds.mode != DataSource.MODE_AUTO:
                raise CommandError('Datasource with pk "%d" is not in auto mode' % ds_id)
            worker(ds, filename, options)

        #for shop in Shop.objects.all():
        #    products = Product.objects.filter(
        #        datasource__in=shop.datasources.all(), status=True)
        #    max_products = shop.tariff.max_products
        #    if not max_products:
        #        continue
        #    if products.count() > max_products:
        #        p_ids = set(products[max_products:].values_list('pk', flat=True))
        #        Product.objects.filter(pk__in=p_ids).update(status=False)

    def handle(self, *args, **options):
        if not options['force']:
            bashCommand = "ps aux | grep python | grep fileimport | grep -v grep | wc -l"
            process = subprocess.Popen(bashCommand, shell=True, stdout=subprocess.PIPE)
            count = int(process.stdout.read())
            if count>2:
                raise CommandError('Too many fileimporters at same time. Went to have some rest')
                exit()
        self.handle_worker(*args, **options)
