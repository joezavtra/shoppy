# coding=utf-8
from Core.models import Shop
from django.core.management import BaseCommand
from django.utils import timezone
import logging

__author__ = 'Prosto Chewey <prosto.chewey@gmail.com>'

l = logging.getLogger('app.block_shops_after_paid_till')


class Command(BaseCommand):
    def handle(self, *args, **options):
        l.debug('Start block_shops_after_paid_till')
        now = timezone.now()

        l.debug('Find shops with paid_till__lte = "%s"' % str(now))
        shops = Shop.objects.filter(paid_till__lte=now, status=True)
        shops_count = shops.count()
        if shops_count:
            shops_ids = ','.join(map(str, shops.values_list('id', flat=True)))
            l.debug('Founded %d shops with pk: "%s"' % (shops_count, shops_ids))

            l.debug('Trying to deactivate...')
            try:
                shops.update(status=False)
                l.debug('Shops deactivated')
            except:
                l.exception('Error on deactivate shops')
        else:
            l.debug('All shops with paid_till datetime '
                    'less than now() already deactivated')

        l.debug('End block_shops_after_paid_till')