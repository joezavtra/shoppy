# coding=utf-8
import csv
import json
import logging
import os
from optparse import make_option
import traceback
from datetime import datetime
import urllib2

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import CommandError
from django.core.management.base import BaseCommand
from django.db import transaction
import time
import facebook
from odnoklassniki_api import Odnoklassniki
import vkontakte

from Core.models import Shop, DeliveryPoint, City
from Core.utils import split_list


__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

l = logging.getLogger('app')

class Command(BaseCommand):

    def handle(self, *args, **options):
        l.info('Start get_social_profiles')
        try:
            l.info('Start Facebook users')
            self.get_fb_users()
            l.info('End Facebook users')
        except:
            l.exception('Facebook get_social_profiles')
        try:
            l.info('Start Vk users')
            self.get_vk_users()
            l.info('End Vk users')
        except:
            l.exception('Vkontakte get_social_profiles')
        try:
            l.info('Start Ok users')
            self.get_ok_users()
            l.info('End Ok users')
        except:
            l.exception('Odnoklassniki get_social_profiles')

    def get_ok_users(self):
        for shop in Shop.objects.all().select_related('shopprofile'):
            l.info('Shop: %d' % shop.pk)
            if not shop.shopprofile.ok_app_public or not shop.shopprofile.ok_app_secret:
                l.info('-- Ignore: has not ok_app_public or ok_app_secret')
                continue
            try:
                l.info('Auth Odnoklassniki API...')
                ok = Odnoklassniki(
                    app_key=shop.shopprofile.ok_app_public,
                    app_secret=shop.shopprofile.ok_app_secret
                )
                l.info('-- Ok')
            except Exception:
                l.info('-- Fail. Ignore shop')
                continue

            l.info('Get Ok users...')
            db_ok_users = dict()
            for cart in shop.cart_set.all().select_related('customer').exclude(customer__ok_id=None).exclude(customer__ok_id=''):
                db_ok_users[cart.customer.ok_id] = cart.customer
            l.info('-- Ok. users cnt: %d' % len(db_ok_users))

            i = 3
            l.info('Iter by DB users pools/stacks (by 100 users)...')
            for users_ids in split_list(db_ok_users.keys(), 100):
                if i == 0:
                    i = 3
                    time.sleep(1)
                i -= 1

                l.info('-- Get Ok users...')
                try:
                    ok_users = ok.users.getInfo(
                        uids=','.join(users_ids),
                        fields='uid,first_name,last_name,age,location,gender,birthday'
                    )
                    l.info('-- Ok. Found users cnt: %d' % len(ok_users))
                except:
                    l.exception('-- Error. See traceback. '
                                'Ignore this stack. '
                                'User list: %s' % ','.join(map(str, users_ids)))
                    continue

                l.info('-- Iter by found users stack...')
                for user in ok_users:
                    user_id = str(user['uid'])
                    if user_id not in db_ok_users:
                        l.info('---- [ ! ] UserId "%s" from Ok '
                               'not in DB. Ignore user' % str(user_id))
                        continue
                    city_name = (user.get('location', {}) or {}).get('city', None)
                    if city_name is not None:
                        city_obj, created = City.objects.get_or_create(name=city_name)
                        if created:
                            city_obj.save()
                    else:
                        city_obj = None
                    u = db_ok_users[user_id]
                    u.name = user['first_name']
                    u.last_name = user['last_name']
                    # u.nickname = user['domain']
                    bdate = user.get('birthday', None)
                    if bdate:
                        try:
                            bdate = datetime.strptime(bdate, '%Y-%m-%d')
                        except:
                            bdate = datetime.strptime('1904-'+bdate, '%Y-%m-%d')
                        u.bdate_y = None if bdate.year == 1904 else bdate.year
                        u.bdate_m = bdate.month
                        u.bdate_d = bdate.day
                    u.city = city_obj
                    if user.get('gender', None):
                        if user['gender'] == 'male':
                            sex = 2
                        else:
                            sex = 1
                    else:
                        sex = None
                    u.sex = sex
                    u.save()
                l.info('-- End stack.')
            l.info('-- End Iter')

    def get_fb_users(self):
        for shop in Shop.objects.all().select_related('shopprofile'):
            l.info('Shop: %d' % shop.pk)
            if not shop.shopprofile.fb_app_id or not shop.shopprofile.fb_app_secret:
                l.info('-- Ignore: has not fb_app_id or fb_app_secret')
                continue
            try:
                l.info('Get Facebook token...')
                fb_token = facebook.get_app_access_token(app_id=shop.shopprofile.fb_app_id, app_secret=shop.shopprofile.fb_app_secret)
                l.info('-- Ok. Token: %s' % str(fb_token))
                fb = facebook.GraphAPI(access_token=fb_token)
            except urllib2.HTTPError:
                l.info('-- Fail. Ignore shop')
                continue

            l.info('Get Facebook users...')
            db_fb_users = dict()
            for cart in shop.cart_set.all().select_related('customer').exclude(customer__fb_id=None).exclude(customer__fb_id=''):
                db_fb_users[cart.customer.fb_id] = cart.customer
            l.info('-- Ok. users cnt: %d' % len(db_fb_users))

            l.info('Build FQL...')
            fields = ','.join(['uid', 'first_name', 'last_name', 'username', 'sex', 'birthday_date'])
            user_ids = ','.join(map(str, db_fb_users.keys()))
            fql = 'select %s from user where uid in (%s)' % (fields, user_ids)
            l.info('-- Ok. FQL: %s' % str(fql))

            l.info('Execute FQL...')
            fb_users = fb.fql(fql)
            l.info('-- Ok. Found users cnt: %d' % len(fb_users))

            l.info('Iter by found users...')
            for user in fb_users:
                user_id = str(user['uid'])
                if user_id not in db_fb_users:
                    l.info('-- [ ! ] UserId "%s" from Facebook '
                           'not in DB. Ignore user' % str(user_id))
                    continue
                u = db_fb_users[user_id]
                u.name = user['first_name']
                u.last_name = user['last_name']
                u.nickname = user['username']
                bdate = user.get('birthday_date', None)
                if bdate:
                    try:
                        bdate = datetime.strptime(bdate, '%d/%m/%Y')
                    except:
                        bdate = datetime.strptime(bdate+'/1904', '%d/%m/%Y')
                    u.bdate_y = None if bdate.year == 1904 else bdate.year
                    u.bdate_m = bdate.month
                    u.bdate_d = bdate.day

                sex = user.get('sex', None)
                if sex == 'female':
                    u.sex = 2
                elif sex == 'male':
                    u.sex = 1
                else:
                    u.sex = None
                u.save()
            l.info('-- Ok. Iter is over')

    def get_vk_users(self):
        for shop in Shop.objects.all().select_related('shopprofile'):
            l.info('Shop: %d' % shop.pk)
            if not shop.shopprofile.vk_app_id or not shop.shopprofile.vk_app_secret:
                l.info('-- Ignore: has not vk_app_id or vk_app_secret')
                continue
            try:
                l.info('Auth Vk API...')
                vk = vkontakte.API(api_id=shop.shopprofile.vk_app_id, api_secret=shop.shopprofile.vk_app_secret, timeout=10, v='5.21')
                l.info('-- Ok')
            except vkontakte.VKError:
                l.info('-- Fail. Ignore shop')
                continue

            l.info('Get Vk users...')
            db_vk_users = dict()
            for cart in shop.cart_set.all().select_related('customer').exclude(customer__vk_id=None).exclude(customer__vk_id=''):
                db_vk_users[cart.customer.vk_id] = cart.customer
            l.info('-- Ok. users cnt: %d' % len(db_vk_users))

            i = 3
            l.info('Iter by DB users pools/stacks (by 1000 users)...')
            for users_ids in split_list(db_vk_users.keys(), 1000):
                if i == 0:
                    i = 3
                    time.sleep(1)
                i -= 1
                if '0' in users_ids:
                    users_ids.remove('0')

                l.info('-- Get Vk users...')
                try:
                    vk_users = vk.users.get(
                        user_ids=','.join(users_ids),
                        fields='bdate,sex,city,relation,domain'
                    )
                    l.info('-- Ok. Found users cnt: %d' % len(vk_users))
                except:
                    l.exception('-- Error. See traceback. '
                                'Ignore this stack. '
                                'User list: %s' % ','.join(map(str, users_ids)))
                    continue

                l.info('-- Iter by found users stack...')
                for user in vk_users:
                    user_id = str(user['id'])
                    if user_id not in db_vk_users:
                        l.info('---- [ ! ] UserId "%s" from Vk '
                               'not in DB. Ignore user' % str(user_id))
                        continue
                    city_name = (user.get('city', {}) or {}).get('title', None)
                    if city_name is not None:
                        city_obj, created = City.objects.get_or_create(name=city_name)
                        if created:
                            city_obj.save()
                    else:
                        city_obj = None
                    u = db_vk_users[user_id]
                    u.name = user['first_name']
                    u.last_name = user['last_name']
                    u.nickname = user['domain']
                    bdate = user.get('bdate', None)
                    if bdate:
                        try:
                            bdate = datetime.strptime(bdate, '%d.%m.%Y')
                        except:
                            bdate = datetime.strptime(bdate+'.1904', '%d.%m.%Y')
                        u.bdate_y = None if bdate.year == 1904 else bdate.year
                        u.bdate_m = bdate.month
                        u.bdate_d = bdate.day

                    u.city = city_obj
                    u.sex = user.get('sex', None)
                    u.relation = user.get('relation', None)
                    u.save()
                l.info('-- End stack.')
            l.info('-- End Iter')