# coding=utf-8
import csv
import json
import os
from optparse import make_option

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import CommandError
from django.core.management.base import BaseCommand
from django.db import transaction

from Core.models import Shop, DeliveryPoint, City


__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


def line_handler(line):
    try:
        return {
            'oid': line[0],
            'name': line[1],
            'point_name': line[4],
            'city': line[9],
            'lat': float(line[13]),
            'lng': float(line[14]),
            'json_data': {
                'email': line[8],
                'telephone': line[7],
                'address': line[6],
                'work_time': line[15] + ' ' + line[16],
                'time_to_delivery': line[17],
            }
        }
    except:
        return None


class Command(BaseCommand):
    """
    Импорт ПВЗ из csv
    """
    option_list = BaseCommand.option_list + (
        make_option(
            '--shop-id',
            action='store',
            help='Internal shop ID',
            dest='shop_id'),
        make_option(
            '--filename',
            action='store',
            help='Path to file for parse',
            dest='filename'),
    )

    def handle(self, *args, **options):
        if options['filename'] is None:
            raise CommandError('Filename is required')
        if options['shop_id'] is None:
            raise CommandError('Shop ID is required')
        filename = options['filename']
        shop_id = int(options['shop_id'])
        if not os.path.exists(filename):
            raise CommandError('File "%s" does not exists' % filename)
        try:
            shop = Shop.objects.get(pk=shop_id)
        except ObjectDoesNotExist:
            raise CommandError('Shop with pk "%d"' % shop_id)

        reader = csv.reader(
            open(filename, 'rb'),
            delimiter=';',
            quotechar='"',
            lineterminator="\n"
        )
        reader.next()
        with transaction.commit_on_success():
            for line in reader:
                data = line_handler(line)
                if data is not None:
                    city_obj, created = City.objects.get_or_create(
                        name=data['city']
                    )
                    city_obj.save()
                    transaction.commit()
                    print data['city'], city_obj.id, city_obj.name

                    point_obj, created = DeliveryPoint.objects.get_or_create(
                        shop=shop,
                        oid=data['oid'],
                        lat=data['lat'],
                        lng=data['lng']
                    )

                    point_obj.city = city_obj
                    point_obj.point_name = data['point_name']
                    point_obj.name = data['name']
                    point_obj.lat = data['lat']
                    point_obj.lng = data['lng']
                    point_obj.json_data = json.dumps(data['json_data'])
                    point_obj.save()
