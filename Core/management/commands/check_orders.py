# coding=utf-8
import logging
import urllib
from django.core.management import BaseCommand
import requests
import hashlib
from Core.models import Order

import socket
import gevent
from gevent.queue import Queue
from gevent import monkey

monkey.patch_socket()
monkey.patch_ssl()

__author__ = 'Prosto Chewey <prosto.chewey@gmail.com>'

l = logging.getLogger('app.check_orders')


def handle_task(order_pk, ext_order_id):
    url = 'http://www.e5.ru/partners/getOrderStatus/'
    secret_key = 'VK-ShoppyBoom'
    source = '82'
    params = {
        'source': source,
        'order_id': ext_order_id,
        'hash': hashlib.md5(source + ext_order_id + secret_key).hexdigest()
    }
    res_url = ''.join([url, '?', urllib.urlencode(params)])
    r = requests.get(res_url)
    data = r.json()
    if not data.get('success', True):
        l.error('Status == false for url "%s"' % res_url)
        return
    status = data['orderStatus']

    order = Order.objects.get(pk=order_pk)

    if status == 'new':
        order.status = Order.STATUS_NEW
        # заказ только что поступил
        pass
    elif status == 'wait':
        order.status = Order.STATUS_IN_PROGRESS
        # заказ в обработке
        pass
    elif status == 'done':
        order.status = Order.STATUS_PAID
        # заказ выкуплен покупателем
        pass
    elif status == 'cancel':
        order.status = Order.STATUS_CANCELED
        # заказ отменен
        pass
    else:
        l.error('Unknown orderStatus field in response. '
                'Url: "%s" Response: "%s"' % (res_url, r.content))
        return
    order.save()
    l.info('Success update order info. '
           'Order PK: "%s" Status: "%s"' % (order_pk, order))


def worker(tasks):
    while True:
        task = tasks.get()
        if not task:
            tasks.put(None)
            break
        order_pk, ext_order_id = task
        handle_task(order_pk, ext_order_id)
        gevent.sleep(0)


class Command(BaseCommand):
    def handle(self, *args, **options):
        tasks = Queue()
        orders = Order.objects\
            .exclude(ext_order_id=None)\
            .exclude(ext_order_id='')
        for order in orders:
            tasks.put((int(order.pk), str(order.ext_order_id)))
        tasks.put(None)
        workers = []
        for i in xrange(10):
            workers.append(gevent.spawn(worker, tasks))

        gevent.joinall(workers)