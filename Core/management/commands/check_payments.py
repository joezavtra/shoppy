# coding=utf-8
import hashlib
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from lxml import etree
import requests
from Core.models import ClientPayment, Order

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class Command(BaseCommand):
    def handle(self, *args, **options):
        base_url = 'https://merchant.roboxchange.com/WebService/Service.asmx/OpState'
        k = '{http://merchant.roboxchange.com/WebService/}'
        for cp in ClientPayment.objects.filter(
                status=ClientPayment.STATUS_IN_PROCESS):
            merchant_login = settings.ROBOKASSA_LOGIN
            invoice_id = str(cp.pk)
            passwd2 = settings.ROBOKASSA_PASSWORD_2
            signature = hashlib.md5(':'.join([merchant_login, invoice_id, passwd2])).hexdigest().lower()
            r = requests.post(base_url, data=dict(
                MerchantLogin=merchant_login,
                InvoiceID=invoice_id,
                Signature=signature
            ))
            root = etree.fromstring(r.content)
            state = root.find(k + 'State')
            if state is None:
                continue
            code = state.find(k + 'Code')
            if code is None:
                continue
            if code.text == '100':
                delta = timezone.timedelta(days=cp.days if cp.days else 0)
                cp.shop.paid_till = cp.shop.paid_till + delta
                cp.status = ClientPayment.STATUS_OK
                cp.save()

        for order in Order.objects.filter(status=Order.STATUS_WAIT_FOR_PAY).select_related('shop'):
            rk_platform = order.shop.get_platforms()['robokassa']
            if not rk_platform:
                continue
            merchant_login = rk_platform.login
            invoice_id = str(order.pk)
            signature = hashlib.md5(':'.join([merchant_login, invoice_id, rk_platform.password2])).hexdigest().lower()
            r = requests.post(base_url, data=dict(
                MerchantLogin=merchant_login,
                InvoiceID=invoice_id,
                Signature=signature
            ))
            root = etree.fromstring(r.content)
            state = root.find(k + 'State')
            if not state:
                continue
            code = state.find(k + 'Code')
            if not code:
                continue
            if code.text == '100':
                order.status = Order.STATUS_PAID
                order.save()
