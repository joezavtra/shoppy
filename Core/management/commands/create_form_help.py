# coding=utf-8
import inspect
import logging
from django.conf import settings
from django import forms
from django.core.management.base import BaseCommand
from Core.models import FormDescription


__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

logger = logging.getLogger('app')


class Command(BaseCommand):
    """
    Создает записи в БД (подсказки к полям форм) для каждой формы
    """

    def _import(self, from_package, module_names):
        return __import__(from_package, globals(), locals(), module_names, -1)

    def get_apps(self):
        for app_name in settings.INSTALLED_APPS:
            if app_name in settings.FORM_HELP_FOR_APPS:
                try:
                    module_ = self._import(app_name, ['forms'])
                    if hasattr(module_, 'forms'):
                        yield module_
                except ImportError:
                    pass

    def _get_forms(self, forms_module):
        formclass = (forms.Form, forms.ModelForm)
        for name in dir(forms_module):
            obj = getattr(forms_module, name)
            if callable(obj):
                if inspect.isclass(obj):
                    if issubclass(obj, formclass):
                        yield obj

    def handle(self, *args, **options):
        for app in self.get_apps():
            app_forms = app.forms
            for form in self._get_forms(app_forms):
                form_name = form.__module__ + '.' + form.__name__
                for field_name, field in form.base_fields.items():
                    field_name = form_name + '.' + field_name
                    label = field.label
                    if not isinstance(label, basestring):
                        label = None
                    obj, created = FormDescription.objects.get_or_create(
                        field_name=field_name)
                    obj.field_label = label
                    obj.save()
