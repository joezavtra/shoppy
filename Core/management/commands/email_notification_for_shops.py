# coding=utf-8
import logging
from django.conf import settings
from django.core.management import BaseCommand
from django.template.loader import render_to_string
from django.utils import timezone
from Core.models import Shop, EmailQueue

__author__ = 'Prosto Chewey <prosto.chewey@gmail.com>'

l = logging.getLogger('app.email_notification_for_shops')
BCC_EMAIL = 'support@shoppyboom.ru'
DELTA_PATTERNS = {
    0: {
        # Код, для вывода в лог информации о том, какое письмо для сформировано
        'code': 'shop deactivated',
        # Тема, которая будет добавлена в тему письма
        'subject': u'Ваш магазин в социальных сетях заблокирован',
        # Шаблон, который будет исопльзован для рендеринга тела письма
        'template': 'email/shop_notify_deactivate.html'
    },
    1: {
        'code': 'before 1 day',
        'subject': u'Важно! Завтра Ваш магазин в социальных сетях будет заблокирован',
        'template': 'email/shop_notify_before_1_day.html'
    },
    7: {
        'code': 'before 7 days',
        'subject': u'Важно! Осталась неделя до окончания действия Вашего тарифа на ShoppyBoom',
        'template': 'email/shop_notify_before_1_week.html'
    },
    14: {
        'code': 'before 14 days',
        'subject': u'Попробуйте все возможности магазина в соцсетях! (ShoppyBoom)',
        'template': 'email/shop_notify_before_2_week.html'
    },
}
ROUND_UP = getattr(settings, 'EMAIL_NOTIFICATION_FOR_SHOPS_ROUNDING_TO_UP')


class EmailNotificationException(Exception):
    pass


def handle_shop(shop_obj):
    if not shop_obj.email:
        raise EmailNotificationException(
            'Shop object has no email or email is blank')
    if not shop_obj.paid_till:
        raise EmailNotificationException(
            'Shop object has no paid_till or paid_till is blank')

    # Округляем дату окончания действия платежа до 00:00 следующего дня
    paid_till_date = shop_obj.paid_till.date()
    if ROUND_UP:
        paid_till_date = paid_till_date + timezone.timedelta(days=1)

    cur_date = timezone.now().date()
    delta_days = (paid_till_date - cur_date).days
    if delta_days < 0:
        l.debug('Shop "%d" paid_till less than now date' % shop_obj.pk)
        return

    for days, email_data in DELTA_PATTERNS.iteritems():
        if delta_days == days:
            data = dict(shop=shop_obj)
            html_string = render_to_string(email_data['template'], data)
            EmailQueue.objects.create(
                email_from=u"ShoppyBoom.ru <no-reply@shoppyboom.ru>",
                email_to=shop_obj.email,
                bcc=BCC_EMAIL,
                reply_to=u'Служба заботы Shoppyboom.RU <support@shoppyboom.ru>',
                subject=email_data['subject'],
                message_type=EmailQueue.MTYPE_HTML,
                body=html_string
            )
            l.debug('Create notification "%s" for shop with pk "%d"' % (
                email_data['code'], shop_obj.pk))
            break


class Command(BaseCommand):
    def handle(self, *args, **options):
        shops = Shop.objects.all()
        for shop_obj in shops:
            try:
                l.debug(
                    'Start handle shop_obj with pk: "%d"' % shop_obj.pk)
                handle_shop(shop_obj)
                l.debug(
                    'Success handle shop_obj with pk: "%d"' % shop_obj.pk)
            except Exception as e:
                l.exception(
                    'Error on handle shop_obj with pk: "%d"' % shop_obj.pk)
