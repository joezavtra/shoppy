# -*- coding: utf-8 -*-
import sys, traceback
import calendar
from collections import defaultdict
import random
import logging
import os
import time
import hashlib
import urllib
from urllib import urlencode
import urlparse
import uuid
import warnings
from os import path
from django.utils.encoding import force_text
from jsonfield import JSONField
import pytz
import requests
from wand.image import Image
from datetime import timedelta,datetime

from itertools import chain

from django.conf import settings

from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.template import Context, Template
from django.db import models
from django.db.models import Q
from django.db.models import F
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_save, post_delete, pre_delete
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils import timezone
from Core.managers import *
from Core import field_validators
from Core.utils import abs_url
import json
from app_walletone import utils
from robokassa.api_robokassa import Robokassa

from mptt.models import MPTTModel, TreeForeignKey

logger = logging.getLogger('app')


def get_default_platform(code):
    def _create():
        platform, created = Platform.objects.get_or_create(code=code)
        if created:
            platform.save()
        return platform
    return _create


def get_default_cur(code):
    def _get():
        cur, created = Currency.objects.get_or_create(code=code)
        if created:
            cur.save()
        return cur
    return _get


class ShoppyUser(models.Model):
    sys_user = models.OneToOneField(User, related_name='shoppyuser')
    token = models.CharField(
        max_length=100, default=lambda: str(uuid.uuid4()), unique=True)
    token_created = models.DateTimeField(default=timezone.now)
    recovery_token = models.CharField(
        max_length=100, unique=True, blank=True, null=True)
    recovery_token_expired = models.DateTimeField(blank=True, null=True)
    roles = models.ManyToManyField(
        'Role', verbose_name=u'роли сотрудника', related_name='users')
    tel = models.CharField(
        max_length=255, verbose_name=u'номер телефона', blank=True, null=True,
        validators=[field_validators.telephone_number])
    vk_token = models.CharField(
        max_length=100, blank=True, null=True, default=None,
        verbose_name=u'Vkontakte токен')
    vk_token_expired = models.DateTimeField(blank=True, null=True, verbose_name=u'дата действия Vkontakte токен')

    class Meta(object):
        verbose_name = u'Профиль пользователя (клиента)'
        verbose_name_plural = u'Профили пользователей (клиентов)'

    def get_user_name(self):
        """
        Возвращает некоторую строку - человеческий идентификатор данного
        пользователя. Исопльзуется для того, что бы вывести какое то строковое
        представление для данного объекта

        :return: string
        """
        if self.sys_user.first_name:
            return self.sys_user.first_name
        return self.sys_user.username

    def can_create_shop(self):
        return True

    def create_recovery_token(self):
        self.recovery_token = str(uuid.uuid4())
        time_delta = timezone.timedelta(
            hours=settings.PASSWORD_RECOVERY_TOKEN_EXPIRED_HOURS)
        self.recovery_token_expired = timezone.now() + time_delta
        self.save()

    def recovery_token_is_valid(self):
        if timezone.now() > self.recovery_token_expired:
            return False
        return True

    def destroy_recovery_token(self):
        self.recovery_token = None
        self.recovery_token_expired = None
        self.save()

    def vk_token_valid(self):
        if self.vk_token_expired is None:
            return False
        if self.vk_token_expired < timezone.now():
            return False
        if self.vk_token == '':
            return False

        return True


class Client(models.Model):
    CLIENT_TYPE_INDIVIDUAL = 1
    CLIENT_TYPE_LEGAL = 2
    CLIENT_TYPE_CHOICES = (
        (CLIENT_TYPE_INDIVIDUAL, u'Физическое лицо'),
        (CLIENT_TYPE_LEGAL, u'Юридическое лицо'),
    )
    L_PERMISSION_DOVERENNOST = 1
    L_PERMISSION_USTAV = 2
    L_PERMISSION_CHOICES = (
        (L_PERMISSION_DOVERENNOST, u'Доверенность'),
        (L_PERMISSION_USTAV, u'Устав'),
    )
    name = models.CharField(max_length=200, verbose_name=u'название')
    cdate = models.DateTimeField('date created', default=timezone.now)
    status = models.BooleanField(default=True, verbose_name=u'статус')
    users = models.ManyToManyField(User, related_name='clients')
    balance = models.FloatField(verbose_name=u'баланс', default=0.0)
    partner = models.ForeignKey(
        'Partner', related_name='clients', blank=True, null=True)
    is_validated = models.BooleanField(default=False, verbose_name=u'проверен?',
        help_text=u'Поставьте галочку, если все данные клиента проверены. '
                  u'После этого пользователь не сможет отредактировать данные '
                  u'о клиенте')
    client_type = models.PositiveIntegerField(
        choices=CLIENT_TYPE_CHOICES, default=None, blank=True, null=True,
        verbose_name=u'тип')
    # FIXME: Исправить verbose_name для этого поля - это форма собственности?
    l_permission = models.PositiveIntegerField(
        choices=L_PERMISSION_CHOICES, default=None, blank=True,
        null=True, verbose_name=u'доверенность или устав')
    l_doverennost_number = models.CharField(
        blank=True, null=True, verbose_name=u'номер доверенности',
        max_length=10)
    l_doverennost_date = models.DateField(
        blank=True, null=True, verbose_name=u'дата выдачи доверенности')
    l_fio_gendir = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name=u'ФИО генерального директора')
    l_org_name = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name=u'наименование организации')
    l_inn = models.CharField(
        blank=True, null=True, verbose_name=u'ИНН', max_length=12)
    l_kpp = models.CharField(
        blank=True, null=True, verbose_name=u'КПП', max_length=20)
    l_okved = models.CharField(
        blank=True, null=True, verbose_name=u'ОКВЭД', max_length=20)
    l_okpo = models.CharField(
        blank=True, null=True, verbose_name=u'ОКПО', max_length=20)
    l_ogrn = models.CharField(
        blank=True, null=True, verbose_name=u'ОГРН', max_length=20)
    l_bank = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=u'банк')
    l_bik = models.CharField(
        blank=True, null=True, verbose_name=u'БИК', max_length=20)
    l_corr_account = models.CharField(
        blank=True, null=True, verbose_name=u'корреспондентский счет', max_length=20)
    l_current_account = models.CharField(
        blank=True, null=True, verbose_name=u'рассчетный счет', max_length=20)
    l_legal_addr = models.TextField(
        blank=True, null=True, verbose_name=u'юридический адрес')
    l_fact_addr = models.TextField(
        blank=True, null=True, verbose_name=u'фактический адрес')
    l_tel = models.CharField(
        max_length=20, blank=True, null=True, verbose_name=u'телефон')
    l_email = models.EmailField(
        max_length=255, blank=True, null=True,
        verbose_name=u'электронная почта')

    class Meta(object):
        verbose_name = u'Клиент'
        verbose_name_plural = u'Клиенты'

    def __unicode__(self):
        return self.name


class Shop(models.Model):
    # Принято, что имя параметра связано со значнием. То есть значение будет
    # равно кол-вам показов баннера в месяц.
    EMAIL_BANNER_FREQUENCY_NONE = 0
    EMAIL_BANNER_FREQUENCY_1PMONTH = 1
    EMAIL_BANNER_FREQUENCY_3PMONTH = 3
    EMAIL_BANNER_FREQUENCY_5PMONTH = 5
    EMAIL_BANNER_FREQUENCY_10PMONTH = 10
    EMAIL_BANNER_FREQUENCY_CHOICES = (
        (EMAIL_BANNER_FREQUENCY_NONE, u'Не показывать'),
        (EMAIL_BANNER_FREQUENCY_1PMONTH, u'1 раз в месяц'),
        (EMAIL_BANNER_FREQUENCY_3PMONTH, u'3 раза в месяц'),
        (EMAIL_BANNER_FREQUENCY_5PMONTH, u'5 раз в месяц'),
        (EMAIL_BANNER_FREQUENCY_10PMONTH, u'10 раз в месяц'),
    )
    client = models.ForeignKey(Client, related_name='shops')
    name = models.CharField(u'Название магазина', max_length=200, blank=True)
    alias = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'Тема оформления (необязательно)')
    cdate = models.DateTimeField(u'Дата создания', default=timezone.now)
    description = models.TextField(u'Описание', blank=True)
    status = models.BooleanField(u'Активен', default=True)
    users = models.ManyToManyField(User, related_name='shops', blank=True, null=True, verbose_name=u'Персонал')
    domain = models.CharField(max_length=255, default='', unique=True, verbose_name=u'Адрес магазина')

    email = models.CharField(max_length=200, blank=True, null=True, verbose_name=u'Email для уведомлений о заказах и отзывах')

    user_from = models.CharField(max_length=200, blank=True, null=True, verbose_name=u'Имя отправителя в письмах клиентам')
    user_reply = models.CharField(max_length=200, blank=True, null=True, verbose_name=u'Email для ответа покупателей')
    user_subject = models.CharField(max_length=200, blank=True, null=True, verbose_name=u'Тема уведомления')
    user_sign = models.TextField(verbose_name=u'Подпись в письме покупателю', blank=True, null=True)
    hello_text = models.TextField(verbose_name=u'Текст приветствия', blank=True, null=True)
    paid_till = models.DateTimeField(verbose_name=u'дата окончания оплаты магазина', blank=True, null=True, default=timezone.now)
    tariff = models.ForeignKey('Tariff', blank=True, null=True, verbose_name=u'тариф')
    datasources = models.ManyToManyField('DataSource', verbose_name=u'источники данных', related_name='shops')

    catalog = models.ForeignKey(
        'ShopCatalog', blank=True, null=True, default=None,
        verbose_name=u'запись в каталоге')
    email_banner_frequency = models.PositiveIntegerField(
        verbose_name=u'частота показа баннера с предложением оставить email',
        choices=EMAIL_BANNER_FREQUENCY_CHOICES,
        default=EMAIL_BANNER_FREQUENCY_NONE)
    currency = models.ForeignKey(
        'Currency', verbose_name=u'валюта магазина',
        default=get_default_cur('RUB'),
        help_text=u'Валюта, в которой указаны цены на все товары магазина')

    class Meta(object):
        verbose_name = u'Магазин'
        verbose_name_plural = u'Магазины'

    def __unicode__(self):
        return u'%s_%s' % (self.id, self.name)

    def get_email_banner_interval(self):
        """
        Возвращает число - кол-во дней между каждым опросом email.
        То есть в зависимости от периода высчитываем кол-во дней, через которое
        будет повторяться баннер. Округление до меньшего для гарантированного
        показа заданного кол-ва баннеров в месяц

        :return: int()
        """
        cur_date = datetime.now()
        month, days_in_month = calendar.monthrange(cur_date.year, cur_date.month)
        try:
            return days_in_month / self.email_banner_frequency
        except:
            return -1

    def get_group_links(self):
        """
        Возвращает словарь со ссылками на группы, к которым привязано приложение

        :return: dict()
        """
        vk_link = None
        fb_link = None

        if self.shopprofile.vk_groupid and int(self.shopprofile.vk_groupid) != 0:
            vk_link = 'http://vk.com/club%s' % self.shopprofile.vk_groupid
        if self.shopprofile.fb_groupid:
            fb_link = '%s' % self.shopprofile.fb_groupid
        return {
            'vk': vk_link,
            'fb': fb_link
        }

    def get_new_payments_count(self):
        return self.payments.filter(status=ClientPayment.STATUS_NO).count()

    def calculate_tariff_migration(self, new_tariff):
        """
        Вернет datetime на который изменится текущий paid_till в случае перехода
        текущего магазина на указанный тариф
        :param new_tariff: Tariff()
        """
        if not new_tariff.max_products:
            return None

        paid_till_sec = time.mktime(self.paid_till.timetuple())
        now_day_sec = time.mktime(datetime.now().timetuple())
        delta_sec = paid_till_sec - now_day_sec
        try:
            sec = delta_sec * self.tariff.cost / new_tariff.cost
            sec = sec if sec > 0 else 0
            res = now_day_sec + sec
        except:
            res = paid_till_sec
        # FIXME: исправить на использование timezone
        return datetime.fromtimestamp(res) + timedelta(hours=4)

    def get_category_cache_key(self):
        return "SBApp:s%d:api:categories" % (self.id)

    def clear_category_cache(self):
        cache.delete(self.get_category_cache_key())

    def get_available_products_cnt(self):
        if self.tariff.max_products:
            res = self.tariff.max_products - Product.objects.filter(
                status=True,
                datasource_id__in=[d.id for d in self.datasources.all()]
            ).count()
            if res > -1:
                return res
            return 0
        return -1

    def get_product_count(self):
        return Product.objects.filter(datasource_id__in=[d.id for d in self.datasources.all()]).count()

    def get_platforms(self):
        return self.platforms.get_platforms()

    def get_logo_filename(self):
        return os.path.join(
            settings.MEDIA_ROOT, self.get_shop_dir(), 'logo.png'
        )

    def get_logo_url(self):
        return os.path.join(
            settings.MEDIA_URL, self.get_shop_dir(), 'logo.png'
        )

    def set_logo(self, image):
        with Image(file=image) as img:
            img.format = 'png'
            if img.height > 44:
                h = 44.0
                w = h / float(img.height) * img.width
                img.resize(int(w), int(h))
            img.save(filename=self.get_logo_filename())

    def get_shop_dir(self):
        if self.pk is not None:
            p = 'shop/%d' % self.pk
            p1 = path.join(settings.MEDIA_ROOT, p)
            if not path.exists(p1):
                os.makedirs(p1)
            return p
        return None

    def get_img_dir(self):
        d = '%s/goods/' % self.get_shop_dir()
        abs_d = os.path.join(settings.MEDIA_ROOT, d)
        if not os.path.exists(abs_d):
            os.makedirs(abs_d)
        return d

    def cats_count(self):
        summ = 0
        for ds in self.datasources.all():
            summ += ds.categories.count()
        return summ

    def is_editable(self):
        warnings.warn('DON`T USE METHOD "is_editable"')
        return False

    def hasPromocodes(self):
        active_coupons = self.discount_set.filter(
            Discount.active_only_filter(),
            coupon_key__isnull=False,
        )
        if active_coupons.count() > 0:
           return True
        return False

    def get_cabinet_url(self):
        url = u"/cabinet/new_shop/"
        if self.id:
            url = u"/cabinet/%d/" % self.id
        return url

    def get_cabinet_goods_url(self):
        return u"/cabinet/%d/goods/" % self.id

    def get_cabinet_categories_url(self):
        return u"/cabinet/%d/categories/" % self.id

    def get_cabinet_news_url(self):
        return u"/cabinet/%d/news/" % self.id

    def get_cabinet_pages_url(self):
        return u"/cabinet/%d/pages/" % self.id

    def get_app_url(self, apptype=None):
        if apptype is None:
            url = self.get_url()
        elif apptype.code == 'vk':
            url = self.shopprofile.vk_url
        elif apptype.code == 'fb':
            url = self.shopprofile.fb_url
        elif apptype.code == 'fb_tab':
            url = self.shopprofile.fb_taburl
        elif apptype.code == 'ok':
            url = self.shopprofile.ok_url
        return url

    def get_url(self):
        return u"http://%s.%s" % (self.domain,settings.BASE_HOST_NAME)

    def get_vk_url(self):
        return self.get_url() + u"/vk/"

    def get_fb_url(self):
        return self.get_url() + u"/fb/"

    def get_ok_url(self):
        return self.get_url() + u"/ok/"

    def get_comments_count(self):
        products = Product.objects.none()
        for ds in self.datasources.all():
            products += ds.products.all()
        return Comment.objects.filter(product__in=products, reply_to=None).count()

    def get_alerts(self):
        # TODO: выпилить
        warnings.warn(
            'Do not use this method. All in '
            'Cabinet.context_processor.notifications', DeprecationWarning)
        return []

class ShopProfile(models.Model):
    shop = models.OneToOneField(Shop, primary_key=True)
    fb_taburl = models.CharField(
        max_length=200,
        verbose_name=u'URL tab-приложения в Facebook',
        blank=True,
        null=True
    )
    fb_url = models.CharField(
        max_length=200,
        verbose_name=u'URL приложения в Facebook',
        blank=True,
        null=True
    )
    fb_app_id = models.CharField(
        max_length=30,
        verbose_name=u'Facebook AppID',
        blank=True,
        null=True
    )
    fb_app_secret = models.CharField(
        max_length=100,
        verbose_name=u'Facebook App Secret key',
        blank=True,
        null=True
    )
    vk_url = models.CharField(
        max_length=200,
        verbose_name=u'URL приложения в VK',
        blank=True,
        null=True
    )
    vk_app_id = models.CharField(
        max_length=30,
        verbose_name=u'Vkontakte AppID',
        blank=True,
        null=True
    )
    vk_app_secret = models.CharField(
        max_length=100,
        verbose_name=u'Vkontakte App Secret key',
        blank=True,
        null=True
    )
    ok_url = models.CharField(
        max_length=200,
        verbose_name=u'URL приложения в Одноклассниках',
        blank=True,
        null=True
    )
    ok_app_id = models.CharField(
        max_length=30,
        verbose_name=u'Одноклассники AppID',
        blank=True,
        null=True
    )
    ok_app_public = models.CharField(
        max_length=100,
        verbose_name=u'Одноклассники публичный ключ',
        blank=True,
        null=True
    )
    ok_app_secret = models.CharField(
        max_length=100,
        verbose_name=u'Одноклассники секретный ключ',
        blank=True,
        null=True
    )
    ga_fb = models.TextField(
        verbose_name=u'Google Analytics для FB',
        null=True,
        blank=True
    )
    ga_vk = models.TextField(
        verbose_name=u'Google Analytics для VK',
        null=True,
        blank=True
    )
    ga_ok = models.TextField(
        verbose_name=u'Google Analytics для Одноклассников',
        null=True,
        blank=True
    )
    vk_groupid = models.CharField(
        verbose_name=u'идентификатор группы Vkontakte',
        max_length=100,
        null=True,
        blank=True,
        default=None)
    vk_groupname = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name=u'название группы Vkontakte')
    fb_groupid = models.CharField(
        verbose_name=u'ссылка на группу Facebook',
        max_length=100,
        null=True,
        blank=True,
        default=None)

    class Meta(object):
        verbose_name = u'Профиль магазина'
        verbose_name_plural = u'Профили магазина'


class Page(models.Model):
    shop = models.ForeignKey(Shop, related_name='pages', editable=False)
    name = models.CharField(u'Заголовок', max_length=200)
    alias = models.CharField(
        u'Короткое имя, например about',
        max_length=200,
        validators=[field_validators.page_alias_validator]
    )
    order = models.IntegerField(u'Порядковый номер', default=0)
    content = models.TextField(u'Текст', blank=True, null=True)

    class Meta(object):
        verbose_name = u'Страница магазина'
        verbose_name_plural = u'Страницы магазина'

    def __unicode__(self):
        return self.name

    def get_cabinet_page_edit_url(self):
        return u"/cabinet/%d/pages/%d/" % (self.shop_id, self.id)


class News(models.Model):
    shop = models.ForeignKey(Shop, editable=False)
    cdate = models.DateTimeField(u'Дата публикации', editable=False)
    name = models.CharField(u'Заголовок', max_length=200)
    content = models.TextField(u'Текст')

    class Meta(object):
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    def __unicode__(self):
        return self.name

    def get_cabinet_news_edit_url(self):
        return u"/cabinet/%d/news/%d/" % (self.shop_id, self.id)


class DataSource(models.Model):
    MODE_AUTO = 1
    MODE_MANUAL = 2
    MODE_CHOICES = (
        (MODE_AUTO, u'Автоматический'),
        (MODE_MANUAL, u'Ручной'),
    )
    name = models.CharField(
        max_length=100, default=u'Default', verbose_name=u'название источника')
    client = models.ForeignKey(
        Client, verbose_name=u'владелец источника')
    yml_url = models.URLField(
        verbose_name=u'ссылка на YML файл', blank=True, null=True)
    mode = models.PositiveIntegerField(
        choices=MODE_CHOICES, verbose_name=u'режим работы источника')
    last_fetch = models.DateTimeField(
        verbose_name=u'Последний опрос', default=timezone.now)
    next_fetch = models.DateTimeField(
        verbose_name=u'Очередной опрос', default=timezone.now)
    processing = models.DateTimeField(
        verbose_name=u'Начало последней обработки', default=timezone.now,
        blank=True, null=True)
    root_cat_oid = models.CharField(
        max_length=255, blank=True, null=True,
        verbose_name=u'категория, от которой начинается импорт')

    class Meta(object):
        verbose_name = u'Источник данных'
        verbose_name_plural = u'Источники данных'

    def __unicode__(self):
        return self.name

    def get_last_fileimport_log(self):
        try:
            return FileImportLog.objects.filter(datasource=self).order_by('-timestamp')[0]
        except:
            return None

    def get_available_products_cnt(self):
        data = set(map(lambda x: x.get_available_products_cnt(), self.shops.all()))
        available = max(data or [0])
        if -1 in data:
            data.remove(-1)
            unlim = True
        else:
            unlim = False
        for item in data:
            if available > item > -1:
                available = item
        if available > -1:
            return available
        else:
            return -1

    def is_processing(self):
        if self.processing and self.processing >= timezone.now() - timezone.timedelta(minutes=15):
            return True
        return False


class Category(MPTTModel):
    datasource = models.ForeignKey(
        DataSource, related_name='categories', verbose_name=u'источник данных')
    oid = models.CharField(max_length=32, null=True, blank=True, db_index=True)
    order = models.IntegerField(default=0)
    name = models.CharField(max_length=200)
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='subcats'
    )
    status = models.BooleanField(default=True, db_index=True)
    active_products_count = models.IntegerField(default=0)
    lvl = models.PositiveIntegerField(
        default=1, verbose_name=u'уровень вложенности')
    mtime = models.DateTimeField(default=timezone.now)

    class MPTTMeta:
        level_attr = 'mptt_level'
        order_insertion_by = ['order']

    class Meta(object):
        ordering = ['order']
        unique_together = ('datasource', 'oid')
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.name

    def get_current_lvl(self):
        """Вычисляет lvl на текущий момент"""
        lvl = 1
        if not self.parent:
            return lvl
        parent = self.parent
        while parent:
            lvl += 1
            parent = parent.parent
        return lvl

    def upd_lvl(self):
        """Обновляет параметр lvl у себя и всех детей"""

        self.lvl = self.get_current_lvl()
        self.save()
        parents = [self]
        childrens = Category.objects.filter(parent__in=parents)
        child_lvl = self.lvl + 1
        while childrens:
            childrens.update(lvl=child_lvl)
            childrens = Category.objects.filter(parent__in=list(childrens))
            child_lvl += 1
        return True

    def upd_product_cnt(self):
        """
        Обновляет свойство self.active_products_count
        """

        if self.pk is not None:

            active_products_count = ProductCategories.objects.filter(
                category=self,
                product__status=True
            ).count()
            for child_cat in Category.objects.filter(parent=self):
                child_cnt = child_cat.upd_product_cnt()
                if child_cat.status:
                    active_products_count += child_cnt
            if self.active_products_count != active_products_count:
                self.active_products_count = active_products_count
                self.save()
        else:
            self.active_products_count = 0
        return self.active_products_count

    def get_nesting_id_list(self,all=False):
        descedants = self.get_descendants(include_self=True).filter(status=True)

        if all:
            descedants = descedants.filter(active_products_count__gt=0)

        ids = descedants.values_list('id', flat=True)

        return ids

    def renumber_subcats(self):
        subcats = self.subcats.all().order_by('order')
        i = 0
        for c in subcats:
            c.order = i
            c.save()
            i += 1
        return

    def get_parent_id_list(self):
        ids = [self.id]
        if self.parent_id:
            ids += self.parent.get_parent_id_list()
        return ids

    def get_category_path(self, items=None):
        if items is None:
            items = []
        if self.parent:
            items = [self.parent] + items
            return self.parent.get_category_path(items)
        return items

    def same_level_cats(self):
        return self.get_siblings().order_by('order')
        #if self.parent:
        #    return self.parent.subcats.all().exclude(pk=self.pk)
        #return Category.objects.filter(datasource=self.datasource, parent=None).exclude(pk=self.pk)

    def get_all_subcats_pk(self):
        """
        Возвращает список всех Primary Key дочерних категорий(рекурсивно)

        :return: list(Category.pk)
        """
        pk_list = []
        for cat in self.subcats.all():
            pk_list.append(cat.pk)
            pk_list += cat.get_all_subcats_pk()
        return pk_list

    def get_all_subcats(self):
        """
        Возвращает queryset всех дочерних категорий(рекурсивно)

        :return: list(Category)
        """
        return Category.objects.filter(pk__in=self.get_all_subcats_pk())


class ProductCategories(models.Model):
    product = models.ForeignKey('Product')
    category = models.ForeignKey('Category')

    class Meta(object):
        verbose_name = u'Связь продукт - категория'
        verbose_name_plural = u'Связи продуктов с категориями'
        db_table = 'Core_product_categories'


class Product(models.Model):
    IMAGES_SEPARATOR = '#|#'
    categories = models.ManyToManyField(
        Category,
        related_name='products',
        through=ProductCategories,
        blank=False,
        null=False,
        verbose_name=u'Категории'
    )
    datasource = models.ForeignKey(DataSource, related_name='products')
    oid = models.CharField(max_length=32, db_index=True, verbose_name=u'Артикул товара')
    name = models.CharField(max_length=200, verbose_name=u'Название')
    desc = models.TextField(db_column="description", blank=True, verbose_name=u'Описание')
    manual_desc = models.TextField(
        blank=True, null=True, verbose_name=u'переопределенное описание товара')
    misc = models.TextField(default='')
    images = models.TextField(
        default='http://shoppyboom.ru/images/design/main_bg.gif')
    status = models.BooleanField(default=True, verbose_name=u'Виден')
    url = models.CharField(max_length=200, blank=True, verbose_name=u'Оригинальный адрес товара')
    price = models.FloatField(default=0.0, verbose_name=u'Цена')
    model_oid = models.CharField(max_length=255)
    mtime = models.DateTimeField(default=timezone.now, db_index=True)

    class Meta(object):
        verbose_name = u'Продукт'
        verbose_name_plural = u'Продукты'
        # unique_together = ('datasource', 'model_oid')

    def __unicode__(self):
        return u"%s" % self.name
    #
    #def __init__(self, *args, **kwargs):
    #    super(Product, self).__init__(*args, **kwargs)
    #    self._origin_values = dict()
    #    for field in self._meta.fields:
    #        self._origin_values[field.name] = getattr(self, field.name)

    def cover(self):
        try:
            return self.images.split(self.IMAGES_SEPARATOR, 1)[0]
        except:
            return None

    def abs_cover(self):
        """
        Возвращает абсолютный адрес обложки товара (для того, что бы они были
        доступны, например из почты)

        """
        return abs_url(self.cover(), settings.BASE_HOST_NAME)

    def image_list(self):
        return self.images.split(self.IMAGES_SEPARATOR)

    def get_full_category(self):
        for cat in self.categories.all():
            yield cat.get_category_path() + [cat]

    def get_full_cabinet_url(self):
        url = u"http://shoppyboom.ru/cabinet/%d/goods/%d/" % (
            self.category.shop_id, self.id)
        return url

    def get_app_url(self,shop=None,apptype=None):
        try:
            if apptype.code == 'www':
                return self.get_site_url(shop=shop)
            elif apptype.code == 'vk':
                return self.get_vk_url(shop=shop)
            elif apptype.code == 'fb':
                return self.get_fb_url(shop=shop)
            elif apptype.code == 'fb_tab':
                return self.get_fb_tab_url(shop=shop)
            elif apptype.code == 'ok':
                return self.get_ok_url(shop=shop)
        except:
            pass
        return self.get_site_url()

    def get_site_url(self, shop=None):
        if shop:
            return '%s/#product/%d' % (shop.get_url(), self.id)
        return None

    def get_vk_url(self, shop=None):
        if shop:
            if shop.shopprofile.vk_url:
                return '%s#product/%d' % (shop.shopprofile.vk_url, self.id)
        return None

    def get_fb_url(self, shop=None):
        try:
            if shop.shopprofile.fb_url:
                return '%s/product/%d' % (shop.shopprofile.fb_url, self.id)
        except:
            logger.error('error on Product.get_fb_url. Shop: %d' % shop.id if shop else 0)
        return None

    def get_fb_tab_url(self, shop=None):
        try:
            if shop.shopprofile.fb_url:
                return shop.shopprofile.fb_taburl + '?app_data=/product/%s' % self.id
        except:
            logger.error('error on Product.get_fb_tab_url. Shop: %d' % shop.id if shop else 0)
        return None

    def get_ok_url(self, shop=None):
        if shop:
            if shop.shopprofile.ok_url:
                return '%s?custom_args=/product/%d' % (shop.shopprofile.ok_url, self.id)
        return None


class Offer(models.Model):
    product = models.ForeignKey(Product, related_name='offers')
    oid = models.CharField(max_length=100, verbose_name=u'Артикул')
    params = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    price = models.FloatField(default=0.0)
    images = models.TextField(
        default='http://shoppyboom.ru/images/design/main_bg.gif'
    )
    status = models.BooleanField(default=True)
    available = models.BooleanField(
        default=True, verbose_name=u'товар на заказ')

    class Meta(object):
        verbose_name = u'Модификация продукта'
        verbose_name_plural = u'Модификации продуктов'
        #unique_together = ('product', 'oid')

    def get_size(self):
        if not self.params:
            return ''
        params = json.loads(self.params)
        if u'Размер' in params:
            return params[u'Размер']
        return ''

    def set_size(self, size):
        if not self.params:
            self.params = '{}'
        params = json.loads(self.params)
        params[u'Размер'] = size
        self.params = json.dumps(params)
        return params[u'Размер']

    def get_params(self):
        if not self.params:
            return dict()
        return json.loads(self.params)


class Cart(models.Model):
    discount = models.ForeignKey(
        'Discount',
        blank=True,
        null=True,
        related_name='carts'
    )
    customer = models.ForeignKey('Customer', related_name='carts')
    shop = models.ForeignKey(Shop)
    cdate = models.DateTimeField(verbose_name='date created', default=timezone.now)
    mdate = models.DateTimeField(verbose_name='date modified', default=timezone.now, blank=True, null=True, db_index=True)
    status = models.BooleanField('ordered', default=False)

    class Meta(object):
        verbose_name = u'Корзина'
        verbose_name_plural = u'Корзины'

    def __unicode__(self):
        return str(self.id)

    def total(self):
        total = 0
        for item in self.items.all():
            total += item.product.price * item.count
        #if self.discount:
        #    total -= float(total) / 100.0 * float(self.discount.value)
        return total

    def update_notice_time(self):
        self.mdate = timezone.now()
        self.save()


    def set_max_available_discount(self):
        result_price = self.price_with_discount()
        discounts = self.shop.discount_set.filter(
            Discount.active_only_filter(),
            coupon_key__isnull=True
        )
        for discount in discounts:
            price_with_discount = self.price_with_discount(discount)
            if price_with_discount < result_price:
                self.discount = discount
                result_price = price_with_discount
                self.save()

        if self.price_with_discount() == self.total():
            self.discount = None


    def get_cartitem_list(self, discount=None):
        """
        Возвращает список товаров с учетом скидки

        """
        if not discount:
            discount = self.discount

        for item in self.items.all():
            yield dict(
                item=item,
                price_with_discount=item.price_with_discount(discount)
            )

    def price_with_discount(self, discount=None):
        if not discount:
            discount = self.discount

        if not discount:
            return self.total()

        if discount.application == Discount.APPL_CART:
            return discount.apply(self.total())

        sum = 0.0
        for d in self.get_cartitem_list(discount):
             sum += d['price_with_discount']['price']
        return sum


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, related_name='items')
    product = models.ForeignKey(Product)
    offer = models.ForeignKey(Offer, default=None, null=True, blank=True)
    count = models.IntegerField(default=1)

    class Meta(object):
        verbose_name = u'Запись в корзине'
        verbose_name_plural = u'Записи в корзине'

    def __unicode__(self):
        return u"%s" % self.product.name

    def save(self, *args, **kwargs):
        self.cart.update_notice_time()
        return super(CartItem, self).save(*args, **kwargs)

    def delete(self, using=None):
        self.cart.update_notice_time()
        return super(CartItem, self).delete(using=using)

    def price(self):
        return self.count * self.product.price

    def price_with_discount(self, discount=None):
        price = self.price()

        if not discount:
            discount = self.cart.discount

        if not discount:
            return {'price': price, 'discount': None}

        if discount.application == Discount.APPL_CART:
            return {'price': price, 'discount': None}


        if discount.application == Discount.APPL_CATEGORIES:
            product_categories = chain.from_iterable(
                self.product.get_full_category()
            )
            discount_categories_ids = discount.categories.values_list(
                'id',
                flat=True
            )

            for product_category in product_categories:
                if product_category.id in discount_categories_ids:
                    price = discount.apply(price, self.count)

            return {'price': price, 'discount': discount}


        if discount.application == Discount.APPL_PRODUCTS:
            if discount.products.filter(pk=self.product.pk):
                price = discount.apply(price)

            return {'price': price, 'discount': discount}


        return {'price': discount.apply(price), 'discount': discount}


class Discount(models.Model):

    APPL_CART = 'cart'
    APPL_CATEGORIES = 'categories'
    APPL_PRODUCTS = 'products'
    APPLICATION_CHOICES = (
        (APPL_CART, u'Корзина'),
        (APPL_CATEGORIES, u'Определённые категории'),
        (APPL_PRODUCTS, u'Определённые товары'),
    )

    VT_RUBLES = 1
    VT_PERCENT = 2
    VALUE_TYPE_CHOICES = (
        (VT_RUBLES, u'Руб.'),
        (VT_PERCENT, u'%'),
    )

    shop = models.ForeignKey(Shop)
    name = models.CharField(max_length=100, verbose_name=u'название')
    value = models.IntegerField(verbose_name=u'значение')
    value_type = models.PositiveIntegerField(
        choices=VALUE_TYPE_CHOICES,
        default=VT_RUBLES,
        verbose_name=u'как считать скидку'
    )

    application = models.CharField(
        max_length=100,
        choices=APPLICATION_CHOICES,
        verbose_name=u'Область применения скидки',
        default=APPL_CART
    )

    time_from = models.DateTimeField(
        verbose_name="Начало действия",
        null=True,
        blank=True
    )

    time_to = models.DateTimeField(
        verbose_name="Окончание действия",
        null=True,
        blank=True
    )

    min_cart_price = models.FloatField(
        blank=False,
        null=False,
        verbose_name=u'Минимальная стоимость корзины',
        default=0
    )

    products = models.ManyToManyField(
        Product,
        verbose_name=u'продукты, на которые распространяется скидка',
        blank=True,
        null=True
    )

    categories = models.ManyToManyField(
        Category,
        verbose_name=u'категории, на которые распространяется скидка',
        blank=True,
        null=True
    )

    status = models.BooleanField(
            default=True,
            verbose_name=u'Скидка активна'
    )

    # Coupon
    coupon_key = models.CharField(
            max_length=60,
            verbose_name="Ключевое слово",
            null=True,
            blank=True
    )

    max_activation_count = models.PositiveIntegerField(
            default=0,
            verbose_name="Количество активаций (0 - неограничено)",
            null=True,
            blank=True
    )

    cur_activation_count = models.PositiveIntegerField(default=0)

    class Meta(object):
        verbose_name = u'Скидка'
        verbose_name_plural = u'Скидки'


    @staticmethod
    def active_only_filter():
        now = timezone.now()
        return Q(
            Q(
                Q(time_from__isnull=True) |
                Q(time_from__lt=now)
            ),
            Q(
                Q(time_to__isnull=True) |
                Q(time_to__gt=now)
            ),
            Q(
                Q(max_activation_count=0) |
                Q(cur_activation_count__lt=F('max_activation_count'))
            ),
            status=True
        )


    def __unicode__(self):
        return self.get_value()

    def apply(self, price, count=1):
        if self.value_type == self.VT_RUBLES:
            price = price - (self.value * count)
        else:
            price = price - (price * float(self.value) / 100.0)

        if price < 0:
            return 0.0

        return price

    def get_value(self):
        if self.value_type == self.VT_RUBLES:
            return u"%d руб." % self.value
        return u"%d%%" % self.value



class FileImportLog(models.Model):
    datasource = models.ForeignKey(DataSource, related_name='fileimport_logs')
    timestamp = models.DateTimeField(default=timezone.now())
    report_text = models.TextField(verbose_name=u'текст отчета')
    categories_created = models.PositiveIntegerField(default=0)
    categories_deactivated = models.PositiveIntegerField(default=0)
    categories_total = models.PositiveIntegerField(default=0)
    products_created = models.PositiveIntegerField(default=0)
    products_deactivated = models.PositiveIntegerField(default=0)
    products_total = models.PositiveIntegerField(default=0)
    text_err = models.TextField(
        blank=True, null=True, verbose_name=u'текст ошибки', default=None)

    class Meta(object):
        verbose_name = u'отчет'
        verbose_name_plural = u'отчеты о импорте YML'


class SphinxUpdateCnt(models.Model):
    """
    Хранит в себе пары имя таблицы - максимальный идентификатор таблицы.
    Необходим для использования в Sphinx Search Engine. Добавлен в модели
    только ради миграций. Редактировать значения таблицы не рекомендуется
    """

    table_name = models.CharField(max_length=100, primary_key=True)
    max_id = models.IntegerField(default=0)


class Customer(models.Model):
    SEX_CHOICES = (
        (1, u'Женщина'),
        (2, u'Мужчина'),
    )
    RELATION_CHOICES = (
        (1, u'не женат/не замужем'),
        (2, u'есть друг/есть подруга'),
        (3, u'помолвлен/помолвлена'),
        (4, u'женат/замужем'),
        (5, u'всё сложно'),
        (6, u'в активном поиске'),
        (7, u'влюблён/влюблена'),
    )
    name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    nickname = models.CharField(max_length=255, blank=True, null=True)
    bdate_y = models.PositiveIntegerField(blank=True, null=True, default=None)
    bdate_m = models.PositiveIntegerField(blank=True, null=True, default=None)
    bdate_d = models.PositiveIntegerField(blank=True, null=True, default=None)
    email = models.CharField(
        max_length=255, verbose_name=u'email пользователя', blank=True,
        null=True, default=None)
    email_last_update = models.DateField(
        verbose_name=u'дата последнего появления баннера '
                     u'с просьбой ввести email',
        default=timezone.now)
    city = models.ForeignKey(
        'City', blank=True, null=True, verbose_name=u'город')
    sex = models.PositiveIntegerField(
        choices=SEX_CHOICES, verbose_name=u'пол', blank=True, null=True)
    relation = models.PositiveIntegerField(
        choices=RELATION_CHOICES, verbose_name=u'семейное положение',
        blank=True, null=True)
    vk_id = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        default=None,
        db_index=True
    )
    fb_id = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        default=None,
        db_index=True
    )
    ok_id = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        default=None,
        db_index=True
    )

    class Meta(object):
        verbose_name = u'Пользователь приложения'
        verbose_name_plural = u'Пользователи приложений'

    def __unicode__(self):
        return u'%d_%s' % (self.id, self.name)

    @property
    def birthday_str(self):
        if self.bdate_y and self.bdate_m and self.bdate_d:
            return u'%02d.%02d.%d' % (self.bdate_d, self.bdate_m, self.bdate_y, )
        elif self.bdate_m and self.bdate_d:
            return u'%02d.%02d' % (self.bdate_d, self.bdate_m, )
        return None

    @property
    def full_name(self):
        if self.name and self.last_name:
            return u'%s %s' % (self.name, self.last_name)
        elif self.name:
            return self.name
        elif self.last_name:
            return self.last_name
        return None

    def is_need_show_banner_for_shop(self, shop_obj):
        """
        Сообщает необходимость показа баннера с предложением ввести email.
        Так как кол-во показов баннера харнится в настройках магазина -
        небходимо вычислять это значение для конкретного магазина

        :param shop_obj: models.Shop()
        :return: True или False
        """
        if self.email or shop_obj.email_banner_frequency == Shop.EMAIL_BANNER_FREQUENCY_NONE:
            return False

        cur_date = datetime.now().date()
        # Если дата не установлена - указываем текущую и разрашаем баннер
        if not self.email_last_update:
            self.email_last_update = cur_date
            self.save()
            return True
        # Если дата равна текущей - показываем
        if self.email_last_update == cur_date:
            return True
        interval = shop_obj.get_email_banner_interval()
        # Если дата след показа равно текущей - отображаем баннер обновляем дату
        next_date = self.email_last_update + timezone.timedelta(days=interval)
        if next_date == cur_date:
            self.email_last_update = cur_date
            self.save()
            return True
        return False

    def get_platform(self):
        if self.fb_id is not None:
            return u'fb'
        if self.vk_id is not None:
            return u'vk'
        if self.ok_id is not None:
            return u'ok'
        return None

    def get_profile_url(self):
        return self.get_vk_url() or self.get_fb_url() or self.get_ok_url()

    def get_fb_url(self):
        if not self.fb_id:
            return None
        return 'http://facebook.com/%s' % str(self.fb_id)

    def get_vk_url(self):
        if not self.vk_id:
            return None
        return 'http://vk.com/id%s' % str(self.vk_id)

    def get_ok_url(self):
        if not self.ok_id:
            return None
        return 'http://www.odnoklassniki.ru/profile/%s' % self.ok_id

    def get_cart_for_shop(self, shop_obj):
        carts = self.carts.filter(status=False, shop=shop_obj)
        if carts:
            return carts[0]
        cart = Cart.objects.create(
            shop=shop_obj, customer=self
        )
        return cart


class Comment(models.Model):
    MAX_RATING = 10
    RATING_CHOICES = [(i, i) for i in xrange(1, MAX_RATING + 1)]

    shop = models.ForeignKey(Shop, related_name='comments')
    product = models.ForeignKey(Product, related_name='comments')
    customer = models.ForeignKey(
        Customer,
        related_name='comments',
        blank=True,
        null=True
    )
    text = models.TextField(blank=True, null=True)
    rating = models.IntegerField(default=1, choices=RATING_CHOICES)
    created = models.DateTimeField(default=timezone.now)
    reply_to = models.ForeignKey('self', blank=True, null=True, related_name='reply_list', default=None)

    class Meta(object):
        verbose_name = u'Комментарий'
        verbose_name_plural = u'Комментарии'


class City(models.Model):
    name = models.CharField(max_length=255, unique=True)
    lat = models.FloatField(blank=True,null=True)
    lng = models.FloatField(blank=True,null=True)

    class Meta(object):
        verbose_name = u'Город'
        verbose_name_plural = u'Города'

    def __unicode__(self):
        return self.name


class DeliveryPoint(models.Model):
    shop = models.ForeignKey(Shop, related_name='delivery_points')
    oid = models.CharField(max_length=100)
    city = models.ForeignKey(City, blank=True, null=True)
    point_name = models.CharField(max_length=100)
    name = models.CharField(max_length=255)
    lat = models.FloatField()
    lng = models.FloatField()
    json_data = models.TextField(default='{}')

    class Meta(object):
        verbose_name = u'Пункт выдачи'
        verbose_name_plural = u'Пункты выдачи'
        unique_together = ('shop', 'oid')


class BannerSlot(models.Model):
    PLACE_CHOICES = (
        ('slot1', u'Вверху, на каждой странице'),
        ('slot2', u'Корзина'),
        ('slot3', u'В списке товаров'),
    )
    place = models.CharField(
        max_length=10, unique=True, choices=PLACE_CHOICES,
        verbose_name=u'кодовое название',
        help_text=u'может использоваться клиентом для получения баннеров для '
                  u'заданного слота')
    size_width = models.PositiveSmallIntegerField(
        default=None, blank=True, null=True, verbose_name=u'ширина баннера')
    size_height = models.PositiveSmallIntegerField(
        default=None, blank=True, null=True, verbose_name=u'высота баннера')

    class Meta(object):
        verbose_name = u'слот для баннеров'
        verbose_name_plural = u'слоты для баннеров'

    def __unicode__(self):
        if not (self.size_width and self.size_height):
            return self.get_place_display()
        else:
            return self.get_place_display() + u' (размер изображения %dx%d)' % (
                self.size_width, self.size_height, )


class Banner(models.Model):
    BANNER_TYPE_TEXT = 1
    BANNER_TYPE_GRAPHIC = 2
    BANNER_TYPE_CAROUSEL = 3
    BANNER_TYPE_CHOICES = (
        (BANNER_TYPE_TEXT, u'Текстовый', ),
        (BANNER_TYPE_GRAPHIC, u'Графический', ),
        (BANNER_TYPE_CAROUSEL, u'Карусель', ),
    )
    BANNER_TYPE_CODES = (
        (BANNER_TYPE_TEXT, 'text', ),
        (BANNER_TYPE_GRAPHIC, 'graphical', ),
        (BANNER_TYPE_CAROUSEL, 'carousel', ),
    )
    shop = models.ForeignKey(Shop, related_name='banners')
    banner_type = models.PositiveSmallIntegerField(
        choices=BANNER_TYPE_CHOICES, default=BANNER_TYPE_TEXT,
        verbose_name=u'тип баннера')
    cdate = models.DateTimeField(u'Дата публикации', default=timezone.now)
    name = models.CharField(u'Заголовок', max_length=200)
    slot = models.ForeignKey(
        BannerSlot, related_name='banners', verbose_name=u'Расположение баннера')
    status = models.BooleanField(default=True, verbose_name=u'Виден')

    class Meta(object):
        verbose_name = u'Баннер'
        verbose_name_plural = u'Баннеры'

    def __unicode__(self):
        return self.name


class BannerText(Banner):
    text = models.TextField(verbose_name=u'Текст')
    product = models.ForeignKey(
        'Product', verbose_name=u'товар', related_name='banners_text',
        blank=True, null=True)

    class Meta(object):
        verbose_name = u'баннер (текстовый)'
        verbose_name_plural = u'баннеры (текстовые)'


class BannerGraphic(Banner):
    class Meta(object):
        verbose_name = u'баннер (графический)'
        verbose_name_plural = u'баннеры (графические)'


def banner_graphic_image_upload_to(instance, filename):
    return os.path.join('banner/graphic', str(instance.pk), filename)


class BannerGraphicImage(models.Model):
    IMAGE_UPLOAD_TO = 'banner/graphic/'
    banner = models.ForeignKey(BannerGraphic, related_name='images')
    image = models.ImageField(upload_to=banner_graphic_image_upload_to, verbose_name=u'изображение')
    product = models.ForeignKey(Product, related_name='banner_images', null=True, blank=True, verbose_name=u'товар')


class BannerCarousel(Banner):
    product_group = models.ForeignKey(
        'ProductGroup', related_name='banners_carousel',
        verbose_name=u'группа продуктов')

    class Meta(object):
        verbose_name = u'баннер (графический)'
        verbose_name_plural = u'баннеры (графические)'


class AppType(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10, db_index=True, unique=True)

    class Meta(object):
        verbose_name = u'Тип приложения'
        verbose_name_plural = u'Типы приложений'

    def __unicode__(self):
        return u"%s" % self.name


class Order(models.Model):
    STATUS_NEW = 1
    STATUS_IN_PROGRESS = 2
    STATUS_COMPLETE = 3
    STATUS_CANCELED = 4
    STATUS_WAIT_FOR_PAY = 5
    STATUS_NOT_PAID = 6
    STATUS_PAID = 7
    STATUS_CHOICES = (
        (STATUS_NEW, u'Новый'),
        (STATUS_IN_PROGRESS, u'В обработке'),
        (STATUS_COMPLETE, u'Выполнен'),
        (STATUS_CANCELED, u'Отменен'),
        (STATUS_WAIT_FOR_PAY, u'Ожидает оплату'),
        (STATUS_NOT_PAID, u'Не оплачен'),
        (STATUS_PAID, u'Оплачен'),
    )
    STATUS_MAP = {
        STATUS_NEW: [STATUS_IN_PROGRESS, STATUS_CANCELED, STATUS_WAIT_FOR_PAY],
        STATUS_IN_PROGRESS: [STATUS_CANCELED, STATUS_COMPLETE],
        STATUS_COMPLETE: None,
        STATUS_CANCELED: None,
        STATUS_WAIT_FOR_PAY: [STATUS_PAID, STATUS_NOT_PAID],
        STATUS_NOT_PAID: [STATUS_CANCELED, STATUS_WAIT_FOR_PAY],
        STATUS_PAID: [STATUS_IN_PROGRESS],
    }
    app_type = models.ForeignKey(AppType, related_name='orders', blank=True, null=True)
    cart = models.ForeignKey(Cart, related_name='orders', unique=True)
    shop = models.ForeignKey(Shop, related_name='orders')
    customer = models.ForeignKey('Customer', related_name='orders')
    name = models.CharField(max_length=64)
    phone = models.CharField(max_length=64)
    email = models.CharField(max_length=64)
    address = models.CharField(max_length=64)
    comment = models.CharField(max_length=64)
    deliverypoint = models.ForeignKey(DeliveryPoint, blank=True, null=True)
    cdate = models.DateTimeField(u'Создан', default=timezone.now)
    approved_comment = models.CharField(max_length=255, verbose_name=u'причина отмены заказа администратором', blank=True, null=True)
    ext_order_id = models.CharField(
        max_length=200, blank=True, null=True, default=None)
    status = models.PositiveIntegerField(
        choices=STATUS_CHOICES, default=STATUS_NEW,
        verbose_name=u'статус заказа')

    class Meta(object):
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'

    def __unicode__(self):
        if self.customer_id and self.shop_id:
            return u"%d_%d" % (self.customer_id, self.shop_id)
        return self.pk

    def get_next_statuses(self, values_only=False):
        """
        Вернет список словарей, который будет являться списком возможных
        дальнейших смен статусов. Каждый словарь содержит значение нового
        статуса (val) и его текстовое представление (text)

        :return: [dict(val=int, text=unicode)]
        """
        statuses = []
        d = dict(self.STATUS_CHOICES)
        if values_only:
            m_fun = lambda x: x
        else:
            m_fun = lambda x: {'val': x, 'text': force_text(d.get(x, x), strings_only=True)}
        for s_id in self.STATUS_MAP[self.status] or []:
            statuses.append(m_fun(s_id))
        return statuses

    def prepare_usernotice_data(self):
        # Собираем данные для рендеринга письма
        user_data = dict(
            shop_logo=abs_url(self.shop.get_logo_url(), settings.BASE_HOST_NAME),
            shop_name=self.shop.name,
            items=[],
            discount=False,
            sign=self.shop.user_sign,
            order_total=self.cart.total(),
            hello_text=self.shop.hello_text
        )
        for cart_item in self.cart.items.all().select_related('product'):
            user_data['items'].append(dict(
                oid= cart_item.offer.oid if cart_item.offer else cart_item.product.oid,
                cover=cart_item.product.abs_cover(),
                name=cart_item.product.name,
                product_price=cart_item.product.price,
                count=cart_item.count,
                price=cart_item.price(),
                offer=cart_item.offer,
                url=cart_item.product.get_app_url(shop=self.shop,apptype=self.app_type)
            ))
        if self.cart.discount:
            summ_with_discount = self.cart.discount.apply(user_data['order_total'])
            if summ_with_discount != user_data['order_total']:
                user_data['discount'] = dict(
                    name=self.cart.discount.name,
                    value=self.cart.discount.get_value(),
                    price=summ_with_discount
                )
        return user_data

    def send_notice(self):
        user_data = self.prepare_usernotice_data()
        user_notice_html = render_to_string('email/user_notice.html', user_data)
        try:
            if self.email:
                EmailQueue.objects.create(
                    email_from=u"%s <no-reply@shoppyboom.ru>" % self.shop.user_from,
                    email_to=self.email,
                    bcc='orders@shoppyboom.ru',
                    reply_to=self.shop.user_reply,
                    subject=self.shop.user_subject,
                    message_type=EmailQueue.MTYPE_HTML,
                    body=user_notice_html
                )
        except:
            logger.error(u'Can not send notice email to customer for order: "%d"' % self.pk)

        ret = dict(order=self)
        if user_data['discount']:
            ret['discount'] = user_data['discount']

        subject = u'[%s] Новый заказ (%s)' % (self.shop.name, self.app_type.name)

        bcc_email = ['orders@shoppyboom.ru']
        if self.shop_id == 4:
            bcc_email.append('supermansha@gmail.com')
            bcc_email.append('Nataliya.Kachanova@x5.ru')
            subject = u'Заказ из Shoppyboom'

        client_notice_html = render_to_string('email/client_notice.html', ret)
        try:
            EmailQueue.objects.create(
                email_from=u'ShoppyBoom.Ru <no-reply@shoppyboom.ru>',
                email_to=self.shop.email,
                bcc=','.join(bcc_email),
                reply_to=u'Команда ShoppyBoom.Ru <team@shoppyboom.ru>',
                subject=subject,
                message_type=EmailQueue.MTYPE_HTML,
                body=client_notice_html
            )
        except:
            logger.error(u'Can not create notice email to shop admin for order: "%d"' % self.pk)

    def get_app_url(self):
        platform = self.app_type.code
        shop_url = self.shop.get_app_url(apptype=self.app_type)
        if platform == u'fb':
            return shop_url
        if platform == u'fb_tab':
            return shop_url
        if platform == u'vk':
            return shop_url + "#"
        if platform == u'ok':
            return shop_url + "#"
        if platform == u'www':
            return shop_url + "#"
        return self.shop.get_url()

    def get_success_url(self):
        return self.get_app_url() + u"order/%d/success" % self.id

    def get_failed_url(self):
        return self.get_app_url() + u"order/%d/failed" % self.id


class OrderHistory(models.Model):
    order = models.ForeignKey(Order, related_name='history')
    timestamp = models.DateTimeField(default=timezone.now)
    new_status = models.PositiveIntegerField(choices=Order.STATUS_CHOICES)
    author = models.ForeignKey(User, blank=True, null=True)

    class Meta(object):
        verbose_name = u'История заказа'
        verbose_name_plural = u'История заказов'


class ShopExtra(models.Model):
    shop = models.ForeignKey(Shop, related_name='extra')
    key = models.CharField(max_length=200)
    value = models.CharField(max_length=255)

    class Meta(object):
        unique_together = ('shop', 'key')

    @staticmethod
    def get_cache_key(shop_pk, param_name):
        return '%d_%s' % (shop_pk, param_name)

    def get_param(self, shop_pk, param_name, default=None):
        key = self.get_cache_key(shop_pk, param_name)
        value = cache.get(key, None)
        if value is None:
            try:
                value = ShopExtra.objects.get(shop__pk=shop_pk, key=param_name).value
                cache.set(key, value)
            except ObjectDoesNotExist:
                value = default
        return value


class Notification(models.Model):
    TYPE_SMS = 'sms'
    TYPE_TEXT = 'text'
    TYPE_CHOICES = (
        (TYPE_SMS, u'SMS'),
        (TYPE_TEXT, u'Текст'),
    )
    shop = models.ForeignKey(Shop, related_name='notifications')
    text = models.TextField(verbose_name=u'текст оповещения')
    type = models.CharField(max_length=10, choices=TYPE_CHOICES, default=TYPE_TEXT)
    date = models.DateTimeField(
        default=timezone.now,
        verbose_name=u'дата отправки оповещения',
        unique=True,
        help_text=u'Период между оповещениями должен составлять не менее 1 '
                  u'часа. Максимальное кол-во оповещений в сутки - 3. Нельзя '
                  u'содавать два оповещения с одинаковым текстом подряд. В '
                  u'тексте для смс оповещения можно использовать только '
                  u'латинские буквы и цифры'
    )

    class Meta(object):
        verbose_name = u'Оповещение'
        verbose_name_plural = u'Оповещения'


class Platform(models.Model):
    CODE_ROBOKASSA = 'robokassa'
    CODE_CHECKOUT = 'checkout'
    CODE_CHOICES = (
        (CODE_CHECKOUT, u'Checkout'),
        (CODE_ROBOKASSA, u'Robokassa'),
    )
    name = models.CharField(max_length=100, verbose_name=u'навзание платформы')
    code = models.CharField(max_length=10, verbose_name=u'код платформы', choices=CODE_CHOICES, unique=True)
    logo = models.ImageField(upload_to='platform/logo/', blank=True, null=True)

    class Meta(object):
        verbose_name = u'Платформа'
        verbose_name_plural = u'Платформы'

    def __unicode__(self):
        return self.name


class CheckoutPlatform(models.Model):
    platform = models.ForeignKey(Platform, verbose_name=u'платформа', default=get_default_platform(Platform.CODE_CHECKOUT), related_name='checkout_platforms')
    api_key = models.CharField(max_length=100, verbose_name=u'ключ платформы')

    class Meta(object):
        verbose_name = u'Ключ для Checkout'
        verbose_name_plural = u'Ключи для Checkout'

    def reset(self):
        self.api_key = ''
        self.save()


class RobokassaPlatform(models.Model):
    platform = models.ForeignKey(Platform, verbose_name=u'платформа', default=get_default_platform(Platform.CODE_ROBOKASSA), related_name='robokassa_platforms')
    login = models.CharField(max_length=100, verbose_name=u'идентификатор магазина')
    password1 = models.CharField(max_length=50, verbose_name=u'пароль 1')
    password2 = models.CharField(max_length=50, verbose_name=u'пароль 2')

    class Meta(object):
        verbose_name = u'Ключ для Robokassa'
        verbose_name_plural = u'Ключи для Robokassa'

    def reset(self):
        self.login = ''
        self.password1 = ''
        self.password2 = ''
        self.save()


class ShopPlatform(models.Model):
    shop = models.OneToOneField(Shop, related_name='platforms')
    robokassa = models.OneToOneField(RobokassaPlatform, related_name='shop_platform', blank=True, null=True)
    checkout = models.OneToOneField(CheckoutPlatform, related_name='shop_platform', blank=True, null=True)

    class Meta(object):
        verbose_name = u'Платформа магазина'
        verbose_name_plural = u'Платформы магазинов'

    def get_platforms(self):
        return dict(
            robokassa=self.robokassa,
            checkout=self.checkout
        )


class Role(models.Model):
    ALIAS_ADMIN = 'admin'
    ALIAS_PUBLISHER = 'publisher'
    ALIAS_CHOICES = (
        (ALIAS_ADMIN, u'Администратор'),
        (ALIAS_PUBLISHER, u'Паблик'),
    )
    name = models.CharField(max_length=100, verbose_name=u'название')
    alias = models.CharField(max_length=20, verbose_name=u'алиас роли', choices=ALIAS_CHOICES, unique=True)

    class Meta(object):
        verbose_name = u'роль'
        verbose_name_plural = u'роли'

    def __unicode__(self):
        return self.name


class EmailQueue(models.Model):
    MTYPE_HTML = 1
    MTYPE_TEXT = 2
    MESSAGE_TYPE_CHOICES = (
        (MTYPE_HTML, u'HTML'),
        (MTYPE_TEXT, u'Plain Text'),
    )
    email_from = models.CharField(
        max_length=255, verbose_name=u'от чьего имени отправлять сообщение')
    email_to = models.TextField(
        verbose_name=u'получатели (через запятую)')
    bcc = models.TextField(
        blank=True, null=True, verbose_name=u'скрытые получатели')
    reply_to = models.CharField(
        max_length=255, verbose_name=u'адрес ответа', blank=True, null=True)
    subject = models.CharField(
        max_length=255, verbose_name=u'тема сообщения')
    message_type = models.PositiveIntegerField(
        choices=MESSAGE_TYPE_CHOICES, default=MTYPE_HTML)
    body = models.TextField(
        verbose_name=u'тело сообщения')
    is_complete = models.BooleanField(
        default=False, verbose_name=u'сообщение отправлено?')

    class Meta(object):
        verbose_name = u'задача'
        verbose_name_plural = u'очередь сообщений'

    def send_email(self, connection=None):
        """
        Отправляет сообщение

        """
        m = self.build_email(connection=connection)
        m.send()

    def build_email(self, connection=None):
        m = EmailMessage(
            subject=self.subject,
            body=self.body,
            connection=connection,
            from_email=self.email_from,
            to=self.email_to.split(','),
            bcc=self.bcc.split(',') if self.bcc else None
        )
        if self.reply_to:
            m.extra_headers['Reply-To'] = self.reply_to
        if self.message_type == self.MTYPE_HTML:
            m.content_subtype = 'html'
        return m


class FormDescription(models.Model):
    field_name = models.CharField(
        max_length=255, unique=True, verbose_name=u'имя поля')
    field_label = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=u'label поля')
    help_text = models.TextField(
        blank=True, null=True, verbose_name=u'текст описания поля')

    class Meta(object):
        verbose_name = u'подсказка для поля'
        verbose_name_plural = u'подсказки для полей'

    def get_local_name(self):
        return self.field_name.rsplit('.', 1)[-1]


class ClientPayment(models.Model):
    STATUS_OK = 1
    STATUS_NO = 2
    STATUS_IN_PROCESS = 3
    STATUS_CHOICES = (
        (STATUS_OK, u'Оплачен'),
        (STATUS_NO, u'Не оплачен'),
        (STATUS_IN_PROCESS, u'В процессе'),
    )
    client = models.ForeignKey(
        Client,
        related_name='payments',
        verbose_name=u'клиент, к котрому будет привязан счет'
    )
    shop = models.ForeignKey(
        Shop,
        related_name='payments',
        verbose_name=u'магазин, к которому будет привязан счет'
    )
    amount = models.FloatField(
        verbose_name=u'сумма для оплаты в рублях')
    desc = models.TextField(default='', verbose_name=u'причина платежа')
    ctime = models.DateTimeField(default=timezone.now, verbose_name=u'Выставлен')
    mtime = models.DateTimeField(default=timezone.now, verbose_name=u'Изменен')
    status = models.PositiveIntegerField(
        choices=STATUS_CHOICES,
        default=STATUS_NO,
        verbose_name=u'статус оплаты'
    )
    days = models.PositiveIntegerField(
        default=None, blank=True, null=True,
        verbose_name=u'кол-во дней для продления срока работы магазина')

    class Meta(object):
        verbose_name = u'Клиентский платеж'
        verbose_name_plural = u'Клиентские платежи'

    def get_amount(self):
        return '%.2f' % self.amount
    get_amount.short_description = u'Сумма заказа'
    get_amount.admin_order_field = 'amount'

    def is_paid(self):
        return self.status == self.STATUS_OK

    def is_walletone(self):
        if settings.WALLETONE_ID:
            return True
        return False

    def is_robokassa(self):
        if settings.ROBOKASSA_LOGIN:
            return True
        return False

    def get_walletone_payment_data(self):
        callback_url = '%s%s' % (settings.WALLETONE_BASE_CALLBACK_DOMAIN, reverse('walletone_callback'))
        data = dict(
            WMI_MERCHANT_ID=settings.WALLETONE_ID,
            WMI_PAYMENT_AMOUNT=self.get_amount(),
            WMI_CURRENCY_ID='643',
            WMI_PAYMENT_NO=self.get_payment_no(),
            WMI_DESCRIPTION=self.desc,
            WMI_SUCCESS_URL=callback_url,
            WMI_FAIL_URL=callback_url
        )
        sign = utils.get_signature(data, settings.WALLETONE_SECRET)
        data['WMI_SIGNATURE'] = sign
        return data

    def get_payment_no(self):
        return str(self.pk)

    def get_robokassa_payment_data(self):
        robokassa_url = Robokassa(
            m_login=settings.ROBOKASSA_LOGIN,
            m_pass1=settings.ROBOKASSA_PASSWORD_1,
            m_pass2=settings.ROBOKASSA_PASSWORD_2,
            order_id=self.get_payment_no(),
            order_sum=self.get_amount()
        ).get_link_to_payment()
        return dict(robokassa_url=robokassa_url)

    def __unicode__(self):
        return self.desc


class Tariff(models.Model):
    UPDATE_PERIOD_1HOUR = 10
    UPDATE_PERIOD_1DAY = 20
    UPDATE_PERIOD_1WEEK = 30
    UPDATE_PERIOD_CHOICES = (
        (UPDATE_PERIOD_1HOUR, u'Каждый час'),
        (UPDATE_PERIOD_1DAY, u'Ежедневно'),
        (UPDATE_PERIOD_1WEEK, u'Еженедельно'),
    )
    order = models.PositiveIntegerField(verbose_name=u'порядок в списке тарифов')
    name = models.CharField(max_length=100, verbose_name=u'название тарифа')
    desc = models.TextField(verbose_name=u'краткое описание тарифа')
    max_products = models.PositiveIntegerField(
        default=0,
        verbose_name=u'максимальное кол-во продуктов')
    cost = models.FloatField(
        default=None, blank=True, null=True, verbose_name=u'стоимость в месяц')
    is_default = models.BooleanField(
        default=False, verbose_name=u'тариф по-умолчанию')
    is_archive = models.BooleanField(default=False, verbose_name=u'архивный')
    has_whitelabel = models.BooleanField(
        default=True, verbose_name=u'логотип ShoppyBoom')
    vk_enable = models.BooleanField(
        default=True, verbose_name=u'поддержка Vkontakte')
    fb_enable = models.BooleanField(
        default=True, verbose_name=u'поддержка Facebook')
    ok_enable = models.BooleanField(
        default=True, verbose_name=u'поддержка Odnoklassniki')
    update_period = models.PositiveIntegerField(
        choices=UPDATE_PERIOD_CHOICES,
        default=UPDATE_PERIOD_1HOUR,
        verbose_name=u'частота обновления')

    class Meta(object):
        verbose_name = u'Тариф'
        verbose_name_plural = u'Тарифы'

    def __unicode__(self):
        if self.cost:
            return u'%s %.02f руб./мес' % (self.name, self.cost)
        return u'%s (бесплатно)' % self.name


class Partner(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'название')
    desc = models.TextField(verbose_name=u'описание')
    contact_name = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=u'имя контакта')
    email = models.EmailField(
        blank=True, null=True, verbose_name=u'email для связи')
    benefits = models.TextField(
        blank=True, null=True, verbose_name=u'преимущества')
    code = models.CharField(
        max_length=200, unique=True, verbose_name=u'код активации')

    class Meta(object):
        verbose_name = u'партнер'
        verbose_name_plural = u'партнеры'


class ShopCatalog(models.Model):
    name = models.CharField(max_length=100)
    parent = models.ForeignKey(
        'self', related_name='themes', blank=True, null=True)

    class Meta(object):
        verbose_name = u'Магазин в каталоге'
        verbose_name_plural = u'Связь магазинов с каталогом'

    def __unicode__(self):
        return self.name


class Currency(models.Model):
    name = models.CharField(
        max_length=255, verbose_name=u'наименование валюты',
        help_text=u'прим. Российский рубль')
    code = models.CharField(
        max_length=3, unique=True, verbose_name=u'буквенный код валюты',
        help_text=u'<a href="http://ru.wikipedia.org/wiki/Общероссийский_'
                  u'классификатор_валют">Классификатор валют</a>')
    text = models.CharField(
        max_length=20, verbose_name=u'сокращенное наименование валюты',
        help_text=u'Исопльзуется для отображения в денежных интерфейсах. '
                  u'Прим: руб.')

    class Meta(object):
        verbose_name = u'Валюта'
        verbose_name_plural = u'Валюты'

    def __unicode__(self):
        return u'%s (%s)' % (self.code, self.name)


class CallbackOrder(models.Model):
    product = models.ForeignKey(Product, related_name='callback_orders')
    shop = models.ForeignKey(Shop, related_name='callback_orders')
    app_type = models.ForeignKey(AppType, related_name='callback_orders')
    customer = models.ForeignKey(Customer, related_name='callback_orders')
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=100)

    def send_notice(self):
        """
        Оповещение о новой заявке
        """
        html_str = render_to_string(
            'email/client_new_callback_order.html', dict(order=self))

        subject = u'[%s] Запрос звонка (%s)' % (self.shop.name, self.app_type.name)

        try:
            EmailQueue.objects.create(
                email_from=u'ShoppyBoom.Ru <no-reply@shoppyboom.ru>',
                email_to=self.shop.email,
                bcc='orders@shoppyboom.ru',
                reply_to=u'Команда ShoppyBoom.Ru <team@shoppyboom.ru>',
                subject=subject,
                message_type=EmailQueue.MTYPE_HTML,
                body=html_str
            )
        except:
            logger.error(u'Error on create notification '
                         u'for callbackOrder "%d"' % self.pk)


class ProductGroup(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'название')
    shop = models.ForeignKey(
        Shop, related_name='product_groups', verbose_name=u'магазин',
        db_index=True)
    products = models.ManyToManyField(
        Product, related_name='groups', verbose_name=u'продукты')
    use_for_slider = models.BooleanField(
        default=False, db_index=True,
        verbose_name=u'показывать эту группу товаров в слайдере')
    show_in_cart = models.BooleanField(
        default=False, db_index=True,
        verbose_name=u'показывать эту группу товаров в корзине')

    class Meta(object):
        verbose_name = u'группа товаров'
        verbose_name_plural = u'группы товаров'

    def __unicode__(self):
        return self.name


class Group2Product(models.Model):
    group = models.ForeignKey(
        ProductGroup, related_name='group2product_rel',
        db_column='productgroup_id')
    product = models.ForeignKey(
        Product, related_name='group2product_rel', db_column='product_id')

    class Meta(object):
        db_table = 'Core_productgroup_products'
        unique_together = ('group', 'product', )


class Landing(models.Model):
    shop = models.ForeignKey(Shop, related_name='landings')

    def get_sections(self):
        sections = [self.section_main, self.section_counter, self.section_products, self.section_compare, self.section_comments]
        sections = filter(bool, sections)
        simple_sections = self.sections_simple.all()
        return sections + list(simple_sections)

    def is_active(self):
        active = False
        for s in self.get_sections():
            active = active or s.is_active
        return active


class LSectionMain(models.Model):
    TYPE_CODE = 'main'
    TYPE_DESC = u'Основной раздел для того-то и того-то'
    landing = models.OneToOneField(Landing, related_name='section_main')
    is_active = models.BooleanField(default=True, verbose_name=u'активный')
    order = models.PositiveIntegerField(default=1, verbose_name=u'позиция')
    title = models.TextField(verbose_name=u'заголовок')
    desc = models.TextField(verbose_name=u'текст')
    custom_style = models.CharField(max_length=500, verbose_name=u'Стили оформления', blank=True, null=True)

    def __unicode__(self):
        return u'Основной'

    def get_cabinet_url(self):
        return reverse('lsection_main', kwargs={
            'shop_id': self.landing.shop_id,
            'landing_pk': self.landing_id})


class LSectionCounter(models.Model):
    TYPE_CODE = 'counter'
    TYPE_DESC = u'Раздел, отображающий обратный отсчет ' \
                u'до указанной даты и времени'
    landing = models.OneToOneField(Landing, related_name='section_counter')
    is_active = models.BooleanField(default=True, verbose_name=u'активный')
    order = models.PositiveIntegerField(default=2, verbose_name=u'позиция')
    title = models.TextField(verbose_name=u'заголовок')
    date = models.DateTimeField(verbose_name=u'дата, до которой будет отсчитывать счетчик')
    show_in_footer = models.BooleanField(default=False, verbose_name=u'показывать в нижней части страницы')
    custom_style = models.CharField(max_length=500, verbose_name=u'Стили оформления', blank=True, null=True)

    def __unicode__(self):
        return u"Счетчик"

    def get_cabinet_url(self):
        return reverse('lsection_counter', kwargs={
            'shop_id': self.landing.shop_id,
            'landing_pk': self.landing_id})


class LSectionProducts(models.Model):
    TYPE_CODE = 'products'
    TYPE_DESC = u'Раздел со списком продуктов из выбранной группы продуктоа'
    landing = models.OneToOneField(Landing, related_name='section_products')
    is_active = models.BooleanField(default=True, verbose_name=u'активный')
    order = models.PositiveIntegerField(default=3, verbose_name=u'позиция')
    title = models.TextField(verbose_name=u'заголовок')
    product_group = models.ForeignKey(ProductGroup, related_name='sections_products', verbose_name=u'группа продуктов')
    custom_style = models.CharField(max_length=500, verbose_name=u'Стили оформления', blank=True, null=True)

    def __unicode__(self):
        return u"Товары"

    def get_cabinet_url(self):
        return reverse('lsection_products', kwargs={
            'shop_id': self.landing.shop_id,
            'landing_pk': self.landing_id})


class LSectionCompare(models.Model):
    TYPE_CODE = 'compare'
    TYPE_DESC = u'Раздел с таблицей сравнения'
    landing = models.OneToOneField(Landing, related_name='section_compare')
    is_active = models.BooleanField(default=True, verbose_name=u'активный')
    order = models.PositiveIntegerField(default=4, verbose_name=u'позиция')
    left_title = models.CharField(max_length=200, verbose_name=u'заголовок левой части')
    left_text = models.TextField(verbose_name=u'текст левой части')
    right_title = models.CharField(max_length=200, verbose_name=u'заголовок правой части')
    right_text = models.TextField(verbose_name=u'текст правой части')
    custom_style = models.CharField(max_length=500, verbose_name=u'Стили оформления', blank=True, null=True)

    def __unicode__(self):
        return u"Сравнение"

    def get_cabinet_url(self):
        return reverse('lsection_compare', kwargs={
            'shop_id': self.landing.shop_id,
            'landing_pk': self.landing_id})


class LSectionComments(models.Model):
    TYPE_CODE = 'comments'
    TYPE_DESC = u'Отзывы пользователей'
    landing = models.OneToOneField(Landing, related_name='section_comments')
    is_active = models.BooleanField(default=True, verbose_name=u'активный')
    order = models.PositiveIntegerField(default=5, verbose_name=u'позиция')
    title = models.TextField(verbose_name=u'заголовок')
    custom_style = models.CharField(max_length=500, verbose_name=u'Стили оформления', blank=True, null=True)

    def __unicode__(self):
        return u"Комментарии"

    def get_cabinet_url(self):
        return reverse('lsection_comments', kwargs={
            'shop_id': self.landing.shop_id,
            'landing_pk': self.landing_id})


class LSectionComment(models.Model):
    section_comments = models.ForeignKey(LSectionComments, related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField(verbose_name=u'текст')


class LSectionSimple(models.Model):
    TYPE_CODE = 'simple'
    TYPE_DESC = u'Свободный раздел'
    landing = models.ForeignKey(Landing, related_name='sections_simple')
    is_active = models.BooleanField(default=True, verbose_name=u'активный')
    order = models.PositiveIntegerField(default=6, verbose_name=u'позиция')
    title = models.TextField(verbose_name=u'заголовок')
    text = models.TextField(verbose_name=u'текст')
    custom_style = models.CharField(max_length=500, verbose_name=u'Стили оформления', blank=True, null=True)

    def __unicode__(self):
        return self.title

    def get_cabinet_url(self):
        return reverse('lsection_simple_edit', kwargs={
            'shop_id': self.landing.shop_id,
            'landing_pk': self.landing_id,
            'section_pk': self.pk})


class VKOffer(models.Model):
    name = models.CharField(verbose_name=u'Название VK акции',max_length=100)
    shop = models.ForeignKey(Shop, verbose_name=u'Магазин', null=True, blank=True, default=None)
    vk_id = models.IntegerField(verbose_name=u'Идентификатор акции')
    vk_secret = models.CharField(verbose_name=u'Секретный ключ',max_length=20)

    def __unicode__(self):
        return self.name

    @classmethod
    def check(cls, vk_sid, vk_lead_id, vk_uid, vk_hash):
        #vk_uid: ['241652450']
        #vk_lead_id: ['8549']
        #vk_sid: ['test5a78936bee8ace38d9']
        #vk_hash: ['52d976e29c1c057bc796f8645031abfc']

        vk_lead_id  = int(vk_lead_id)
        vk_uid      = int(vk_uid)

        print vk_sid, vk_lead_id, vk_uid, vk_hash
        try:
            vko = cls.objects.get(vk_id=vk_lead_id)
            check = hashlib.md5( "%s_%d_%d_%s" % (vk_sid, vk_lead_id, vk_uid, vko.vk_secret) ).hexdigest()
            if check == vk_hash:
                logger.debug(">>>>>>>>>>  AWESOME! LEAD HAS COME!")
                return True
        except:
            return False

        return False

    @classmethod
    def complete(cls, r):
        vk_sid = r.session.get('vk_offer_sid', None)
        vk_lead_id = int( r.session.get('vk_offer_lead_id', 0) )

        if vk_sid and vk_lead_id:
            try:
                logger.debug(">>>>>>>>>>  AWESOME! LEAD DID HIS JOB! %d %s" % (vk_lead_id, vk_sid))
                vko = cls.objects.get(vk_id=vk_lead_id)

                url = 'https://api.vk.com/method/leads.complete?'
                params = dict(
                    vk_sid=vk_sid,
                    secret=vko.vk_secret
                )
                url = url + urllib.urlencode(params)
                resp = requests.get(url)

                del r.session['vk_offer_sid']
                del r.session['vk_offer_lead_id']
                r.session.modified = True
            except:
                pass

        return

@receiver(pre_save, sender=Product)
def update_mtime(instance, **kwargs):
    instance.mtime = timezone.now()


@receiver(pre_save, sender=ClientPayment)
def update_mtime(instance, **kwargs):
    instance.mtime = timezone.now()


@receiver(pre_save, sender=Shop)
def create_paid_till(instance, **kwargs):
    if not instance.paid_till or not instance.pk:
        instance.paid_till = instance.cdate + timezone.timedelta(days=31)


@receiver(post_save, sender=User)
def create_profile(instance, created, **kwargs):
    if created:
        u = ShoppyUser(sys_user=instance)
        u.save()


@receiver(post_save, sender=Shop)
def create_shop(instance, **kwargs):
    if not ShopProfile.objects.filter(shop=instance):
        shop_profile_obj = ShopProfile(shop=instance)
        shop_profile_obj.save()
    if not ShopPlatform.objects.filter(shop=instance):
        shop_platform_obj = ShopPlatform(shop=instance)
        shop_platform_obj.save()


@receiver(post_save, sender=ShopExtra)
def cache_params(instance, **kwargs):
    key = instance.get_cache_key(instance.shop.pk, instance.key)
    logger.debug('Cache key "%s"' % key)
    cache.set(key, instance.value)


@receiver(pre_save, sender=ShopExtra)
def key_upper(instance, **kwargs):
    instance.key = instance.key.upper()


@receiver(pre_save, sender=Shop)
def uniq_domain(instance, **kwargs):
    if not instance.pk:
        if not instance.domain or instance.domain == 'shoppyboom.ru' or instance.domain == '':
            instance.domain = str(uuid.uuid4())
        if not instance.tariff:
            try:
                instance.tariff = Tariff.objects.get(is_default=True)
            except:
                logger.error('Cannot find default tariff')


@receiver(pre_save, sender=Category)
def update_mtime(instance, **kwargs):
    instance.mtime = timezone.now()

# DEBUG
@receiver(pre_delete, sender=User)
def tb(instance, **kwargs):
    logger.error("%s DEBUG %s" % ("=" * 10,"=" * 10))
    logger.error("%d %s"%(instance.id,instance))
    for s in traceback.format_stack():
        logger.error(s)

@receiver(pre_delete, sender=Client)
def tb(instance, **kwargs):
    logger.error("%s DEBUG %s" % ("=" * 10,"=" * 10))
    logger.error("%d %s"%(instance.id,instance))
    for s in traceback.format_stack():
        logger.error(s)

@receiver(pre_delete, sender=Shop)
def tb(instance, **kwargs):
    logger.error("%s DEBUG %s" % ("=" * 10,"=" * 10))
    logger.error("%d %s"%(instance.id,instance))
    for s in traceback.format_stack():
        logger.error(s)


