# coding=utf-8
import re
import string
from django.conf import settings

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

from django.core.exceptions import ValidationError


def shop_alias_validator(alias):
    if alias is None:
        raise ValidationError(u'"None" is not valid value' % alias)
    if not alias:
        raise ValidationError(u'"" is not valid value')
    if not alias:
        raise ValidationError(u'"" is not valid value')


def page_alias_validator(alias):
    valid_chars = string.ascii_lowercase + string.digits + '_'
    for char in alias:
        if char not in valid_chars:
            raise ValidationError(u'"%s" is not valid char' % char)


def telephone_number(number):
    r = settings.RE_TELEPHONE
    if number:
        try:
            re.match(r, number.strip()).group()
        except:
            raise ValidationError(u'Номер телефона введен в неверном формате')
