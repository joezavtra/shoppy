# coding=utf-8
import json
import logging
import random
from urlparse import urljoin
from django.conf import settings
import requests
import time


class AmoManager(object):
    def __init__(self):
        super(AmoManager, self).__init__()
        self.logger = logging.getLogger('amocrm')
        self.company_name = settings.AMOCRM_COMPANY_NAME
        self.user_login = settings.AMOCRM_USER_LOGIN
        self.user_hash = settings.AMOCRM_USER_HASH
        self.base_url = 'https://%s.amocrm.ru' % self.company_name
        self.type = 'json'
        self.session = requests.session()

    def _get_url(self, url):
        return urljoin(self.base_url, url) + '?type=' + self.type

    def _call_post(self, url, data=None):
        if self.type == 'json':
            if data is not None:
                r = self.session.post(url, data)
            else:
                r = self.session.post(url)
            return r.json()
        return None

    def auth(self):
        url = self._get_url('/private/api/auth.php')
        data = {'USER_LOGIN': self.user_login, 'USER_HASH': self.user_hash}
        res = self._call_post(url, data)
        if res.get('response', {}).get('auth', False):
            return True
        return False

    def contact_add(self, contacts):
        url = self._get_url('/private/api/v2/json/contacts/set')
        for c in contacts:
            if c.get('request_id', None) is None:
                c['request_id'] = random.randint(10000, 99999)
            if c.get('name', None) is None:
                self.logger.warning('Contact has not param "name"')
                c['name'] = 'No Name'
            if c.get('responsible_user_id', None) is None:
                self.logger.error('Contact has not param "responsible_user_id"')
                c['responsible_user_id'] = 0
            if c.get('linked_leads_id', None) is None:
                c['linked_leads_id'] = []
            if c.get('company_name', None) is None:
                self.logger.warning('Contact has not param "company_name"')
                c['company_name'] = 'No Name'
            if c.get('custom_fields', None) is None:
                c['custom_fields'] = []
            if c.get('date_create', None) is None:
                c['date_create'] = int(time.time())
        data = {'request': {'contacts': {'add': contacts}}}
        return self._call_post(url, json.dumps(data))

    def lead_add(self, leads):
        url = self._get_url('/private/api/v2/json/leads/set')
        data = {'request': {'leads': {'add': leads}}}
        return self._call_post(url, json.dumps(data))
