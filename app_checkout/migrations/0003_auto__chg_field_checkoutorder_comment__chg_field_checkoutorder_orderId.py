# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'CheckoutOrder.comment'
        db.alter_column(u'app_checkout_checkoutorder', 'comment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'CheckoutOrder.orderId'
        db.alter_column(u'app_checkout_checkoutorder', 'orderId', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'CheckoutOrder.deliveryCost'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryCost', self.gf('django.db.models.fields.FloatField')(null=True))

        # Changing field 'CheckoutOrder.deliveryPostindex'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryPostindex', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'CheckoutOrder.deliveryPlace'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryPlace', self.gf('django.db.models.fields.TextField')(max_length=255, null=True))

        # Changing field 'CheckoutOrder.deliveryOrderCost'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryOrderCost', self.gf('django.db.models.fields.FloatField')(null=True))

        # Changing field 'CheckoutOrder.deliveryType'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryType', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'CheckoutOrder.clientFIO'
        db.alter_column(u'app_checkout_checkoutorder', 'clientFIO', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'CheckoutOrder.deliveryId'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryId', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'CheckoutOrder.deliveryMaxTerm'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryMaxTerm', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'CheckoutOrder.clientEmail'
        db.alter_column(u'app_checkout_checkoutorder', 'clientEmail', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True))

        # Changing field 'CheckoutOrder.deliveryMinTerm'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryMinTerm', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'CheckoutOrder.deliveryWeight'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryWeight', self.gf('django.db.models.fields.FloatField')(null=True))

        # Changing field 'CheckoutOrder.address'
        db.alter_column(u'app_checkout_checkoutorder', 'address', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'CheckoutOrder.clientPhone'
        db.alter_column(u'app_checkout_checkoutorder', 'clientPhone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'CheckoutOrder.deliveryPlaceId'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryPlaceId', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'CheckoutOrder.deliveryStreetId'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryStreetId', self.gf('django.db.models.fields.TextField')(null=True))

    def backwards(self, orm):

        # Changing field 'CheckoutOrder.comment'
        db.alter_column(u'app_checkout_checkoutorder', 'comment', self.gf('django.db.models.fields.CharField')(default=None, max_length=255))

        # Changing field 'CheckoutOrder.orderId'
        db.alter_column(u'app_checkout_checkoutorder', 'orderId', self.gf('django.db.models.fields.IntegerField')(default=None))

        # Changing field 'CheckoutOrder.deliveryCost'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryCost', self.gf('django.db.models.fields.FloatField')(default=None))

        # Changing field 'CheckoutOrder.deliveryPostindex'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryPostindex', self.gf('django.db.models.fields.IntegerField')(default=None))

        # Changing field 'CheckoutOrder.deliveryPlace'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryPlace', self.gf('django.db.models.fields.TextField')(default=None, max_length=255))

        # Changing field 'CheckoutOrder.deliveryOrderCost'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryOrderCost', self.gf('django.db.models.fields.FloatField')(default=None))

        # Changing field 'CheckoutOrder.deliveryType'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryType', self.gf('django.db.models.fields.CharField')(default=None, max_length=255))

        # Changing field 'CheckoutOrder.clientFIO'
        db.alter_column(u'app_checkout_checkoutorder', 'clientFIO', self.gf('django.db.models.fields.TextField')(default=None))

        # Changing field 'CheckoutOrder.deliveryId'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryId', self.gf('django.db.models.fields.CharField')(default=None, max_length=255))

        # Changing field 'CheckoutOrder.deliveryMaxTerm'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryMaxTerm', self.gf('django.db.models.fields.IntegerField')(default=None))

        # Changing field 'CheckoutOrder.clientEmail'
        db.alter_column(u'app_checkout_checkoutorder', 'clientEmail', self.gf('django.db.models.fields.EmailField')(default=None, max_length=75))

        # Changing field 'CheckoutOrder.deliveryMinTerm'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryMinTerm', self.gf('django.db.models.fields.IntegerField')(default=None))

        # Changing field 'CheckoutOrder.deliveryWeight'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryWeight', self.gf('django.db.models.fields.FloatField')(default=None))

        # Changing field 'CheckoutOrder.address'
        db.alter_column(u'app_checkout_checkoutorder', 'address', self.gf('django.db.models.fields.TextField')(default=None))

        # Changing field 'CheckoutOrder.clientPhone'
        db.alter_column(u'app_checkout_checkoutorder', 'clientPhone', self.gf('django.db.models.fields.CharField')(default=None, max_length=255))

        # Changing field 'CheckoutOrder.deliveryPlaceId'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryPlaceId', self.gf('django.db.models.fields.CharField')(default=None, max_length=255))

        # Changing field 'CheckoutOrder.deliveryStreetId'
        db.alter_column(u'app_checkout_checkoutorder', 'deliveryStreetId', self.gf('django.db.models.fields.TextField')(default=None))

    models = {
        u'Core.cart': {
            'Meta': {'object_name': 'Cart'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)'}),
            'coupon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'carts'", 'null': 'True', 'to': u"orm['Core.Coupon']"}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'carts'", 'to': u"orm['Core.Customer']"}),
            'discount': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'carts'", 'null': 'True', 'to': u"orm['Core.Discount']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'Core.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'lng': ('django.db.models.fields.FloatField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'Core.client': {
            'Meta': {'object_name': 'Client'},
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'clients'", 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.coupon': {
            'Meta': {'unique_together': "(('stock', 'key'),)", 'object_name': 'Coupon'},
            'cur_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'max_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'coupons'", 'to': u"orm['Core.Stock']"}),
            'time_from': ('django.db.models.fields.DateTimeField', [], {}),
            'time_to': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'Core.customer': {
            'Meta': {'unique_together': "(('vk_id', 'fb_id'),)", 'object_name': 'Customer'},
            'fb_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'vk_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'Core.deliverypoint': {
            'Meta': {'unique_together': "(('shop', 'oid'),)", 'object_name': 'DeliveryPoint'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.City']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'json_data': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'lat': ('django.db.models.fields.FloatField', [], {}),
            'lng': ('django.db.models.fields.FloatField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'oid': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'delivery_points'", 'to': u"orm['Core.Shop']"})
        },
        u'Core.discount': {
            'Meta': {'object_name': 'Discount'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.Shop']"}),
            'threshold': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'value': ('django.db.models.fields.IntegerField', [], {}),
            'value_type': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        },
        u'Core.order': {
            'Meta': {'object_name': 'Order'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'unique': 'True', 'to': u"orm['Core.Cart']"}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': u"orm['Core.Customer']"}),
            'deliverypoint': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Core.DeliveryPoint']", 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': u"orm['Core.Shop']"}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'Core.shop': {
            'Meta': {'object_name': 'Shop'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cdate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)'}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'shops'", 'to': u"orm['Core.Client']"}),
            'contacts': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'data_url': ('django.db.models.fields.URLField', [], {'default': "''", 'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'delivery': ('django.db.models.fields.TextField', [], {'max_length': '10000'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'hello_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)'}),
            'mode': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'next_fetch': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)'}),
            'notice': ('django.db.models.fields.TextField', [], {'default': 'u\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"> <head>  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  <title>Demystifying Email Design</title>  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>  <style>    .list {background:#eee}    .list th {font-size: 150%; padding: 15px; }    .list td {text-align: right; padding: 10px 15px; background:#fff;}    .list td.name {text-align: left}  </style></head><body><table style="width:800px"><tr><td><h1>\\u041f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u044c</h1><table class="list" cellspacing=1><tr><td>\\u0418\\u043c\\u044f: </td><td><a href="{{ order.cart.customer.get_profile_url }}">{{ order.name }}</a></td></tr><tr><td>\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d: </td><td> {{ order.phone }}</td></tr><tr><td>Email: </td><td>{{ order.email }}</td></tr><tr><td>\\u0410\\u0434\\u0440\\u0435\\u0441: </td><td>{{ order.address }}</td></tr><tr><td>\\u041f\\u0412\\u0417: </td><td>[{{ order.deliverypoint.oid }}] {{ order.deliverypoint.name }}</td></tr><tr><td>\\u041a\\u043e\\u043c\\u043c\\u0435\\u043d\\u0442\\u0430\\u0440\\u0438\\u0439 \\u043a \\u0437\\u0430\\u043a\\u0430\\u0437\\u0443:</td><td>{{ order.comment }}</td></tr></table></td></tr><tr><td><h1>\\u0418\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u044f \\u043e \\u0437\\u0430\\u043a\\u0430\\u0437\\u0435</h1><table class="list" cellspacing=1><tr><th>\\u2116</th><th>\\u0430\\u0440\\u0442\\u0438\\u043a\\u0443\\u043b</th><th>\\u043d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435</th><th>\\u0446\\u0435\\u043d\\u0430</th><th>\\u043a\\u043e\\u043b-\\u0432\\u043e</th><th>\\u0438\\u0442\\u043e\\u0433\\u043e</th></tr>{% for item in order.cart.items.all %}<tr><td>{{forloop.counter}}</td><td>{{ item.product.oid }}</td><td class=\\\'name\\\'>{{ item.product.name|safe }}</td><td>{{ item.product.price }}&nbsp;\\u0440\\u0443\\u0431.</td><td>{{ item.count }}&nbsp;\\u0448\\u0442.</td><td><strong>{{ item.price }}</strong>&nbsp;\\u0440\\u0443\\u0431.</td></tr>{% endfor %}<tr>  <td colspan=5 style="text-align: right; font-size: 150%"><strong>\\u0418\\u0442\\u043e\\u0433\\u043e:</strong></td>  <td style="text-align: right; font-size: 150%"><strong>{{ order.cart.total }}</strong>&nbsp;\\u0440\\u0443\\u0431.</td></tr>{% if discount %}<tr>  <td colspan=5 style="text-align: right; font-size: 120%">    {{ order.cart.discount.name }}:    {% if order.cart.coupon %}    <br>\\u043f\\u043e \\u043a\\u0443\\u043f\\u043e\\u043d\\u0443 &laquo;{{ order.cart.coupon.key }}&raquo;    {%endif%}  </td>  <td style="text-align: right; font-size: 120%">{{ discount.value }}</td></tr><tr>  <td colspan=5 style="text-align: right; font-size: 150%"><strong>\\u0421 \\u0443\\u0447\\u0435\\u0442\\u043e\\u043c \\u0441\\u043a\\u0438\\u0434\\u043a\\u0438:</strong></td>  <td style="text-align: right; font-size: 150%"><strong>{{ discount.price }}</strong>&nbsp;\\u0440\\u0443\\u0431.</td></tr>{%endif%}</table></td></tr><tr><td align="right"><img src="https://shoppyboom.ru/images/design/main_logo.gif"></td></tr></table></body></html>\'', 'null': 'True', 'blank': 'True'}),
            'processing': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'robokassa_login': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'robokassa_passwd_1': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'robokassa_passwd_2': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'user_from': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_reply': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_sign': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'user_subject': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'shops'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'Core.stock': {
            'Meta': {'object_name': 'Stock'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 3, 11, 0, 0)'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'discount': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stocks'", 'to': u"orm['Core.Discount']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'shop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stocks'", 'to': u"orm['Core.Shop']"})
        },
        u'app_checkout.checkoutorder': {
            'Meta': {'object_name': 'CheckoutOrder'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'clientEmail': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'clientFIO': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'clientPhone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'deliveryCost': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'deliveryId': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'deliveryMaxTerm': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'deliveryMinTerm': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'deliveryOrderCost': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'deliveryPlace': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'deliveryPlaceId': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'deliveryPostindex': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'deliveryStreetId': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'deliveryType': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'deliveryWeight': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'checkouts'", 'to': u"orm['Core.Order']"}),
            'orderId': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['app_checkout']