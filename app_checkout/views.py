# coding=utf-8
import logging
import urlparse
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from Core.models import Order
from app_checkout import utils
from app_checkout.forms import CheckoutOrderForm


l = logging.getLogger('app')


@csrf_exempt
def checkout_callback(r, order_pk):
    """
    Представление получает коллбэк от чекаута, сохраняет информацию о заказе и
    редиректит пользователя по указанному URL в параметре "r". Если параметр "r"
    не указан - происходит редирект на приложение этого магазина. Платформа
    приложения (db, vk или ok) определяется топорным способом на основе того,
    какие данные есть у пользователя

    :param r:
    :param order_pk:
    :return:
    """
    order_obj = get_object_or_404(Order, pk=order_pk)
    form = CheckoutOrderForm(r.POST or None)
    shop_obj = order_obj.shop
    checkout_platform = shop_obj.get_platforms()['checkout']
    if form.is_valid():
        obj = form.save(commit=False)
        obj.order = order_obj
        obj.save()
        order_obj.name = obj.clientFIO
        order_obj.phone = obj.clientPhone
        order_obj.email = obj.clientEmail
        order_obj.address = obj.deliveryPlace + ' ' + obj.address
        order_obj.comment = obj.comment
        order_obj.save()
        order_obj.send_notice()
        l.info('Run "notify_checkout" for order "%d"' % order_obj.pk)
        utils.notify_checkout(
            api_key=checkout_platform.api_key,
            checkout_order_pk=obj.orderId,
            local_order_pk=order_obj.pk)

    if 'r' in r.GET:
        return redirect(urlparse.unquote(r.GET['r']))

    c_platform = order_obj.app_type.code

    url = "http://shoppyboom.ru"

    if c_platform == 'www':
        url = shop_obj.get_url() + '/#order/%d/success' % order_obj.pk
    elif c_platform == 'vk':
        url = shop_obj.shopprofile.vk_url + '#/order/%d/success' % order_obj.pk
    elif c_platform == 'fb':
        url = shop_obj.shopprofile.fb_url + '/order/%d/success' % order_obj.pk
    elif c_platform == 'fb_tab':
        url = shop_obj.shopprofile.fb_taburl + '?app_data=/order/%d/success' % order_obj.pk
    elif c_platform == 'ok':
        url = shop_obj.shopprofile.ok_url + '?custom_args=/order/%d/success' % order_obj.pk

    return redirect(url)
