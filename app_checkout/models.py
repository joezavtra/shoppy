# coding=utf-8
from django.db import models
from Core.models import Order


class CheckoutOrder(models.Model):
    order = models.ForeignKey(
        Order,
        related_name='checkouts'
    )
    comment = models.CharField(
        max_length=255,
        verbose_name=u'Комментарий пользователя',
        blank=True,
        null=True
    )
    deliveryId = models.CharField(
        max_length=255,
        verbose_name=u'идентификатор службы доставки в платформе',
        blank=True,
        null=True
    )
    address = models.TextField(
        verbose_name=u'Адрес доставки',
        blank=True,
        null=True
    )
    deliveryPlace = models.TextField(
        max_length=255,
        verbose_name=u'Населённый пункт покупателя',
        blank=True,
        null=True
    )
    deliveryOrderCost = models.FloatField(
        verbose_name=u'Общая стоимость заказа (с доставкой)',
        blank=True,
        null=True
    )
    orderId = models.IntegerField(
        verbose_name=u'идентификатор заказа в платформе',
        blank=True,
        null=True
    )
    clientPhone = models.CharField(
        max_length=255,
        verbose_name=u'Телефон покупателя',
        blank=True,
        null=True
    )
    deliveryCost = models.FloatField(
        verbose_name=u'Стоимость доставки',
        blank=True,
        null=True
    )
    deliveryStreetId = models.TextField(
        verbose_name=u'Улица покупателя',
        blank=True,
        null=True
    )
    deliveryMinTerm = models.IntegerField(
        verbose_name=u'Минимальное количество дней доставки',
        blank=True,
        null=True
    )
    deliveryMaxTerm = models.IntegerField(
        verbose_name=u'Максимальное количество дней доставки',
        blank=True,
        null=True
    )
    clientFIO = models.TextField(
        verbose_name=u'ФИО покупателя',
        blank=True,
        null=True
    )
    deliveryPlaceId = models.CharField(
        max_length=255,
        verbose_name=u'идентификатор постамата или ПВЗ',
        blank=True,
        null=True
    )
    deliveryPostindex = models.IntegerField(
        verbose_name=u'Почтовый индекс адреса доставки покупателя',
        blank=True,
        null=True
    )
    deliveryWeight = models.FloatField(
        verbose_name=u'Общий вес товаров',
        blank=True,
        null=True
    )
    clientEmail = models.EmailField(
        verbose_name=u'e-mail покупателя',
        blank=True,
        null=True
    )
    deliveryType = models.CharField(
        max_length=255,
        verbose_name=u'Тип выбранной доставки, постамат, ПВЗ или курьер '
                     u'(postamat, pvz, express)',
        blank=True,
        null=True
    )
