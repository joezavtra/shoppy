from django import forms
from app_checkout.models import CheckoutOrder

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


class CheckoutOrderForm(forms.ModelForm):
    class Meta(object):
        model = CheckoutOrder
        exclude = ('order', )