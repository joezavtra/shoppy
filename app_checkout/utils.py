# coding=utf-8
import logging
import requests
import json

__author__ = 'Prosto Chewey <prosto.chewey@gmail.com>'


l = logging.getLogger('app')


def notify_checkout(
        api_key, checkout_order_pk, local_order_pk, payment_method=None):
    """
    Отправляет на сервер Checkout информацию о нашем заказе

    :param api_key: API ключ магазина на платформе Checkout
    :param checkout_order_pk: ID заказа в Checkout (CheckoutOrder.orderId)
    :param local_order_pk: ID заказа в нашем сервисе (CheckoutOrder.order.pk)
    :param payment_method: Способ оплаты(мы используем prepay)
    :return:
    """
    checkout_order_pk = str(checkout_order_pk)
    local_order_pk = str(local_order_pk)
    try:
        url = 'http://platform.checkout.ru/service/order/' + checkout_order_pk
        payload = {
            'apiKey': api_key,
            'order': {'shopOrderId': local_order_pk}
        }
        if payment_method:
            payload['order']['paymentMethod'] = str(payment_method)
        headers = {'content-type': 'application/json'}
        r = requests.post(url, data=json.dumps(payload), headers=headers)
        return r.json()
    except Exception as e:
        l.exception(
            'Error on try send POST request to Checkout: %s' % str(e))
    return None