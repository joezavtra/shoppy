from app_checkout.models import CheckoutOrder

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'

from django.contrib import admin


admin.site.register(CheckoutOrder)