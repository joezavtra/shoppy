from django.conf.urls import patterns, url
from app_checkout.views import *

__author__ = 'Zhuravlev Kirill <kirill.a.zhuravlev@gmail.com>'


urlpatterns = patterns('',
    url(r'^callback/(?P<order_pk>\d+)/$', checkout_callback),
)