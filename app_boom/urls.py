from django.conf.urls import patterns, url
from views import *

__author__ = 'Prosto Chewey <prosto.chewey@gmail.com>'


urlpatterns = patterns('',
    url(r'^report/by_clients/$', ReportByClients.as_view(), name='report_by_clients'),
    url(r'^report/by_shops/$', ReportByShops.as_view(), name='report_by_shops'),
)