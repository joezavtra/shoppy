# coding=utf-8
import codecs
import csv
from datetime import datetime
import json
import cStringIO
from django.contrib.auth.decorators import user_passes_test, login_required
from django.core.urlresolvers import reverse
from django.db.models import Count, Sum
from django.http import HttpResponse
from django.utils import timezone
from django.views.generic import View, ListView
from django.views.generic.base import TemplateResponseMixin
from Core.decorators import render_to
from Core.models import Client, Shop, Product, Order, DataSource


def for_superuser():
    return user_passes_test(lambda u: u.is_superuser, login_url='/admin')


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return user_passes_test(lambda u: u.is_superuser, login_url='/admin')(view)


class CSVResponseMixin(object):
    csv_filename = 'report.csv'
    date_format = '%d.%m.%Y'
    encoder = codecs.getincrementalencoder('utf-8')()
    queue = cStringIO.StringIO()

    def get_csv_filename(self):
        return self.csv_filename

    def render_to_response(self, data_list):
        response = HttpResponse(content_type='text/csv')
        cd = 'attachment; filename="{0}"'.format(self.get_csv_filename())
        response['Content-Disposition'] = cd

        writer = csv.writer(self.queue, dialect=csv.excel)
        for row in data_list:
            row_str = []
            for item in row:
                if type(item) == bool:
                    item = u'Да' if item else u'Нет'
                elif isinstance(item, datetime):
                    item = item.strftime(self.date_format)
                elif item == None:
                    item = u'Нет данных'
                row_str.append(unicode(item).encode('utf-8'))

            writer.writerow(row_str)
            data = self.queue.getvalue()
            data = data.decode('utf-8')
            data = self.encoder.encode(data)
            response.write(data)
            self.queue.truncate(0)

        return response


class ReportMixin(LoginRequiredMixin, CSVResponseMixin, TemplateResponseMixin):
    object_list = []
    filter = None

    def get_filter(self, r):
        return self.filter(r.GET, queryset=self.get_queryset())

    def get_data_list(self, r):
        filter_obj = self.get_filter(r)
        objects = self.get_objects(filter_obj)
        res = [objects[0].keys()]
        for obj in objects:
            res.append(obj.values())
        return res

    def get_context(self, r):
        filter_obj = self.get_filter(r)
        objects = self.get_objects(filter_obj)
        cur_month = filter_obj.data.get('month', '')
        return dict(objects=objects, filter_obj=filter_obj, cur_month=cur_month)

    def get(self, request, *args, **kwargs):
        if request.GET.get('as_csv', False) or self.template_name is None:
            return CSVResponseMixin.render_to_response(self, self.get_data_list(request))
        else:
            return TemplateResponseMixin.render_to_response(self, self.get_context(request))


class ReportByClients(ListView, ReportMixin):

    def __new__(cls, *args, **kwargs):
        # import here because:
        #   ReportByClientFilter attributes are ORM-dependant
        #   It crushes first run of South migrations
        from Cabinet.filters import ReportByClientFilter

        if not cls.filter:
            cls.filter = ReportByClientFilter

        return super(ReportByClients, cls).__new__(cls, *args, **kwargs)

    template_name = 'boom/report/by_clients.html'
    queryset = Client.objects.all().order_by('cdate')

    def get_objects(self, filter_obj):
        clients = []
        for client in filter_obj:
            try:
                last_login = max(client.users.all().values_list('last_login', flat=True))
                # last_login = client.users.all().only('last_login').order_by('-last_login')[0].last_login
            except:
                last_login = None
            clients.append(dict(
                pk=client.pk,
                registered=client.cdate,
                name=client.name,
                cabinet_client_profile_url=reverse('client_profile', kwargs={'client_pk': client.pk}),
                cabinet_client_shop_list_url=reverse('client_shop_list', kwargs={'client_pk': client.pk}),
                is_active=client.status,
                shops_count=client.shops.count(),
                datasource_count=client.datasource_set.count(),
                last_login=last_login
            ))
        return clients


class ReportByShops(ListView, ReportMixin):

    def __new__(cls, *args, **kwargs):
        # import here because:
        #   ReportByShopFilter attributes are ORM-dependant
        #   It crushes first run of South migrations
        from Cabinet.filters import ReportByShopFilter

        if not cls.filter:
            cls.filter = ReportByShopFilter

        return super(ReportByShops, cls).__new__(cls, *args, **kwargs)


    template_name = 'boom/report/by_shops.html'
    queryset = Shop.objects.all().select_related('shopprofile').annotate(
        order_count=Count('orders')).order_by('cdate')

    def get_objects(self, filter_obj):
        shops = []
        for shop in filter_obj:
            datasources = DataSource.objects.filter(pk__in=shop.datasources.all().values_list('pk', flat=True))
            product_count = datasources.annotate(product_count=Count('products')).aggregate(product_sum=Sum('product_count'))['product_sum']
            shops.append(dict(
                pk=shop.pk,
                name=shop.name,
                cabinet_shop_url=reverse('shop', kwargs={'shop_id': shop.pk}),
                cabinet_shop_datasources_url=reverse('shop_datasource_list', kwargs={'shop_id': shop.pk}),
                cabinet_shop_products_url=reverse('shop_goods', kwargs={'shop_id': shop.pk}),
                cabinet_shop_orders_url=reverse('shop_orders', kwargs={'shop_id': shop.pk}),
                datasource_count=datasources.count(),
                created=shop.cdate,
                product_count=product_count,
                order_count=shop.order_count,
                fb_url=shop.shopprofile.fb_url,
                vk_url=shop.shopprofile.vk_url,
                ok_url=shop.shopprofile.ok_url
            ))
        return shops


@for_superuser()
@render_to('boom/report/by_clients.html')
def report_by_clients(r):
    # import here because:
    #   ReportByClientFilter attributes are ORM-dependant
    #   It crushes first run of South migrations
    from Cabinet.filters import ReportByClientFilter

    clients = []

    client_filter = ReportByClientFilter(r.GET, queryset=Client.objects.all())
    for client in client_filter:
        try:
            last_login = max(client.users.all().values_list('last_login', flat=True))
        except:
            last_login = None
        clients.append(dict(
            pk=client.pk,
            registered=client.cdate,
            name=client.name,
            is_active=client.status,
            shops_count=client.shops.count(),
            datasource_count=client.datasource_set.count(),
            last_login=last_login
        ))
    return dict(clients=clients, client_filter=client_filter)


@for_superuser()
@render_to('boom/report/by_shops.html')
def report_by_shops(r):
    # import here because:
    #   ReportByShopFilter attributes are ORM-dependant
    #   It crushes first run of South migrations
    from Cabinet.filters import ReportByShopFilter

    shops = []

    shop_filter = ReportByShopFilter(r.GET, queryset=Shop.objects.all().select_related('shopprofile').annotate(order_count=Count('orders')))
    for shop in shop_filter:
        datasources = DataSource.objects.filter(pk__in=shop.datasources.all().values_list('pk', flat=True))
        product_count = datasources.annotate(product_count=Count('products')).aggregate(product_sum=Sum('product_count'))['product_sum']
        shops.append(dict(
            pk=shop.pk,
            name=shop.name,
            datasource_count=datasources.count(),
            created=shop.cdate,
            product_count=product_count,
            order_count=shop.order_count,
            fb_url=shop.shopprofile.fb_url,
            vk_url=shop.shopprofile.vk_url,
            ok_url=shop.shopprofile.ok_url
        ))
    return dict(shops=shops, shop_filter=shop_filter)
